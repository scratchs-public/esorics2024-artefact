#include<stdio.h>

int modular_pow(int base, int exponent, int modulus)
{
	if (modulus == 1) {
		return 0;
	}
	//Assert :: (modulus - 1) * (modulus - 1) does not overflow base
	int result = 1;
	base = base % modulus;
	while (exponent > 0)
	{
		  if ((exponent % 2)==1)
		  {
		     result = (result * base) % modulus;
		  }
		  exponent = exponent >> 1;
		  base = (base * base) % modulus;
	}
	return result;
}
int main()
{
	int final_result = modular_pow(10,2,3);
	printf("%d", final_result);
	return final_result;
}
