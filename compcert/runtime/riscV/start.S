.global _start
.global __global_pointer$
.global main
_start:
  .option push
  .option norelax
  la gp, __global_pointer$
  .option pop
  call main
  # jal ra, main

  li a7, 0 # exit
  ecall
