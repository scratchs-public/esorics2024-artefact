(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, Collège de France and Inria Paris            *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the GNU Lesser General Public License as        *)
(*  published by the Free Software Foundation, either version 2.1 of   *)
(*  the License, or  (at your option) any later version.               *)
(*  This file is also distributed under the terms of the               *)
(*  INRIA Non-Commercial License Agreement.                            *)
(*                                                                     *)
(* *********************************************************************)

(** Platform-specific built-in functions *)

Require Import String Coqlib.
Require Import AST Integers Floats Values.
Require Import Builtins0.

Inductive platform_builtin : Type :=
  | Lock | Unlock.

Local Open Scope string_scope.

Definition platform_builtin_table : list (string * platform_builtin) :=
  ("__builtin_lock", Lock)
    :: ("__builtin_unlock", Unlock) :: nil.

Definition platform_builtin_sig (b: platform_builtin) : signature :=
  match b with
  | Lock => mksignature (Tint :: nil) Tvoid cc_default
  | Unlock => mksignature (Tint ::nil) Tvoid cc_default
  end.

Definition bs_sem_none (l:list val) : option val := None.

Lemma rettype_ok :
  (forall vl : list val, val_opt_has_rettype (bs_sem_none vl) Tvoid).
Proof.
  unfold bs_sem_none.
  simpl. auto.
Qed.

Lemma inject_ok :
  forall (j : meminj) (vl vl' : list val), Val.inject_list j vl vl' -> val_opt_inject j (bs_sem_none vl) (bs_sem_none vl').
Proof.
  intros.
  simpl. exact I.
Qed.

Definition builtin_sem_default := mkbuiltin Tvoid bs_sem_none rettype_ok inject_ok.

Definition platform_builtin_sem (b: platform_builtin) : builtin_sem (sig_res (platform_builtin_sig b)) :=
  match b with
  | Lock => builtin_sem_default
  | Unlock => builtin_sem_default
  end.
