Ltac inv H := inversion H; clear H; subst.

Ltac destruct_and H :=
  match type of H with
  | ?H1 /\ ?H2 =>
      let f1 := fresh H in
      let f2 := fresh H in
      destruct H as (f1 & f2);
      destruct_and f1 ; destruct_and f2
  |  _ => idtac
  end.

Ltac destr_in H :=
  match type of H with
    context [match ?a with _ => _ end] => destruct a eqn:?
  end.

Ltac destr :=
  match goal with
    |- context [match ?a with _ => _ end] => destruct a eqn:?
  end.

Ltac dinv H := repeat destr_in H; inv H.

Class EqDec (T: Type) :=
  { eq_dec: forall t1 t2: T, { t1 = t2 } + { t1 <> t2 } }.

Definition beq_dec {A} {EQ: EqDec A} a1 a2 : bool :=
  if eq_dec a1 a2 then true else false.

Ltac trim H :=
  match type of H with
    ?A -> ?B =>
      let x := fresh "H" in
      assert (x: A); [clear H | specialize (H x); clear x]
  end.
