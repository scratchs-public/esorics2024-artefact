From formal Require Import tactics Util.
Require Import OrderedType.
Require Import FSets FSetAVL.

Parameter tagid: Type.
Parameter taglt: tagid -> tagid -> Prop.
Parameter taglt_trans : forall x y z, taglt x y -> taglt y z -> taglt x z.
Parameter taglt_not_eq : forall x y, taglt x y -> ~ eq x y.
Parameter tag_compare : forall x y : tagid, Compare taglt eq x y.
Context `{eqdec_tagid: EqDec tagid}.
Definition tag_eq_dec: forall x y : tagid, { eq x y } + { ~ eq x y } :=
  eq_dec.


Module OrderedTag <: OrderedType.
  Definition t := tagid.
  Definition eq := @eq t.
  Definition lt := taglt.
  Definition eq_refl : forall x, eq x x := @eq_refl _.
  Definition eq_sym  : forall x y, eq x y -> eq y x.
  Proof.
    unfold eq; intros. subst; auto.
  Qed.
  Definition eq_trans  : forall x y z, eq x y -> eq y z -> eq x z.
  Proof.
    unfold eq; intros. subst; auto.
  Qed.

  Definition lt_trans : forall x y z : t, lt x y -> lt y z -> lt x z :=
    taglt_trans.

  Definition lt_not_eq : forall x y : t, lt x y -> ~ eq x y :=
    taglt_not_eq.

  Definition compare : forall x y : t, Compare lt eq x y := tag_compare.

  Definition eq_dec: forall x y, { eq x y } + { ~ eq x y } := tag_eq_dec.
End OrderedTag.

Module TagS := FSetAVL.Make (OrderedTag).


Lemma cardinal_le_in_impl:
  forall t1 t2,
    (forall x, TagS.In x t1 -> TagS.In x t2) ->
    TagS.cardinal t1 <= TagS.cardinal t2.
Proof.
  intros.
  rewrite ! TagS.cardinal_1.
  apply NoDup_incl_length.
  eapply NoDupA_NoDup.
  apply TagS.elements_3w.
  red.
  intros.
  assert (InA eq a (TagS.elements t1)).
  rewrite InA_alt. eexists; split; eauto.
  apply TagS.elements_2 in H1. apply H in H1.
  apply TagS.elements_1 in H1.
  rewrite InA_alt in H1. destruct H1 as (? & ? & ?). subst. auto.
Qed.


Lemma TagS_In_mem:
  forall x s,
    TagS.mem x s = true <-> TagS.In x s.
Proof.
  split. apply TagS.mem_2. apply TagS.mem_1.
Qed.

Parameter setid: Type.
Definition tagset: Type := tagid * setid.
Context `{eqdec_setid: EqDec setid}.

Instance eqdec_tagset: EqDec tagset.
Proof.
  constructor. intros (t1 & s1) (t2 & s2).
  destruct (eq_dec t1 t2), (eq_dec s1 s2); intuition congruence.
Defined.

Parameter nways: nat.


Parameter numlines: nat.
Definition lineid := Fin.t numlines.
Instance eqdec_line: EqDec lineid.
Proof.
  constructor. apply Fin.eq_dec.
Defined.
Opaque lineid eqdec_line.

Definition all_line_ids:= all_fins numlines.

Lemma all_line_ids_spec: forall l, In l all_line_ids.
Proof.
  apply all_fins_spec.
Qed.

Context `{eqdec_tag: EqDec tagid}.

Parameter cache_usage: Type.
