From formal Require Import oni.
Require Import List.
Require Import OrderedType.
Require Import Lia.
Import ListNotations.
Require Import Bool.
Import BoolNotations.
Require Import Permutation.
Set Implicit Arguments.
From formal Require Import Util tactics CacheTypes syntax coresem mem.
Require Import FunctionalExtensionality.
Open Scope nat_scope.

Parameter line_length: nat.

Inductive target_event : Type :=
| te_hit
| te_wb (ts: tagset)
| te_fetch (ts: tagset).

Record cline :=
  {
    cl_locked: bool;
    cl_tag: tagid;
    cl_dirty: bool;
    cl_content: list val;
    cl_content_length: length cl_content = line_length;
  }.

Definition cacheline := option cline.

Definition ways := lineid -> cacheline.
Record wf_ways (w: ways) : Prop := {
    wft_nodup: forall l1 l2 cl1 cl2,
      w l1 = Some cl1 -> w l2 = Some cl2 -> cl_tag cl1 = cl_tag cl2 -> l1 = l2 /\ cl1 = cl2;
  }.

Definition tcache := setid -> cache_usage * { w | wf_ways w }.

Definition line_of_tag (w: ways) (t: tagid) : option lineid :=
  List.fold_left (fun acc l =>
                    match acc with
                      Some x => acc
                    | None =>
                        match w l with
                          None => None
                        | Some cl =>
                            if eq_dec (cl_tag cl) t then Some l else None
                        end
                    end
    ) all_line_ids None.

Definition update_tcache (t: tcache) (s: setid) v :=
  fun s' => if eq_dec s s' then v else t s'.

Definition update_ways (w: ways) (l: lineid) v : ways :=
  fun l' => if eq_dec l l' then v else w l'.

Definition locked_tags (C: tcache) s : TagS.t :=
  let '(u,w) := C s in
  List.fold_left (fun acc l =>
                    match proj1_sig w l with
                    | Some cl => if cl_locked cl then TagS.add (cl_tag cl) acc else acc
                    | _ => acc
                    end
    ) all_line_ids TagS.empty.

Parameter update_usage: ways -> cache_usage -> val -> bool -> bool -> mem_input -> cache_usage.
Parameter line_to_evict: ways -> cache_usage -> lineid.

Parameter addresses_of_tagset: tagset -> list val.
Parameter address_to_wordid: val -> nat.
Axiom address_to_wordid_valid:
  forall addr, address_to_wordid addr < line_length.

Axiom same_tagset_wordid_eq:
  forall a1 a2, addr_to_tagset a1 = addr_to_tagset a2 -> address_to_wordid a1 = address_to_wordid a2 -> a1 = a2.

Definition get_line (m: mem) (addr: val) : list val :=
  List.map m (addresses_of_tagset (addr_to_tagset addr)).

Definition get_word (addr: val) (l: list val): val :=
  nth (address_to_wordid addr) l addr.

Definition set_line (m: mem) (addr: val) (l: list val): mem :=
  let addrs := addresses_of_tagset (addr_to_tagset addr) in
  List.fold_left (fun m '(a, v) => do_store m a v) (combine addrs l) m.

Fixpoint set_nth {A:Type} (l: list A) (n: nat) (a: A): list A :=
  match n, l with
  | O, x::r => a::r
  | O, [] => []
  | S n, [] => []
  | S n, x::r => x :: set_nth r n a
  end.

Definition set_word (addr: val) (l: list val) (v: val) : list val :=
  set_nth l (address_to_wordid addr) v.

Lemma nth_set_nth:
  forall {A:Type} n (l: list A) v addr,
    n < length l ->
    nth n (set_nth l n v) addr = v.
Proof.
  induction n; simpl; intros; eauto. destruct l; simpl in *. easy. auto.
  destruct l. easy. simpl. apply IHn. simpl in *; lia.
Qed.

Lemma get_set_word:
  forall addr l v, length l = line_length -> get_word addr (set_word addr l v) = v.
Proof.
  intros.
  unfold get_word, set_word. intros.
  apply nth_set_nth. rewrite H; apply address_to_wordid_valid.
Qed.

Lemma nth_set_nth_other:
  forall {A:Type} n (l: list A) v addr n1,
    n <> n1 ->
    nth n1 (set_nth l n v) addr = nth n1 l addr.
Proof.
  induction n; simpl; intros; eauto; destruct l; simpl in *; try easy.
  destruct n1; auto. easy.
  destruct n1; auto.
Qed.

Lemma get_set_word_other:
  forall addr0 addr l v,
    address_to_wordid addr0 <> address_to_wordid addr ->
    get_word addr0 (set_word addr l v) = get_word addr0 l.
Proof.
  intros.
  unfold get_word, set_word.
  apply nth_set_nth_other. auto.
Qed.


Lemma fold_left_do_store_nin:
  forall l addr0 m,
    ~ In addr0 (map fst l) ->
    fold_left (fun (m0 : mem) '(a, v) => do_store m0 a v) l m addr0 = m addr0.
Proof.
  induction l; simpl; intros; eauto. destruct a; simpl in *.
  rewrite IHl. 2: auto.
  unfold do_store. destr; subst; eauto. intuition congruence.
Qed.

Lemma fold_left_do_store:
  forall l addr0 v m,
    In (addr0, v) l ->
    NoDup (map fst l) ->
    fold_left (fun (m0 : mem) '(a, v) => do_store m0 a v) l m addr0 = v.
Proof.
  induction l; simpl; intros; eauto. easy.
  inv H0. destruct H.
  - subst. simpl in *.
    rewrite fold_left_do_store_nin; auto.
    unfold do_store; destr; congruence.
  - destruct a. simpl in *. erewrite IHl; auto.
Qed.

Axiom length_addresses_of_tagset:
  forall addr, length (addresses_of_tagset addr) = line_length.

Axiom address_to_wordid_nth:
  forall addr, nth_error (addresses_of_tagset (addr_to_tagset addr)) (address_to_wordid addr) = Some addr.

Lemma nth_error_combine:
  forall {A B: Type} n (l1: list A) (l2: list B) v1 v2,
    nth_error l1 n = Some v1 ->
    nth_error l2 n = Some v2 ->
    nth_error (combine l1 l2) n = Some (v1, v2).
Proof.
  induction n; simpl; intros; eauto;
    destr_in H; inv H; destr_in H0; inv H0.
  simpl. auto.
  simpl. eauto.
Qed.

Lemma map_fst_combine:
  forall {A B: Type} (l1: list A) (l2: list B),
    length l1 = length l2 ->
    map fst (combine l1 l2) = l1.
Proof.
  induction l1; simpl; intros; eauto. destruct l2; simpl in *. lia.
  rewrite IHl1; auto.
Qed.

Axiom addresses_of_tagset_nodup:
  forall ts, NoDup (addresses_of_tagset ts).

Axiom addresses_of_tagset_in:
  forall a ts,
    In a (addresses_of_tagset ts) <-> addr_to_tagset a = ts.

Lemma set_line_get:
  forall m addr line addr0,
    length line = line_length ->
    set_line m addr line addr0 = if eq_dec (addr_to_tagset addr) (addr_to_tagset addr0)
                                 then get_word addr0 line
                                 else m addr0.
Proof.
  unfold set_line. intros.
  destr. unfold get_word.
  - generalize (address_to_wordid_nth addr0). intro NTH.
    generalize (proj2 (nth_error_Some line (address_to_wordid addr0)) (eq_rect _ _ (address_to_wordid_valid _) _ (eq_sym H))).
    intro NTHl. destruct (nth_error line (address_to_wordid addr0)) eqn:?. 2: congruence. clear NTHl.
    erewrite fold_left_do_store. eauto. rewrite e.
    generalize (nth_error_combine _ _ _ NTH Heqo).
    intro NTHe.
    apply nth_error_In in NTHe. erewrite nth_error_nth; eauto.
    rewrite map_fst_combine.
    apply addresses_of_tagset_nodup.
    rewrite length_addresses_of_tagset. auto.
  - rewrite fold_left_do_store_nin; auto.
    rewrite map_fst_combine. 2: rewrite length_addresses_of_tagset; auto.
    rewrite addresses_of_tagset_in. auto.
Qed.

Lemma get_line_equiv:
  forall m1 m2 a,
    mem_equiv m1 m2 ->
    perm a = rx \/ process_of_addr a = Some A ->
    get_line m1 a = get_line m2 a.
Proof.
  intros. inv H.
  unfold get_line.
  apply map_ext_in. intros.
  rewrite addresses_of_tagset_in in H.
  rewrite me_same_code; auto.
  rewrite <- perm_tagset_to_addr_to_tagset. rewrite H, perm_tagset_to_addr_to_tagset.
  unfold process_of_addr in *. rewrite H in *. auto.
Qed.

Lemma get_set_line:
  forall addr addr0
         (EQ: addr_to_tagset addr = addr_to_tagset addr0)
         m l,
    length l = line_length ->
    get_line (set_line m addr l) addr0 = l.
Proof.
  unfold get_line, set_line. intros.
  rewrite <- EQ.
  generalize (length_addresses_of_tagset (addr_to_tagset addr)).
  generalize (addresses_of_tagset_nodup (addr_to_tagset addr)).
  generalize (addresses_of_tagset (addr_to_tagset addr)). intros. rewrite <- H1 in H.
  revert m H0 H. clear H1. revert l l0. clear.
  induction l; destruct l0; simpl; intros; eauto; try lia.
  inv H0.
  rewrite IHl; auto. f_equal.
  rewrite fold_left_do_store_nin. unfold do_store.
  destruct (eq_dec v v); congruence.
  rewrite map_fst_combine; auto. congruence.
Qed.

Lemma get_word_line:
  forall m addr,
    m addr = get_word addr (get_line m addr).
Proof.
  intros.
  cut (Some (m addr) = Some (get_word addr (get_line m addr))). congruence.
  unfold get_word, get_line.
  rewrite <- nth_error_nth'.
  erewrite map_nth_error. reflexivity.
  rewrite address_to_wordid_nth. simpl. auto.
  rewrite map_length, length_addresses_of_tagset.
  apply address_to_wordid_valid.
Qed.

Lemma get_set_line_other:
  forall m addr l addr2 (len: length l = line_length),
    addr_to_tagset addr <> addr_to_tagset addr2 ->
    get_line (set_line m addr l) addr2 = get_line m addr2.
Proof.
  unfold get_line, set_line. intros.
  apply map_ext_in. intros.
  rewrite addresses_of_tagset_in in H0.
  rewrite fold_left_do_store_nin. auto.
  rewrite map_fst_combine.
  rewrite addresses_of_tagset_in. congruence.
  rewrite length_addresses_of_tagset. auto.
Qed.

Lemma get_line_tagset_to_addr_to_tagset:
  forall m addr,
    get_line m (tagset_to_addr (addr_to_tagset addr)) = get_line m addr.
Proof.
  unfold get_line. intros. destruct (addr_to_tagset addr); rewrite addr_to_tagset_to_addr.
  reflexivity.
Qed.

Lemma set_nth_length:
  forall {A} (l: list A) n v,
    length (set_nth l n v) = length l.
Proof.
  induction l; destruct n; simpl; intros; eauto.
Qed.

Lemma set_word_preserves_length:
  forall a l v, length l = line_length -> length (set_word a l v) = line_length.
Proof.
  intros. setoid_rewrite set_nth_length. auto.
Qed.

Lemma length_get_line m a:
  length (get_line m a) = line_length.
Proof.
  unfold get_line. rewrite map_length, length_addresses_of_tagset. auto.
Qed.

Opaque get_line get_word set_line set_word.

(* addr may be None if nothing in cache must be written back to memory *)
Definition write_back (m: mem) (rt: tcache) (l: lineid) (addr: option val) : mem * list target_event :=
  match addr with
    Some addr =>
      let '(t, s) := addr_to_tagset addr in
      let '(u, w) := rt s in
      match proj1_sig w l with
        None => (m, [])
      | Some cl =>
          if cl_dirty cl then
            (set_line m addr (cl_content cl), [te_wb (t,s)])
          else (m, [])
      end
  | _ => (m, [])
  end.

Program Definition update_cache_line (C: tcache) (addr: val) (l: lineid) (f: cline -> cline) (Hf: forall cl, cl_tag (f cl) = cl_tag cl) : tcache :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := C s in
  match proj1_sig w l with
    None => C
  | Some cl =>
      let cl' := f cl in
      update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some cl')) _)
  end.
Next Obligation.
  clear Heq_anonymous1. inv H.
  constructor. unfold update_ways. intros.
  destr_in H; inv H; destr_in H0; inv H0; simpl in *; auto.
  elim n. eapply wft_nodup0; eauto.
  elim n. eapply wft_nodup0; eauto.
Qed.

Definition write_in_cache_cl (cl: cline) (addr: val) (v: val) :=
  {|
    cl_locked := cl_locked cl;
    cl_tag := cl_tag cl;
    cl_dirty := true;
    cl_content := _;
    cl_content_length := set_word_preserves_length addr _ v (cl_content_length cl)
  |}.
Lemma write_in_cache_cl_preserves_tag cl addr v: cl_tag (write_in_cache_cl cl addr v) = cl_tag cl.
Proof. reflexivity. Qed.

Definition write_in_cache (C: tcache) (addr: val) (v: val) (l: lineid) : tcache :=
  update_cache_line C addr l _ (fun cl => write_in_cache_cl_preserves_tag cl addr v).

Definition tcache_update_usage (C: tcache) (addr: val) (mi: mem_input) : tcache :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := C s in
  let '(cached, locked) :=
    match line_of_tag (proj1_sig w) t with
    | Some l =>
        match proj1_sig w l with
          Some cl => (true, cl_locked cl)
        | None    => (false, false)
        end
    | None => (false, false)
    end in
  update_tcache C s (update_usage (proj1_sig w) u addr cached locked mi, w).

Definition tcache_line_to_evict (C: tcache) (addr: val): lineid :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := C s in
  line_to_evict (proj1_sig w) u.

Definition tcache_line_of_addr (C: tcache) (addr: val): option lineid :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := C s in
  line_of_tag (proj1_sig w) t.

Definition address_of_evicted_line (C: tcache) (l: lineid) (addr: val): option val :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := C s in
  match proj1_sig w l with
    Some cl => Some (tagset_to_addr (cl_tag cl, s))
  | None    => None
  end.

Lemma line_of_tag_none:
  forall w t,
    line_of_tag w t = None ->
    forall l cl,
      w l = Some cl -> cl_tag cl <> t.
Proof.
  unfold line_of_tag. intros. intro EQ. subst.
  eapply fold_left_maintains in H.
  destruct H. apply H1.
  instantiate (1:= fun l => exists cl0, w l = Some cl /\ cl_tag cl = cl_tag cl0).
  exists l; split. apply all_line_ids_spec. rewrite H0. eauto.
  destruct x. right. inversion 1. left; reflexivity.
  intros. subst. simpl in H2. destruct H2. destruct H1. rewrite H1. destruct eq_dec; congruence.
  intros. destruct acc. congruence. congruence.
Qed.

Lemma wf_ways_update:
  forall w l cl,
    wf_ways w ->
    (forall c, cl = Some c -> line_of_tag w (cl_tag c) = None) ->
    wf_ways (update_ways w l cl).
Proof.
  intros w l cl WF LOT. destruct WF; constructor. unfold update_ways. intros.
  destr_in H; destr_in H0; subst; eauto.
  - inv H0. auto.
  - specialize (LOT _ eq_refl).
    eapply line_of_tag_none in LOT. 2: eauto. congruence.
  - specialize (LOT _ eq_refl).
    eapply line_of_tag_none in LOT. 2: eauto. congruence.
Qed.

Program Definition fetch (m: mem) (C: tcache) (addr: val) (l: lineid)
  (pf: tcache_line_of_addr C addr = None)
  : tcache :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := C s in
  let cl' := {|
              cl_locked := false;
              cl_tag := t;
              cl_dirty := false;
              cl_content := _;
              cl_content_length := length_get_line m addr;
            |} in
  update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some cl')) _).
Next Obligation.
  apply wf_ways_update. auto. intros. inv H0.
  unfold tcache_line_of_addr in pf. rewrite <- Heq_anonymous0 in pf.
  rewrite <- Heq_anonymous in pf. simpl in pf. simpl. auto.
Qed.

Definition tcache_line_of_addr' c a : (tcache_line_of_addr c a = None) + lineid.
Proof.
  destruct (tcache_line_of_addr c a) eqn:?. exact (inr l).
  exact (inl eq_refl).
Defined.

Program Definition read_from_cache (c: tcache) (addr: val) (l: lineid) : val :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := c s in
  match proj1_sig w l with
    Some cl => get_word addr (cl_content cl)
  | None => _
  end.

Definition tcache_is_locked (c: tcache) (addr: val): bool :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := c s in
  match line_of_tag (proj1_sig w) t with
    Some l =>
      match proj1_sig w l with
        Some cl => cl_locked cl
      | None => false
      end
  | None => false
  end.

Definition num_lockable_in_set (c: tcache) (addr: val): nat :=
  let '(t, s) := addr_to_tagset addr in
  nways - 1 - TagS.cardinal (locked_tags c s).

Definition lock_line_cl (cl: cline) : cline :=
  {|
    cl_locked := true;
    cl_tag := cl_tag cl;
    cl_dirty := true;
    cl_content := cl_content cl;
    cl_content_length := cl_content_length cl;
  |}.
Lemma lock_line_cl_preserves_tag cl: cl_tag (lock_line_cl cl) = cl_tag cl.
Proof. reflexivity. Qed.

Definition lock_line (C: tcache) (addr: val) (l: lineid) : tcache :=
  update_cache_line C addr l _ (lock_line_cl_preserves_tag).

Definition unlock_line_cl (cl: cline) : cline :=
  {|
    cl_locked := false;
    cl_tag := cl_tag cl;
    cl_dirty := cl_dirty cl;
    cl_content := cl_content cl;
    cl_content_length := cl_content_length cl;
  |}.
Lemma unlock_line_cl_preserves_tag cl: cl_tag (unlock_line_cl cl) = cl_tag cl.
Proof. reflexivity. Qed.

Definition unlock_line (C: tcache) (addr: val) (l: lineid) : tcache :=
  update_cache_line C addr l _ (unlock_line_cl_preserves_tag).

Definition maybe_writeback_fetch (m: mem) (c: tcache) (a: val) :=
  match tcache_line_of_addr' c a with
  | inl pf =>
      let l := tcache_line_to_evict c a in
      let a' := address_of_evicted_line c l a in
      let '(m, twb) := write_back m c l a' in
      let c := fetch m c a l pf in
      (m, c, l, twb ++ [te_fetch (addr_to_tagset a)])
  | inr l =>
      (m, c, l, [te_hit])
  end.

Definition load_wb (m: mem) (c: tcache) (a: val) : (mem * tcache * list target_event * val) :=
  let '(m, c, l, events) := maybe_writeback_fetch m c a in
  let c := tcache_update_usage c a (mi_load a) in
  let v := read_from_cache c a l in
  (m, c, events, v).

Definition store_wb (m: mem) (c: tcache) (a: val) (v: val) : (mem * tcache * list target_event) :=
  let '(m, c, l, events) := maybe_writeback_fetch m c a in
  let c := tcache_update_usage c a (mi_store a v) in
  let c := write_in_cache c a v l in
  (m, c, events).

Definition lock_wb (m: mem) (c: tcache) (a: val) : option (mem * tcache * list target_event) :=
  if Nat.ltb 0 (num_lockable_in_set c a) || tcache_is_locked c a
  then
    let '(m, c, l, events) := maybe_writeback_fetch m c a in
    let c := tcache_update_usage c a (mi_lock a) in
    let c := lock_line c a l in
    Some (m, c, events)
  else None.

Definition unlock_wb (m: mem) (c: tcache) (a: val) : (mem * tcache * list target_event) :=
  let '(m, c, l, events) := maybe_writeback_fetch m c a in
  let c := tcache_update_usage c a (mi_unlock a) in
  let c := unlock_line c a l in
  (m, c, events).

Definition target_mem_interface (p: process) (mt: mem * tcache) mi :
  option (mem * tcache * mem_output * list target_event ) :=
  let '(m, C) := mt in
  match mi with
    mi_load addr =>
      let/opt _ := check_process_of_addr addr p in
      let '(m, C, t, v) := load_wb m C addr in
      Some (m, C, Some v, t)
  | mi_store addr v =>
      let/opt _ := check_process_of_addr addr p in
      let '(m, C, t) := store_wb m C addr v in
      Some (m, C, None, t)
  | mi_lock addr =>
      let/opt _ := check_process_of_addr addr p in
      let/opt out := lock_wb m C addr in
      let '(m, C, t) := out in
      Some (m, C, None, t)
  | mi_unlock addr =>
      let/opt _ := check_process_of_addr addr p in
      let  '(m, C, t) := unlock_wb m C addr in
      Some (m, C, None, t)
  end.

Definition write_in_cache_cl_wt (cl: cline) (addr: val) (v: val) :=
  {|
    cl_locked := cl_locked cl;
    cl_tag := cl_tag cl;
    cl_dirty := cl_dirty cl;
    cl_content := _;
    cl_content_length := set_word_preserves_length addr _ v (cl_content_length cl)
  |}.
Lemma write_in_cache_cl_wt_preserves_tag cl addr v: cl_tag (write_in_cache_cl_wt cl addr v) = cl_tag cl.
Proof. reflexivity. Qed.

Definition write_in_cache_wt (C: tcache) (addr: val) (v: val) (l: lineid) : tcache :=
  update_cache_line C addr l _ (fun cl => write_in_cache_cl_wt_preserves_tag cl addr v).

Definition store_wt (around: bool) (m: mem) (c: tcache) (a: val) (v: val) : (mem * tcache * list target_event) :=
  let '(m, c, l, events, write_in_mem) := 
    match tcache_line_of_addr' c a with
    | inl pf =>
        if around
        then (m, c, None, [], true)
        else
          let l := tcache_line_to_evict c a in
          let a' := address_of_evicted_line c l a in
          let '(m, twb) := write_back m c l a' in
          let c := fetch m c a l pf in
          (m, c, Some l, twb ++ [te_fetch (addr_to_tagset a)], true)
    | inr l =>
        (m, c, Some l, [te_hit], negb (tcache_is_locked c a))
    end in
  let c := tcache_update_usage c a (mi_store a v) in
  let c := match l with Some l => write_in_cache_wt c a v l | None => c end in
  let m := if write_in_mem then do_store m a v else m in
  (m, c, events).

Definition unlock_wt (m: mem) (c: tcache) (a: val) : (mem * tcache * list target_event) :=
  let '(m, c, l, events1) := maybe_writeback_fetch m c a in
  let c := tcache_update_usage c a (mi_unlock a) in
  let c := unlock_line c a l in
  let '(m, events2) := write_back m c l (Some a) in
  (m, c, events1 ++ events2).


Definition target_mem_interface_wt (around: bool) (p: process) (mt: mem * tcache) mi :
  option (mem * tcache * mem_output * list target_event ) :=
  let '(m, C) := mt in
  match mi with
    mi_load addr =>
      let/opt _ := check_process_of_addr addr p in
      let '(m, C, t, v) := load_wb m C addr in
      Some (m, C, Some v, t)
  | mi_store addr v =>
      let/opt _ := check_process_of_addr addr p in
      let '(m, C, t) := store_wt around m C addr v in
      Some (m, C, None, t)
  | mi_lock addr =>
      let/opt _ := check_process_of_addr addr p in
      let/opt out := lock_wb m C addr in
      let '(m, C, t) := out in
      Some (m, C, None, t)
  | mi_unlock addr =>
      let/opt _ := check_process_of_addr addr p in
      let  '(m, C, t) := unlock_wt m C addr in
      Some (m, C, None, t)
  end.


Definition ways_init (w: ways) : Prop :=
  forall l,
    w l = None.

Definition tcache_init (tc: tcache) : Prop :=
  forall s u w,
    tc s = (u, w) ->
    ways_init (proj1_sig w).

Definition target_cache (init_mem: input -> mem -> Prop) :
  mem_interface (list target_event) mem_input mem_output (mem * tcache) :=
  {|
    mi_init := fun i '(m,t) => init_mem i m /\ tcache_init t;
    mi_interface := target_mem_interface
  |}.

Definition target_cache_wt (around: bool) (init_mem: input -> mem -> Prop) :
  mem_interface (list target_event) mem_input mem_output (mem * tcache) :=
  {|
    mi_init := fun i '(m,t) => init_mem i m /\ tcache_init t;
    mi_interface := target_mem_interface_wt around
  |}.

Lemma deterministic_memprog:
  forall init_state init_mem id,
    deterministic (memprog init_state (target_cache init_mem) id step_hole).
Proof.
  red. intros init_state0 init_mem0 id s t t' s' s'' STEP1 STEP2.
  simpl in *. destruct s, s', s''. apply step_hole_mi in STEP1. apply step_hole_mi in STEP2.
  inv STEP1; inv STEP2; destruct (get_instr_determ GI GI0) as (EQi & EQt & EQm); inv EQi; split; auto; try congruence.
Qed.


Lemma deterministic_memprog_wt around:
  forall init_state init_mem id,
    deterministic (memprog init_state (target_cache_wt around init_mem) id step_hole).
Proof.
  red. intros init_state0 init_mem0 id s t t' s' s'' STEP1 STEP2.
  simpl in *. destruct s, s', s''. apply step_hole_mi in STEP1. apply step_hole_mi in STEP2.
  inv STEP1; inv STEP2; destruct (get_instr_determ GI GI0) as (EQi & EQt & EQm); inv EQi; split; auto; try congruence.
Qed.

Lemma update_tcache_same:
  forall rt s u w,
    update_tcache rt s (u, w) s = (u, w).
Proof.
  unfold update_tcache. intros. destr; congruence.
Qed.

Lemma update_ways_same:
  forall w l cl,
    update_ways w l cl l = cl.
Proof.
  unfold update_ways; intros. destr; congruence.
Qed.

Lemma update_ways_other:
  forall w l cl l1,
    l <> l1 ->
    update_ways w l cl l1 = w l1.
Proof.
  intros. unfold update_ways; destr; eauto. congruence.
Qed.

Lemma line_of_tag_some:
  forall w t l,
    line_of_tag w t = Some l ->
    exists cl, w l = Some cl /\ cl_tag cl = t.
Proof.
  intros. unfold line_of_tag in H.
  eapply fold_left_prop_inv in H.
  destruct H. inv H.
  destruct H as (x & acc & IN & ACCnotl & EQ).
  destr_in EQ. congruence.
  repeat destr_in EQ; inv EQ. eauto.
  destruct a. destruct (eq_dec l0 l); eauto.
  subst; left; auto. right; inversion 1; congruence. right; congruence.
Qed.


Lemma line_of_tag_spec:
  forall w (WF: wf_ways w) t l,
    line_of_tag w t = Some l <-> exists cl, w l = Some cl /\ cl_tag cl = t.
Proof.
  split; intros.
  - apply line_of_tag_some. auto.
  - destruct H as (cl & EQ & TAG). subst.
    destruct (line_of_tag w (cl_tag cl)) eqn:?.
    + apply line_of_tag_some in Heqo. destruct Heqo as (? & EQ' & EQ'').
      eapply WF in EQ''; eauto. destruct EQ''. subst. auto.
    + eapply line_of_tag_none in Heqo; eauto. congruence.
Qed.

Lemma line_of_tag_update_ways1:
  forall w t l cl l1 (wf: wf_ways w),
    line_of_tag w t = Some l ->
    wf_ways (update_ways w l1 (Some cl)) ->
    line_of_tag (update_ways w l1 (Some cl)) t =
      if eq_dec l l1 then
        if eq_dec (cl_tag cl) t then Some l1 else None
      else Some l.
Proof.
  intros.
  apply line_of_tag_some in H. destruct H as (? & EQ & TAG).
  destr. subst. destr.
  rewrite line_of_tag_spec; auto. rewrite update_ways_same. eauto.
  destruct (line_of_tag (update_ways w l1 (Some cl)) (cl_tag x)) eqn:?; auto.
  apply line_of_tag_some in Heqo.
  destruct Heqo as ( ? & EQ2 & TAG2).
  unfold update_ways in EQ2. destr_in EQ2. inv EQ2. congruence.
  eelim wf. intros A. specialize (A _ _ _ _ EQ2 EQ TAG2). intuition subst; congruence.
  rewrite line_of_tag_spec; auto. unfold update_ways. destr; subst; try congruence. eauto.
Qed.

Lemma line_of_tag_update:
  forall cl t w (WF: wf_ways w) l,
    cl_tag cl = t ->
    line_of_tag w t = None ->
    line_of_tag (update_ways w l (Some cl)) t = Some l.
Proof.
  intros.
  apply line_of_tag_spec. subst.
  apply wf_ways_update. auto.
  unfold update_ways. intros. inv H. auto.
  unfold update_ways.
  destr; try congruence. eauto.
Qed.

Lemma line_of_tag_update_ways:
  forall w (WF: wf_ways w) l cl (pf: wf_ways (update_ways w l (Some cl))) t,
    line_of_tag (update_ways w l (Some cl)) t =
      if eq_dec (cl_tag cl) t then Some l
      else match line_of_tag w t with
             Some l1 => if eq_dec l l1 then None else Some l1
           | None => None
           end.
Proof.
  intros.
  destr.
  destruct (line_of_tag w t) eqn:?.
  erewrite line_of_tag_update_ways1. 3: eauto. 2-3: eauto. subst.
  repeat destr; subst; auto; try congruence.
  eapply line_of_tag_some in Heqo. destruct Heqo as (? & EQ & TAG).
  destruct pf. eelim (wft_nodup0 l l0). congruence.
  rewrite update_ways_same. eauto.
  rewrite update_ways_other. eauto. auto. auto.
  eapply line_of_tag_update; auto.
  destr.
  erewrite line_of_tag_update_ways1. 3: eauto. 2-3: eauto. subst.
  repeat destr; subst; auto; try congruence.
  destruct (line_of_tag (update_ways w l (Some cl)) t) eqn:?; auto.
  apply line_of_tag_some in Heqo0. destruct Heqo0 as (? & EQ & TAG).
  unfold update_ways in EQ. destr_in EQ. congruence. subst.
  eapply line_of_tag_none in Heqo; eauto. congruence.
Qed.

Lemma update_cache_line_unfold_some:
  forall C addr l f Hf cl,
    let '(t, s) := addr_to_tagset addr in
    let '(u, w) := C s in
    proj1_sig w l = Some cl ->
    exists p,
      update_cache_line C addr l f Hf =
        update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some (f cl))) p).
Proof.
  intros. destr. destr. intros.
  unfold update_cache_line.
  revert Heqt.
  generalize (eq_refl (addr_to_tagset addr)).
  generalize (addr_to_tagset addr) at 1 3 4. intros. revert e. rewrite Heqt. intros.
  generalize (@eq_refl (prod cache_usage (@sig ways (fun w : ways => wf_ways w))) (C s)).
  generalize (C s) at 1 3. intros. destruct p.
  destruct s1. simpl in *.
  generalize (@eq_refl cacheline (x l)).
  generalize (x l) at 1 3. intros.
  destruct o.
  rewrite <- e0 in Heqp.  inv Heqp. simpl in H. assert (cl = c1) by congruence. subst.
  eexists; eauto.
  rewrite <- e0 in Heqp.  inv Heqp. simpl in H. congruence.
Qed.

Lemma update_cache_line_unfold_none:
  forall C addr l f Hf,
    let '(t, s) := addr_to_tagset addr in
    let '(u, w) := C s in
    proj1_sig w l = None ->
    update_cache_line C addr l f Hf = C.
Proof.
  intros. destr. destr. intros.
  unfold update_cache_line.
  revert Heqt.
  generalize (eq_refl (addr_to_tagset addr)).
  generalize (addr_to_tagset addr) at 1 3 4. intros. revert e. rewrite Heqt. intros.
  generalize (@eq_refl (prod cache_usage (@sig ways (fun w : ways => wf_ways w))) (C s)).
  generalize (C s) at 1 3. intros. destruct p.
  destruct s1. simpl in *.
  generalize (@eq_refl cacheline (x l)).
  rewrite <- e0 in Heqp.  inv Heqp. simpl in H.
  generalize (x l) at 1 3. intros.
  destruct o. congruence. auto.
Qed.

Lemma update_cache_line_eq:
  forall C addr l f Hf,
    let '(t, s) := addr_to_tagset addr in
    let '(u, w) := C s in
    match proj1_sig w l with
      Some cl =>
        exists p,
        update_cache_line C addr l f Hf =
          update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some (f cl))) p)
    | None =>
        update_cache_line C addr l f Hf = C
    end.
Proof.
  intros. destr. destr. destr.
  generalize (update_cache_line_unfold_some C addr l _ Hf c0).
  rewrite Heqt, Heqp, Heqo. intro A. trim A. reflexivity.
  destruct A as (p1 & UCL).
  rewrite UCL. eauto.
  generalize (update_cache_line_unfold_none C addr l _ Hf).
  rewrite Heqt, Heqp, Heqo. eauto.
Qed.

Lemma fetch_unfold:
  forall m C addr l pf,
    let '(t, s) := addr_to_tagset addr in
    let '(u, w) := C s in
    let cl' := {|
                cl_locked := false;
                cl_tag := t;
                cl_dirty := false;
                cl_content := _;
                cl_content_length := length_get_line m addr
              |} in
    exists p,
      fetch m C addr l pf =
        update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some cl')) p).
Proof.
  intros. destr. destr. intros.
  unfold fetch.
  revert Heqt.
  generalize (eq_refl (addr_to_tagset addr)).
  generalize (addr_to_tagset addr) at 1 3 4. intros. revert e. rewrite Heqt. intros.
  generalize (eq_refl (C s)).
  generalize (C s) at 1 3. intros. destruct p.
  subst. rewrite <- e0 in Heqp. inv Heqp.
  eexists. eauto.
Qed.

Lemma write_in_cache_unfold:
  forall C addr l v,
    let '(t, s) := addr_to_tagset addr in
    let '(u, w) := C s in
    match proj1_sig w l with
      Some cl =>
        exists p,
        write_in_cache C addr v l =
          update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some (write_in_cache_cl cl addr v))) p)
    | None => write_in_cache C addr v l = C
    end.
Proof.
  intros.
  apply (update_cache_line_eq C addr l _ (fun cl => write_in_cache_cl_preserves_tag cl addr v)).
Qed.

Lemma write_in_cache_wt_unfold:
  forall C addr l v,
    let '(t, s) := addr_to_tagset addr in
    let '(u, w) := C s in
    match proj1_sig w l with
      Some cl =>
        exists p,
        write_in_cache_wt C addr v l =
          update_tcache C s (u, exist _ (update_ways (proj1_sig w) l (Some (write_in_cache_cl_wt cl addr v))) p)
    | None => write_in_cache_wt C addr v l = C
    end.
Proof.
  intros.
  apply (update_cache_line_eq C addr l _ (fun cl => write_in_cache_cl_wt_preserves_tag cl addr v)).
Qed.


Lemma read_from_cache_eq:
  forall c addr l,
    read_from_cache c addr l =
      let '(t, s) := addr_to_tagset addr in
      let '(u, w) := c s in
      match proj1_sig w l with
        Some cl => get_word addr (cl_content cl)
      | None => addr
      end.
Proof.
  unfold read_from_cache. intros.
  generalize (@eq_refl _ (addr_to_tagset addr)). generalize (addr_to_tagset addr) at 1 3. intros.
  destr.
  generalize (@eq_refl _ (c s)). generalize (c s) at 1 3. intros. destr.
  generalize (@eq_refl _ (proj1_sig s0 l)). generalize (proj1_sig s0 l) at 1 3. intros. destr.
  rewrite <- e, <- e0, <- e1. auto.
  rewrite <- e. setoid_rewrite <- e0. setoid_rewrite <- e1.
  unfold read_from_cache_obligation_1. simpl.
  destr. auto.
Qed.



Axiom update_usage_locked_load_store:
  forall w u locked cached mi addr,
    locked = true ->
    match mi with mi_load addr0 | mi_store addr0 _ => addr = addr0 | _ => False end ->
    update_usage w u addr locked cached mi = u.


Axiom line_to_evict_not_locked:
    forall w u cl,
      w (line_to_evict w u) = Some cl -> cl_locked cl = false.


Definition tag_locked (t: tagid) (w: ways) :=
  exists l cl, w l = Some cl /\ cl_locked cl = true /\ cl_tag cl = t.

Definition tagset_locked (ts: tagset) (C: tcache) : Prop :=
  let '(t,s) := ts in
  let '(u, w) := C s in
  tag_locked t (proj1_sig w).


Lemma tag_locked_spec:
  forall C s u w,
    C s = (u,w) -> forall t,
      tag_locked t (proj1_sig w) <-> TagS.In t (locked_tags C s).
Proof.
  intros. unfold locked_tags.
  split.
  - intros (l & EQ).
    rewrite H. eapply fold_left_prop1. 2: apply all_line_ids_spec.
    {
      simpl. intros.
      destruct (proj1_sig w x) eqn:?; auto.
      destruct c; auto. simpl. destruct cl_locked0; auto.
      apply TagS.add_2. auto.
    }
    intros. destruct EQ as (cl & EQ & LOCKED & TAG). rewrite EQ, LOCKED.
    apply TagS.add_1. auto.
  - rewrite H. intro FL.
    eapply fold_left_prop_inv in FL.
    destruct FL as [FL|FL]. easy.
    + destruct FL as (? & ? & _ & NIN & IN).
      red.
      destruct (proj1_sig w x) eqn:?. 2: congruence. destr_in IN. 2: congruence.
      destruct (eq_dec t (cl_tag c)).
      subst; eauto.
      apply TagS.add_3 in IN. congruence. auto.
    + intros.
      destruct (TagS.mem t a) eqn:?.
      apply TagS.mem_2 in Heqb; eauto.
      right; intro IN; apply TagS.mem_1 in IN. congruence.
Qed.


Lemma tagset_locked_update_spec t s rt s' u w:
  tagset_locked (t,s) (update_tcache rt s' (u, w)) <->
    if eq_dec s' s
    then tag_locked t (proj1_sig w)
    else tagset_locked (t,s) rt.
Proof.
  unfold update_tcache.
  unfold tagset_locked.
  destruct (eq_dec s' s). 2: tauto. tauto.
Qed.


Definition coherent_cache (m: mem) (c: tcache):=
  forall addr t s,
    addr_to_tagset addr = (t, s) ->
    forall w u,
      c s = (u, w) ->
      forall l,
        line_of_tag (proj1_sig w) t = Some l ->
        forall cl,
          proj1_sig w l = Some cl ->
          cl_dirty cl = false ->
          get_line m addr = cl_content cl.

Definition tcache_locked_dirty (c: tcache):=
  forall addr t s,
    addr_to_tagset addr = (t, s) ->
    forall w u,
      c s = (u, w) ->
      forall l,
        line_of_tag (proj1_sig w) t = Some l ->
        forall cl,
          proj1_sig w l = Some cl ->
          cl_locked cl = true ->
          cl_dirty cl = true.

Lemma read_from_cache_update_usage:
  forall c addr mi addr0 l,
    read_from_cache (tcache_update_usage c addr mi) addr0 l = read_from_cache c addr0 l.
Proof.
  intros.
  rewrite ! read_from_cache_eq.
  unfold tcache_update_usage.
  destr. destr. destr_in Heqp. destr_in Heqp. destr_in Heqp.
  unfold update_tcache in Heqp.
  destr_in Heqp.
  - subst. rewrite Heqp0. inv Heqp. auto.
  - rewrite Heqp. auto.
Qed.

Lemma read_from_cache_fetch_same:
  forall m rt addr l e,
    read_from_cache (fetch m rt addr l e) addr l = get_word addr (get_line m addr).
Proof.
  intros.
  rewrite read_from_cache_eq.
  generalize (fetch_unfold m rt addr l e).
  destr. destr. simpl. intros (p & EQ). rewrite EQ. destr. inv Heqp0.
  unfold update_tcache in H0.
  destr_in H0; try congruence. inv H0. simpl. unfold update_ways. destruct (eq_dec l l). 2: congruence. simpl. auto.
Qed.


Definition line_locked (c: tcache) addr l :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := c s in
  match proj1_sig w l with
    Some cl => cl_locked cl
  | None => false
  end.

Lemma tagset_locked_fetch t s mt rt addr l e:
  line_locked rt addr l = false ->
  tagset_locked (t, s) (fetch mt rt addr l e) <-> tagset_locked (t, s) rt.
Proof.
  intros.
  generalize (fetch_unfold mt rt addr l e).
  destr. destr. simpl. intros (p & EQ). rewrite EQ. clear EQ. destr.
  unfold update_tcache in Heqp0.
  destr_in Heqp0.
  - inv Heqp0. simpl. unfold update_ways. rewrite Heqp.
    unfold tag_locked.
    split; intros (l0 & cl & EQ & LOCKED & TAG).
    destr_in EQ; eauto. inv EQ. simpl in LOCKED. congruence.
    unfold tcache_line_of_addr in e.
    eexists l0, _. destr. 2: eauto. subst.
    exfalso.
    unfold line_locked in H.
    rewrite Heqt0, Heqp in H. rewrite EQ in H. congruence.
  - rewrite Heqp0. tauto.
Qed.


Lemma tagset_locked_update_cache_line:
  forall ts tc addr l f Hf (Hf2: forall cl, cl_locked (f cl) = cl_locked cl),
    tagset_locked ts (update_cache_line tc addr l f Hf) <-> tagset_locked ts tc.
Proof.
  intros.
  generalize (update_cache_line_eq tc addr l _ Hf). destr. destr.
  destr; [intros (p & EQ)|intros EQ]; rewrite EQ; clear EQ. 2: tauto.
  destruct ts.
  rewrite tagset_locked_update_spec. simpl.
  destr.
  - subst. rewrite Heqp.
    unfold update_ways. unfold tag_locked.
    split; intros (l0 & cl & EQ & LOCKED & TAG).
    destr_in EQ; eauto. inv EQ. rewrite Hf2 in LOCKED. eauto.
    subst. exists l0.
    destr; subst; eauto.
    setoid_rewrite Heqo in EQ. inv EQ.
    eexists; split. eauto. rewrite Hf2, Hf; auto.
  - tauto.
Qed.

Lemma write_in_cache_locked_dirty:
  forall rt addr v l,
    tcache_locked_dirty rt ->
    tcache_locked_dirty (write_in_cache rt addr v l).
Proof.
  unfold tcache_locked_dirty. intros.
  generalize (write_in_cache_unfold rt addr l v ). destr. destr.
  destr.
  intros (p & EQ); rewrite EQ in H1; clear EQ.
  unfold update_tcache in H1; destr_in H1; inv H1; eauto.
  simpl in *.
  rewrite line_of_tag_update_ways in H2; auto. 2: apply proj2_sig.
  repeat destr_in H2; inv H2; eauto.
  rewrite update_ways_same in H3; inv H3. simpl in *. auto.
  rewrite update_ways_other in H3; auto. eauto.
  intro EQ; rewrite EQ in H1; eauto.
Qed.

Lemma line_of_tag_update_same_tag:
  forall w l cl f t,
    wf_ways w ->
    w l = Some cl ->
    (forall cl, cl_tag (f cl) = cl_tag cl) ->
    forall l0,
      line_of_tag (update_ways w l (Some (f cl))) t = Some l0 ->
      line_of_tag w t = Some l0.
Proof.
  intros.
  rewrite line_of_tag_spec in *; auto.
  unfold update_ways in H2. destr_in H2; subst.
  destruct H2 as (? & EQ & TAG); inv EQ. eauto. eauto.
  constructor. simpl. intros.
  unfold update_ways in H3, H4. destr_in H3; inv H3; destr_in H4; inv H4; eauto.
  inv H.
  exfalso; apply n; eapply wft_nodup0; eauto.
  inv H. exfalso; apply n; symmetry; eapply wft_nodup0; eauto.
  inv H; eauto.
Qed.

Lemma write_in_cache_wt_locked_dirty:
  forall rt addr v l,
    tcache_locked_dirty rt ->
    tcache_locked_dirty (write_in_cache_wt rt addr v l).
Proof.
  unfold tcache_locked_dirty. intros.
  generalize (write_in_cache_wt_unfold rt addr l v ). destr. destr.
  destr.
  - intros (p & EQ); rewrite EQ in H1; clear EQ.
    unfold update_tcache in H1; destr_in H1; inv H1; eauto.
    simpl in *.
    assert (line_of_tag (proj1_sig s1) t = Some l0).
    eapply line_of_tag_update_same_tag. apply proj2_sig. apply Heqo. intros; apply write_in_cache_cl_wt_preserves_tag.
    simpl. eauto.
    unfold update_ways in H3. destr_in H3; eauto. inv H3. simpl. eauto.
  - intro EQ; rewrite EQ in H1; eauto.
Qed.

Lemma tagset_locked_lock_line:
  forall addr t s tc l t0 s0,
    addr_to_tagset addr = (t, s) ->
    tcache_line_of_addr tc addr = Some l ->
    tagset_locked (t0, s0) (lock_line tc addr l) <->
      tagset_locked (t0, s0) tc \/ (t0, s0) = (t, s).
Proof.
  intros.
  unfold tcache_line_of_addr in H0. rewrite H in H0. destr_in H0.
  edestruct (line_of_tag_some) as (? & EQ & TAG); eauto. subst.
  unfold lock_line.
  generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag). rewrite H, Heqp. setoid_rewrite EQ.
  intros (p & EQ2). rewrite EQ2; clear EQ2.
  rewrite tagset_locked_update_spec. simpl.
  destr.
  - subst. rewrite Heqp.
    unfold update_ways. unfold tag_locked.
    split.
    + intros (l0 & cl & EQ2 & LOCKED & TAG).
      destr_in EQ2; eauto. inv EQ2. simpl. auto.
      subst. left; exists l0. eauto.
    + intros [(l0 & cl & EQ2 & LOCKED & TAG) | EQ2]. subst.
      destruct (eq_dec l l0). subst. rewrite EQ in EQ2. inv EQ2.
      exists l0. destruct (eq_dec l0 l0); [|congruence]. eexists; split. eauto. simpl. auto.
      exists l0. destruct (eq_dec l l0); [congruence|]. eauto.
      inv EQ2.
      exists l.  destruct (eq_dec l l); [|congruence]. eauto.
  - destr. split; intuition.  inv H2. congruence.
Qed.


Lemma tagset_locked_unlock_line:
  forall addr t s tc l t0 s0,
    addr_to_tagset addr = (t, s) ->
    tcache_line_of_addr tc addr = Some l ->
    tagset_locked (t0, s0) (unlock_line tc addr l) <->
      tagset_locked (t0, s0) tc /\ (t0, s0) <> (t, s).
Proof.
  intros.
  unfold tcache_line_of_addr in H0. rewrite H in H0. destr_in H0.
  edestruct (line_of_tag_some) as (? & EQ & TAG); eauto. subst.
  unfold unlock_line.
  generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag). rewrite H, Heqp. setoid_rewrite EQ.
  intros (p & EQ2). rewrite EQ2; clear EQ2.
  rewrite tagset_locked_update_spec. simpl.
  destr.
  - subst. rewrite Heqp.
    unfold update_ways. unfold tag_locked.
    split.
    + intros (l0 & cl & EQ2 & LOCKED & TAG).
      destr_in EQ2; eauto. inv EQ2. simpl in LOCKED. congruence.
      subst. split. eauto.  intro EQ3; inv EQ3.
      destruct (proj2_sig s1).
      specialize (wft_nodup0 _ _ _ _ EQ2 EQ). intuition congruence.
    + intros [(l0 & cl & EQ2 & LOCKED & TAG) EQ3]. subst.
      exists l0. destruct (eq_dec l l0). subst. rewrite EQ in EQ2. inv EQ2. congruence.
      eauto.
  - destr. split; intuition.  inv H2. congruence.
Qed.




Lemma maybe_writeback_tcache_line_of_addr:
  forall m1 r1 addr1 m3 r3 t1 v1
         (L1: maybe_writeback_fetch m1 r1 addr1 = (m3, r3, t1, v1)),
    tcache_line_of_addr r3 addr1 = Some t1.
Proof.
  intros m1 r1 addr1 m3 r3 t1 v1 L1.
  unfold maybe_writeback_fetch in L1.
  repeat destr_in L1; inv L1; eauto.
  - generalize (fetch_unfold m3 r1 addr1 (tcache_line_to_evict r1 addr1) e).
    destr.  destr. simpl; intros (p & EQ); rewrite EQ. clear EQ.
    unfold tcache_line_of_addr. rewrite Heqt. rewrite update_tcache_same. simpl.
    rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig.
    simpl. destr; congruence.
  - unfold tcache_line_of_addr' in Heqs; destr_in Heqs; inv Heqs; auto.
Qed.

Lemma tcache_line_of_addr_update_usage:
  forall addr t mi addr1,
    tcache_line_of_addr (tcache_update_usage t addr mi) addr1 = tcache_line_of_addr t addr1.
Proof.
  unfold tcache_line_of_addr, tcache_update_usage. intros.
  repeat destr.
  repeat destr_in Heqp.
  unfold update_tcache in Heqp; destr_in Heqp; inv Heqp. congruence. congruence.
Qed.


Definition tcache_addr_dirty (c: tcache) (addr: val) : bool :=
  let '(t, s) := addr_to_tagset addr in
  let '(u, w) := c s in
  match line_of_tag (proj1_sig w) t with
    None => false
  | Some l =>
      match proj1_sig w l with
        Some cl => cl_dirty cl
      | None => false
      end
  end.

Lemma tcache_addr_dirty_update_usage:
  forall addr t mi addr1,
    tcache_addr_dirty (tcache_update_usage t addr mi) addr1 = tcache_addr_dirty t addr1.
Proof.
  unfold tcache_addr_dirty, tcache_update_usage. intros.
  destr. destr. destr_in Heqp. destr_in Heqp. destr_in Heqp.
  unfold update_tcache in Heqp. destr_in Heqp; inv Heqp. rewrite Heqp0. auto. rewrite H0. auto.
Qed.

Lemma tcache_locked_dirty_correct:
  forall t addr,
    tcache_locked_dirty t ->
    tagset_locked (addr_to_tagset addr) t ->
    tcache_addr_dirty t addr = true.
Proof.
  intros t addr TLD TL. unfold tcache_addr_dirty.
  red in TLD. red in TL.
  repeat destr_in TL.
  specialize (TLD _ _ _ Heqt0 _ _ Heqp). destruct TL as (? & ? & EQ & LOCKED & TAG). subst.
  assert (line_of_tag (proj1_sig s0) (cl_tag x0) = Some x).
  rewrite line_of_tag_spec. eauto. apply proj2_sig. rewrite H in *.
  specialize (TLD _ eq_refl). rewrite EQ in *.
  specialize (TLD _ eq_refl). auto.
Qed.



Lemma maybe_writeback_fetch_line_tag:
  forall m c a m2 c2 l t,
    maybe_writeback_fetch m c a = (m2, c2, l, t) ->
    tcache_line_of_addr c2 a = Some l.
Proof.
  unfold maybe_writeback_fetch; intros m c a m2 c2 l t MWF.
  repeat destr_in MWF; inv MWF.
  - generalize (fetch_unfold m2 c a (tcache_line_to_evict c a) e). unfold tcache_line_of_addr.
    destr.  destr. intros (p & EQ); rewrite EQ. clear EQ. rewrite update_tcache_same. simpl.
    rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig.
    simpl. destr. 2: congruence. auto.
  - unfold tcache_line_of_addr' in Heqs; destr_in Heqs; inv Heqs; auto.
Qed.

Lemma tcache_update_usage_inv:
  forall c a mi s u w,
    tcache_update_usage c a mi s = (u, w) ->
    exists u,
      c s = (u, w).
Proof.
  unfold tcache_update_usage. intros. repeat destr_in H; inv H.
  unfold update_tcache in H1; destr_in H1; inv H1; eauto.
Qed.


Lemma tcache_is_locked_tagset_locked:
  forall t addr,
    tcache_is_locked t addr = true ->
    tagset_locked (addr_to_tagset addr) t.
Proof.
  unfold tcache_is_locked, tagset_locked. intros.
  repeat destr_in H; inv H. red. eexists. setoid_rewrite Heqc0.
  apply line_of_tag_some in Heqo. rewrite Heqc0 in Heqo. destruct Heqo as (? & EQ & TAG). inv EQ. eauto.
Qed.


Lemma tcache_is_locked_tagset_locked_iff:
  forall t addr,
    tcache_is_locked t addr = true <->
    tagset_locked (addr_to_tagset addr) t.
Proof.
  unfold tcache_is_locked, tagset_locked. intros.
  destr. destr.
  split.
  intros H. repeat destr_in H; inv H. red. eexists. setoid_rewrite Heqc0.
  apply line_of_tag_some in Heqo. rewrite Heqc0 in Heqo. destruct Heqo as (? & EQ & TAG). inv EQ. eauto.
  intros (l & cl & EQ & LOCKED & TAG).
  destruct (line_of_tag_spec (proj2_sig s0)t0 l). rewrite EQ in H0.
  rewrite H0; eauto. rewrite EQ. auto.
Qed.
