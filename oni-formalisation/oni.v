Require Import List Classical Relations.
Import RelationClasses.
Import ListNotations.
Set Implicit Arguments.
From formal Require Import tactics Util.

Parameter input: Type.

Inductive process := D | A.

Instance process_eq: EqDec process.
Proof.
  constructor. decide equality.
Defined.

Section P.
  Variable state: Type.
  Variable event: Type.

Record program := {
    init: input -> state -> Prop;
    step: state -> event -> state -> Prop;
  }.

Definition deterministic (p: program) : Prop :=
  forall s t t' s' s'',
    step p s t s' ->
    step p s t' s'' ->
    t = t' /\ s' = s''.

Definition init_equivalent (phi0: state -> state -> Prop) (p: program) (equivP: state -> state -> Prop) : Prop :=
  forall i i' a a', init p i a -> init p i' a' -> phi0 a a' -> equivP a a'.

Definition F (p: program) (a: state) :=
  ~ exists b t, step p a t b.

Lemma step_non_final : forall p a t b, step p a t b -> ~ F p a.
Proof.
  unfold F.
  firstorder.
Qed.

Definition setinit (p: program) :=
  fun s => exists i, init p i s.

Inductive multistep (p: program): nat -> state -> list event -> state -> Prop :=
| multistep_0: forall s, multistep p O s nil s
| multistep_plus:
  forall a t a' t' n a'',
    step p a t a' ->
    multistep p n a' t' a'' ->
    multistep p (S n) a (t :: t') a''.

Lemma multistep_one:
  forall p s t s',
    step p s t s' ->
    multistep p 1 s [t] s'.
Proof.
  intros.
  econstructor. eauto. econstructor.
Qed.

Definition ONI (phi0: state -> state -> Prop) (p: program) :=
  forall i i' a a',
         init p i a ->
         init p i' a' ->
         phi0 a a' ->
         forall t n b t' b',
                multistep p n a t b ->
                multistep p n a' t' b' ->
                t = t' /\ (F p b <-> F p b').

Definition ONIwk (phi0: state -> state -> Prop) (p: program) :=
  forall i i' a a',
         init p i a ->
         init p i' a' ->
         phi0 a a' ->
         forall t n b t' b',
                multistep p n a t b ->
                multistep p n a' t' b' ->
                t = t' /\ (F p b -> F p b').

Lemma ONIwk_ONI :
  forall  (phi0: state -> state -> Prop) (p: program)
          (SYM:Symmetric phi0)
          (ONIWK : ONIwk phi0 p), ONI phi0 p.
Proof.
  unfold ONI,ONIwk.
  intros.
  generalize (ONIWK _ _ _ _ H H0 H1  _ _ _ _ _ H2 H3).
  intros (EQ & IMPL). subst.
  split ; auto.
  split; auto.
  eapply (ONIWK _ _ _ _ H0 H (SYM _ _ H1)).
  eauto. eauto.
Qed.

Lemma ONI_eq :
  forall  (phi0: state -> state -> Prop) (p: program)
          (SYM:Symmetric phi0),
          ONIwk phi0 p <-> ONI phi0 p.
Proof.
  split ;intros.
  eapply ONIwk_ONI;eauto.
  unfold ONI,ONIwk in *.
  intros.
  specialize (H _ _ _ _  H0 H1 H2 _ _ _ _ _ H3 H4).
  tauto.
Qed.

End P.

Section MEMINTERFACE.
  Variable res_event: Type.
  Variables mem_input mem_output: Type.
  Variable R: Type.
  Record mem_interface :=
    {
      mi_init: input -> R -> Prop;
      mi_interface: process -> R -> mem_input -> option (R * mem_output * res_event);
    }.

  Variable mi: mem_interface.

  Definition mi_init_equivalent (equiv_res: R -> R -> Prop) :=
    forall i ra ra', mi_init mi i ra -> mi_init mi i ra' -> equiv_res ra ra'.

End MEMINTERFACE.

Section WITHMEM.
  Variable state: Type.
  Variable res_event: Type.
  Variables mem_input mem_output: Type.

  Variable R: Type.
  Variable init_state: input -> state -> Prop.
  Variable mintf: mem_interface res_event mem_input mem_output R.

  Variable id: process.

  Variable core_event: Type.

  Variable step_hole: list core_event -> state -> list (mem_input * mem_output) -> state -> Prop.

  Inductive check_mem : R -> list (mem_input * mem_output) -> list res_event -> R -> Prop :=
  | check_mem_nil:
    forall r,
      check_mem r [] [] r
  | check_mem_cons:
    forall r1 mi r2 mo t l t2 r3,
      mi_interface mintf id r1 mi = Some (r2, mo,t) ->
      check_mem r2 l t2 r3 ->
      check_mem r1 ((mi,mo)::l) (t::t2) r3.

  Inductive step_mem : state * R -> list (core_event + res_event) -> state * R -> Prop :=
  | step_intro:
    forall ce s1 l t r1 r2 s2 tr,
      step_hole ce s1 l s2 ->
      check_mem r1 l t r2 ->
      tr = (map inr t ++ map inl ce) ->
      step_mem (s1, r1) tr (s2, r2)
  .

  Inductive memprog_init : input -> state * R -> Prop :=
  | memprog_init_intro:
    forall i s r,
      init_state i s ->
      mi_init mintf i r ->
      memprog_init i (s,r).

  Definition memprog : program (state * R) (list (core_event + res_event)) :=
    {|
      init := memprog_init;
      step := step_mem;
    |}.

End WITHMEM.

Section EQUIV.

  Variables A B: Type.

  Variable equivA: A -> A -> Prop.
  Variable equivB: B -> B -> Prop.

  Inductive equivAB : A * B -> A * B -> Prop :=
  | equivAB_intro:
    forall a1 b1 a2 b2,
      equivA a1 a2 ->
      equivB b1 b2 ->
      equivAB (a1, b1) (a2, b2).

End EQUIV.

Section SIM.
  Variable stateS eventS: Type.
  Variable stateT eventT: Type.
  Variable pS: program stateS eventS.
  Variable pT: program stateT eventT.

  Record bsim (sim: stateS -> stateT -> Prop) :=
    {
      bsim_step:
      forall alpha beta a t,
        sim a alpha ->
        step pT alpha t beta ->
        exists b t',
          step pS a t' b /\ sim b beta;
      bsim_init:
      forall i alpha,
        init pT i alpha ->
        exists a, init pS i a /\
        sim a alpha;
    }.

  Variable phi: stateS -> stateS -> Prop.
  Variable psi: stateT -> stateT -> Prop.

  Record lockstep2sim
    (equivS: stateS -> stateS -> Prop)
    (equivT: stateT -> stateT -> Prop)
    (sim: stateS -> stateT -> Prop) :=
    {
      l2s_cube:
      forall a a' b b' alpha alpha' beta beta' t tau tau',
        equivS a a' -> step pS a t b ->
        step pS a' t b' ->
        equivT alpha alpha' ->
        step pT alpha tau beta ->
        step pT alpha' tau' beta' ->
        sim a alpha -> sim a' alpha' ->
        sim b beta -> sim b' beta' ->
        equivS b b' /\ equivT beta beta' /\ tau = tau';
      l2s_init_s: init_equivalent phi pS equivS;
      l2s_init_t: init_equivalent psi pT equivT;
      l2s_init_implies:
        forall i i' alpha alpha' a a',
               init pT i alpha ->
               init pT i' alpha' ->
               psi alpha alpha' ->
               init pS i a ->
               init pS i' a' ->
               sim a alpha -> sim a' alpha' ->
               phi a a';
      l2s_final_t:
      forall a b a' b' alpha alpha' t,
        sim a alpha ->
        sim a' alpha'  ->
        equivS a a' ->
        step pS a t b ->
        step pS a' t b' ->
        equivT alpha alpha' ->
        (F pT alpha -> F pT alpha')
    }.


  Lemma final_impl :
    forall equivS equivT sim
           (BSIM: bsim sim)
           (L2S: lockstep2sim equivS equivT sim)
           a alpha a' alpha'
           (SIMA : sim a alpha)
           (SIMA': sim a' alpha')
           (eqS : equivS a a')
           (eqT : equivT alpha alpha')
           (ONIS : forall (t : list eventS) (n : nat) (b : stateS) (t' : list eventS) (b' : stateS),
               multistep pS n a t b -> multistep pS n a' t' b' -> t = t' /\ (F pS b <-> F pS b'))
           (D : F pS a <-> F pS a')
      (n: ~ F pT alpha'),
      F pT alpha -> False.
  Proof.
    intros.
    assert (n':=n).
    unfold F in n. apply NNPP in n.
    destruct n as (beta' & t & Salpha').
    eapply bsim_step in Salpha'; eauto.
    destruct Salpha' as (b' & t' & Sa' & SIM).
    assert (Sa := Sa').
    apply step_non_final in Sa.
    rewrite <- D0 in Sa.
    unfold F in  Sa.
    apply NNPP in Sa.
    destruct Sa as (b & t1 & Sa).
    assert ([t1] = [t']).
    { eapply ONIS.
      eapply multistep_one;eauto.
      eapply multistep_one;eauto.
    }
    inv H0.
    assert (F pT alpha -> F pT alpha').
    { eapply l2s_final_t; eauto. }
    tauto.
  Qed.

  Lemma iff_eq_sym : forall {A: Type} (a b:A), a = b <-> b = a.
  Proof.
    intuition congruence.
  Qed.

  Lemma iff_sym : forall (a b:Prop), (a <->  b) <-> (b <->  a).
  Proof.
    intuition congruence.
  Qed.

  Theorem oni_pres:
    forall equivS equivT sim
           (SYMPSI:Symmetric psi)
           (BSIM: bsim sim)
           (L2S: lockstep2sim equivS equivT sim)
           (ONIS: ONI phi pS),
      ONI psi pT.
  Proof.
    intros equivS equivT sim SYMPSI BSIM L2S ONIS.
    rewrite <- ONI_eq.
    red.
    intros i i' alpha alpha' INITalpha INITalpha' PSI.
    intros tau n beta tau' beta' MSTEP1 MSTEP2.
    destruct (bsim_init BSIM _ _ INITalpha) as (a & INITa & sima).
    pose proof (bsim_init BSIM _ _ INITalpha') as (a' & INITa' & sima').
    pose proof (l2s_init_t L2S _ _ INITalpha INITalpha' PSI) as eqT.
    assert (PHIaa' := l2s_init_implies L2S _ _ _ _ INITalpha INITalpha' PSI INITa INITa' sima sima').
    pose proof (l2s_init_s L2S _ _ INITa INITa' PHIaa') as eqS.
    specialize (ONIS _ _ _ _ INITa INITa' PHIaa').
    clear INITalpha INITalpha' PSI PHIaa'.
    clear INITa INITa' i i'.
    revert n  alpha tau beta MSTEP1 alpha' tau' beta' MSTEP2 a a' sima sima' eqS eqT ONIS.
    induction n; simpl; intros; eauto.
    + inv MSTEP1. inv MSTEP2. split. auto.
      destruct (ONIS [] O a [] a') as (eq & D).
      constructor. constructor.
      intros. destruct (classic (F pT beta'));auto.
      exfalso.
      eapply final_impl . eauto. eauto. apply sima. apply sima'. all:auto.
    + inv MSTEP1. inv MSTEP2.
      edestruct bsim_step as (b & tt' & STEP1 & simb). eauto. apply sima. eauto.
      edestruct bsim_step as (b' & tt'' & STEP2 & simb'). eauto. apply sima'. eauto.
      assert ([tt'] = [tt'']). {
        eapply ONIS.
        eapply multistep_one. eauto.
        eapply multistep_one. eauto. } inv H.
      destruct (l2s_cube L2S _ _ _ _ _ _ _ _ _ _ _
                  eqS STEP1 STEP2
                  eqT H0 H2 sima sima' simb simb'
               ) as (eqS' & eqT' & eqtrace). subst.
      edestruct (IHn _ _ _ H1 _ _ _ H3) as (eqtau & eqtrace); eauto.
      {
        intros t n0 b0 t'1 b'0 MSTEP3 MSTEP4.
        edestruct ONIS as (eqtrace & eqfinal).
        eapply multistep_plus. eauto. apply MSTEP3.
        eapply multistep_plus. eauto. apply MSTEP4. inv eqtrace. auto.
      }
      subst. auto.
      + auto.
  Qed.

End SIM.

Section SCHED.
  Variables event: Type.
  Variable scheduler_state: Type.
  Record scheduler := {
      sc_init: scheduler_state -> Prop;
      sc_next_process: scheduler_state -> process;
      sc_update: scheduler_state -> event -> scheduler_state
    }.
  Variable R: Type.
  Variable state: Type.
  Variable p1: program (state * R) event.
  Variable p2: program (state * R) event.
  Variable S: scheduler.

  Inductive init_compose:
    input -> (state * state * R * scheduler_state) -> Prop :=
    init_compose_intro:
      forall i sigma1 sigma2 r s,
        init p1 i (sigma1, r) ->
        init p2 i (sigma2, r) ->
        sc_init S s ->
        init_compose i (sigma1, sigma2, r, s).

  Inductive step_compose:
    (state * state * R * scheduler_state) ->
    (process * event) ->
    (state * state * R * scheduler_state) -> Prop :=
  | step_compose_left:
    forall sigma1 sigma2 r s t r' s' sigma1'
           (SCHED_D : sc_next_process S s = D)
           (STEP_D : step p1 (sigma1, r) t (sigma1', r'))
           (SCHED_UP : sc_update S s t = s'),
      step_compose (sigma1, sigma2, r, s) (D, t)
        (sigma1', sigma2, r', s')
  | step_compose_right:
    forall sigma1 sigma2 r s t r' s' sigma2'
           (SCHED_A : sc_next_process S s = A)
           (STEP_A  : step p2 (sigma2, r) t (sigma2', r'))
           (SCHED_UP : sc_update S s t = s'),
      step_compose (sigma1, sigma2, r, s) (A, t)
        (sigma1, sigma2', r', s').

  Definition compose :=
    {|
      init := init_compose;
      step := step_compose
    |}.


  Lemma compose_deterministic:
    deterministic p1 ->
    deterministic p2 ->
    deterministic compose.
  Proof.
    unfold deterministic.
    intros DET1 DET2 s t t' s' s'' STEP1 STEP2.
    inv STEP1; inv STEP2; try congruence.
    generalize (DET1 _ _ _ _ _ STEP_D STEP_D0). intros (? & A); inv A. auto.
    generalize (DET2 _ _ _ _ _ STEP_A STEP_A0). intros (? & A); inv A. auto.
  Qed.


  Inductive equivTR
    (equivP: state -> state -> Prop)
    (equivR: R -> R -> Prop)
    : state * state * R * scheduler_state ->
      state * state * R * scheduler_state -> Prop :=
  | equivTR_intro:
    forall sigma1 sigma1' sigma2 r1 r2 s,
      equivP sigma1 sigma1' ->
      equivR r1 r2 ->
      equivTR equivP equivR (sigma1, sigma2, r1, s) (sigma1', sigma2, r2, s).

  Lemma compose_init_equivalent phi equivP equivR:
    init_equivalent (fun '(s1,r1) '(s2,r2) => phi s1 s2) p1 (equivAB equivP equivR) ->
    init_equivalent (fun '(s1,s2,r,s) '(s1',s2',r',s') =>
                       phi s1 s1' /\ s2 = s2' /\ r = r' /\ s = s')
      compose (equivTR equivP equivR).
  Proof.
    unfold init_equivalent.
    intros IE i i' a a' INIT1 INIT2 PHI.
    inv INIT1. inv INIT2. destruct PHI as (PHI & ? & ? & ?). subst.
    generalize (IE _ _ _ _ H H2 PHI). intros A; inv A. constructor; eauto.
  Qed.

End SCHED.

Section ONI_RES.
  Variables eventA eventB eventC: Type.
  Variable state: Type.
  Variable RS: Type.
  Variable RT: Type.
  Variables mem_input mem_output: Type.

  Variable mintfS: mem_interface eventB mem_input mem_output RS.
  Variable mintfT: mem_interface eventC mem_input mem_output RT.
  Variable step_hole: state -> mem_input -> (state -> mem_output -> state) -> Prop.

  Variable sim_r: RS -> RT -> Prop.

  Definition sim_res : (state * RS) -> (state * RT) -> Prop :=
    fun '(ss,rs) '(st,rt) => ss = st /\ sim_r rs rt.

  Record bsim_res Pid :=
    {
      bsim_res_step:
      forall rs rt,
        sim_r rs rt ->
        forall id tau mi mo rt' ,
          Pid id ->
          mi_interface mintfT id rt mi = Some (rt', mo, tau) ->
          exists rs' t' ,
            mi_interface mintfS id rs mi = Some (rs', mo, t')
            /\ sim_r rs' rt' ;
      bsim_res_init:
      forall i rt,
        mi_init mintfT i rt ->
        exists rs,
          mi_init mintfS i rs /\
            sim_r rs rt
    }.

  Variable equiv: state -> state -> Prop.
  Variable equiv_resS: RS -> RS -> Prop.
  Variable equiv_resT: RT -> RT -> Prop.

  Inductive equivS: (state * RS) -> (state * RS) -> Prop :=
    | equivS_intro: forall s1 s2 rs1 rs2, equiv s1 s2 -> equiv_resS rs1 rs2 -> equivS (s1,rs1) (s2,rs2).
  Inductive equivT: (state * RT) -> (state * RT) -> Prop :=
    | equivT_intro: forall s1 s2 rs1 rs2, equiv s1 s2 -> equiv_resT rs1 rs2 -> equivT (s1,rs1) (s2,rs2).

  Record lockstep2sim_res := {
      l2s_res_cube:
      forall rs1 rt1 rs2 rt2 rs1' rs2' rt1' rt2' mi mo t' tau tau',
        sim_r rs1 rt1 ->
        sim_r rs2 rt2 ->
        equiv_resS rs1 rs2 ->
        mi_interface mintfS D rs1 mi = Some (rs1', mo, t') ->
        mi_interface mintfS D rs2 mi = Some (rs2', mo, t') ->
        equiv_resT rt1 rt2 ->
        mi_interface mintfT D rt1 mi = Some (rt1', mo, tau) ->
        mi_interface mintfT D rt2 mi = Some (rt2', mo, tau') ->
        equiv_resS rs1' rs2' /\ equiv_resT rt1' rt2' /\ tau = tau';

      l2s_res_init_s : forall i a a', mi_init mintfS i a -> mi_init mintfS i a' -> equiv_resS a a';
      l2s_res_init_t : forall i a a', mi_init mintfT i a -> mi_init mintfT i a' -> equiv_resT a a';

      l2s_res_final:
      forall r1 r2 id mi mo t2 r1',
        equiv_resT r1 r2 ->
        mi_interface mintfT id r1 mi = Some (r1', mo, t2) ->
        exists t2' mo' r2',
          mi_interface mintfT id r2 mi = Some (r2', mo', t2');
      l2s_sim_eq : forall alpha alpha' a a',
        equiv_resT alpha alpha' ->
        sim_r a alpha -> sim_r a' alpha' -> equiv_resS a a'
    }.

End ONI_RES.

Section OPT_SIM.
  Variable stateS eventS: Type.
  Variable stateT eventT: Type.
  Variable pS: program stateS eventS.
  Variable pT: program stateT (process * eventT).

  Record opt_bsim (sim: stateS -> stateT -> Prop) :=
    {
      opt_bsim_step_1:
      forall alpha beta a t,
        sim a alpha ->
        step pT alpha (D, t) beta ->
        (exists b t',
          step pS a t' b /\ sim b beta);
      opt_bsim_step_2:
      forall alpha beta a t,
        sim a alpha ->
        step pT alpha (A, t) beta ->
        sim a beta;
      opt_bsim_init:
      forall i alpha,
        init pT i alpha ->
        exists a', init pS i a' /\
        sim a' alpha;
    }.

  Record opt_lockstep2sim
           (phi: stateS -> stateS -> Prop)
           (psi: stateT -> stateT -> Prop)
    (equivS: stateS -> stateS -> Prop)
    (equivT: stateT -> stateT -> Prop)
    (sim: stateS -> stateT -> Prop)
    :=
    {
      opt_l2s_cube:
      forall a a' b b' alpha alpha' beta beta' t tau tau',
        equivS a a' ->
        step pS a t b ->
        step pS a' t b' ->
        equivT alpha alpha' ->
        step pT alpha (D, tau) beta ->
        step pT alpha' (D, tau') beta' ->
        sim a alpha -> sim a' alpha' ->
        sim b beta -> sim b' beta' ->
        equivS b b' /\ equivT beta beta' /\ tau = tau';
      opt_l2s_step_preserves_equiv:
      forall s1 s2 t t' s1' s2',
        step pT s1 (A, t) s1' ->
        step pT s2 (A, t') s2' ->
        equivT s1 s2 ->
        t = t' /\ equivT s1' s2';
      opt_l2s_init_s: init_equivalent phi pS equivS;
        opt_l2s_init_t: init_equivalent psi pT equivT;
        opt_l2s_init_implies:
        forall i i' alpha alpha' a a',
               init pT i alpha ->
               init pT i' alpha' ->
               psi alpha alpha' ->
               init pS i a ->
               init pS i' a' ->
               sim a alpha -> sim a' alpha' ->
               phi a a';
      opt_l2s_final_t:
      forall a b a' b' alpha alpha' t,
        sim a alpha ->
        sim a' alpha'  ->
        equivS a a' ->
        step pS a t b ->
        step pS a' t b' ->
        equivT alpha alpha' ->
        (F pT alpha -> F pT alpha');
      opt_l2s_final_t2:
      forall alpha alpha' t beta',
        equivT alpha alpha' ->
        step pT alpha' (A, t) beta' ->
        exists beta t0,
          step pT alpha t0 beta;
      opt_l2s_same_nextprocess:
      forall alpha alpha' tau tau' beta beta',
        equivT alpha alpha' ->
        step pT alpha tau beta ->
        step pT alpha' tau' beta' ->
        fst tau = fst tau'
    }.

  Theorem opt_oni_pres:
    forall phi psi equivS equivT sim
           (SYMPSI : Symmetric psi)
           (equivT_sym: Symmetric equivT)
           (BSIM: opt_bsim sim)
           (L2S: opt_lockstep2sim phi psi equivS equivT sim)
           (ONIS: ONI phi pS),
      ONI psi pT.
  Proof.
    intros phi psi equivS equivT sim SYMPSI equivT_sym BSIM L2S ONIS.
    rewrite <- ONI_eq.
    red.
    intros i i' alpha alpha' INITalpha INITalpha' PSI.
    intros tau n beta tau' beta' MSTEP1 MSTEP2.
    assert (INITa : exists a, init pS i a /\ sim a alpha).
    {
      destruct (opt_bsim_init BSIM _ _ INITalpha) as (a & INITa & sima); eauto.
    }
    destruct INITa as (a & INITa & sima).
    assert (eqT : equivT alpha alpha').
    {
      apply (opt_l2s_init_t L2S _ _ _ _ INITalpha INITalpha' PSI).
    }
    assert (INITa' : exists a', init pS i' a' /\ sim a' alpha').
    {
      destruct (opt_bsim_init BSIM _ _ INITalpha') as (a' & INITa' & sima'); eauto.
    }
    destruct INITa' as (a' & INITa' & sima').
    assert (PHIaa' : phi a a') by  exact (opt_l2s_init_implies L2S _ _ _ _ _ _ INITalpha INITalpha' PSI INITa INITa' sima sima').
    assert (eqS : equivS a a') by exact (opt_l2s_init_s L2S _ _ _ _ INITa INITa' PHIaa').
    specialize (ONIS _ _ _ _ INITa INITa' PHIaa').
    clear INITalpha INITalpha' PHIaa' PSI.
    clear INITa INITa' i i'.
    revert n  alpha tau beta MSTEP1 alpha' tau' beta' MSTEP2 a a' sima sima' eqS eqT ONIS.
    induction n; simpl; intros; eauto.
    + inv MSTEP1. inv MSTEP2. split. auto.
      destruct (ONIS [] O a [] a') as (eq & samefinal).
      constructor. constructor.
      {
        destruct (classic (F pT beta')) as [f|n]. auto.
        intro Fbeta. exfalso.
        unfold F in n.
        apply NNPP in n.
        destruct n as (beta2' & (id & t) & STEP).
        destruct id.
        - edestruct (opt_bsim_step_1) as (b & t' & STEPs & SIM). eauto. apply sima'. eauto.
          assert (STEPa: exists b' t'', step pS a t'' b').
          {
            unfold F in samefinal.
            apply not_iff_compat in samefinal.
            repeat rewrite NNPP_iff in *.
            firstorder.
          }
          destruct STEPa as (b' & t'' & STEPa).
          destruct (ONIS [t''] 1 b' [t'] b).
          apply multistep_one. auto.
          apply multistep_one. auto. inv H.
          generalize (@opt_l2s_final_t _ _ _ _ _ L2S  _ _ _ _ _ _ _
                        sima sima' eqS STEPa STEPs eqT).
          intro EQ. apply EQ in Fbeta. apply Fbeta. eauto.
        - apply Fbeta.
          eapply opt_l2s_final_t2; eauto.
      }
    + inv MSTEP1. inv MSTEP2.
      assert (fst t = fst t0). {
        eapply opt_l2s_same_nextprocess; eauto.
      }
      destruct t, t0. simpl in H. subst.
      destruct p0.
      * edestruct opt_bsim_step_1 as (b & tt' & STEP1 & simb). eauto. apply sima. eauto.
        edestruct opt_bsim_step_1 as (b' & tt'' & STEP2 & simb'). eauto. apply sima'. eauto.
        assert ([tt'] = [tt'']). {
          eapply ONIS.
          eapply multistep_one. eauto.
          eapply multistep_one. eauto. } inv H.
        destruct (opt_l2s_cube L2S _ _ _ _ _ _ _ _ _ _ _
                    eqS STEP1 STEP2
                    eqT H0 H2 sima sima' simb simb'
                 ) as (eqS' & eqT' & eqtrace). subst.
        edestruct (IHn _ _ _ H1 _ _ _ H3) as (eqtau & eqtrace); eauto.
        {
          intros t n0 b0 t'1 b'0 MSTEP3 MSTEP4.
          edestruct ONIS as (eqtrace & eqfinal).
          eapply multistep_plus. eauto. apply MSTEP3.
          eapply multistep_plus. eauto. apply MSTEP4. inv eqtrace. auto.
        }
        subst. auto.
      *
        edestruct opt_l2s_step_preserves_equiv
          as (eqe0 & EQ'). eauto. apply H0. apply H2. auto.
        subst.
        assert (SIMA: sim a a'0) by (eapply opt_bsim_step_2; eauto).
        assert (SIMA': sim a' a'1) by (eapply opt_bsim_step_2; eauto).
        generalize (IHn _ _ _ H1 _ _ _ H3 _ _ SIMA SIMA' eqS EQ' ONIS).
        intros (eqtrace & eqfinal). subst. auto.
    + auto.
  Qed.

End OPT_SIM.

Definition proj_proc {state event:Type} (p : program state (process * event)) (proc:process) : program state event :=
  Build_program (init p) (fun s e s' => step p s (proc,e) s').


Lemma deterministic_proj_proc:
  forall {state event: Type} (p: program state (process * event)) (proc: process),
    deterministic p ->
    deterministic (proj_proc p proc).
Proof.
  intros state event p proc DET.
  red. intros s t t' s' s'' STEP1 STEP2. simpl in *.
  specialize (DET _ _ _ _ _ STEP1 STEP2). destruct_and DET. inv DET0. auto.
Qed.


Lemma F_proj : forall state event (p : program state (process * event)) a,
    F p a -> forall proc, F (proj_proc p proc) a.
Proof.
  intros.
  unfold F,proj_proc in *; simpl in *.
  rewrite! not_exists_forall_not.
  repeat intro; apply H.
  firstorder.
Qed.

Lemma step_proj :  forall state event (p: program state (process * event))
                          proc  e a a',
    step p a (proc, e) a' ->
    step (proj_proc p proc) a e a'.
Proof.
  unfold proj_proc;simpl.
  auto.
Qed.


Section OPT_SEP_SIM.
  Variable stateS eventS: Type.
  Variable stateT eventT: Type.
  Variable pS: program stateS eventS.
  Variable pT: program stateT (process * eventT).


  Definition same_nextprocess {state event :Type}  (p: program state (process * event)) (equiv: state -> state -> Prop) :=
      forall alpha alpha' tau tau' beta beta',
        equiv alpha alpha' ->
        step p alpha tau beta ->
        step p alpha' tau' beta' ->
        fst tau = fst tau'.

  Record attacker_prop {stateS stateT event: Type} (p: program stateT event)
    (sim : stateS -> stateT -> Prop) (equiv : stateT -> stateT -> Prop) :=
    {
      att_equiv : forall s1 s2 t s1',
        step p s1 t s1' ->
        equiv s1 s2 ->
        exists s2',
          step p s2 t s2' /\
            equiv s1' s2';
      att_bsim :
      forall alpha beta a t,
        sim a alpha ->
        step p alpha t beta ->
        sim a beta;
    }.

  Theorem proj_oni_pres:
    forall phi psi equivS equivT sim
           (SYMPSI : Symmetric psi)
           (equivT_sym: Symmetric equivT)
           (BSIM: bsim pS (proj_proc pT D) sim)
           (SAMENXT : same_nextprocess pT equivT)
           (ATTK : attacker_prop (proj_proc pT A) sim equivT)
           (DET: deterministic pT)
           (L2S:  lockstep2sim pS (proj_proc pT D) phi psi equivS equivT sim)
           (ONIS: ONI phi pS),
      ONI psi pT.
  Proof.
    intros phi psi equivS equivT sim SYMPSI equivT_sym BSIM SAMENXT ATTK DET L2S ONIS.
    rewrite <- ONI_eq.
    red.
    intros i i' alpha alpha' INITalpha INITalpha' PSI.
    intros tau n beta tau' beta' MSTEP1 MSTEP2.
    assert (INITa : exists a, init pS i a /\ sim a alpha).
    {
      destruct (bsim_init BSIM _ _ INITalpha) as (a & INITa & sima); eauto.
    }
    destruct INITa as (a & INITa & sima).
    assert (eqT : equivT alpha alpha').
    {
      apply (l2s_init_t L2S _ _ _ _ INITalpha INITalpha' PSI).
    }
    assert (INITa' : exists a', init pS i' a' /\ sim a' alpha').
    {
      destruct (bsim_init BSIM _ _ INITalpha') as (a' & INITa' & sima'); eauto.
    }
    destruct INITa' as (a' & INITa' & sima').
    assert (PHIaa' : phi a a') by  exact (l2s_init_implies L2S _ _ _ _ _ _ INITalpha INITalpha' PSI INITa INITa' sima sima').
    assert (eqS : equivS a a') by exact (l2s_init_s L2S _ _ _ _ INITa INITa' PHIaa').
    specialize (ONIS _ _ _ _ INITa INITa' PHIaa').
    clear INITalpha INITalpha' PHIaa' PSI.
    clear INITa INITa' i i'.
    revert n  alpha tau beta MSTEP1 alpha' tau' beta' MSTEP2 a a' sima sima' eqS eqT ONIS.
    induction n; simpl; intros; eauto.
    + inv MSTEP1. inv MSTEP2. split. auto.
      destruct (ONIS [] O a [] a') as (eq & samefinal).
      constructor. constructor.
      {
        destruct (classic (F pT beta')) as [f|n]. auto.
        intro Fbeta. exfalso.
        unfold F in n.
        apply NNPP in n.
        destruct n as (beta2' & (id & t) & STEP).
        destruct id.
        - edestruct (bsim_step) as (b & t' & STEPs & SIM). eauto. apply sima'. unfold proj_proc. simpl. eauto.
          assert (STEPa: exists b' t'', step pS a t'' b').
          {
            unfold F in samefinal.
            apply not_iff_compat in samefinal.
            repeat rewrite NNPP_iff in *.
            firstorder.
          }
          destruct STEPa as (b' & t'' & STEPa).
          destruct (ONIS [t''] 1 b' [t'] b).
          apply multistep_one. auto.
          apply multistep_one. auto. inv H.
          generalize (@l2s_final_t _ _ _ _ _ _ _ _ _ _ _ L2S  _ _ _ _ _ _ _
                        sima sima' eqS STEPa STEPs eqT).
          intro EQ.
          eapply F_proj in Fbeta.
          apply EQ in Fbeta. apply Fbeta. firstorder.
        - apply Fbeta.
          apply step_proj in STEP.
          edestruct @att_equiv as (s2' & STEP1 & EQ1). eauto. eauto. eauto. simpl in STEP1. eauto.
      }
    + inv MSTEP1. inv MSTEP2.
      assert (fst t = fst t0). {
        eapply SAMENXT; eauto.
      }
      destruct t, t0. simpl in H. subst.
      destruct p0.
      * edestruct bsim_step as (b & tt' & STEP1 & simb). eauto. apply sima.
        eapply step_proj; eauto.
        edestruct bsim_step as (b' & tt'' & STEP2 & simb'). eauto. apply sima'. eapply step_proj;eauto.
        assert ([tt'] = [tt'']). {
          eapply ONIS.
          eapply multistep_one. eauto.
          eapply multistep_one. eauto. } inv H.
        destruct (l2s_cube L2S _ _ _ _ _ _ _ _ _ _ _
                    eqS STEP1 STEP2
                    eqT H0 H2 sima sima' simb simb'
                 ) as (eqS' & eqT' & eqtrace). subst.
        edestruct (IHn _ _ _ H1 _ _ _ H3) as (eqtau & eqtrace); eauto.
        {
          intros t n0 b0 t'1 b'0 MSTEP3 MSTEP4.
          edestruct ONIS as (eqtrace & eqfinal).
          eapply multistep_plus. eauto. apply MSTEP3.
          eapply multistep_plus. eauto. apply MSTEP4. inv eqtrace. auto.
        }
        subst. auto.
      *
        edestruct (att_equiv ATTK)
          as (eqe0 & STEP' & EQ'). apply H0. eauto.
        simpl in STEP'. eapply DET in STEP'. trim STEP'. eauto. destruct STEP' as (EQ1 & EQ2). inv EQ1.
        assert (SIMA: sim a a'0).
        {
          eapply att_bsim;eauto; eapply step_proj; eauto.
        }
        assert (SIMA': sim a' a'1). {
            eapply att_bsim;eauto; eapply step_proj; eauto.
        }
        generalize (IHn _ _ _ H1 _ _ _ H3 _ _ SIMA SIMA' eqS EQ' ONIS).
        intros (eqtrace & eqfinal). subst. auto.
    + auto.
  Qed.

End OPT_SEP_SIM.

Section ONI_FINAL.
  Variable state: Type.
  Variables eventA eventB eventC: Type.

  Variable phi0: state -> state -> Prop.

  Variables mem_input mem_output: Type.
  Variables RS RT: Type.

  Variable mintfS: mem_interface eventB mem_input mem_output RS.
  Variable mintfT: mem_interface eventC mem_input mem_output RT.
  Variable mintfTA: mem_interface eventC mem_input mem_output RT.

  Variable step_hole: list eventA -> state -> list (mem_input * mem_output) -> state -> Prop.

  Variable scheduler_type: Type.
  Variable sch: scheduler (list (eventA + eventC)) scheduler_type.

  (* Equivalence relations on programs, source resources, target resources. *)
  Variable equivP: state -> state -> Prop.
  Variable equiv_resS: RS -> RS -> Prop.
  Variable equiv_resT: RT -> RT -> Prop.

  (* (Some of) These equivalences need to be (at least) symmetric. *)
  Hypothesis equivP_sym: Symmetric equivP.
  Hypothesis phi0_sym : Symmetric phi0.
  Hypothesis equiv_resT_equiv: Equivalence equiv_resT.

  (* Simulation relation between source and target resources. (set of defender
     locked lines vs. concrete cache) *)
  Variable sim_r: RS -> RT -> Prop.
  (* Backward simulation and lockstep 2-simulation for resources. *)
  Hypothesis bsim_resource: bsim_res mintfS mintfT sim_r (fun id => id = D).

  (* Simulation relation between target resources. On the left, it is used only
     by the defender. On the right, it is shared with the attacker. *)

  (* New backward simulation for resources, with an "option" diagram. Steps are
     labelled with the identity of the process using the resource. Target steps
     labelled [D] are matched with exactly one source step, while target steps
     labelled [A] are matched with zero source step. *)
  Record opt_bsim_res := {
      opt_br_step_A:
      (forall r r' (SIM : sim_r r r')
              t' r'2 mi mo
              (RBSTEP: mi_interface mintfTA A r' mi = Some (r'2, mo, t')),
          sim_r r r'2);
    }.

  Hypothesis opt_bsim_resource: opt_bsim_res.

  Hypothesis sim_r_inj : forall r1 r2 rho,
      sim_r r1 rho -> sim_r r2 rho -> equiv_resS r1 r2.

  Definition equiv :  (state * state * RT * scheduler_type) -> (state * state * RT * scheduler_type) -> Prop :=
    fun '(s1, s2, r, s) '(s1', s2', r', s') =>
      equivP s1 s1' /\ s2 = s2' /\  equiv_resT r r' /\ s = s'.

  Definition sim :  (state * RS) -> (state * state * RT * scheduler_type) -> Prop :=
    fun '(s1, r) '(s1', s2', r', s') =>
      s1 = s1' /\ sim_r r r'.

  Variable init_state: input -> state -> Prop.

  Lemma bsim_res_step_check_mem:
    forall l alpha t alpha' ,
      check_mem mintfT D alpha l t alpha' ->
      forall a,
        sim_r a alpha ->
        exists t' a' ,
          check_mem mintfS D a l t' a' /\
            sim_r a' alpha'.
  Proof.
    induction 1; simpl; intros; eauto.
    - eexists _, _; split; eauto. constructor.
    - edestruct bsim_res_step as (rs' & t' & EQ & SIM). apply bsim_resource. apply H1. reflexivity. eauto.
      edestruct IHcheck_mem as (tt' & a' & CMI & SIM'). apply SIM.
      eexists _, _; split; eauto. econstructor; eauto.
  Qed.

  Lemma opt_bsim_res_check_mem:
      forall r' r'2 l t,
        check_mem mintfTA A r' l t r'2 ->
        forall r,
          sim_r r r' ->
          sim_r r r'2.
  Proof.
    induction 1; simpl; intros; eauto.
    eapply opt_bsim_resource in H. eapply IHcheck_mem; eauto.
    auto.
  Qed.

  Variable init_state2: input -> state -> Prop.
  Lemma opt_bsim_final:
    opt_bsim
      (memprog init_state mintfS D step_hole)
      (compose
         (memprog init_state mintfT D step_hole)
         (memprog init_state2 mintfTA A step_hole)
         sch)
      sim.
  Proof.
    constructor.
    + intros. destruct a as (a,ar).
      destruct alpha as (((alpha,x),alphar),s).
      simpl in H. destruct H ; subst.
      inv H0. inv STEP_D.
      eapply bsim_res_step_check_mem in H6; eauto.
      destruct H6 as (br & tb & RSTEP & SIM).
      eexists _, _; split. econstructor. eauto. eauto.
      simpl. reflexivity. split; auto.
    + intros.
      inv H0.
      inv STEP_A.
      destruct a as (a,ar).
      simpl in H. destruct H;subst.
      simpl. split;auto.
      eapply opt_bsim_res_check_mem in H6; eauto.
    + intros.
      inv H.
      inv H0. inv H1.
      eapply bsim_resource in H6.
      destruct H6 as (rs & IS & SIM).
      exists (sigma1,rs).
      simpl. split ; auto.
      constructor;auto.
  Qed.

  Hypothesis mem_interfaceS_equiv:
    forall rs1 rs2 id mi rs1' rs2' mo1 mo2 t1 t2,
      equiv_resS rs1 rs2 ->
      mi_interface mintfS id rs1 mi = Some (rs1', mo1, t1) ->
      mi_interface mintfS id rs2 mi = Some (rs2', mo2, t2) ->
      equiv_resS rs1' rs2' /\ mo1 = mo2 /\ t1 = t2.

  Lemma check_mem_equiv:
    forall id l r1 t r2 r1' t' r2' ,
      equiv_resS r1 r1' ->
      check_mem mintfS id r1 l t r2 ->
      check_mem mintfS id r1' l t' r2' ->
      t = t' /\ equiv_resS r2 r2'.
  Proof.
    induction l; simpl; intros; eauto.
    - inv H0. inv H1. split; auto.
    - inv H0. inv H1.
      generalize (mem_interfaceS_equiv id _ H H5 H9).
      intros (? & ? & ?). subst.
      edestruct IHl. 2: apply H8. 2: apply H10. apply H0.
      subst. auto.
  Qed.

  Variable phi : state * RS -> state * RS -> Prop.
  Variable psi : state * state * RT * scheduler_type -> state * state * RT * scheduler_type -> Prop.
  Hypothesis phi_sym: Symmetric phi.
  Hypothesis psi_sym: Symmetric psi.
  Hypothesis opt_l2s_final:
    opt_lockstep2sim
      (memprog init_state mintfS D step_hole)
      (compose
         (memprog init_state mintfT D step_hole)
         (memprog init_state2 mintfTA A step_hole)
         sch)
      phi
      psi
      (equivS equivP equiv_resS) equiv sim.

  Theorem oni_final:
    ONI
      phi
      (memprog init_state mintfS D step_hole) ->
    ONI
      psi
      (compose
         (memprog init_state mintfT D step_hole)
         (memprog init_state2 mintfTA A step_hole)
         sch).
  Proof.
    intros ONIS.
    eapply opt_oni_pres with (equivS := equivS equivP equiv_resS) (equivT := equiv) (sim := sim).
    - apply psi_sym.
    - unfold equiv.
      red; intros.
      destruct y as (((s1,s2),r),s).
      destruct x as (((s1',s2'),r'),s').
      intuition subst;auto. symmetry. auto.
    - apply opt_bsim_final.
    - apply opt_l2s_final.
    - auto.
  Qed.

End ONI_FINAL.

Check oni_final.
