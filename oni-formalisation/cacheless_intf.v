From formal Require Import oni syntax Util mem.
Inductive cacheless_event :=
| cle_load (addr: val)
| cle_store (addr v: val)
| cle_lock (addr: val)
| cle_unlock (addr: val).

Definition cacheless_mem_interface (p: process) (m: mem) (mi: mem_input):
  option (mem * mem_output * cacheless_event) :=
  match mi with
    mi_load addr =>
      let/opt _ := check_process_of_addr addr p in
      Some (m, Some (m addr), cle_load addr)
  | mi_store addr v =>
      let/opt _ := check_process_of_addr addr p in
      Some (do_store m addr v, None, cle_store addr v)
  | mi_lock addr =>
      let/opt _ := check_process_of_addr addr p in
      Some (m, None, cle_lock addr)
  | mi_unlock addr =>
      let/opt _ := check_process_of_addr addr p in
      Some (m, None, cle_unlock addr)
  end.

Definition cacheless_mem_intf (cacheless_init: input -> mem -> Prop): mem_interface cacheless_event mem_input mem_output mem :=
  {|
    mi_init := cacheless_init;
    mi_interface := cacheless_mem_interface
  |}.
