From formal Require Import oni.
Require Import List.
Require Import OrderedType.
Require Import Lia.
Import ListNotations.
Require Import Bool.
Import BoolNotations.
Require Import Permutation.
Set Implicit Arguments.
From formal Require Import Util tactics CacheTypes syntax.
Require Import FunctionalExtensionality.

Section CORESEM.

  Inductive core_event :=
    ce_arith (v: option val)
  | ce_jump (newPC: val).

  Definition set_reg (rs: regset) (r: reg) (v: val) : regset :=
    if eq_dec r R0
    then rs
    else fun rr => if eq_dec r rr then v else rs rr.

  Lemma set_reg_other:
    forall rs r v r' (diff: r <> r'),
      set_reg rs r v r' = rs r'.
  Proof.
    unfold set_reg. intros.
    destruct eq_dec. auto.
    destruct eq_dec. congruence. auto.
  Qed.

  Lemma set_reg_same:
    forall rs r v (diff: r <> R0),
      set_reg rs r v r = v.
  Proof.
    unfold set_reg. intros.
    destruct eq_dec. congruence.
    destruct eq_dec. auto. congruence.
  Qed.

  Definition nextinstr rs :=
    set_reg rs PC (incr (rs PC)).

  Section WITHMEM.
    Variable MT: Type.
    Variable Mevent: Type.
    Variable mintf: mem_interface Mevent mem_input mem_output MT.

    Inductive stateC :=
    | StateC (R: regset) (M: MT).

    Variable id: process.
    Inductive get_instr : stateC -> instr -> list Mevent -> MT -> Prop :=
    | get_instr_intro:
      forall R M i addr (v: val) M' t,
        R PC = addr ->
        mi_interface mintf id M (mi_load addr) = Some (M', Some v, t) ->
        decode v = Some i ->
        perm addr = rx ->
        get_instr (StateC R M) i [t] M'.

    (* "Natural" semantics where memory accesses are performed through the
       memory interface *)
    Inductive core_step : stateC -> list (core_event + Mevent) -> stateC -> Prop :=
    | core_step_arith:
      forall R M rd rs1 rs2 aop v1 v2 t ti M'
             (GI: get_instr (StateC R M) (arith rd rs1 rs2 aop) ti M'),
        rd <> PC ->
        R rs1 = v1 ->
        R rs2 = v2 ->
        t = leak_arith aop v2 ->
        core_step (StateC R M) (map inr ti ++ [inl (ce_arith t)]) (StateC (set_reg (nextinstr R) rd (sem_aop aop v1 v2)) M')
    | core_step_jump:
      forall R M pc rd rs1 rs2 jop v1 v2 newPC ti M'
             (GI: get_instr (StateC R M) (jump rd rs1 rs2 jop) ti M'),
        R PC = pc ->
        R rs1 = v1 ->
        R rs2 = v2 ->
        newPC = sem_jop jop v1 v2 pc ->
        core_step (StateC R M) (map inr ti ++ [inl (ce_jump newPC)]) (StateC (set_reg (set_reg R rd (incr pc)) PC newPC) M')
    | core_step_load:
      forall R M rd rs v addr ti M' M'' tm
             (GI: get_instr (StateC R M) (load rd rs) ti M'),
        rd <> PC ->
        R rs = addr ->
        mi_interface mintf id M' (mi_load addr) = Some (M'', Some v, tm) ->
        core_step (StateC R M) (map inr (ti ++ [tm])) (StateC (set_reg (nextinstr R) rd v) M'')
    | core_step_store:
      forall R M rs1 rs2 addr ti M' M'' tm
             (GI: get_instr (StateC R M) (store rs1 rs2) ti M'),
        R rs1 = addr ->
        mi_interface mintf id M' (mi_store addr (R rs2)) = Some (M'', None, tm) ->
        perm addr = rw ->
        core_step (StateC R M) (map inr (ti ++ [tm])) (StateC (nextinstr R) M'')
    | core_step_lock:
      forall R M rs1 addr ti M' M'' tm
             (GI: get_instr (StateC R M) (lock rs1) ti M'),
        R rs1 = addr ->
        mi_interface mintf id M' (mi_lock addr) = Some (M'', None, tm) ->
        core_step (StateC R M) (map inr (ti ++ [tm])) (StateC (nextinstr R) M'')
    | core_step_unlock:
      forall R M rs1 addr ti M' M'' tm
             (GI: get_instr (StateC R M) (unlock rs1) ti M'),
        R rs1 = addr ->
        mi_interface mintf id M' (mi_unlock addr) = Some (M'', None, tm) ->
        core_step (StateC R M) (map inr (ti ++ [tm])) (StateC (nextinstr R) M'')
    .

    (* More abstract semantics given as two components:
       - the core semantics by `get_instr_hole` and `step_hole` where states are
         only register sets, plus memory constraints
       - the memory interface, separately.
     *)
    Inductive get_instr_hole : regset -> instr -> list (mem_input * mem_output) -> Prop :=
    | get_instr_hole_intro:
      forall R i addr (v: val),
        R PC = addr ->
        decode v = Some i ->
        perm addr = rx ->
        get_instr_hole R i [(mi_load addr, Some v)].

    Inductive step_hole : list core_event -> regset -> list (mem_input * mem_output) -> regset -> Prop :=
    | step_hole_arith:
      forall R rd rs1 rs2 aop v1 v2 t l
             (GI: get_instr_hole R (arith rd rs1 rs2 aop) l),
        rd <> PC ->
        R rs1 = v1 ->
        R rs2 = v2 ->
        t = leak_arith aop v2 ->
        step_hole [ce_arith t] R l (set_reg (nextinstr R) rd (sem_aop aop v1 v2))
    | step_hole_jump:
      forall R pc rd rs1 rs2 jop v1 v2 newPC l
             (GI: get_instr_hole R (jump rd rs1 rs2 jop) l),
        R PC = pc ->
        R rs1 = v1 ->
        R rs2 = v2 ->
        newPC = sem_jop jop v1 v2 pc ->
        step_hole [(ce_jump newPC)] R l (set_reg (set_reg R rd (incr pc)) PC newPC)
    | step_hole_load:
      forall R rd rs v addr l
             (GI: get_instr_hole R (load rd rs) l),
        rd <> PC ->
        R rs = addr ->
        step_hole [] R (l ++ (mi_load addr,Some v)::[]) (set_reg (nextinstr R) rd v)
    | step_hole_store:
      forall R rs1 rs2 addr l
             (GI: get_instr_hole R (store rs1 rs2) l),
        R rs1 = addr ->
        perm addr = rw ->
        step_hole [] R (l ++ (mi_store addr (R rs2),None)::[]) (nextinstr R)
    | step_hole_lock:
      forall R rs1 addr l
             (GI: get_instr_hole R (lock rs1) l),
        R rs1 = addr ->
        step_hole [] R (l ++ [(mi_lock addr,None)]) (nextinstr R)
    | step_hole_unlock:
      forall R rs1 addr l
             (GI: get_instr_hole R  (unlock rs1) l),
        R rs1 = addr ->
        step_hole [] R (l ++ [(mi_unlock addr,None)]) (nextinstr R)
    .


    (* Equivalence of both semantics. *)
    Lemma get_instr_hole_get_instr:
      forall R i M l l2 t0 M',
        get_instr_hole R i l ->
        check_mem mintf id M (l ++ l2) t0 M' ->
        exists t t2 M'',
          t0 = t ++ t2 /\
            get_instr (StateC R M) i t M'' /\
            check_mem mintf id M'' l2 t2 M'.
    Proof.
      intros R i M l l2 t0 M' H H0.
      inv H. simpl in H0. inv H0.
      eexists [t], t2, _. split. reflexivity.
      split. 2: eauto.
      econstructor; eauto.
    Qed.

    Lemma step_hole_mi:
      forall  R R' M M' t,
        step_mem mintf id step_hole (R, M) t (R', M') ->
        core_step (StateC R M) t (StateC R' M').
    Proof.
      intros R R' M M' t SM.
      inv SM. inv H4.
      - assert (GIHGI := @get_instr_hole_get_instr R _ M l [] t0 M' GI).
        rewrite app_nil_r in GIHGI.
        specialize (GIHGI H5).
        destruct GIHGI as (? & ? & M'' & EQt & GI' & CM).
        inv CM.
        eapply core_step_arith; eauto. rewrite app_nil_r; eauto.
      - assert (GIHGI := @get_instr_hole_get_instr R _ M l [] t0 M' GI).
        rewrite app_nil_r in GIHGI.
        specialize (GIHGI H5).
        destruct GIHGI as (? & ? & M'' & EQt & GI' & CM).
        inv CM.
        eapply core_step_jump; eauto. rewrite app_nil_r; eauto.
      - assert (GIHGI := get_instr_hole_get_instr _ GI H5).
        destruct GIHGI as (? & ? & M'' & EQt & GI' & CM).
        inv CM. inv H8.
        rewrite app_nil_r. eapply core_step_load; eauto.
      - assert (GIHGI := get_instr_hole_get_instr _ GI H5).
        destruct GIHGI as (? & ? & M'' & EQt & GI' & CM).
        inv CM. inv H8.
        rewrite app_nil_r. auto.
        eapply core_step_store; eauto.
      - assert (GIHGI := get_instr_hole_get_instr _ GI H5).
        destruct GIHGI as (? & ? & M'' & EQt & GI' & CM).
        inv CM. inv H7.
        rewrite app_nil_r.
        eapply core_step_lock; eauto.
      - assert (GIHGI := get_instr_hole_get_instr _ GI H5).
        destruct GIHGI as (? & ? & M'' & EQt & GI' & CM).
        inv CM. inv H7.
        rewrite app_nil_r.
        eapply core_step_unlock; eauto.
    Qed.

    Lemma get_instr_get_instr_hole:
      forall R M i t M',
        get_instr (StateC R M) i t M' ->
        exists v t0,
          get_instr_hole R i [(mi_load (R PC), Some v)] /\
            mi_interface mintf id M (mi_load (R PC)) = Some (M', Some v, t0) /\
            decode v = Some i /\
            t = [t0].
    Proof.
      intros R M i t M' GI.
      inv GI. rewrite H2. exists v, t0.
      repeat split; auto.
    Qed.

    Lemma step_hole_mi_inv:
      forall  R R' M M' t,
        core_step (StateC R M) t (StateC R' M') ->
        step_mem mintf id step_hole (R, M) t (R', M').
    Proof.
      intros R R' M M' t SM.
      inv SM.
      - assert (GIGIH := get_instr_get_instr_hole GI).
        destruct GIGIH as (? & ? & GIH & MI & DEC & EQt); subst.
        econstructor. eapply step_hole_arith; eauto. econstructor. eauto.
        constructor. reflexivity.
      - assert (GIGIH := get_instr_get_instr_hole GI).
        destruct GIGIH as (? & ? & GIH & MI & DEC & EQt); subst.
        econstructor. eapply step_hole_jump; eauto. econstructor. eauto.
        constructor. reflexivity.
      - assert (GIGIH := get_instr_get_instr_hole GI).
        destruct GIGIH as (? & ? & GIH & MI & DEC & EQt); subst.
        econstructor. eapply step_hole_load; eauto. econstructor. eauto.
        econstructor. eauto. constructor. reflexivity.
      - assert (GIGIH := get_instr_get_instr_hole GI).
        destruct GIGIH as (? & ? & GIH & MI & DEC & EQt); subst.
        econstructor. eapply step_hole_store; eauto. econstructor. eauto.
        econstructor; eauto.
        constructor. reflexivity.
      - assert (GIGIH := get_instr_get_instr_hole GI).
        destruct GIGIH as (? & ? & GIH & MI & DEC & EQt); subst.
        econstructor. eapply step_hole_lock; eauto. econstructor. eauto.
        econstructor; eauto.
        constructor. reflexivity.
      - assert (GIGIH := get_instr_get_instr_hole GI).
        destruct GIGIH as (? & ? & GIH & MI & DEC & EQt); subst.
        econstructor. eapply step_hole_unlock; eauto. econstructor. eauto.
        econstructor; eauto.
        constructor. reflexivity.
    Qed.

    Definition core_program init : program stateC (list (core_event + Mevent)) :=
      {|
        init := init;
        step := core_step;
      |}.

    Lemma get_instr_determ:
      forall s i i' t t' m m' , get_instr s i t m -> get_instr s i' t' m' -> i = i' /\ t = t' /\ m = m'.
    Proof.
      intros s i i' t t' m m' GI1 GI2.
      inv GI1. inv GI2. intuition congruence.
    Qed.


    Lemma core_program_deterministic i: deterministic (core_program i).
    Proof.
      red. simpl. intros s t t' s' s'' CS1 CS2.
      inv CS1; inv CS2; generalize (get_instr_determ GI GI0); intuition try congruence.
    Qed.

  End WITHMEM.
End CORESEM.




Lemma get_instr_ext:
  forall {MT ME} (mi: mem_interface MT _ _ ME) rs1 m1 t m2 rs1' i,
    get_instr mi A (StateC rs1 m1) i t m2 ->
    (forall r, rs1 r = rs1' r) ->
    get_instr mi A (StateC rs1' m1) i t m2.
Proof.
  intros MT ME mi rs1 m1 t m2 rs1' i GI EXT.
  inv GI.
  econstructor; eauto. rewrite <- EXT; auto. congruence.
Qed.

Lemma set_reg_ext:
  forall rs1 rs2 (EXT: forall r, rs1 r = rs2 r) r v r2,
    set_reg rs1 r v r2 = set_reg rs2 r v r2.
Proof.
  unfold set_reg; intros; repeat destr; eauto.
Qed.

Lemma nextinstr_ext:
  forall rs1 rs2 (EXT: forall r, rs1 r = rs2 r) r2,
    nextinstr rs1 r2 = nextinstr rs2 r2.
Proof.
  unfold nextinstr; intros. rewrite EXT. apply set_reg_ext. auto.
Qed.

Lemma core_step_ext:
  forall {MT ME} (mi: mem_interface MT _ _ ME) rs1 m1 t rs2 m2 rs1' ,
    core_step mi A (StateC rs1 m1) t (StateC rs2 m2) ->
    (forall r, rs1 r = rs1' r) ->
    exists rs2' ,
      core_step mi A (StateC rs1' m1) t (StateC rs2' m2) /\
        (forall r, rs2 r = rs2' r).
Proof.
  intros MT ME mi rs1 m1 t rs2 m2 rs1' CS EXT.
  inv CS;
    (eapply (get_instr_ext) in GI; [| eassumption ]).
  - eexists; split. eapply core_step_arith; eauto. rewrite EXT; auto.
    intros. rewrite !EXT; eauto using set_reg_ext, nextinstr_ext.
  - eexists; split. eapply core_step_jump; eauto. rewrite ! EXT; auto.
    intros. rewrite !EXT; eauto using set_reg_ext, nextinstr_ext.
  - eexists; split. eapply core_step_load; eauto. rewrite <- ! EXT; eauto.
    intros. eauto using set_reg_ext, nextinstr_ext.
  - eexists; split. eapply core_step_store; eauto; rewrite <- ! EXT; eauto.
    intros. eauto using set_reg_ext, nextinstr_ext.
  - eexists; split. eapply core_step_lock; eauto. rewrite <- ! EXT; eauto.
    intros. eauto using set_reg_ext, nextinstr_ext.
  - eexists; split. eapply core_step_unlock; eauto. rewrite <- ! EXT; eauto.
    intros. eauto using set_reg_ext, nextinstr_ext.
Qed.
