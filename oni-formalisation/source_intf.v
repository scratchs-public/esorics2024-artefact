From formal Require Import oni.
Require Import List.
Require Import OrderedType.
Require Import Lia.
Import ListNotations.
Require Import Bool.
Import BoolNotations.
Set Implicit Arguments.
From formal Require Import Util tactics CacheTypes syntax coresem mem.
From formal Require Import cacheless_intf.

Inductive source_event :=
| se_mem (ts: option tagset).

Definition scache := setid -> TagS.t.

Definition leak_memory_S (ts: tagset) (b: bool) : source_event :=
  se_mem (if b then None else Some ts).

Definition same_set (ts1 ts2: tagset) :=
  if eq_dec (snd ts1) (snd ts2) then true else false.

Definition tagset_in_scache (ts: tagset) (s: scache) : bool :=
  TagS.mem (fst ts) (s (snd ts)).

Definition slocked sc a :=
  let '(t,s) := addr_to_tagset a in
  tagset_in_scache (t, s) sc.

Definition add_tagset_in_scache (ts: tagset) (sc: scache) : scache :=
  let '(t,s) := ts in
  fun s' => if eq_dec s s' then
              TagS.add t (sc s)
            else sc s'.

Definition remove_tagset_in_scache (ts: tagset) (sc: scache) : scache :=
  let '(t,s) := ts in
  fun s' => if eq_dec s s' then
              TagS.remove t (sc s)
            else sc s'.

Definition source_cache_interface (L: scache) (mi: mem_input) : option (scache * source_event) :=
  match mi with
    mi_load addr =>
      let ts := addr_to_tagset addr in
      let t := leak_memory_S ts (tagset_in_scache ts L) in
      Some (L, t)
  | mi_store addr v =>
      let ts := addr_to_tagset addr in
      let t := leak_memory_S ts (tagset_in_scache ts L) in
      Some (L, t)
  | mi_lock addr =>
      let ts := addr_to_tagset addr in
      if Nat.ltb (TagS.cardinal (L (snd ts))) (nways - 1) || tagset_in_scache ts L
      then
        Some (add_tagset_in_scache ts L, se_mem (Some ts))
      else None
  | mi_unlock addr =>
      let ts := addr_to_tagset addr in
      Some (remove_tagset_in_scache ts L, se_mem (Some ts))
  end.

Definition source_mem_interface (p: process) (m: mem * scache) (mi: mem_input):
  option ((mem * scache) * mem_output * source_event) :=
  let '(M, L) := m in
  let/opt out := cacheless_mem_interface p M mi in
  let '(M', mo, t) := out in
  let/opt out := source_cache_interface L mi in
  let '(L', t) := out in
  Some ((M', L'), mo, t).

Definition source_cache (cacheless_init: input -> mem -> Prop) : mem_interface source_event _ _ (mem * scache) :=
  {|
    mi_init := fun i '(m,l) => cacheless_init i m /\ forall s, l s = TagS.empty;
    mi_interface := source_mem_interface
  |}.



Lemma tag_mem_add:
  forall t0 t r,
    TagS.mem t0 (TagS.add t r) = TagS.mem t0 r || beq_dec t t0.
Proof.
  intros.
  destruct (TagS.mem t0 (TagS.add t r)) eqn:?.
  apply TagS.mem_2 in Heqb.
  unfold beq_dec.
  destruct (eq_dec t t0).
  rewrite orb_true_r; auto.
  eapply TagS.add_3 in Heqb.
  apply TagS.mem_1 in Heqb. rewrite Heqb; auto. auto.
  unfold beq_dec.
  destruct (eq_dec t t0).
  - subst.
    generalize (TagS.add_1 r (@eq_refl _ t0)). intro IN. apply TagS.mem_1 in IN. congruence.
  -
    cut (TagS.mem t0 r = true -> TagS.mem t0 (TagS.add t r) = true).
    intro HYP.
    destruct (TagS.mem t0 r) eqn:?; auto. rewrite HYP in Heqb. congruence.
    auto.
    intros.
    apply TagS.mem_2 in H.
    apply TagS.mem_1.
    apply TagS.add_2. auto.
Qed.


Lemma add_tagset_in_scache_in:
  forall ts ts' rs,
    tagset_in_scache ts (add_tagset_in_scache ts' rs) =
      tagset_in_scache ts rs || beq_dec ts ts'.
Proof.
  unfold add_tagset_in_scache.
  unfold tagset_in_scache. intros.
  destruct ts'. destruct ts. simpl.
  destruct eq_dec.
  - subst. rewrite tag_mem_add. f_equal.
    unfold beq_dec.
    destruct eq_dec; destruct eq_dec; try congruence.
  - unfold beq_dec. destruct eq_dec; try congruence.
    rewrite orb_false_r. auto.
Qed.


Lemma remove_tagset_spec t s ts rs:
  tagset_in_scache (t, s) (remove_tagset_in_scache ts rs) = true <->
    tagset_in_scache (t, s) rs = true /\ (t, s) <> ts.
Proof.
  unfold tagset_in_scache, remove_tagset_in_scache. simpl.
  destruct ts.
  destruct eq_dec.
  - subst.
    split.
    + intros MR.
      apply TagS.mem_2 in MR. split. apply TagS.mem_1.
      eapply TagS.remove_3; eauto.
      intro EQ. inv EQ.
      apply TagS.remove_1 in MR. auto. auto.
    + intros (MEM & DIFF). assert (t <> t0) by intuition congruence. clear DIFF.
      apply TagS.mem_2 in MEM. apply TagS.mem_1.
      apply TagS.remove_2. auto. auto.
  - intuition congruence.
Qed.


Definition equiv_scache (L1 L2: scache) :=
  forall ts,
    tagset_in_scache ts L1 = tagset_in_scache ts L2.

Lemma get_instr_core_equiv_source:
  forall init_mem rsd1 rsd2 sm1 sm2 id sc1 sc2 i1 i2 t1 t2 m3 m4,
    rsd1 PC = rsd2 PC ->
    mem_equiv sm1 sm2 ->
    equiv_scache sc1 sc2 ->
    get_instr (source_cache init_mem) id (StateC rsd1 (sm1, sc1)) i1 t1 m3 ->
    get_instr (source_cache init_mem) id (StateC rsd2 (sm2, sc2)) i2 t2 m4 ->
    i1 = i2 /\ t1 = t2 /\ mem_equiv (fst m3) (fst m4) /\ equiv_scache (snd m3) (snd m4).
Proof.
  intros init_mem rsd1 rsd2 sm1 sm2 id sc1 sc2 i1 i2 t1 t2 m3 m4 EQPC ME SCE GI1 GI2.
  inv GI1. inv GI2.
  simpl in H2, H4.
  unfold option_bind in H2, H4. rewrite EQPC in H2.
  destruct (check_process_of_addr (rsd2 PC) id) eqn:?; inv H2. inv H4.
  generalize ME. intro ME'. inv ME.
  specialize (me_same_code  _ (or_introl H7)).
  rewrite EQPC in me_same_code. rewrite me_same_code in *.
  assert (i1 = i2) by congruence. subst.
  split; auto. split; auto.
  rewrite SCE. auto.
Qed.


Definition source_mem_equiv :=
  equivAB mem_equiv equiv_scache.


Lemma source_mem_interface_equiv:
  forall M1 M2 M1' M2' mi1 mi2 ov1 ov2 tm,
    same_kind mi1 mi2 ->
    (forall addr1 v1 addr2 v2, mi1 = mi_store addr1 v1  -> mi2 = mi_store addr2 v2 -> perm addr1 = rw /\ perm addr2 = rw) ->
    source_mem_interface D M1 mi1 = Some (M1', ov1, tm)  ->
    source_mem_interface D M2 mi2 = Some (M2', ov2, tm)  ->
    source_mem_equiv M1 M2 ->
    source_mem_equiv M1' M2' /\
      ((is_load_or_store mi1 /\ slocked (snd M1) (addr_of_mi mi1) && slocked (snd M2) (addr_of_mi mi2) = true) \/ addr_to_tagset (addr_of_mi mi1) = addr_to_tagset (addr_of_mi mi2)) /\
      process_of_addr (addr_of_mi mi1) = Some D /\
      process_of_addr (addr_of_mi mi2) = Some D.
Proof.
  intros M1 M2 M1' M2' mi1 mi2 ov1 ov2 tm SK PERM SMI1 SMI2 SME.
  inv SME.
  destruct mi1, mi2; simpl in SK; try easy; simpl in *;
    unfold option_bind in SMI1, SMI2; repeat destr_in SMI1; repeat destr_in SMI2; inv SMI1; inv SMI2;
    repeat destr_in Heqo; inv Heqo; repeat destr_in Heqo0; inv Heqo0.
  - split; auto. split; auto.
    repeat destr_in H4; inv H4. unfold slocked. repeat destr. rewrite Heqb, Heqb0; auto.
    split; eauto using check_process_of_addr_impl.
    split; eauto using check_process_of_addr_impl.
  - split; auto. split; auto.
    specialize (PERM _ _ _ _ eq_refl eq_refl). destruct PERM.
    eapply mem_equiv_do_store; eauto. 
    repeat destr_in H4; inv H4. unfold slocked. repeat destr. rewrite Heqb, Heqb0; auto.
    split; eauto using check_process_of_addr_impl.
    split; eauto using check_process_of_addr_impl.
  - repeat destr_in Heqo1; inv Heqo1. repeat destr_in Heqo2; inv Heqo2. split; auto. split; auto.
    red; intros. rewrite ! add_tagset_in_scache_in.
    rewrite H0.  reflexivity.
    split; eauto using check_process_of_addr_impl.
  - split; auto. split; auto.
    red; intros. destruct (addr_to_tagset addr0). destruct ts.
    apply ZMicromega.eq_true_iff_eq.
    rewrite ! remove_tagset_spec.
    rewrite H0. tauto.
    split; eauto using check_process_of_addr_impl.
Qed.
