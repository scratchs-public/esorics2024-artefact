Require Import List Lia.
Require Import OrderedType.
Import ListNotations.
Require Import RelationClasses.
From formal Require Import tactics.

Lemma fold_left_prop:
  forall {A B: Type},
  forall (P: A -> Prop) (f: A -> B -> A)
         (MAINTAINS: forall acc x, P acc -> P (f acc x))
         l,
    (forall acc x
            (IN: In x l)
            (ATTAIN: forall acci, P (f acci x)),
        P (fold_left f l acc)) /\
      (forall acc, P acc -> P (fold_left f l acc)).
Proof.
  induction l; simpl; intros; eauto. easy.
  destruct IHl as (GET & KEEP).
  split.
  - intros ? ? [EQ|IN] ?.
    subst. apply KEEP. apply ATTAIN.
    eapply GET. apply IN. auto.
  - intros acc PA.
    apply KEEP. apply MAINTAINS. auto.
Qed.

Lemma fold_left_prop1:
  forall {A B: Type},
  forall (P: A -> Prop) (f: A -> B -> A)
         (MAINTAINS: forall acc x, P acc -> P (f acc x))
         l,
    (forall acc x
            (IN: In x l)
            (ATTAIN: forall acci, P (f acci x)),
        P (fold_left f l acc)).
Proof.
  intros. eapply fold_left_prop in MAINTAINS; auto.
  destruct MAINTAINS as (H1 & H2). eapply H1; eauto.
Qed.


Lemma fold_left_prop2:
  forall {A B: Type},
  forall (P: A -> Prop) (f: A -> B -> A)
         (MAINTAINS: forall acc x, P acc -> P (f acc x))
         l,
    (forall acc, P acc -> P (fold_left f l acc)).
Proof.
  intros. eapply fold_left_prop in MAINTAINS; auto.
  destruct MAINTAINS as (H1 & H2). eapply H2; eauto.
Qed.



Lemma fold_left_prop_inv:
  forall {A B: Type},
  forall (P: A -> Prop) (Pdec: forall a, {P a} + {~ P a}) (f: A -> B -> A)
         l acc,
    P (fold_left f l acc) ->
    P acc \/ exists x acc, In x l /\ ~P acc /\ P (f acc x).
Proof.
  induction l; simpl; intros; eauto.
  specialize (IHl _ H).
  destruct IHl as [Pa | (x & acc0 & EQIN & NPA & PF)].
  - destruct (Pdec acc). auto.
    right. exists a, acc. split; auto.
  - right; do 2 eexists; eauto.
Qed.

Lemma fold_left_maintains:
  forall {A B: Type} (f: A -> B -> A) P (Pdec: forall x, {P x} + {~P x}) (Q: _ -> Prop)
         (Qinvalidates: forall acc x, P acc -> Q x -> ~ P (f acc x))
         (NPstays: forall acc x, ~ P acc -> ~ P (f acc x)) l acc,
    P (fold_left f l acc) ->
    P acc /\ (~ exists x, In x l /\ Q x).
Proof.
  induction l; simpl; intros; eauto. split; auto. intros (x & [] & _).
  apply IHl in H.
  destruct H as (Pfacca & NE).
  destruct (Pdec acc). split; auto. intros (x & IN & Qx).
  destruct IN as [EQ | IN]. subst. eapply Qinvalidates. apply p. apply Qx. apply Pfacca.
  apply NE. eauto.
  eapply NPstays in n. apply n in Pfacca. easy.
Qed.

Lemma fold_left_ext: forall {A B: Type} (f1 f2: A -> B -> A) (EXT: forall a b, f1 a b = f2 a b)
                            l acc,
    fold_left f1 l acc = fold_left f2 l acc.
Proof.
  induction l; simpl; intros; eauto. rewrite EXT. apply IHl.
Qed.

Fixpoint all_nats n: list nat :=
  match n with
    O => nil
  | S n => n :: all_nats n
  end.
Lemma all_nats_lt:
  forall n x, In x (all_nats n) -> x < n.
Proof.
  induction n; simpl; intros; eauto. easy. destruct H. lia. eapply IHn in H. lia.
Qed.

Lemma all_nats_lt_inv:
  forall n x, x < n -> In x (all_nats n).
Proof.
  induction n; simpl; intros; eauto. easy.
  destruct (PeanoNat.Nat.eq_dec x n). auto. right; apply IHn. lia.
Qed.

Fixpoint all_Plist {A B} P (f: forall a: A, P a -> B)
  (l: list A) (Pl: forall x, In x l -> P x)
  {struct l}: list B.
Proof.
  destruct l. apply nil.
  refine (f a _ :: (all_Plist _ _ P f l _)).
  apply Pl. left; reflexivity.
  intros; apply Pl. right; auto.
Defined.

Definition all_fins n: list (Fin.t n).
Proof.
  apply all_Plist with (P:=fun x => x < n) (l:=all_nats n).
  intros. eapply Fin.of_nat_lt. apply H.
  apply all_nats_lt.
Defined.

Lemma all_plist_spec:
  forall {A B} (P: A -> Prop) (f: forall a: A, P a -> B) (Fextpf: forall a p1 p2, f a p1 = f a p2) l (Pl: forall x, In x l -> P x) x,
    In x (all_Plist P f l Pl) <-> exists y pf, In y l /\ f y pf = x.
Proof.
  induction l; simpl; intros; eauto.
  split. intros []. intros (? & ? & [] & ?).
  rewrite IHl.
  clear IHl.
  split.
  - intros [EQ | (y & pf & IN & EQ)].
    subst. eauto. eauto.
  - intros (y & pf & IN & EQ). subst.
    destruct IN as [EQ|IN]; subst. left. apply Fextpf.
    right; eauto.
    Unshelve. apply Pl. auto. apply pf.
Qed.

Lemma all_fins_spec:
  forall n x,
    In x (all_fins n) .
Proof.
  intros; unfold all_fins. erewrite all_plist_spec.
  destruct (Fin.to_nat x) eqn:?.
  exists x0. exists l. split. apply all_nats_lt_inv. auto.
  generalize (Fin.of_nat_to_nat_inv x). rewrite Heqs. simpl. auto.
  intros; apply Fin.of_nat_ext.
Qed.


Inductive option_rel {A: Type} (R: A -> A -> Prop) : option A -> option A -> Prop :=
| option_rel_nil: option_rel R None None
| option_rel_cons:
  forall a b, R a b -> option_rel R (Some a) (Some b).


Lemma option_rel_sym {A: Type} (R: A -> A -> Prop):
  Symmetric R -> Symmetric (option_rel R).
Proof.
  red; intros. inv H0. constructor.
  econstructor; eauto.
Qed.


Instance option_eq {A: Type} `{aeq: EqDec A}: EqDec (option A).
Proof.
  constructor. decide equality. apply aeq.
Defined.


Definition option_bind {A B: Type} (f: A -> option B) (elt: option A) : option B :=
  match elt with
  | None => None
  | Some a => f a
  end.

Notation "'let/opt' var ':=' expr 'in' body" :=
  (option_bind (fun var => body) expr) (at level 200).

Require Import Classical.
Lemma NNPP_iff : forall (A:Prop), (~~ A) <-> A.
Proof. intros. tauto. Qed.

Lemma not_exists_forall_not : forall (U:Type) (P: U -> Prop), ~ (exists n, P n) <-> forall n, ~ P n.
Proof.
  firstorder.
Qed.


Lemma list_in_shorter:
  forall {A: Type} (l l0: list A), NoDup l -> (forall x, In x l -> In x l0) -> length l <= length l0.
Proof.
  intros. apply NoDup_incl_length. auto. red; auto.
Qed.

Lemma NoDupA_NoDup:
  forall {A:Type} (l: list A),
    NoDupA eq l -> NoDup l.
Proof.
  induction 1; simpl; intros; eauto.
  constructor. constructor; auto.
  rewrite InA_alt in H.
  intro IN. apply H. eauto.
Qed.
