From formal Require Import oni.
Require Import List.
Require Import OrderedType.
Require Import Lia.
Import ListNotations.
Require Import Bool.
Import BoolNotations.
Require Import Permutation.
Set Implicit Arguments.
From formal Require Import Util tactics CacheTypes syntax coresem mem target_intf.
Require Import FunctionalExtensionality.
Open Scope nat_scope.

Definition equiv_cline (s: setid) (cl1 cl2: cline) : Prop :=
  cl_locked cl1 = cl_locked cl2
  /\ cl_tag cl1 = cl_tag cl2
  /\ cl_dirty cl1 = cl_dirty cl2
  /\ (perm (tagset_to_addr (cl_tag cl1, s)) = rx \/ process_of_tag (cl_tag cl1) = Some A ->
      cl_content cl1 = cl_content cl2).

Definition equiv_line s (cl1 cl2: cacheline) : Prop :=
  option_rel (equiv_cline s) cl1 cl2.

Definition equiv_ways s (w1 w2: ways) : Prop :=
  forall l, equiv_line s (w1 l) (w2 l).

Definition equiv_set s (w1 w2: cache_usage * sig wf_ways) : Prop :=
  fst w1 = fst w2 /\ equiv_ways s (proj1_sig (snd w1)) (proj1_sig (snd w2)).

Definition equiv_tcache (t1 t2: tcache) :=
  forall s, equiv_set s (t1 s) (t2 s).

Instance equiv_cline_eq: forall s, Equivalence (equiv_cline s).
Proof.
  constructor. red; red; auto.
  red. unfold equiv_cline. destruct x, y; simpl; intuition subst; intuition eauto.
  red. unfold equiv_cline. destruct x, y, z; simpl; intuition subst; eauto.
Qed.

Instance option_rel_eq: forall {T: Type} (R: T -> T -> Prop) `{Req: Equivalence _ R}, Equivalence (option_rel R).
Proof.
  intros.
  constructor.
  - red; intros. destruct x; constructor. reflexivity.
  - red; intros. inv H; constructor. symmetry; auto.
  - red; intros. inv H; inv H0; constructor. etransitivity; eauto.
Qed.

Instance equiv_line_eq: forall s, Equivalence (equiv_line s).
Proof.
  intros. apply option_rel_eq.
Qed.


Instance equiv_ways_eq: forall s, Equivalence (equiv_ways s).
Proof.
  intros. constructor; red; red; intros.
  reflexivity.
  symmetry; auto.
  etransitivity; eauto.
Qed.


Instance equiv_set_eq: forall s, Equivalence (equiv_set s).
Proof.
  intros. constructor; red; red; intros.
  split; reflexivity.
  destruct H; split; symmetry; auto.
  destruct H, H0; split; etransitivity; eauto.
Qed.

Instance equiv_tcache_eq: Equivalence equiv_tcache.
Proof.
  intros. constructor; red; red; intros.
  reflexivity.
  symmetry; auto.
  etransitivity; eauto.
Qed.


Axiom update_usage_equiv2:
  forall u a1 a2 b1 b2 ce1 ce2 w1 w2 s (EQ: equiv_ways s w1 w2),
    addr_to_tagset a1 = addr_to_tagset a2 ->
    same_kind ce1 ce2 ->
    update_usage w2 u a1 b1 b2 ce1 = update_usage w1 u a2 b1 b2 ce2.

Lemma update_usage_equiv:
  forall u a b1 b2 ce w1 w2 s (EQ: equiv_ways s w1 w2),
    update_usage w2 u a b1 b2 ce = update_usage w1 u a b1 b2 ce.
Proof.
  intros. eapply update_usage_equiv2; eauto.
  destruct ce; simpl; auto.
Qed.

Axiom line_to_evict_equiv:
  forall u w1 w2 s (EQ: equiv_ways s w1 w2),
    line_to_evict w2 u = line_to_evict w1 u.

Lemma locked_tags_equiv:
  forall rt1 rt2 s,
    equiv_tcache rt1 rt2 ->
    locked_tags rt1 s = locked_tags rt2 s.
Proof.
  unfold locked_tags.
  intros. destr. destr. apply fold_left_ext.
  intros. specialize (H s). red in H. destruct H.
  rewrite Heqp, Heqp0 in H0. simpl in H0. red in H0. red in H0.
  destruct s0, s1.
  specialize (H0 b). simpl in *.
  inv H0. auto. red in H3. destruct_and H3.
  rewrite H0, H3. destr; auto.
Qed.

Lemma line_of_tag_equiv_ways:
  forall t s w1 w2,
    equiv_ways s w1 w2 ->
    line_of_tag w1 t = line_of_tag w2 t.
Proof.
  unfold equiv_ways. intros.
  unfold line_of_tag.
  apply fold_left_ext. intros.
  destr. auto. red in H.
  specialize (H b).
  inv H. auto. red in H2. destruct_and H2.
  rewrite H2. destr; auto.
Qed.


Lemma tcache_line_of_addr_equiv2:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a1 a2 (EQ: addr_to_tagset a1 =addr_to_tagset a2) ,
      tcache_line_of_addr c1 a1 = tcache_line_of_addr c2 a2.
Proof.
  unfold tcache_line_of_addr. intros. rewrite EQ. destr.
  specialize (H s). destr. destr.
  eapply line_of_tag_equiv_ways; eauto. apply H.
Qed.

Lemma tcache_line_of_addr_equiv:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a ,
      tcache_line_of_addr c1 a = tcache_line_of_addr c2 a.
Proof.
  intros; apply tcache_line_of_addr_equiv2; auto.
Qed.

Lemma tcache_line_of_addr'_equiv2:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a1 a2 (EQ: addr_to_tagset a1 = addr_to_tagset a2) l,
      tcache_line_of_addr' c1 a1 = inr l ->
      tcache_line_of_addr' c2 a2 = inr l.
Proof.
  unfold tcache_line_of_addr'. intros. destr_in H0; inv H0.
  rewrite <- (tcache_line_of_addr_equiv2 H EQ). intros.
  rewrite Heqo. auto.
Qed.

Lemma tcache_line_of_addr'_equiv:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a l,
      tcache_line_of_addr' c1 a = inr l ->
      tcache_line_of_addr' c2 a = inr l.
Proof.
  intros; eapply tcache_line_of_addr'_equiv2; eauto.
Qed.

Lemma tcache_line_of_addr'_equiv_none2:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a1 a2 (EQ: addr_to_tagset a1 = addr_to_tagset a2) l,
      tcache_line_of_addr' c1 a1 = inl l ->
      exists l', tcache_line_of_addr' c2 a2 = inl l'.
Proof.
  unfold tcache_line_of_addr'. intros.
  generalize (tcache_line_of_addr_equiv2 H EQ). intros A; rewrite <- A.
  destr_in H0; inv H0. eauto.
Qed.

Lemma tcache_line_of_addr'_equiv_none:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a l,
      tcache_line_of_addr' c1 a = inl l ->
      exists l', tcache_line_of_addr' c2 a = inl l'.
Proof.
  intros; eapply tcache_line_of_addr'_equiv_none2; eauto.
Qed.

Lemma tcache_line_to_evict_equiv2:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a1 a2 (EQ: addr_to_tagset a1 = addr_to_tagset a2),
      tcache_line_to_evict c1 a1 = tcache_line_to_evict c2 a2.
Proof.
  unfold tcache_line_to_evict. intros. destr.
  specialize (H s). rewrite <- EQ. do 2 destr. destruct H; simpl in *; subst.
  symmetry; eapply line_to_evict_equiv. eauto.
Qed.

Lemma tcache_line_to_evict_equiv:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a,
      tcache_line_to_evict c1 a = tcache_line_to_evict c2 a.
Proof.
  intros; apply tcache_line_to_evict_equiv2; auto.
Qed.

Lemma address_of_evicted_line_equiv2:
  forall c1 c2 l a1 a2 (EQ: addr_to_tagset a1 = addr_to_tagset a2),
    equiv_tcache c1 c2 ->
    address_of_evicted_line c1 l a1 = address_of_evicted_line c2 l a2.
Proof.
  unfold address_of_evicted_line. intros.
  destr. rewrite <- EQ. specialize (H s). destruct (c1 s) eqn:?, (c2 s) eqn:?; destruct H; simpl in *. subst.
  destruct s0, s1; simpl in *. specialize (H0 l). inv H0. auto.
  red in H2. destruct_and H2. congruence.
Qed.

Lemma address_of_evicted_line_equiv:
  forall c1 c2 l a,
    equiv_tcache c1 c2 ->
    address_of_evicted_line c1 l a = address_of_evicted_line c2 l a.
Proof.
  intros; apply address_of_evicted_line_equiv2; auto.
Qed.
Lemma update_tcache_equiv:
  forall rt1 rt2 s uw1 uw2,
    equiv_tcache rt1 rt2 ->
    equiv_set s uw1 uw2 ->
    equiv_tcache (update_tcache rt1 s uw1) (update_tcache rt2 s uw2).
Proof.
  intros.
  unfold update_tcache.
  red. intros.
  destruct eq_dec; subst; auto.
Qed.

Lemma tcache_update_usage_equiv2:
  forall c1 c2 a1 a2 mi1 mi2,
    equiv_tcache c1 c2 ->
    addr_to_tagset a1 = addr_to_tagset a2 ->
    same_kind mi1 mi2 ->
    equiv_tcache (tcache_update_usage c1 a1 mi1) (tcache_update_usage c2 a2 mi2).
Proof.
  intros c1 c2 a1 a2 mi1 mi2 CEQ EQ SK.
  unfold tcache_update_usage.
  destr. rewrite <- EQ.
  generalize (CEQ s). destruct (c1 s) eqn:?, (c2 s) eqn:?. intros (A & B); simpl in *. subst.
  destruct s0, s1; simpl in *.
  rewrite (line_of_tag_equiv_ways t B).
  destr. destr.
  destr_in Heqp1.
  - generalize (B l). intro C; inv C. setoid_rewrite <- H in Heqp2. rewrite <- H0 in Heqp1. inv Heqp1. inv Heqp2.
    apply update_tcache_equiv; auto. red; simpl.
    split; auto.
    symmetry; eapply update_usage_equiv2; eauto. congruence. destruct mi1, mi2; simpl in SK; auto.
    setoid_rewrite <- H in Heqp1. rewrite <- H0 in Heqp2. inv Heqp1. inv Heqp2.
    apply update_tcache_equiv; auto. red; simpl.
    split; auto.
    red in H1. intuition. rewrite H2.
    symmetry; eapply update_usage_equiv2; eauto. congruence. destruct mi1, mi2; simpl in SK; auto.
  - inv Heqp1; inv Heqp2.
    apply update_tcache_equiv; auto. red; simpl.
    split; auto.
    symmetry; eapply update_usage_equiv2; eauto. congruence. destruct mi1, mi2; simpl in SK; auto.
Qed.

Lemma tcache_update_usage_equiv:
  forall c1 c2 a mi,
    equiv_tcache c1 c2 ->
    equiv_tcache (tcache_update_usage c1 a mi) (tcache_update_usage c2 a mi).
Proof.
  intros.
  apply tcache_update_usage_equiv2; auto. destruct mi; simpl; auto.
Qed.
Lemma update_ways_equiv:
  forall s w1 w2 l cl1 cl2,
    equiv_ways s w1 w2 ->
    equiv_line s cl1 cl2 ->
    equiv_ways s (update_ways w1 l cl1) (update_ways w2 l cl2).
Proof.
  unfold equiv_ways, update_ways.
  intros. destr; auto.
Qed.

Lemma fetch_equiv2:
  forall c1 c2 m1 m2 l a1 a2 pf1 pf2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    addr_to_tagset a1 = addr_to_tagset a2 ->
    equiv_tcache (fetch m1 c1 a1 l pf1) (fetch m2 c2 a2 l pf2).
Proof.
  intros.
  generalize (fetch_unfold m1 c1 a1 l pf1). destr. destr. simpl. intros (p1 & EQ1).
  generalize (fetch_unfold m2 c2 a2 l pf2). rewrite <- H1.
  generalize (H s). rewrite Heqp. destr. simpl. intros (? & EQl). simpl in *. subst.
  intros (p2 & EQ2).
  rewrite EQ1, EQ2.
  apply update_tcache_equiv. auto. split; auto. simpl.
  apply update_ways_equiv. auto.
  red; simpl. 
  constructor. split; simpl; auto.
  split; auto. split; auto.
  clear EQ1 EQ2 p1 p2.
  rewrite <- Heqt.
  rewrite perm_tagset_to_addr_to_tagset.
  intros.
  erewrite <- (get_line_tagset_to_addr_to_tagset m2 a2). rewrite <- H1, <- Heqt.
  rewrite get_line_tagset_to_addr_to_tagset.
  apply get_line_equiv. auto. unfold process_of_addr. rewrite Heqt. simpl. auto.
Qed.

Lemma fetch_equiv:
  forall c1 c2 m1 m2 l a pf1 pf2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    equiv_tcache (fetch m1 c1 a l pf1) (fetch m2 c2 a l pf2).
Proof.
  intros. eapply fetch_equiv2; eauto.
Qed.

Lemma write_in_cache_equiv2:
  forall c1 c2 a1 a2 v1 v2 l,
    equiv_tcache c1 c2 ->
    addr_to_tagset a1 = addr_to_tagset a2 ->
    perm a1 = rw -> process_of_addr a1 = Some D ->
    tcache_line_of_addr c1 a1 = Some l ->
    equiv_tcache (write_in_cache c1 a1 v1 l) (write_in_cache c2 a2 v2 l).
Proof.
  intros c1 c2 a1 a2 v1 v2 l CEQ ATTeq PERM POA TLOA.
  destruct (addr_to_tagset a1) eqn:?.
  generalize (CEQ s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  specialize (EQ l). red in EQ.
  generalize (write_in_cache_unfold c1 a1 l v1). rewrite Heqt, Heqp.
  generalize (write_in_cache_unfold c2 a2 l v2). rewrite <- ATTeq, Heqp0.
  inv EQ.
  - setoid_rewrite <- H. setoid_rewrite <- H0. intros A B; rewrite A, B. auto.
  - setoid_rewrite <- H. setoid_rewrite <- H0.
    intros (p1 & EQ1) (p2 & EQ2).
    rewrite EQ1, EQ2. clear EQ1 EQ2.
    apply update_tcache_equiv. auto. split; auto. simpl.
    apply update_ways_equiv.
    specialize (CEQ s). rewrite Heqp, Heqp0 in CEQ. destruct CEQ; simpl in *; auto.
    red. constructor. unfold equiv_cline. simpl. red in H1.  destruct_and H1.
    split; auto. split; auto.  split; auto.
    intros. rewrite H5; auto.
    unfold tcache_line_of_addr in TLOA. rewrite Heqt, Heqp in TLOA.
    edestruct line_of_tag_some as (? & EQ & TAG). apply TLOA. setoid_rewrite EQ in H; inv H.
    unfold process_of_addr in POA. rewrite Heqt in POA. simpl in POA.
    destruct H4. 2: congruence.
    rewrite <- Heqt in H. rewrite perm_tagset_to_addr_to_tagset in H. congruence.
Qed.


Lemma write_in_cache_wt_equiv2:
  forall c1 c2 a1 a2 v1 v2 l,
    equiv_tcache c1 c2 ->
    addr_to_tagset a1 = addr_to_tagset a2 ->
    perm a1 = rw -> process_of_addr a1 = Some D ->
    tcache_line_of_addr c1 a1 = Some l ->
    equiv_tcache (write_in_cache_wt c1 a1 v1 l) (write_in_cache_wt c2 a2 v2 l).
Proof.
  intros c1 c2 a1 a2 v1 v2 l CEQ ATTeq PERM POA TLOA.
  destruct (addr_to_tagset a1) eqn:?.
  generalize (CEQ s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  specialize (EQ l). red in EQ.
  generalize (write_in_cache_wt_unfold c1 a1 l v1). rewrite Heqt, Heqp.
  generalize (write_in_cache_wt_unfold c2 a2 l v2). rewrite <- ATTeq, Heqp0.
  inv EQ.
  - setoid_rewrite <- H. setoid_rewrite <- H0. intros A B; rewrite A, B. auto.
  - setoid_rewrite <- H. setoid_rewrite <- H0.
    intros (p1 & EQ1) (p2 & EQ2).
    rewrite EQ1, EQ2. clear EQ1 EQ2.
    apply update_tcache_equiv. auto. split; auto. simpl.
    apply update_ways_equiv.
    specialize (CEQ s). rewrite Heqp, Heqp0 in CEQ. destruct CEQ; simpl in *; auto.
    red. constructor. unfold equiv_cline. simpl. red in H1.  destruct_and H1.
    split; auto. split; auto.  split; auto.
    intros. rewrite H5; auto.
    unfold tcache_line_of_addr in TLOA. rewrite Heqt, Heqp in TLOA.
    edestruct line_of_tag_some as (? & EQ & TAG). apply TLOA. setoid_rewrite EQ in H; inv H.
    unfold process_of_addr in POA. rewrite Heqt in POA. simpl in POA.
    destruct H4. 2: congruence.
    rewrite <- Heqt in H. rewrite perm_tagset_to_addr_to_tagset in H. congruence.
Qed.

Lemma write_in_cache_equiv:
  forall c1 c2 a v l,
    equiv_tcache c1 c2 ->
    equiv_tcache (write_in_cache c1 a v l) (write_in_cache c2 a v l).
Proof.
  intros.
  destruct (addr_to_tagset a) eqn:?.
  generalize (H s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  specialize (EQ l). red in EQ.
  generalize (write_in_cache_unfold c1 a l v). rewrite Heqt, Heqp.
  generalize (write_in_cache_unfold c2 a l v). rewrite Heqt, Heqp0.
  inv EQ.
  - setoid_rewrite <- H1. setoid_rewrite <- H2. intuition. rewrite H0, H3. auto.
  - setoid_rewrite <- H1. setoid_rewrite <- H0.
    intros (p1 & EQ1) (p2 & EQ2).
    rewrite EQ1, EQ2.
    apply update_tcache_equiv. auto. split; auto. simpl.
    apply update_ways_equiv.
    specialize (H s). rewrite Heqp, Heqp0 in H. destruct H; simpl in *; auto.
    red. constructor. unfold equiv_cline. simpl. red in H2.  destruct_and H2.
    split; auto. split; auto.  split; auto.  intros.
    rewrite H6. auto. auto.
Qed.

Lemma write_in_cache_wt_equiv:
  forall c1 c2 a v l,
    equiv_tcache c1 c2 ->
    equiv_tcache (write_in_cache_wt c1 a v l) (write_in_cache_wt c2 a v l).
Proof.
  intros.
  destruct (addr_to_tagset a) eqn:?.
  generalize (H s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  specialize (EQ l). red in EQ.
  generalize (write_in_cache_wt_unfold c1 a l v). rewrite Heqt, Heqp.
  generalize (write_in_cache_wt_unfold c2 a l v). rewrite Heqt, Heqp0.
  inv EQ.
  - setoid_rewrite <- H1. setoid_rewrite <- H2. intuition. rewrite H0, H3. auto.
  - setoid_rewrite <- H1. setoid_rewrite <- H0.
    intros (p1 & EQ1) (p2 & EQ2).
    rewrite EQ1, EQ2.
    apply update_tcache_equiv. auto. split; auto. simpl.
    apply update_ways_equiv.
    specialize (H s). rewrite Heqp, Heqp0 in H. destruct H; simpl in *; auto.
    red. constructor. unfold equiv_cline. simpl. red in H2.  destruct_and H2.
    split; auto. split; auto.  split; auto.  intros.
    rewrite H6. auto. auto.
Qed.

Lemma lock_line_equiv2:
  forall c1 c2 a1 a2 l,
    equiv_tcache c1 c2 ->
    addr_to_tagset a1 = addr_to_tagset a2 ->
    equiv_tcache (lock_line c1 a1 l) (lock_line c2 a2 l).
Proof.
  intros. unfold lock_line.
  generalize (update_cache_line_eq c1 a1 l _ lock_line_cl_preserves_tag).
  generalize (update_cache_line_eq c2 a2 l _ lock_line_cl_preserves_tag).
  destr.
  generalize (H s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. rewrite H0. rewrite Heqp. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  specialize (EQ l). red in EQ.
  destruct s0, s1; simpl in*. inv EQ.
  intros A B; rewrite A, B. auto.
  intros (p1 & A) (p2 & B); rewrite A, B.
  apply update_tcache_equiv. auto. split; auto. simpl.
  apply update_ways_equiv.
  specialize (H s). rewrite Heqp, Heqp0 in H. destruct H; simpl in *; auto.
  red. constructor. unfold equiv_cline. simpl. red in H3.  destruct_and H3. auto.
Qed.


Lemma unlock_line_equiv2:
  forall c1 c2 a1 a2 l,
    equiv_tcache c1 c2 ->
    addr_to_tagset a1 = addr_to_tagset a2 ->
    equiv_tcache (unlock_line c1 a1 l) (unlock_line c2 a2 l).
Proof.
  intros. unfold unlock_line.
  generalize (update_cache_line_eq c1 a1 l _ unlock_line_cl_preserves_tag).
  generalize (update_cache_line_eq c2 a2 l _ unlock_line_cl_preserves_tag).
  destr.
  generalize (H s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. rewrite H0. rewrite Heqp. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  specialize (EQ l). red in EQ.
  destruct s0, s1; simpl in*. inv EQ.
  intros A B; rewrite A, B. auto.
  intros (p1 & A) (p2 & B); rewrite A, B.
  apply update_tcache_equiv. auto. split; auto. simpl.
  apply update_ways_equiv.
  specialize (H s). rewrite Heqp, Heqp0 in H. destruct H; simpl in *; auto.
  red. constructor. unfold equiv_cline. simpl. red in H3.  destruct_and H3. auto.
Qed.

Lemma lock_line_equiv:
  forall c1 c2 a l,
    equiv_tcache c1 c2 ->
    equiv_tcache (lock_line c1 a l) (lock_line c2 a l).
Proof.
  intros. eapply lock_line_equiv2; eauto.
Qed.

Lemma unlock_line_equiv:
  forall c1 c2 a l,
    equiv_tcache c1 c2 ->
    equiv_tcache (unlock_line c1 a l) (unlock_line c2 a l).
Proof.
  intros. eapply unlock_line_equiv2; eauto.
Qed.

Lemma write_back_equiv:
  forall c1 c2 m1 m2 l a m1' m2' t1 t2 a0,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    write_back m1 c1 l a = (m1', t1) ->
    write_back m2 c2 l a = (m2', t2) ->
    option_map addr_to_tagset a = option_map addr_to_tagset (address_of_evicted_line c1 l a0) ->
    mem_equiv m1' m2' /\ t1 = t2.
Proof.
  unfold write_back. intros c1 c2 m1 m2 l a m1' m2' t1 t2 a0 H H0 H1 H2 H3.
  destr_in H1. 2: inv H1; inv H2; eauto.
  destr_in H1.
  specialize (H s). destr_in H1. destr_in H2. destruct H; simpl in *; subst.
  repeat destr_in H3; inv H3.
  specialize (H4 l).
  inv H4.
  - setoid_rewrite <- H in H2. setoid_rewrite <- H3 in H1. inv H1. inv H2. split; auto.
  - setoid_rewrite <- H in H1. setoid_rewrite <- H3 in H2.
    red in H6. destruct_and H6. rewrite H7 in *.
    destr_in H1; inv H1; inv H2; split; auto.
    constructor. intros.
    rewrite ! set_line_get. destr; auto.
    clear Heqs2. rewrite Heqt in e. symmetry in e.
    unfold process_of_addr in H1. rewrite e in H1. simpl in H1.
    unfold address_of_evicted_line in H5. repeat destr_in H5; inv H5.
    rewrite addr_to_tagset_to_addr in H8. inv H8.
    rewrite Heqt in H5; inv H5. rewrite H9. auto.
    assert ( s3 = s0) by congruence. subst.
    assert (c3 = a).
    setoid_rewrite Heqc3 in H; inv H. auto. subst.
    rewrite <- e.
    rewrite perm_tagset_to_addr_to_tagset. auto.
    inv H0. apply me_same_code. auto.
    apply cl_content_length.
    apply cl_content_length.
Qed.

Lemma maybe_writeback_fetch_equiv2:
  forall addr1 addr2 m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 l1 l2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    addr_to_tagset addr1 = addr_to_tagset addr2 ->
    maybe_writeback_fetch m1 c1 addr1 = (m1', c1', l1, t1) ->
    maybe_writeback_fetch m2 c2 addr2 = (m2', c2', l2, t2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\
      l1 = l2 /\
      t1 = t2.
Proof.
  intros addr1 addr2 m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 l1 l2 CEQ MEQ ATTeq L1 L2.
  unfold maybe_writeback_fetch in L1, L2.
  destr_in L1.
  - edestruct tcache_line_of_addr'_equiv_none2 as (pf & EQ). eauto. eauto. eauto.
    rewrite EQ in L2.
    destr_in L1; inv L1. destr_in L2; inv L2.
    erewrite <- tcache_line_to_evict_equiv2 in Heqp0; eauto.
    erewrite <- address_of_evicted_line_equiv2 in Heqp0; eauto.
    generalize (write_back_equiv (tcache_line_to_evict c1 addr1) _ _ CEQ MEQ Heqp Heqp0 eq_refl).
    intros (MEQ2 & ->). split; auto.
    erewrite tcache_line_to_evict_equiv2; eauto.
    apply fetch_equiv2; auto.
    split; auto. split; auto.
    erewrite <- (tcache_line_to_evict_equiv2 CEQ); eauto.
    rewrite ATTeq; auto.
  - inv L1.
    erewrite tcache_line_of_addr'_equiv2 in L2; eauto. inv L2. auto.
Qed.

Lemma maybe_writeback_fetch_equiv:
  forall addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 l1 l2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    maybe_writeback_fetch m1 c1 addr = (m1', c1', l1, t1) ->
    maybe_writeback_fetch m2 c2 addr = (m2', c2', l2, t2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\
      l1 = l2 /\
      t1 = t2.
Proof.
  intros.
  eapply maybe_writeback_fetch_equiv2; eauto.
Qed.

Lemma num_lockable_in_set_equiv:
  forall c1 c2 a,
    equiv_tcache c1 c2 ->
    num_lockable_in_set c1 a = num_lockable_in_set c2 a.
Proof.
  unfold num_lockable_in_set. intros.
  destr.
  rewrite (locked_tags_equiv s H). reflexivity.
Qed.


Lemma tcache_is_locked_equiv:
  forall c1 c2 a,
    equiv_tcache c1 c2 ->
    tcache_is_locked c1 a = tcache_is_locked c2 a.
Proof.
  unfold tcache_is_locked. intros.
  destr.
  specialize (H s).
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct H as (EQ0 & EQ); simpl in *; subst.
  destruct s0, s1; simpl in *.
  rewrite <- (line_of_tag_equiv_ways t EQ).
  destr; auto. specialize (EQ l). inv EQ. auto.
  red in H1. intuition.
Qed.

Definition target_mem_equiv :=
  equivAB mem_equiv equiv_tcache.

Instance equivAB_eq:
  forall {A B: Type} (RA: A -> A -> Prop) (RB: B -> B -> Prop)
         `{eqra: Equivalence _ RA} `{eqrb: Equivalence _ RB},
    Equivalence (equivAB RA RB).
Proof.
  intros A B RA RB H H0.
  constructor; red.
  - intros (a, b); econstructor; reflexivity.
  - intros (a,b) (c,d) EQ; inv EQ; constructor; symmetry; eauto.
  - intros (a,b) (c,d) (e,f) EQ1 EQ2; inv EQ1; inv EQ2; econstructor; etransitivity; eauto.
Qed.

Instance target_mem_equiv_eq: Equivalence target_mem_equiv.
Proof.
  typeclasses eauto.
Qed.

Lemma maybe_writeback_fetch_equiv_cube_not_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: maybe_writeback_fetch m1 r1 addr1 = (m3, r3, t1, v1))
         (L1: maybe_writeback_fetch m2 r2 addr2 = (m4, r4, t2, v2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (TL1 : addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2 /\ v1 = v2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME TL1.
  inv TME. unfold maybe_writeback_fetch in *.
  destr_in L1.
  - generalize (tcache_line_of_addr'_equiv_none2 H4 TL1 Heqs).
    intros (l' & EQ). rewrite EQ in L2. destr_in L1. inv L1. destr_in L2; inv L2.
    rewrite <- (tcache_line_to_evict_equiv2 H4 TL1) in Heqp0.
    rewrite <- (address_of_evicted_line_equiv2 _ TL1 H4) in Heqp0.
    generalize (write_back_equiv _ _ _ H4 H2 Heqp Heqp0 eq_refl). intros (MEQ & ->).
    rewrite <- (tcache_line_to_evict_equiv2 H4 TL1).
    split; auto.
    split; auto.
    apply fetch_equiv2; auto.
    split; auto. congruence.
  - rewrite (tcache_line_of_addr'_equiv2 H4 TL1 Heqs) in L2. inv L1. inv L2.
    split; auto. split; auto.
Qed.

Lemma load_wb_equiv_cube_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: load_wb m1 r1 addr1 = (m3, r3, t1, v1))
         (L2 : load_wb m2 r2 addr2 = (m4, r4, t2, v2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (TL1 : tagset_locked (addr_to_tagset addr1) r1)
         (TL2 : tagset_locked (addr_to_tagset addr2) r2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME TL1 TL2.
  inv TME.
  unfold load_wb in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  unfold maybe_writeback_fetch in *.
  destr_in Heqp.
  clear - H4 TL1 e. exfalso.
  unfold tcache_line_of_addr in e. destr_in e. destr_in e.
  red in TL1. rewrite Heqp in TL1. red in TL1.
  destruct TL1 as (l & cl & EQ & LOCKED & TAG).
  eapply line_of_tag_none in EQ; eauto.

  destr_in Heqp0.
  clear - H4 TL2 e. exfalso.
  unfold tcache_line_of_addr in e. destr_in e. destr_in e.
  red in TL2. rewrite Heqp in TL2. red in TL2.
  destruct TL2 as (l & cl & EQ & LOCKED & TAG).
  eapply line_of_tag_none in EQ; eauto.

  inv Heqp. inv Heqp0. split; auto.
  split; auto.
  unfold tcache_update_usage. repeat destr.
  unfold tcache_line_of_addr' in Heqs, Heqs0.
  destr_in Heqs; inv Heqs.
  destr_in Heqs0; inv Heqs0.
  unfold tcache_line_of_addr in Heqo, Heqo0.
  rewrite Heqt1, Heqp in Heqo.
  rewrite Heqt2, Heqp1 in Heqo0.
  rewrite Heqo in Heqp0. rewrite Heqo0 in Heqp2.
  destruct (line_of_tag_some _ _ Heqo) as (? & EQ1 & TAG1).
  destruct (line_of_tag_some _ _ Heqo0) as (? & EQ2 & TAG2).
  rewrite EQ1 in Heqp0. rewrite EQ2 in Heqp2. inv Heqp0; inv Heqp2.
  rewrite ! update_usage_locked_load_store; auto.
  unfold update_tcache.
  generalize (H4 s), (H4 s1). intros (B, C) (D, E). rewrite Heqp in *. rewrite Heqp1 in *. simpl in *. subst.
  red; intros. red.
  repeat destr; subst; simpl; auto. rewrite Heqp in E.  simpl in *.  split; auto. rewrite Heqp1; auto.
  generalize (H4 s3). intros (F, G). auto.
Qed.

Lemma load_wb_equiv_cube_not_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: load_wb m1 r1 addr1 = (m3, r3, t1, v1))
         (L2 : load_wb m2 r2 addr2 = (m4, r4, t2, v2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (TL1 : addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME TL1.
  inv TME.
  unfold load_wb in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  destruct (maybe_writeback_fetch_equiv_cube_not_locked Heqp Heqp0) as (TME & EQt & EQv). split; auto. auto. subst.
  split; auto. inv TME. split; auto.
  apply tcache_update_usage_equiv2; eauto. simpl; auto.
Qed.

Lemma load_wb_equiv:
  forall addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 v1 v2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    load_wb m1 c1 addr = (m1', c1', t1, v1) ->
    load_wb m2 c2 addr = (m2', c2', t2, v2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\
      t1 = t2 /\
      (perm addr = rx \/ process_of_addr addr = Some A -> v1 = v2).
Proof.
  intros addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 v1 v2 CEQ MEQ L1 L2.
  unfold load_wb in L1, L2.
  repeat destr_in L1. repeat destr_in L2. subst. inv L1. inv L2.
  destruct (maybe_writeback_fetch_equiv _ CEQ MEQ Heqp Heqp2) as (CEQ2 & MEQ2 & EQl & EQt). subst.
  split; [|split;[|split]]; auto.
  - apply tcache_update_usage_equiv. auto.
  - intros. rewrite ! read_from_cache_eq. destr.
    generalize (tcache_update_usage_equiv addr (mi_load addr) CEQ2). intro CEQ3.
    destr. symmetry. destr. symmetry.
    specialize (CEQ3 s). rewrite Heqp1, Heqp0 in CEQ3. destruct CEQ3 as (CEQ31 & CEQ32). simpl in *. subst.
    specialize (CEQ32 l2). destruct s0, s1; simpl in *. inv CEQ32; auto.
    red in H2. destruct_and H2. rewrite H6; auto.
    apply maybe_writeback_fetch_line_tag in Heqp.
    apply maybe_writeback_fetch_line_tag in Heqp2.
    unfold tcache_line_of_addr in Heqp, Heqp2.
    rewrite Heqt1 in Heqp, Heqp2.
    apply tcache_update_usage_inv in Heqp0.
    apply tcache_update_usage_inv in Heqp1.
    destruct Heqp0 as (? & Heqp0); destruct Heqp1 as (? & Heqp1).
    rewrite Heqp0 in Heqp. rewrite Heqp1 in Heqp2. simpl in *.
    apply line_of_tag_some in Heqp. apply line_of_tag_some in Heqp2.
    rewrite <- H0 in Heqp. rewrite <- H1 in Heqp2.
    destruct Heqp as (? & EQ & TAG); inv EQ.
    destruct Heqp2 as (? & EQ & TAG2); inv EQ.
    rewrite <- Heqt1.
    unfold process_of_addr in H. rewrite Heqt1 in H. simpl in *.
    rewrite perm_tagset_to_addr_to_tagset. auto.
Qed.


Lemma write_in_cache_equiv_left:
  forall c1 c2 a1 v1 l,
    equiv_tcache c1 c2 ->
    perm a1 = rw -> process_of_addr a1 = Some D ->
    tcache_line_of_addr c1 a1 = Some l ->
    tcache_addr_dirty c1 a1 = true ->
    equiv_tcache (write_in_cache c1 a1 v1 l) c2.
Proof.
  intros c1 c2 a1 v1 l CEQ PERM POA TLOA TAD.
  unfold process_of_addr in POA.
  destruct (addr_to_tagset a1) eqn:?. simpl in *.
  generalize (CEQ s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  generalize (EQ l). intro EQ2. red in EQ2.
  generalize (write_in_cache_unfold c1 a1 l v1). rewrite Heqt, Heqp.
  inv EQ2.
  - setoid_rewrite <- H0. intros A; rewrite A. auto.
  - setoid_rewrite <- H.
    intros (p1 & EQ1).
    rewrite EQ1; clear EQ1.
    red. intros s2. unfold update_tcache. destr; auto. subst. rewrite Heqp0. split; auto. simpl.
    red. intros. red. unfold update_ways. destr; eauto.
    subst.
    unfold tcache_line_of_addr in TLOA. rewrite Heqt, Heqp in TLOA.
    edestruct line_of_tag_some as (? & EQ2 & TAG). apply TLOA. setoid_rewrite EQ2 in H; inv H.
    red in H1. destruct_and H1. rewrite <- H0. constructor.
    split; simpl. auto. split; auto.
    unfold tcache_addr_dirty in TAD. rewrite Heqt, Heqp in TAD. rewrite TLOA in TAD. rewrite EQ2 in TAD.
    split; auto. congruence.
    intros.
    rewrite <- Heqt in H. rewrite perm_tagset_to_addr_to_tagset in H. intuition congruence.
    specialize (EQ l0). red in EQ. apply EQ.
Qed.

Lemma write_in_cache_equiv_right:
  forall c1 c2 a2 v2 l,
    equiv_tcache c1 c2 ->
    perm a2 = rw -> process_of_addr a2 = Some D ->
    tcache_line_of_addr c2 a2 = Some l ->
    tcache_addr_dirty c2 a2 = true ->
    equiv_tcache c1 (write_in_cache c2 a2 v2 l).
Proof.
  intros c1 c2 a2 v2 l CEQ PERM POA TLOA TAD.
  unfold process_of_addr in POA.
  destruct (addr_to_tagset a2) eqn:?. simpl in *.
  generalize (CEQ s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  generalize (EQ l). intro EQ2. red in EQ2.
  generalize (write_in_cache_unfold c2 a2 l v2). rewrite Heqt, Heqp0.
  inv EQ2.
  - setoid_rewrite <- H. intros A; rewrite A. auto.
  - setoid_rewrite <- H0.
    intros (p1 & EQ1).
    rewrite EQ1; clear EQ1.
    red. intros s2. unfold update_tcache. destr; auto. subst. rewrite Heqp. split; auto. simpl.
    red. intros. red. unfold update_ways. destr; eauto.
    subst.
    unfold tcache_line_of_addr in TLOA. rewrite Heqt, Heqp0 in TLOA.
    edestruct line_of_tag_some as (? & EQ2 & TAG). apply TLOA. setoid_rewrite EQ2 in H0; inv H0.
    red in H1. destruct_and H1. rewrite <- H. constructor.
    split; simpl. auto. split; auto.
    unfold tcache_addr_dirty in TAD. rewrite Heqt, Heqp0 in TAD. rewrite TLOA in TAD. rewrite EQ2 in TAD.
    split; auto. congruence.
    intros.
    rewrite H1, <- Heqt in H3. rewrite perm_tagset_to_addr_to_tagset in H3. intuition congruence.
    specialize (EQ l0). red in EQ. apply EQ.
Qed.

Lemma write_in_cache_wt_equiv_left:
  forall c1 c2 a1 v1 l,
    equiv_tcache c1 c2 ->
    perm a1 = rw -> process_of_addr a1 = Some D ->
    tcache_line_of_addr c1 a1 = Some l ->
    tcache_addr_dirty c1 a1 = true ->
    equiv_tcache (write_in_cache_wt c1 a1 v1 l) c2.
Proof.
  intros c1 c2 a1 v1 l CEQ PERM POA TLOA TAD.
  unfold process_of_addr in POA.
  destruct (addr_to_tagset a1) eqn:?. simpl in *.
  generalize (CEQ s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  generalize (EQ l). intro EQ2. red in EQ2.
  generalize (write_in_cache_wt_unfold c1 a1 l v1). rewrite Heqt, Heqp.
  inv EQ2.
  - setoid_rewrite <- H0. intros A; rewrite A. auto.
  - setoid_rewrite <- H.
    intros (p1 & EQ1).
    rewrite EQ1; clear EQ1.
    red. intros s2. unfold update_tcache. destr; auto. subst. rewrite Heqp0. split; auto. simpl.
    red. intros. red. unfold update_ways. destr; eauto.
    subst.
    unfold tcache_line_of_addr in TLOA. rewrite Heqt, Heqp in TLOA.
    edestruct line_of_tag_some as (? & EQ2 & TAG). apply TLOA. setoid_rewrite EQ2 in H; inv H.
    red in H1. destruct_and H1. rewrite <- H0. constructor.
    split; simpl. auto. split; auto.
    unfold tcache_addr_dirty in TAD. rewrite Heqt, Heqp in TAD. rewrite TLOA in TAD. rewrite EQ2 in TAD.
    split; auto.
    intros.
    rewrite <- Heqt in H. rewrite perm_tagset_to_addr_to_tagset in H. intuition congruence.
    specialize (EQ l0). red in EQ. apply EQ.
Qed.

Lemma write_in_cache_wt_equiv_right:
  forall c1 c2 a2 v2 l,
    equiv_tcache c1 c2 ->
    perm a2 = rw -> process_of_addr a2 = Some D ->
    tcache_line_of_addr c2 a2 = Some l ->
    tcache_addr_dirty c2 a2 = true ->
    equiv_tcache c1 (write_in_cache_wt c2 a2 v2 l).
Proof.
  intros c1 c2 a2 v2 l CEQ PERM POA TLOA TAD.
  unfold process_of_addr in POA.
  destruct (addr_to_tagset a2) eqn:?. simpl in *.
  generalize (CEQ s) as EQ. intro.
  destruct (c1 s) eqn:?, (c2 s) eqn:?. destruct EQ as (EQ0 & EQ); simpl in *; subst.
  generalize (EQ l). intro EQ2. red in EQ2.
  generalize (write_in_cache_wt_unfold c2 a2 l v2). rewrite Heqt, Heqp0.
  inv EQ2.
  - setoid_rewrite <- H. intros A; rewrite A. auto.
  - setoid_rewrite <- H0.
    intros (p1 & EQ1).
    rewrite EQ1; clear EQ1.
    red. intros s2. unfold update_tcache. destr; auto. subst. rewrite Heqp. split; auto. simpl.
    red. intros. red. unfold update_ways. destr; eauto.
    subst.
    unfold tcache_line_of_addr in TLOA. rewrite Heqt, Heqp0 in TLOA.
    edestruct line_of_tag_some as (? & EQ2 & TAG). apply TLOA. setoid_rewrite EQ2 in H0; inv H0.
    red in H1. destruct_and H1. rewrite <- H. constructor.
    split; simpl. auto. split; auto.
    unfold tcache_addr_dirty in TAD. rewrite Heqt, Heqp0 in TAD. rewrite TLOA in TAD. rewrite EQ2 in TAD.
    split; auto.
    intros.
    rewrite H1, <- Heqt in H3. rewrite perm_tagset_to_addr_to_tagset in H3. intuition congruence.
    specialize (EQ l0). red in EQ. apply EQ.
Qed.

Lemma store_wb_equiv_cube_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: store_wb m1 r1 addr1 v1 = (m3, r3, t1))
         (L2 : store_wb m2 r2 addr2 v2 = (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (POT1: process_of_addr addr1 = Some D)
         (POT2: process_of_addr addr2 = Some D)
         (PERM1: perm addr1 = rw)
         (PERM2: perm addr2 = rw)
         (TL1 : tagset_locked (addr_to_tagset addr1) r1)
         (TL2 : tagset_locked (addr_to_tagset addr2) r2)
         (TAD1: tcache_addr_dirty r1 addr1 = true)
         (TAD1: tcache_addr_dirty r2 addr2 = true),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME POT1 POT2 PERM1 PERM2 TL1 TL2 TAD1 TAD2.
  inv TME.
  unfold store_wb in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  unfold maybe_writeback_fetch in *.
  destr_in Heqp.
  clear - H4 TL1 e. exfalso.
  unfold tcache_line_of_addr in e. destr_in e. destr_in e.
  red in TL1. rewrite Heqp in TL1. red in TL1.
  destruct TL1 as (l & cl & EQ & LOCKED & TAG).
  eapply line_of_tag_none in EQ; eauto.

  destr_in Heqp0.
  clear - H4 TL2 e. exfalso.
  unfold tcache_line_of_addr in e. destr_in e. destr_in e.
  red in TL2. rewrite Heqp in TL2. red in TL2.
  destruct TL2 as (l & cl & EQ & LOCKED & TAG).
  eapply line_of_tag_none in EQ; eauto.

  inv Heqp. inv Heqp0. split; auto.
  split; auto.
  unfold tcache_update_usage. repeat destr.
  unfold tcache_line_of_addr' in Heqs, Heqs0.
  destr_in Heqs; inv Heqs.
  destr_in Heqs0; inv Heqs0.
  unfold tcache_line_of_addr in Heqo, Heqo0.
  rewrite Heqt1, Heqp in Heqo.
  rewrite Heqt2, Heqp1 in Heqo0.
  rewrite Heqo in Heqp0. rewrite Heqo0 in Heqp2.
  destruct (line_of_tag_some _ _ Heqo) as (? & EQ1 & TAG1).
  destruct (line_of_tag_some _ _ Heqo0) as (? & EQ2 & TAG2).
  rewrite EQ1 in Heqp0. rewrite EQ2 in Heqp2. inv Heqp0; inv Heqp2.
  rewrite ! update_usage_locked_load_store; auto.
  eapply write_in_cache_equiv_right; eauto.
  eapply write_in_cache_equiv_left; eauto.
  - unfold update_tcache. red. intros.
    specialize (H4 s3).
    repeat destr; subst; eauto; try congruence.
  - unfold tcache_line_of_addr. rewrite Heqt1. rewrite update_tcache_same. auto.
  - revert TAD1. unfold tcache_addr_dirty. rewrite Heqt1. rewrite update_tcache_same. rewrite Heqp. rewrite Heqo. rewrite EQ1. auto.
  - unfold tcache_line_of_addr. rewrite Heqt2. rewrite update_tcache_same. auto.
  - revert TAD2. unfold tcache_addr_dirty. rewrite Heqt2. rewrite update_tcache_same. rewrite Heqp1. rewrite Heqo0. rewrite EQ2. auto.
Qed.

Lemma store_wb_equiv_cube_not_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: store_wb m1 r1 addr1 v1 = (m3, r3, t1))
         (L2 : store_wb m2 r2 addr2 v2 = (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (POT1: process_of_addr addr1 = Some D)
         (PERM1: perm addr1 = rw)
         (EQ: addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME POT1 PERM1 EQ.
  inv TME.
  unfold store_wb in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  destruct (maybe_writeback_fetch_equiv_cube_not_locked Heqp Heqp0) as (TME & EQt & EQv). split; auto. auto. subst.
  split; auto. inv TME. split; auto.
  apply write_in_cache_equiv2; auto.
  apply tcache_update_usage_equiv2; eauto. simpl; auto.
  rewrite tcache_line_of_addr_update_usage. eapply maybe_writeback_tcache_line_of_addr; eauto.
Qed.

Lemma store_wb_equiv:
  forall addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 v,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    store_wb m1 c1 addr v = (m1', c1', t1) ->
    store_wb m2 c2 addr v = (m2', c2', t2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\
      t1 = t2.
Proof.
  intros addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 v CEQ MEQ L1 L2.
  unfold store_wb in L1, L2.
  repeat destr_in L1. repeat destr_in L2. subst. inv L1. inv L2.
  destruct (maybe_writeback_fetch_equiv _ CEQ MEQ Heqp Heqp2) as (CEQ2 & MEQ2 & EQl & EQt). subst.
  split; [|split]; auto.
  apply write_in_cache_equiv.
  apply tcache_update_usage_equiv. auto.
Qed.

Lemma store_wt_equiv_cube_locked around:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: store_wt around m1 r1 addr1 v1 = (m3, r3, t1))
         (L2 : store_wt around m2 r2 addr2 v2 = (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (POT1: process_of_addr addr1 = Some D)
         (POT2: process_of_addr addr2 = Some D)
         (PERM1: perm addr1 = rw)
         (PERM2: perm addr2 = rw)
         (TL1 : tagset_locked (addr_to_tagset addr1) r1)
         (TL2 : tagset_locked (addr_to_tagset addr2) r2)
         (TAD1: tcache_addr_dirty r1 addr1 = true)
         (TAD1: tcache_addr_dirty r2 addr2 = true),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME POT1 POT2 PERM1 PERM2 TL1 TL2 TAD1 TAD2.
  inv TME.
  unfold store_wt in *.
  do 4 destr_in L1; inv L1.
  do 4 destr_in L2; inv L2.
  destr_in Heqp.
  clear - H4 TL1 e. exfalso.
  unfold tcache_line_of_addr in e. destr_in e. destr_in e.
  red in TL1. rewrite Heqp in TL1. red in TL1.
  destruct TL1 as (l & cl & EQ & LOCKED & TAG).
  eapply line_of_tag_none in EQ; eauto.

  destr_in Heqp0.
  clear - H4 TL2 e. exfalso.
  unfold tcache_line_of_addr in e. destr_in e. destr_in e.
  red in TL2. rewrite Heqp in TL2. red in TL2.
  destruct TL2 as (l & cl & EQ & LOCKED & TAG).
  eapply line_of_tag_none in EQ; eauto.

  inv Heqp. inv Heqp0. split; auto.
  rewrite <- tcache_is_locked_tagset_locked_iff in TL1, TL2. rewrite TL1, TL2. simpl.
  split; auto.
  eapply write_in_cache_wt_equiv_right; eauto.
  eapply write_in_cache_wt_equiv_left; eauto.
  - unfold tcache_update_usage.
    unfold tcache_is_locked in TL1, TL2.
    repeat destr_in TL1; inv TL1. repeat destr_in TL2; inv TL2.
    unfold update_tcache. red. intros.
    rewrite ! update_usage_locked_load_store; auto.
    specialize (H4 s3). repeat destr; subst; eauto; congruence.
  - unfold tcache_line_of_addr' in Heqs; destr_in Heqs; inv Heqs. rewrite tcache_line_of_addr_update_usage; auto.
  - rewrite tcache_addr_dirty_update_usage; auto.
  - unfold tcache_line_of_addr' in Heqs0; destr_in Heqs0; inv Heqs0. rewrite tcache_line_of_addr_update_usage; auto.
  - rewrite tcache_addr_dirty_update_usage; auto.
Qed.


Lemma tcache_is_locked_equiv2:
  forall c1 c2,
    equiv_tcache c1 c2 ->
    forall a1 a2,
      addr_to_tagset a1 = addr_to_tagset a2 ->
      tcache_is_locked c1 a1 = tcache_is_locked c2 a2.
Proof.
  intros.
  apply ZMicromega.eq_true_iff_eq.
  rewrite ! tcache_is_locked_tagset_locked_iff. rewrite H0.
  rewrite <- ! tcache_is_locked_tagset_locked_iff.
  apply ZMicromega.eq_true_iff_eq.
  apply tcache_is_locked_equiv. auto.
Qed.

Lemma store_wt_equiv_cube_not_locked around:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2
         (L1: store_wt around m1 r1 addr1 v1 = (m3, r3, t1))
         (L2 : store_wt around m2 r2 addr2 v2 = (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (POT1: process_of_addr addr1 = Some D)
         (PERM1: perm addr1 = rw)
         (EQ: addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 v1 v2 L1 L2 TME POT1 PERM1 EQ.
  inv TME.
  unfold store_wt in *.
  do 4 destr_in L1; inv L1.
  do 4 destr_in L2; inv L2.
  destr_in Heqp.
  - edestruct tcache_line_of_addr'_equiv_none2 as (pf & EQ2); eauto. rewrite EQ2 in Heqp0.
    destr_in Heqp.
    + inv Heqp. inv Heqp0. split; auto. split; auto.
      eapply mem_equiv_do_store; eauto.
      rewrite <- perm_tagset_to_addr_to_tagset, <- EQ, perm_tagset_to_addr_to_tagset. auto.
      unfold check_process_of_addr. destr; eauto; congruence.
      unfold check_process_of_addr. replace (process_of_addr addr2) with (process_of_addr addr1). destr; eauto; congruence.
      unfold process_of_addr. congruence.
      apply tcache_update_usage_equiv2; eauto. simpl; auto.
    + repeat destr_in Heqp; inv Heqp.
      repeat destr_in Heqp0; inv Heqp0.
      edestruct (maybe_writeback_fetch_equiv_cube_not_locked) as (TME & EQt & EQv).
      unfold maybe_writeback_fetch. rewrite Heqs. rewrite Heqp1. eauto.
      unfold maybe_writeback_fetch. rewrite EQ2. rewrite Heqp. eauto.
      split; auto. auto. subst. inv TME.
      split; auto. split; auto.
      eapply mem_equiv_do_store; eauto.
      rewrite <- perm_tagset_to_addr_to_tagset, <- EQ, perm_tagset_to_addr_to_tagset. auto.
      unfold check_process_of_addr. destr; eauto; congruence.
      unfold check_process_of_addr. replace (process_of_addr addr2) with (process_of_addr addr1). destr; eauto; congruence.
      unfold process_of_addr. congruence.
      erewrite <- (tcache_line_to_evict_equiv2 H4) at 2. 2: apply EQ.
      apply write_in_cache_wt_equiv2; auto.
      apply tcache_update_usage_equiv2; eauto. simpl; auto.
      rewrite tcache_line_of_addr_update_usage.
      unfold tcache_line_of_addr. destr.
      generalize (fetch_unfold m r1 addr1 (tcache_line_to_evict r1 addr1) e). destr. destr. simpl.
      intros (p & EQ3). rewrite EQ3; clear EQ3. simpl. destr.
      inv Heqt. rewrite update_tcache_same in Heqp2. inv Heqp2. simpl.
      rewrite line_of_tag_update_ways. simpl. destr; congruence.
      apply proj2_sig. auto.
  - erewrite tcache_line_of_addr'_equiv2 in Heqp0; eauto. inv Heqp. inv Heqp0.
    split; auto. split; auto.
    + erewrite (tcache_is_locked_equiv2 H4). 2: apply EQ. destr; auto.
      eapply mem_equiv_do_store; eauto.
      rewrite <- perm_tagset_to_addr_to_tagset, <- EQ, perm_tagset_to_addr_to_tagset. auto.
      unfold check_process_of_addr. destr; eauto; congruence.
      unfold check_process_of_addr. replace (process_of_addr addr2) with (process_of_addr addr1). destr; eauto; congruence.
      unfold process_of_addr. congruence.
    + apply write_in_cache_wt_equiv2; auto.
      apply tcache_update_usage_equiv2; eauto. simpl; auto.
      rewrite tcache_line_of_addr_update_usage.
      unfold tcache_line_of_addr' in Heqs; destr_in Heqs; inv Heqs; auto.
Qed.


Lemma store_wt_equiv around:
  forall addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 v,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    store_wt around m1 c1 addr v = (m1', c1', t1) ->
    store_wt around m2 c2 addr v = (m2', c2', t2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\
      t1 = t2.
Proof.
  intros addr m1 m2 c1 c2 m1' m2' c1' c2' t1 t2 v CEQ MEQ L1 L2.
  unfold store_wt in L1, L2.
  do 4 destr_in L1; inv L1.
  do 4 destr_in L2; inv L2.
  destr_in Heqp.
  - edestruct tcache_line_of_addr'_equiv_none2 as (pf & EQ2); eauto. rewrite EQ2 in Heqp0.
    destr_in Heqp.
    + inv Heqp. inv Heqp0. split; auto. 2: split; auto.
      apply tcache_update_usage_equiv2; eauto. simpl; auto.
      inv MEQ; constructor. intros. unfold do_store. destr; auto.
    + repeat destr_in Heqp; inv Heqp.
      repeat destr_in Heqp0; inv Heqp0.
      edestruct (maybe_writeback_fetch_equiv_cube_not_locked) as (TME & EQt & EQv).
      unfold maybe_writeback_fetch. rewrite Heqs. rewrite Heqp1. eauto.
      unfold maybe_writeback_fetch. rewrite EQ2. rewrite Heqp. eauto.
      split; auto. auto. inv TME.
      split; auto. 2: split; auto.
      2:{
        inv H2; constructor. intros. unfold do_store. destr; auto.
      }
      erewrite <- (tcache_line_to_evict_equiv2 CEQ) at 2.
      apply write_in_cache_wt_equiv; auto.
      apply tcache_update_usage_equiv2; eauto. simpl; auto. auto.
  - erewrite tcache_line_of_addr'_equiv2 in Heqp0; eauto. inv Heqp. inv Heqp0.
    split; auto. 2: split; auto.
    + apply write_in_cache_wt_equiv; auto.
      apply tcache_update_usage_equiv2; eauto. simpl; auto.
    + erewrite (tcache_is_locked_equiv). 2: eauto. destr; auto.
      inv MEQ; constructor. intros. unfold do_store. destr; auto.
Qed.

Lemma lock_wb_equiv_cube_not_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2
         (L1: lock_wb m1 r1 addr1 = Some (m3, r3, t1))
         (L2 : lock_wb m2 r2 addr2 = Some (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (EQ: addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 L1 L2 TME EQ.
  inv TME.
  unfold lock_wb in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  destruct (maybe_writeback_fetch_equiv_cube_not_locked Heqp Heqp0) as (TME & EQt & EQv). split; auto. auto. subst.
  split; auto. inv TME. split; auto.
  apply lock_line_equiv2; auto.
  apply tcache_update_usage_equiv2; eauto. simpl; auto.
Qed.

Lemma unlock_wb_equiv_cube_not_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2
         (L1: unlock_wb m1 r1 addr1 = (m3, r3, t1))
         (L2 : unlock_wb m2 r2 addr2 = (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (EQ: addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 L1 L2 TME EQ.
  inv TME.
  unfold unlock_wb in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  destruct (maybe_writeback_fetch_equiv_cube_not_locked Heqp Heqp0) as (TME & EQt & EQv). split; auto. auto. subst.
  split; auto. inv TME. split; auto.
  apply unlock_line_equiv2; auto.
  apply tcache_update_usage_equiv2; eauto. simpl; auto.
Qed.

Lemma write_back_equiv2:
  forall c1 c2 m1 m2 l a1 a2 m1' m2' t1 t2 a0,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    write_back m1 c1 l a1 = (m1', t1) ->
    write_back m2 c2 l a2 = (m2', t2) ->
    option_map addr_to_tagset a1 = option_map addr_to_tagset (address_of_evicted_line c1 l a0) ->
    (match a1, a2 with
       Some a1, Some a2 => addr_to_tagset a1 = addr_to_tagset a2
     | None, None => True
     | _, _ => False
     end) ->
    mem_equiv m1' m2' /\ t1 = t2.
Proof.
  unfold write_back. intros c1 c2 m1 m2 l a1 a2 m1' m2' t1 t2 a0 H H0 H1 H2 H3 EQ.
  repeat destr_in EQ; inv EQ. 2: inv H1; inv H2; eauto.
  rename H5 into EQ.
  rewrite EQ in *.
  destr_in H1.
  specialize (H s). destr_in H1. destr_in H2. destruct H; simpl in *; subst.
  unfold address_of_evicted_line in H3.
  repeat destr_in H3; inv H3. rewrite addr_to_tagset_to_addr in H5. rewrite H5 in EQ; inv EQ.
  specialize (H4 l).
  inv H4.
  - setoid_rewrite <- H in H2. setoid_rewrite <- H3 in H1. inv H1. inv H2. split; auto.
  - setoid_rewrite <- H in H1. setoid_rewrite <- H3 in H2.
    red in H6. destruct_and H6. rewrite H7 in *.
    destr_in H1; inv H1; inv H2; split; auto.
    constructor. intros.
    rewrite ! set_line_get. rewrite Heqt, H5. destr; eauto.
    clear Heqs2.
    unfold process_of_addr in H1. rewrite <- e in H1. simpl in H1.
    assert (s3 = s0) by congruence. subst.
    assert (a = c3). setoid_rewrite <- H in Heqc3. inv Heqc3; auto. subst.
    rewrite e in H9. rewrite perm_tagset_to_addr_to_tagset in H9. rewrite H9; auto.
    inv H0. eauto.
    apply cl_content_length.
    apply cl_content_length.
Qed.

Lemma line_of_tag_tag:
  forall w t l cl,
    line_of_tag w t = Some l ->
    w l = Some cl ->
    cl_tag cl = t.
Proof.
  intros. apply line_of_tag_some in H. rewrite H0 in H.
  destruct H as (? & EQ & TAG); inv EQ; auto.
Qed.

Lemma unlock_wt_equiv_cube_not_locked:
  forall m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2
         (L1: unlock_wt m1 r1 addr1 = (m3, r3, t1))
         (L2 : unlock_wt m2 r2 addr2 = (m4, r4, t2))
         (TME : target_mem_equiv (m1, r1) (m2, r2))
         (EQ: addr_to_tagset addr1 = addr_to_tagset addr2),
    target_mem_equiv (m3, r3) (m4, r4) /\ t1 = t2.
Proof.
  intros m1 m2 r1 r2 addr1 addr2 m3 m4 r3 r4 t1 t2 L1 L2 TME EQ.
  inv TME.
  unfold unlock_wt in *.
  repeat destr_in L1; inv L1.
  repeat destr_in L2; inv L2.
  destruct (maybe_writeback_fetch_equiv_cube_not_locked Heqp Heqp0) as (TME & EQt & EQv). split; auto. auto. subst.
  inv TME.
  edestruct (fun v pf => write_back_equiv2 l3 _ _ v pf H3 Heqp2 Heqp4) as (MEQ2 & EQt).
  apply unlock_line_equiv2.
  apply tcache_update_usage_equiv2; eauto. simpl; auto. auto.
  simpl.
  unfold address_of_evicted_line, unlock_line. destr.
  destr.
  generalize (update_cache_line_eq (tcache_update_usage t addr1 (mi_unlock addr1)) addr1 l3 _ unlock_line_cl_preserves_tag).
  rewrite Heqt1. destr. apply tcache_update_usage_inv in Heqp3. destruct Heqp3.
  destr. intros (p & EQ2); rewrite EQ2 in Heqp1; clear EQ2.
  rewrite update_tcache_same in Heqp1. inv Heqp1. simpl. rewrite update_ways_same. simpl.
  apply maybe_writeback_fetch_line_tag in Heqp.
  unfold tcache_line_of_addr in Heqp. rewrite Heqt1, H in Heqp.
  eapply line_of_tag_tag in Heqp; eauto. subst. rewrite <- Heqt1. rewrite Heqt1,  addr_to_tagset_to_addr. auto.
  intros EQ2; rewrite EQ2 in Heqp1; clear EQ2.
  apply tcache_update_usage_inv in Heqp1. destruct Heqp1. rewrite H0 in H. inv H. setoid_rewrite Heqo.
  apply maybe_writeback_fetch_line_tag in Heqp.
  unfold tcache_line_of_addr in Heqp. rewrite Heqt1, H0 in Heqp.
  apply line_of_tag_spec in Heqp; auto. 2: apply proj2_sig. setoid_rewrite Heqo in Heqp. destruct Heqp; intuition congruence.
  auto.
  subst.
  split; auto. split; auto.
  apply unlock_line_equiv2; auto.
  apply tcache_update_usage_equiv2; auto. simpl; auto.
Qed.

Lemma lock_wb_equiv:
  forall addr m1 m2 c1 c2 m1' c1' t,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    lock_wb m1 c1 addr = Some (m1', c1', t) ->
    exists m2' c2',
      lock_wb m2 c2 addr = Some (m2', c2', t) /\
        equiv_tcache c1' c2' /\
        mem_equiv m1' m2'.
Proof.
  intros addr m1 m2 c1 c2 m1' c1' t CEQ MEQ LOCK.
  unfold lock_wb in *.
  rewrite <- (num_lockable_in_set_equiv addr CEQ).
  rewrite <- (tcache_is_locked_equiv addr CEQ).
  repeat destr_in LOCK; inv LOCK. repeat destr. subst.
  destruct (maybe_writeback_fetch_equiv _ CEQ MEQ Heqp Heqp0) as (CEQ2 & MEQ2 & EQl & EQt). subst.
  eexists _, _; split. eauto. split; auto.
  apply lock_line_equiv.
  apply tcache_update_usage_equiv.
  auto.
Qed.

Lemma unlock_wb_equiv:
  forall addr m1 m2 c1 c2 m1' c1' m2' c2' t1 t2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    unlock_wb m1 c1 addr = (m1', c1', t1) ->
    unlock_wb m2 c2 addr = (m2', c2', t2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\ t1 = t2.
Proof.
  intros addr m1 m2 c1 c2 m1' c1' m2' c2' t1 t2 CEQ MEQ U1 U2.
  unfold unlock_wb in *.
  repeat destr_in U1. inv U1.
  repeat destr_in U2. inv U2.
  destruct (maybe_writeback_fetch_equiv _ CEQ MEQ Heqp Heqp0) as (CEQ2 & MEQ2 & EQl & EQt). subst.
  split; auto.
  apply unlock_line_equiv.
  apply tcache_update_usage_equiv.
  auto.
Qed.

Lemma address_of_evicted_line_unlock_line:
  forall t a l a2 l2,
    address_of_evicted_line (unlock_line t a l) a2 l2 = address_of_evicted_line t a2 l2.
Proof.
  unfold address_of_evicted_line, unlock_line. intros.
  generalize (update_cache_line_eq t a l _ unlock_line_cl_preserves_tag).
  destr.
  destr. intros EQ.
  destr. destr. symmetry. destr.
  destr_in EQ.
  destruct EQ as (p & EQ); rewrite EQ in Heqp0; clear EQ.
  unfold update_tcache in Heqp0. destr_in Heqp0; inv Heqp0.
  rewrite Heqp in Heqp1; inv Heqp1. simpl. unfold update_ways.
  destruct (eq_dec l a2); subst; simpl; auto. setoid_rewrite Heqo. auto.
  rewrite Heqp1 in H0. inv H0. auto.
  rewrite EQ in Heqp0; clear EQ. rewrite Heqp0 in Heqp1; inv Heqp1; auto.
Qed.

Lemma address_of_evicted_line_tcache_update_usage:
  forall t a mi a2 l2,
    address_of_evicted_line (tcache_update_usage t a mi) a2 l2 = address_of_evicted_line t a2 l2.
Proof.
  unfold address_of_evicted_line, tcache_update_usage. intros.
  destr. destr. destr_in Heqp. destr_in Heqp. destr_in Heqp. clear Heqp1. unfold update_tcache in Heqp.
  destr_in Heqp; inv Heqp. rewrite Heqp0. auto. rewrite H0. auto.
Qed.

Lemma unlock_wt_equiv:
  forall addr m1 m2 c1 c2 m1' c1' m2' c2' t1 t2,
    equiv_tcache c1 c2 ->
    mem_equiv m1 m2 ->
    unlock_wt m1 c1 addr = (m1', c1', t1) ->
    unlock_wt m2 c2 addr = (m2', c2', t2) ->
    equiv_tcache c1' c2' /\
      mem_equiv m1' m2' /\ t1 = t2.
Proof.
  intros addr m1 m2 c1 c2 m1' c1' m2' c2' t1 t2 CEQ MEQ U1 U2.
  unfold unlock_wt in *.
  repeat destr_in U1. inv U1.
  repeat destr_in U2. inv U2.
  destruct (maybe_writeback_fetch_equiv _ CEQ MEQ Heqp Heqp0) as (CEQ2 & MEQ2 & EQl & EQt). subst.
  edestruct write_back_equiv as (MEQ3 & EQ3). 3: apply Heqp2. 3: apply Heqp4.
  apply unlock_line_equiv.
  apply tcache_update_usage_equiv.
  auto.
  auto.
  apply maybe_writeback_fetch_line_tag in Heqp.
  unfold tcache_line_of_addr in Heqp; repeat destr_in Heqp; inv Heqp.
  rewrite address_of_evicted_line_unlock_line, address_of_evicted_line_tcache_update_usage.
  unfold address_of_evicted_line. rewrite Heqt1, Heqp1.
  simpl.
  rewrite line_of_tag_spec in H0. destruct H0 as ( ? & EQ & TAG). rewrite EQ. simpl.
  subst. rewrite addr_to_tagset_to_addr. congruence.
  apply proj2_sig. subst.
  split; auto.
  apply unlock_line_equiv.
  apply tcache_update_usage_equiv. auto.
Qed.

Lemma target_cache_preserves_equiv:
  forall rt1 rt2 t mi p rt1' mo,
    target_mem_equiv rt1 rt2 ->
    target_mem_interface p rt1 mi = Some (rt1', mo, t) ->
    exists rt2' mo' ,
      target_mem_interface p rt2 mi = Some (rt2', mo', t) /\
        target_mem_equiv rt1' rt2'.
Proof.
  simpl.
  intros.
  destruct H as (m1,c1, m2, c2, MEQ, CEQ).
  simpl in *.
  unfold option_bind in *. repeat destr_in H0; inv H0.
  - repeat destr. subst.
    generalize (load_wb_equiv addr CEQ MEQ Heqp0 Heqp1). intros (CEQ2 & MEQ2 & -> & ?).
    eexists _, _; split; eauto. constructor; auto.
  - repeat destr. subst.
    generalize (store_wb_equiv addr v CEQ MEQ Heqp0 Heqp1). intros (CEQ2 & MEQ2 & ->).
    eexists _, _; split; eauto. constructor; auto.
  - edestruct (lock_wb_equiv addr CEQ MEQ Heqo0) as (m2' & c2' & LOCK & CEQ2 & MEQ2).
    rewrite LOCK.
    eexists _, _; split; eauto. constructor; auto.
  - repeat destr. subst.
    generalize (unlock_wb_equiv addr CEQ MEQ Heqp0 Heqp1). intros (CEQ2 & MEQ2 & ->).
    eexists _, _; split; eauto. constructor; auto.
Qed.

Lemma target_cache_wt_preserves_equiv around:
  forall rt1 rt2 t mi p rt1' mo,
    target_mem_equiv rt1 rt2 ->
    target_mem_interface_wt around p rt1 mi = Some (rt1', mo, t) ->
    exists rt2' mo' ,
      target_mem_interface_wt around p rt2 mi = Some (rt2', mo', t) /\
        target_mem_equiv rt1' rt2'.
Proof.
  simpl.
  intros.
  destruct H as (m1,c1, m2, c2, MEQ, CEQ).
  simpl in *.
  unfold option_bind in *. repeat destr_in H0; inv H0.
  - repeat destr. subst.
    generalize (load_wb_equiv addr CEQ MEQ Heqp0 Heqp1). intros (CEQ2 & MEQ2 & -> & ?).
    eexists _, _; split; eauto. constructor; auto.
  - repeat destr. subst.
    generalize (store_wt_equiv around addr v CEQ MEQ Heqp0 Heqp1). intros (CEQ2 & MEQ2 & ->).
    eexists _, _; split; eauto. constructor; auto.
  - edestruct (lock_wb_equiv addr CEQ MEQ Heqo0) as (m2' & c2' & LOCK & CEQ2 & MEQ2).
    rewrite LOCK.
    eexists _, _; split; eauto. constructor; auto.
  - repeat destr. subst.
    generalize (unlock_wt_equiv addr CEQ MEQ Heqp0 Heqp1). intros (CEQ2 & MEQ2 & ->).
    eexists _, _; split; eauto. constructor; auto.
Qed.


Definition is_store mi := match mi with mi_store _ _ => True | _ => False end.

Record target_interface_equiv {ME: Type} (mintf: (input -> mem -> Prop) -> mem_interface ME _ mem_output _) : Prop :=
  {
    tie_preserves_equiv_locked:
    forall im m1 m2  mi1 mi2 m3 m4 t1 t2 v1 v2
           (L1: mi_interface (mintf im) D m1 mi1 = Some (m3, v1, t1))
           (L2: mi_interface (mintf im) D m2 mi2 = Some (m4, v2, t2))
           (TME : target_mem_equiv m1 m2)
           (LS: is_load_or_store mi1)
           (SK: same_kind mi1 mi2)
           (PERM1: is_store mi1 -> perm (addr_of_mi mi1) = rw)
           (PERM2: is_store mi2 -> perm (addr_of_mi mi2) = rw)
           (TL1 : tagset_locked (addr_to_tagset (addr_of_mi mi1)) (snd m1))
           (TL2 : tagset_locked (addr_to_tagset (addr_of_mi mi2)) (snd m2))
           (TLD1: tcache_locked_dirty (snd m1))
           (TLD2: tcache_locked_dirty (snd m2)),
      target_mem_equiv m3 m4 /\ t1 = t2;
    tie_preserves_equiv_same_tagset:
    forall im m1 m2 mi1 mi2 m3 m4 t1 t2 v1 v2
           (L1: mi_interface (mintf im) D m1 mi1 = Some (m3, v1, t1))
           (L2: mi_interface (mintf im) D m2 mi2 = Some (m4, v2, t2))
           (TME : target_mem_equiv m1 m2)
           (SK: same_kind mi1 mi2)
           (PERM1: is_store mi1 -> perm (addr_of_mi mi1) = rw)
           (PERM2: is_store mi2 -> perm (addr_of_mi mi2) = rw)
           (EQts : addr_to_tagset (addr_of_mi mi1) = addr_to_tagset (addr_of_mi mi2)),
      target_mem_equiv m3 m4 /\ t1 = t2;
    tie_preserves_equiv:
    forall im m1 m2 mi m3 t1 v1 id
           (L1: mi_interface (mintf im) id m1 mi = Some (m3, v1, t1))
           (TME : target_mem_equiv m1 m2),
    exists m4 v2,
      mi_interface (mintf im) id m2 mi = Some (m4, v2, t1) /\
        target_mem_equiv m3 m4 /\
        option_rel (fun v1 v2 =>
                      (perm (addr_of_mi mi) = rx \/ process_of_addr (addr_of_mi mi) = Some A -> v1 = v2)) v1 v2;
    tie_process:
    forall im mi m id x
           (L: mi_interface (mintf im) id m mi = Some x),
      process_of_addr (addr_of_mi mi) = Some id;
  }.

Theorem tie_target_cache_wb:
  target_interface_equiv target_cache.
Proof.
  constructor.
  - intros im m1 m2 mi1 mi2 m3 m4 t1 t2 v1 v2 L1 L2 TME LS SK PERM1 PERM2 TL1 TL2 TLD1 TLD2.
    destruct mi1; simpl in LS, SK; destruct mi2; try easy.
    simpl in *. destruct m1, m2, m3, m4; simpl in *.
    unfold option_bind in *. repeat destr_in L1; inv L1. repeat destr_in L2; inv L2.
    eapply load_wb_equiv_cube_locked; eauto.
    simpl in *. destruct m1, m2, m3, m4; simpl in *.
    unfold option_bind in *. repeat destr_in L1; inv L1. repeat destr_in L2; inv L2.
    eapply store_wb_equiv_cube_locked; eauto using check_process_of_addr_impl, tcache_locked_dirty_correct.
  - intros im m1 m2 mi1 mi2 m3 m4 t1 t2 v1 v2 L1 L2 TME SK PERM1 PERM2 EQts.
    destruct m1, m2, m3, m4; simpl in *.
    destruct mi1; simpl in SK; destruct mi2; try easy;
      unfold option_bind in L1, L2;
      repeat destr_in L1; inv L1;
      repeat destr_in L2; inv L2;
      simpl in *.
    eapply load_wb_equiv_cube_not_locked; eauto.
    eapply store_wb_equiv_cube_not_locked; eauto using check_process_of_addr_impl, tcache_locked_dirty_correct.
    eapply lock_wb_equiv_cube_not_locked; eauto.
    eapply unlock_wb_equiv_cube_not_locked; eauto.
  - intros im m1 m2 mi m3 t1 v1 id L1 TME.
    simpl.
    inv TME.
    destruct m3, mi; simpl in *;
    unfold option_bind in *; repeat destr_in L1; inv L1.
    + repeat destr. subst.
      generalize (load_wb_equiv addr H0 H Heqp Heqp0). intros (CEQ2 & MEQ2 & -> & ?).
      eexists _, _; split; eauto. split. constructor; auto. constructor; intros. intuition congruence.
    + repeat destr. subst.
      generalize (store_wb_equiv addr v H0 H Heqp Heqp0). intros (CEQ2 & MEQ2 & ->).
      eexists _, _; split; eauto. constructor; auto. constructor; auto. constructor.
    + edestruct (lock_wb_equiv addr H0 H Heqo0) as (m2' & c2' & LOCK & CEQ2 & MEQ2).
      rewrite LOCK.
      eexists _, _; split; eauto. constructor; auto. constructor; auto. constructor.
    + repeat destr. subst.
      generalize (unlock_wb_equiv addr H0 H Heqp Heqp0). intros (CEQ2 & MEQ2 & ->).
      eexists _, _; split; eauto. constructor; auto. constructor; auto. constructor.
  - intros. destruct m, mi; simpl in *; unfold option_bind in *; repeat destr_in L; inv L; eauto using check_process_of_addr_impl.
Qed.


Theorem tie_target_cache_wt around:
  target_interface_equiv (target_cache_wt around).
Proof.
  constructor.
  - intros im m1 m2 mi1 mi2 m3 m4 t1 t2 v1 v2 L1 L2 TME LS SK PERM1 PERM2 TL1 TL2 TLD1 TLD2.
    destruct mi1; simpl in LS, SK; destruct mi2; try easy.
    simpl in *. destruct m1, m2, m3, m4; simpl in *.
    unfold option_bind in *. repeat destr_in L1; inv L1. repeat destr_in L2; inv L2.
    eapply load_wb_equiv_cube_locked; eauto.
    simpl in *. destruct m1, m2, m3, m4; simpl in *.
    unfold option_bind in *. repeat destr_in L1; inv L1. repeat destr_in L2; inv L2.
    eapply store_wt_equiv_cube_locked; eauto using check_process_of_addr_impl, tcache_locked_dirty_correct.
  - intros im m1 m2 mi1 mi2 m3 m4 t1 t2 v1 v2 L1 L2 TME SK PERM1 PERM2 EQts.
    destruct m1, m2, m3, m4; simpl in *.
    destruct mi1; simpl in SK; destruct mi2; try easy;
      unfold option_bind in L1, L2;
      repeat destr_in L1; inv L1;
      repeat destr_in L2; inv L2;
      simpl in *.
    eapply load_wb_equiv_cube_not_locked; eauto.
    eapply store_wt_equiv_cube_not_locked; eauto using check_process_of_addr_impl, tcache_locked_dirty_correct.
    eapply lock_wb_equiv_cube_not_locked; eauto.
    eapply unlock_wt_equiv_cube_not_locked; eauto.
  - intros im m1 m2 mi m3 t1 v1 id L1 TME.
    simpl.
    inv TME.
    destruct m3, mi; simpl in *;
    unfold option_bind in *; repeat destr_in L1; inv L1.
    + repeat destr. subst.
      generalize (load_wb_equiv addr H0 H Heqp Heqp0). intros (CEQ2 & MEQ2 & -> & ?).
      eexists _, _; split; eauto. split. constructor; auto. constructor; intuition congruence.
    + repeat destr. subst.
      generalize (store_wt_equiv _ addr v H0 H Heqp Heqp0). intros (CEQ2 & MEQ2 & ->).
      eexists _, _; split; eauto. constructor; auto. constructor; auto. constructor.
    + edestruct (lock_wb_equiv addr H0 H Heqo0) as (m2' & c2' & LOCK & CEQ2 & MEQ2).
      rewrite LOCK.
      eexists _, _; split; eauto. constructor; auto. constructor; auto. constructor.
    + repeat destr. subst.
      generalize (unlock_wt_equiv addr H0 H Heqp Heqp0). intros (CEQ2 & MEQ2 & ->).
      eexists _, _; split; eauto. constructor; auto. constructor; auto. constructor.
  - intros. destruct m, mi; simpl in *; unfold option_bind in *; repeat destr_in L; inv L; eauto using check_process_of_addr_impl.
Qed.

Section TIE.
  Variable tci: (input -> mem -> Prop) -> mem_interface (list target_event) mem_input mem_output (mem * tcache).
  Hypothesis TIE: target_interface_equiv tci.
  Variable im: input -> mem -> Prop.
  Definition tc := tci im.

  Lemma target_cache_preserves_equiv2
    : forall (rt1 rt2 : mem * tcache) (t : list target_event) (mi : mem_input) (p : process)
             (rt1' : mem * tcache) (mo : mem_output),
      target_mem_equiv rt1 rt2 ->
      mi_interface tc p rt1 mi = Some (rt1', mo, t) ->
      forall rt2' mo' t' ,
        mi_interface tc p rt2 mi = Some (rt2', mo', t') ->
        t = t' /\ target_mem_equiv rt1' rt2' /\
          option_rel (fun mo mo' => (perm (addr_of_mi mi) = rx \/ process_of_addr (addr_of_mi mi) = Some A -> mo = mo')) mo mo'.
  Proof.
    intros.
    edestruct (tie_preserves_equiv TIE) as (rt3' & mo2' & MI & EQ & LOAD). 2: eauto. eauto.
    setoid_rewrite H1 in MI. inv MI. auto.
  Qed.

  Lemma get_instr_core_equiv_target:
    forall  rsd1 rsd2 id m1 m2 i1 i2 t1 t2 m3 m4,
      rsd1 PC = rsd2 PC ->
      target_mem_equiv m1 m2 ->
      get_instr tc id (StateC rsd1 m1) i1 t1 m3 ->
      get_instr tc id (StateC rsd2 m2) i2 t2 m4 ->
      i1 = i2 /\ t1 = t2 /\ target_mem_equiv m3 m4.
  Proof.
    intros rsd1 rsd2 id m1 m2 i1 i2 t1 t2 m3 m4 EQPC TME GI1 GI2.
    inv GI1. inv GI2.
    simpl in H2, H4.
    unfold option_bind in H2, H4. rewrite EQPC in H2.
    generalize (target_cache_preserves_equiv2 (mi_load (rsd2 PC)) id TME H2 H4).
    intros (A & B & C). simpl. subst.  simpl in *. intuition. inv C. intuition congruence.
  Qed.

  Lemma core_step_target_determ:
    forall id sigma r1 r2 t t' sigma1 sigma2 r3 r4,
      id = A ->
      core_step tc id (StateC sigma r1) t (StateC sigma1 r3) ->
      core_step tc id (StateC sigma r2) t' (StateC sigma2 r4) ->
      target_mem_equiv r1 r2 ->
      t = t' /\ (forall r, sigma1 r = sigma2 r) /\ target_mem_equiv r3 r4.
  Proof.
    intros id sigma r1 r2 t t' sigma1 sigma2 r3 r4 IAMA S1 S2 TME.
    inv S1; inv S2;
      destruct (get_instr_core_equiv_target eq_refl TME GI GI0) as (EQi & EQt & TME2); inv EQi; simpl in *; clear GI GI0 TME.
    - split; auto.
    - split; auto.
    - generalize (target_cache_preserves_equiv2 (mi_load (sigma rs0)) A TME2 H6 H8).
      intros (-> & TNE3 & LOADVAL). clear TME2. split; auto. split; auto.
      inv LOADVAL. trim H1. right. eapply tie_process; eauto. subst.
      intros. apply set_reg_ext. auto.
    - generalize (target_cache_preserves_equiv2 (mi_store (sigma rs0) (sigma rs3)) A TME2 H5 H7).
      intros (-> & TNE3 & LOADVAL). clear TME2. split; auto.
    - generalize (target_cache_preserves_equiv2 (mi_lock (sigma rs0)) A TME2 H5 H6).
      intros (-> & TNE3 & LOADVAL). clear TME2. split; auto.
    - generalize (target_cache_preserves_equiv2 (mi_unlock (sigma rs0)) A TME2 H5 H6).
      intros (-> & TNE3 & LOADVAL). clear TME2. split; auto.
  Qed.

  Lemma get_instr_core_equiv_target_ex:
    forall rsd1 rsd2 m1 m2 id i1 t1 m3,
      rsd1 PC = rsd2 PC ->
      target_mem_equiv m1 m2 ->
      get_instr tc id (StateC rsd1 m1) i1 t1 m3 ->
      exists m4,
        get_instr tc id (StateC rsd2 m2) i1 t1 m4 /\
          target_mem_equiv m3 m4.
  Proof.
    intros rsd1 rsd2 m1 m2 id i1 t1 m3 EQPC TME GI1.
    inv GI1.
    generalize (tie_preserves_equiv TIE _ _ _ H2 TME). intros (m4 & v2 & MI & TME2 & LOADVAL).
    inv LOADVAL. trim H0. simpl.  auto. subst.
    destruct m4.
    eexists (_, _). split.
    econstructor. reflexivity. rewrite <- EQPC. setoid_rewrite MI. eauto. eauto. congruence. auto.
  Qed.

  Lemma get_instr_core_equiv_target_ex':
    forall rsd1 rsd2 m1 m2 id i1 t1 m3,
      rsd1 PC = rsd2 PC ->
      target_mem_equiv m2 m1 ->
      get_instr tc id (StateC rsd2 m1) i1 t1 m3 ->
      exists m4,
        get_instr tc id (StateC rsd1 m2) i1 t1 m4 /\
          target_mem_equiv m3 m4.
  Proof.
    intros rsd1 rsd2 m1 m2 id i1 t1 m3 EQPC TME GI.
    symmetry in TME.
    eapply get_instr_core_equiv_target_ex. 3: eauto. all: eauto.
  Qed.

  Lemma step_impl_target_attacker:
    forall r' t1 sigma1' r'0 ss1 r,
      core_step tc A (StateC ss1 r') t1 (StateC sigma1' r'0) ->
      target_mem_equiv r r' ->
      exists sigma1 r0 t1' ,
        core_step tc A (StateC ss1 r) t1' (StateC sigma1 r0).
  Proof.
    intros r' t1 sigma1' r'0 ss1 r STEP TME.
    inv STEP; edestruct (get_instr_core_equiv_target_ex') as ((?&?) & GI' & TME2); eauto.
    - eexists _, _, _. eapply core_step_arith; eauto.
    - eexists _, _, _. eapply core_step_jump; eauto.
    - edestruct (tie_preserves_equiv TIE) as (rt2' & mo' & MI & MEQ2 & LOADVAR); eauto.
      inv LOADVAR. trim H0. simpl. apply tie_process in MI; eauto. subst.
      eexists _, _, _. eapply core_step_load; eauto.
    - edestruct (tie_preserves_equiv TIE) as (rt2' & mo' & MI & MEQ2 & LOADVAR). eauto. eauto. inv LOADVAR.
      eexists _, _, _. eapply core_step_store; eauto.
    - edestruct (tie_preserves_equiv TIE) as (rt2' & mo' & MI & MEQ2 & LOADVAR). eauto. eauto. inv LOADVAR.
      eexists _, _, _. eapply core_step_lock; eauto.
    - edestruct (tie_preserves_equiv TIE) as (rt2' & mo' & MI & MEQ2 & LOADVAR). eauto. eauto. inv LOADVAR.
      eexists _, _, _. eapply core_step_unlock; eauto.
  Qed.

  Lemma target_cube:
    forall M3 M4 M3' M4' mi1 mi2 ov3 ov4 tm3 tm4,
      same_kind mi1 mi2 ->
      (forall addr1 v1 addr2 v2, mi1 = mi_store addr1 v1  -> mi2 = mi_store addr2 v2 -> perm addr1 = rw /\ perm addr2 = rw) ->
      mi_interface tc D M3 mi1 = Some (M3', ov3, tm3) ->
      mi_interface tc D M4 mi2 = Some (M4', ov4, tm4) ->
      target_mem_equiv M3 M4 ->
      tcache_locked_dirty (snd M3) ->
      tcache_locked_dirty (snd M4) ->
      (is_load_or_store mi1 /\ tcache_is_locked (snd M3) (addr_of_mi mi1) && tcache_is_locked (snd M4) (addr_of_mi mi2) = true) \/ addr_to_tagset (addr_of_mi mi1) = addr_to_tagset (addr_of_mi mi2) ->
      target_mem_equiv M3' M4' /\ tm3 = tm4.
  Proof.
    intros M3 M4 M3' M4' mi1 mi2 ov3 ov4 tm3 tm4 SK PERMS TMI1 TMI2 TME TLD1 TLD2 SAME.
    destruct M3, M4. simpl in *.
    destruct mi1, mi2; simpl in *; destruct SK;
      unfold option_bind in *.
    - destruct SAME. rewrite andb_true_iff in H. destruct H as (_ & L1 & L2).
      eapply tie_preserves_equiv_locked; eauto using tcache_is_locked_tagset_locked; simpl; auto; try easy.
      eapply tie_preserves_equiv_same_tagset; eauto; simpl; auto; try easy.
    - destruct SAME. rewrite andb_true_iff in H. destruct H as (_ & L1 & L2).
      specialize (PERMS _ _ _ _ eq_refl eq_refl).
      eapply tie_preserves_equiv_locked; eauto using tcache_is_locked_tagset_locked; simpl; auto; try easy.
      specialize (PERMS _ _ _ _ eq_refl eq_refl).
      eapply tie_preserves_equiv_same_tagset; eauto; simpl; auto; try easy.
    - destruct SAME as [SAME|SAME]. easy.
      eapply tie_preserves_equiv_same_tagset; eauto; simpl; auto; try easy.
    - destruct SAME as [SAME|SAME]. easy.
      eapply tie_preserves_equiv_same_tagset; eauto; simpl; auto; try easy.
  Qed.

End TIE.
