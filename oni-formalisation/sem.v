From formal Require Import oni.
Require Import List.
Require Import OrderedType.
Require Import Lia.
Import ListNotations.
Require Import Bool.
Import BoolNotations.
Require Import Permutation.
Set Implicit Arguments.
From formal Require Import Util tactics CacheTypes syntax coresem mem.
From formal Require Import cacheless_intf source_intf target_intf.
Open Scope nat_scope.

From formal Require Import target_equiv sim_source_target.


Parameter init_mem: input -> mem -> Prop.
Parameter init_memA: input -> mem -> Prop.
Parameters init_state init_stateA: input -> regset -> Prop.
Parameter phi0: regset * mem -> regset * mem -> Prop.
Parameter sch_st: Type.
Parameter sch: scheduler (list (core_event + list target_event)) sch_st.
Axiom phi0_pc:
  forall r m r0 m0, phi0 (r, m) (r0, m0) -> r PC = r0 PC /\ mem_equiv m m0.

Axiom phi0_sim_mem:
  forall sigma1 m sigma0 m0 m1 m2 t1 t2,
    phi0 (sigma1, m) (sigma0, m0) ->
    sim_mem m1 m t1 ->
    sim_mem m2 m0 t2 ->
    phi0 (sigma1, m1) (sigma0, m2).

Lemma sim_r_inj : forall r1 r2 rho,
    sim_r r1 rho -> sim_r r2 rho -> equiv_scache r1 r2.
Proof.
  intros r1 r2 rho SIM1 SIM2.
  unfold sim_r in SIM1, SIM2.
  red.
  intros ts. destruct ts.
  apply ZMicromega.eq_true_iff_eq.
  rewrite SIM1, SIM2 by auto. tauto.
Qed.

Lemma source_target_cube:
  forall (tc: _ -> mem_interface (list target_event) _ _ _) (TIE: target_interface_equiv tc) M1 M2 M3 M4 M1' M2' M3' M4' mi1 mi2 ov1 ov2 ov3 ov4 tm tm3 tm4,
    same_kind mi1 mi2 ->
    (forall addr1 v1 addr2 v2, mi1 = mi_store addr1 v1  -> mi2 = mi_store addr2 v2 -> perm addr1 = rw /\ perm addr2 = rw) ->
    source_mem_interface D M1 mi1 = Some (M1', ov1, tm)  ->
    source_mem_interface D M2 mi2 = Some (M2', ov2, tm)  ->
    mi_interface (tc init_mem) D M3 mi1 = Some (M3', ov3, tm3) ->
    mi_interface (tc init_mem) D M4 mi2 = Some (M4', ov4, tm4) ->
    source_mem_equiv M1 M2 ->
    target_mem_equiv M3 M4 ->
    sim_source_target M1 M3 ->
    sim_source_target M2 M4 ->
    source_mem_equiv M1' M2' /\
      target_mem_equiv M3' M4' /\ tm3 = tm4.
Proof.
  intros ? TIE M1 M2 M3 M4 M1' M2' M3' M4' mi1 mi2 ov1 ov2 ov3 ov4 tm tm3 tm4 SK PERMS SMI1 SMI2 TMI1 TMI2 SME TME SIM1 SIM2.
  destruct M1, M2, M3, M4.
  eapply source_mem_interface_equiv in SME. 4: apply SMI1. 4: apply SMI2. all: eauto.
  simpl in SME.
  destruct SME as (SME & SAME & POA1 & POA2). split; auto.
  erewrite slocked_tcache_is_locked in SAME. 2: simpl in SIM1; intuition eauto. 2: eauto.
  erewrite slocked_tcache_is_locked in SAME. 2: simpl in SIM2; intuition eauto. 2: eauto.
  eapply target_cube; eauto. all: simpl in *; intuition.
Qed.

Lemma source_same_trace_cases:
  forall mi mi' m1 m2 m3 m4 v0 v1 t
         (SK: same_kind mi mi')
         (SMI1 : source_mem_interface D m1 mi = Some (m3, v0, t))
         (SMI2 : source_mem_interface D m2 mi' = Some (m4, v1, t)),
    (is_load_or_store mi /\ tagset_in_scache (addr_to_tagset (addr_of_mi mi)) (snd m1) = true /\ tagset_in_scache (addr_to_tagset (addr_of_mi mi')) (snd m2) = true) \/
      (addr_to_tagset (addr_of_mi mi) = addr_to_tagset (addr_of_mi mi')) /\ process_of_addr (addr_of_mi mi) = Some D /\ process_of_addr (addr_of_mi mi') = Some D.
Proof.
  intros. destruct mi, mi'; simpl in SK; try easy.
  - destruct m1, m2; simpl in *; unfold option_bind in *.
    dinv SMI1. dinv SMI2. dinv Heqo. dinv Heqo0.
    dinv H2. auto. right; eauto using check_process_of_addr_impl.
  - destruct m1, m2; simpl in *; unfold option_bind in *.
    dinv SMI1. dinv SMI2. dinv Heqo. dinv Heqo0.
    dinv H2. auto. right; eauto using check_process_of_addr_impl.
  - destruct m1, m2; simpl in *; unfold option_bind in *.
    dinv SMI1. dinv SMI2. dinv Heqo. dinv Heqo0. dinv Heqo1. dinv Heqo2.
    right; eauto using check_process_of_addr_impl.
  - destruct m1, m2; simpl in *; unfold option_bind in *.
    dinv SMI1. dinv SMI2. dinv Heqo. dinv Heqo0.
    right; eauto using check_process_of_addr_impl.
Qed.

Definition target_interface_wt (tc: mem_interface (list target_event) mem_input mem_output (mem * tcache)) :=
  forall id m1 mi m2 v t,
    mi_interface tc id m1 mi = Some (m2, v, t) ->
    match mi with
      mi_load _ => v <> None
    | _ => v = None
    end.

Lemma tiw_wb im:
  target_interface_wt (target_cache im).
Proof.
  red.
  intros. destruct m1, m2.
  destruct mi; simpl in H; unfold option_bind in *; simpl in *; dinv H; congruence.
Qed.

Lemma tiw_wt around im:
  target_interface_wt (target_cache_wt around im).
Proof.
  red.
  intros. destruct m1, m2.
  destruct mi; simpl in H; unfold option_bind in *; simpl in *; dinv H; congruence.
Qed.

Definition target_interface_progress (tc: mem_interface (list target_event) mem_input mem_output (mem * tcache)) :=
  forall mi mi' M1 M2 v tm M3
    (TMI: mi_interface tc D M1 mi' = Some (M3, v, tm))
    (MEQ : target_mem_equiv M1 M2)
    (SPEC: forall addr, mi = mi_lock addr -> addr_to_tagset (addr_of_mi mi) = addr_to_tagset (addr_of_mi mi'))
    (POA: check_process_of_addr (addr_of_mi mi) D = Some tt)
    (SK: same_kind mi mi'),
  exists m4 t4 v4 tm4,
    mi_interface tc D M2 mi = Some (m4, t4, v4, tm4).

Lemma tie_wb im:
  target_interface_progress (target_cache im).
Proof.
  red.
  intros. destruct M1, M2.
  destruct mi, mi'; simpl in SK; try easy; simpl in TMI; unfold option_bind in *; simpl in *; dinv TMI.
  - simpl. rewrite POA; simpl. repeat destr. eexists _, _, _, _; split; eauto.
  - simpl. rewrite POA; simpl. repeat destr. eexists _, _, _, _; split; eauto.
  - simpl. rewrite POA; simpl.
    destruct (lock_wb m0 t0 addr) as [((m7, t6),tm6)|] eqn:?; simpl.
    eexists _, _, _, _; split; eauto.
    unfold lock_wb in Heqo0. dinv Heqo0.
    unfold lock_wb in Heqo1. dinv Heqo1.
    exfalso. revert Heqb0 Heqb. (* destruct SPEC as [|SPEC]. easy. *)
    unfold tcache_is_locked, num_lockable_in_set. specialize (SPEC _ eq_refl).
    rewrite ! SPEC. destr.
    inv MEQ.
    generalize (H4 s). intros (A, B). destruct (t0 s) eqn:?, (t s) eqn:?. simpl in *. subst.
    setoid_rewrite (line_of_tag_equiv_ways t1 B).
    rewrite (locked_tags_equiv _ H4).
    destr. 2: congruence.
    specialize (B l). destruct s1, s0; simpl in *. inv B. congruence.
    red in H1. destruct_and H1.
    congruence.
  - simpl. rewrite POA. simpl. repeat destr. eexists _, _, _, _; split; eauto.
Qed.

Lemma tie_wt im around:
  target_interface_progress (target_cache_wt im around).
Proof.
  red.
  intros. destruct M1, M2.
  destruct mi, mi'; simpl in SK; try easy; simpl in TMI; unfold option_bind in *; simpl in *; dinv TMI.
  - simpl. rewrite POA; simpl. repeat destr. eexists _, _, _, _; split; eauto.
  - simpl. rewrite POA; simpl. repeat destr. eexists _, _, _, _; split; eauto.
  - simpl. rewrite POA; simpl.
    destruct (lock_wb m0 t0 addr) as [((m7, t6),tm6)|] eqn:?; simpl.
    eexists _, _, _, _; split; eauto.
    unfold lock_wb in Heqo0. dinv Heqo0.
    unfold lock_wb in Heqo1. dinv Heqo1.
    exfalso. revert Heqb0 Heqb. (* destruct SPEC as [|SPEC]. easy. *)
    unfold tcache_is_locked, num_lockable_in_set. specialize (SPEC _ eq_refl).
    rewrite ! SPEC. destr.
    inv MEQ.
    generalize (H4 s). intros (A, B). destruct (t0 s) eqn:?, (t s) eqn:?. simpl in *. subst.
    setoid_rewrite (line_of_tag_equiv_ways t1 B).
    rewrite (locked_tags_equiv _ H4).
    destr. 2: congruence.
    specialize (B l). destruct s1, s0; simpl in *. inv B. congruence.
    red in H1. destruct_and H1.
    congruence.
  - simpl. rewrite POA. simpl. repeat destr. eexists _, _, _, _; split; eauto.
Qed.

Lemma get_instr_core_equiv_source:
  forall init_mem rsd1 rsd2 sm1 sm2 id i1 i2 t1 t2 m3 m4,
    rsd1 PC = rsd2 PC ->
    source_mem_equiv sm1 sm2 ->
    get_instr (source_cache init_mem) id (StateC rsd1 sm1) i1 t1 m3 ->
    get_instr (source_cache init_mem) id (StateC rsd2 sm2) i2 t2 m4 ->
    i1 = i2 /\ t1 = t2 /\ source_mem_equiv m3 m4.
Proof.
  intros init_mem0 rsd1 rsd2 sm1 sm2 id i1 i2 t1 t2 m3 m4 EQPC SME GI1 GI2.
  inv GI1. inv GI2.
  simpl in H2, H4.
  inv SME. unfold source_mem_interface in H2, H4. simpl in *.
  unfold option_bind in H2, H4. rewrite EQPC in H2.
  destruct (check_process_of_addr (rsd2 PC) id) eqn:?; inv H2. inv H4.
  erewrite me_same_code in H3; eauto.
  assert (i1 = i2) by congruence. subst. rewrite H0.
  split; auto. split; auto. constructor; auto.
Qed.

Lemma get_instr_sim_left  init_mem:
  forall sm tm rs i ti m' ,
    sim_source_target sm tm ->
    get_instr (source_cache init_mem) D (StateC rs sm) i ti m' ->
    sim_source_target m' tm.
Proof.
  intros sm tm rs i ti m' SIM GI1. inv GI1. destruct sm, tm. simpl in *.
  unfold option_bind in *.
  repeat destr_in H2; inv H2.
  repeat destr_in Heqo; inv Heqo.
  simpl. intuition.
Qed.

Lemma source_mem_interface_process:
  forall id M mi x,
    source_mem_interface id M mi = Some x ->
    check_process_of_addr (addr_of_mi mi) id = Some tt.
Proof.
  destruct M; simpl; unfold option_bind; simpl; intros.
  dinv H. destruct mi; simpl in *; unfold option_bind in *; simpl in *; dinv Heqo; auto; destruct u; auto.
Qed.


Opaque target_mem_interface.
Opaque source_mem_interface.


Lemma line_of_tag_eq:
  forall w (W: wf_ways w) l cl t,
    w l = Some cl -> cl_tag cl = t ->
    line_of_tag w t = Some l.
Proof.
  intros.
  apply line_of_tag_spec. auto. eauto.
Qed.

Lemma step_impl_target
  (tc: _ -> mem_interface (list target_event) _ mem_output _)
  (TIP: target_interface_progress (tc init_mem))
  (TIW: target_interface_wt (tc init_mem))
  (TIE: target_interface_equiv tc)
  (TIS: target_interface_bsim tc)
  :
  forall ss1' r' t1 sigma1' r'0 ss1 r rs1 rs1' sd1 sd2 t,
    core_step (source_cache init_mem) D (StateC ss1 rs1) t sd1 ->
    core_step (source_cache init_mem) D (StateC ss1' rs1') t sd2 ->
    sim_source_target rs1 r ->
    sim_source_target rs1' r' ->
    core_step (tc init_mem) D (StateC ss1' r') t1 (StateC sigma1' r'0) ->
    ss1 PC = ss1' PC ->
    target_mem_equiv r r' ->
    source_mem_equiv rs1 rs1' ->
    exists sigma1 r0 t1' ,
      core_step (tc init_mem) D (StateC ss1 r) t1' (StateC sigma1 r0).
Proof.
  intros ss1' r' t1 sigma1' r'0 ss1 r rs1 rs1' sd1 sd2 t SS1 SS2 SIM1 SIM2 H H0 H1 SME.
  destruct r.
  destruct r'. destruct r'0. simpl in *.
  inv H;
    edestruct (get_instr_core_equiv_target_ex' TIE) as ((?&?) & GI' & TME2); eauto;
    simpl in *;
    inv SS2;
    match goal with
      GIS: get_instr (source_cache _) _ (StateC _ ?m) _ _ _,
        SIM: sim_source_target ?m ?tm,
          GIT: get_instr _ _ (StateC _ ?tm) _ _ _ |- _ =>
        edestruct (get_instr_sim TIS SIM GIS GIT)  as (EQi & SST); eauto; inv EQi 
    end;
    inv SS1;
    edestruct (get_instr_core_equiv_source H0 SME GI1 GI0) as (EQi & EQt & SMEi); inv EQi.
  - eexists _, _, _. eapply core_step_arith; eauto.
  - eexists _, _, _. eapply core_step_jump; eauto.
  - simpl in *.
    destruct M', M'', M'0.
    rewrite ! map_app in H4. apply app_inv_head in H4. simpl in H4. inv H4.
    edestruct TIP as (? & ? & ? & ? & TMI).
    eauto. apply TME2. instantiate (1 := mi_load (ss1 (rs))). intuition congruence.
    eapply source_mem_interface_process in H11; eauto.
    simpl; auto.
    generalize (TIW _ _ _ _ _ _ TMI). destruct x1; try congruence. intros _.
    eexists _, _, _. eapply core_step_load; eauto.
  - simpl in *.
    destruct M', M'', M'0.
    rewrite ! map_app in H3. apply app_inv_head in H3. simpl in H3. inv H3.
    edestruct TIP as (? & ? & ? & ? & TMI).
    now eauto. now apply TME2.
    instantiate (1:= mi_store (ss1 rs0) (ss1 rs2)). intuition congruence.
    eapply source_mem_interface_process in H6; eauto.
    simpl; auto.
    eexists _, _, _. eapply core_step_store; eauto. rewrite TMI.
    generalize (TIW _ _ _ _ _ _ TMI). intros ->.
    eauto.
  - simpl in *.
    destruct M', M'', M'0.
    rewrite ! map_app in H3. apply app_inv_head in H3. simpl in H3. inv H3.
    edestruct TIP as (? & ? & ? & ? & TMI).
    now eauto. now apply TME2.
    instantiate (1:= mi_lock (ss1 rs0)). intros ? EQ; inv EQ.
    simpl.
    generalize (fun pf => source_same_trace_cases _ _ _ _ pf H6 H7). simpl. intros [C|D]. auto.
    easy. intuition.
    eapply source_mem_interface_process in H7; eauto.
    simpl; auto.
    eexists _, _, _. eapply core_step_lock; eauto. rewrite TMI; eauto.
    generalize (TIW _ _ _ _ _ _ TMI). intros ->.
    eauto.
  - simpl in *.
    destruct M', M'', M'0.
    rewrite ! map_app in H3. apply app_inv_head in H3. simpl in H3. inv H3.
    edestruct TIP as (? & ? & ? & ? & TMI).
    now eauto. now apply TME2.
    instantiate (1:= mi_unlock (ss1 rs0)). intuition congruence.
    eapply source_mem_interface_process in H7; eauto.
    simpl; auto.
    eexists _, _, _. eapply core_step_unlock; eauto. rewrite TMI.
    generalize (TIW _ _ _ _ _ _ TMI). intros ->.
    eauto.
Qed.

Lemma bsim1
  tc
  (TIB: target_interface_bsim tc )
  :
  bsim (memprog init_state (source_cache init_mem) D (step_hole))
    (memprog init_state (tc init_mem) D (step_hole))
    (fun '(s1, r1) '(s2, r2) => s1 = s2 /\ sim_source_target r1 r2).
Proof.
  edestruct bsim_res. eauto.
  constructor.
  - intros (rs1, m1) (rs2, m2) (srs1, sm1) t (-> & SIM) STEP.
    simpl in STEP.
    apply step_hole_mi in STEP.
    cut (exists mb t',
            core_step (source_cache init_mem) D (StateC rs1 sm1) t' (StateC rs2 mb)
            /\ sim_source_target mb m2).
    {
      intros (mb & t' & CS & SIM2).
      eexists _, _; split.
      apply step_hole_mi_inv. eauto. simpl. auto.
    } destruct sm1, m1. red in SIM. destruct_and SIM.
    destruct m2.
    inv STEP; edestruct get_instr_sim2 as (ti' & (? & ?) & GI' & SIM); simpl in *; eauto.
    + simpl in *.
      eexists _, _. split. eapply core_step_arith; eauto. simpl in *; eauto.
    + eexists _, _. split. eapply core_step_jump; eauto. simpl in *; auto.
    + destruct M'. edestruct bsim_res_step with (rs:=(m2,s0)) as (rs' & t' & MI' & SIM'); eauto.
      simpl in *; eauto. destruct rs'.
      eexists _, _. split. eapply core_step_load; eauto. auto.
    + destruct M'. edestruct bsim_res_step with (rs:=(m2,s0)) as (rs' & t' & MI' & SIM'); eauto.
      simpl in *; eauto. destruct rs'.
      eexists _, _. split. eapply core_step_store; eauto. auto.
    + destruct M'. edestruct bsim_res_step with (rs:=(m2,s0)) as (rs' & t' & MI' & SIM'); eauto.
      simpl in *; eauto. destruct rs'.
      eexists _, _. split. eapply core_step_lock; eauto. auto.
    + destruct M'. edestruct bsim_res_step with (rs:=(m2,s0)) as (rs' & t' & MI' & SIM'); eauto.
      simpl in *; eauto. destruct rs'.
      eexists _, _. split. eapply core_step_unlock; eauto. auto.
  - simpl; intros. inv H.
    edestruct bsim_res_init as (rs & INIT & SIM). eauto.
    eexists; split. econstructor. eauto. eauto. simpl. auto.
Qed.

Lemma bsim
  tc
  (TIB: target_interface_bsim tc ):
  bsim (memprog init_state (source_cache init_mem) D (step_hole))
    (proj_proc (oni.compose (memprog init_state (tc init_mem) D (step_hole))
                  (memprog init_stateA (tc init_memA) A (step_hole))
                  sch) D) (sim sim_source_target).
Proof.
  edestruct bsim1. eauto.
  split.
  - simpl. intros alpha beta a t SIM STEP.
    inv STEP.
    edestruct bsim_step with(a:=a) as (b & t' & STEP2 & SIM2). 2: apply STEP_D.
    destruct a. destruct SIM. subst. split; auto.
    destruct b. destruct SIM2. subst.
    eexists _, _; split. simpl. simpl in STEP2. eauto.
    simpl. split; auto.
  - simpl. intros. inv H.
    edestruct bsim_init as (a & INIT & SIM). eauto. destruct a. destruct SIM.
    subst.
    eexists; split. simpl in INIT.  eauto.
    simpl; split; auto.
Qed.

Lemma attacker_prop_cache2
  (tc: _ -> mem_interface (list target_event) _ mem_output _)
  (TIE: target_interface_equiv tc)
  (TIS: target_interface_bsim tc)
  :
  attacker_prop (memprog init_stateA (tc init_memA) A step_hole)
    ((fun '(s1,r1) '(s2,r2) => sim_source_target r1 r2) : regset * _ -> _ -> Prop)
    (fun '(s1,r1) '(s2,r2) => (forall r, s1 r = s2 r) /\ target_mem_equiv r1 r2).
Proof.
  intros.
  split.
  - intros (rs1, r1) (rs2, r2) t (rs1', r') STEP (EQ & SIM). subst.
    simpl in STEP. eapply step_hole_mi in STEP.
    edestruct step_impl_target_attacker as (? & ? & ? & CS). 2: eauto. eauto.
    symmetry; eauto.
    generalize (core_step_target_determ TIE _ eq_refl STEP CS SIM). intros (-> & EQr & TME).
    edestruct @core_step_ext as (rs2' & CS' & EXT). apply CS. apply EQ.
    eexists; split. apply step_hole_mi_inv. eauto. simpl. split; auto.
    etransitivity; eauto.
  - intros (rs1, r1) (rs2, r2) (rs1', r') t SIM STEP. subst.
    simpl in STEP. eapply step_hole_mi in STEP.
    eapply attacker_preserves_sim in STEP. apply STEP. auto. auto.
Qed.

Lemma attacker_prop_cache
  (tc: _ -> mem_interface (list target_event) _ mem_output _)
  (TIE: target_interface_equiv tc)
  (TIS: target_interface_bsim tc)
:
  attacker_prop
    (proj_proc
       (oni.compose
          (memprog init_state (tc init_mem) D step_hole)
          (memprog init_stateA (tc init_memA) A step_hole)
          sch)
       A)
    (sim sim_source_target)
    (fun '(s1, s2, r, s) '(s1', s2', r', s') => s1 PC = s1' PC /\ (forall r, s2 r = s2' r) /\ target_mem_equiv r r' /\ s = s').
Proof.
  edestruct attacker_prop_cache2; eauto. constructor; simpl; intros.
  - destruct s1 as (((rsd1, rsa1), m1), s1).
    destruct s2 as (((rsd2, rsa2), m2), s2). simpl in H0. destruct_and H0.  subst.
    inv H.
    edestruct att_equiv with (s2:=(rsa2, m2)) as (s2' & STEP2 & EQ2). apply STEP_A. simpl. auto.
    simpl in EQ2. destruct s2'. destruct_and EQ2.
    eexists; split. econstructor; eauto.
    simpl. split; auto.
  - inv H0.
    destruct a as (rs, (m, sc)). red in H. destruct H; subst.
    eapply (att_bsim) with (a:= (sigma2, (m,sc))) in STEP_A. split; auto. auto.
Qed.


Lemma l2s
  (tc: _ -> mem_interface (list target_event) _ mem_output _)
  (TIE: target_interface_equiv tc)
  (TIP: target_interface_progress (tc init_mem))
  (TIW: target_interface_wt (tc init_mem))
  (TIS: target_interface_bsim tc)
:
  lockstep2sim
    (memprog init_state (source_cache init_mem) D step_hole)
    (proj_proc
       (oni.compose
          (memprog init_state (tc init_mem) D step_hole)
          (memprog init_stateA (tc init_memA) A step_hole)
          sch)
       D)
    (fun '(s1, (m1, c1)) '(s2, (m2, c2)) => phi0 (s1, m1) (s2, m2) /\ equiv_scache c1 c2)
    (fun '(s1, s2, (m,c), s) '(s1', s2', (m', c'), s') => phi0 (s1, m) (s1', m') /\ s2 = s2' /\ c = c' /\ s = s') (equivS (fun r1 r2 : reg -> val => r1 PC = r2 PC) source_mem_equiv)
    (fun '(s1, s2, r, s) '(s1', s2', r', s') => s1 PC = s1' PC /\ (forall r, s2 r = s2' r) /\ target_mem_equiv r r' /\ s = s')
    (sim sim_source_target).
Proof.
  constructor; simpl; intros.
  - unfold sim in *.
    destruct a as (rs1, (sm1, sc1)), a' as (rs2, (sm2, sc2)), b as (rs3, (sm3, sc3)), b' as (rs4, (sm4, sc4)).
    destruct alpha as (((rsd1, rsa1), (m1,tc1)), sch1).
    destruct alpha' as (((rsd2, rsa2), (m2,tc2)), sch2).
    destruct beta as (((rsd3, rsa3), (m3,tc3)), sch3).
    destruct beta' as (((rsd4, rsa4), (m4,tc4)), sch4). inv H.
    destruct_and H2. subst.
    destruct_and H5.
    destruct_and H6.
    destruct_and H7.
    destruct_and H8. subst. Opaque sim_source_target. simpl in *.
    inv H3; inv H4.
    simpl in *. apply step_hole_mi in H0.
    apply step_hole_mi in H1.
    apply step_hole_mi in STEP_D.
    apply step_hole_mi in STEP_D0.
    inv H0; inv H1;
    destruct (get_instr_core_equiv_source H9 H14 GI GI0) as (EQi & EQt & SME); inv EQi;
      simpl in *;
      inv STEP_D;
    destruct ((fun pf => get_instr_sim TIS pf GI GI1) H13) as (EQi & SIM1); inv EQi;
      inv STEP_D0;
      destruct ((fun pf => get_instr_sim TIS pf GI0 GI2) H15) as (EQi & SIM2); inv EQi;
      destruct (get_instr_core_equiv_target TIE _ H9 H10 GI1 GI2) as (EQi & EQt & SME2); inv EQi; simpl in *.
    + repeat match goal with |- _ /\ _ => split end; auto.
      * constructor.
        repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto.
        apply pc_not_zero. apply pc_not_zero. auto.
      * repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
      * apply app_inv_head in H3. inv H3. congruence.
      * apply app_inv_head in H3. inv H3. congruence.
    + repeat match goal with |- _ /\ _ => split end; auto.
      * constructor.
        repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
        apply app_inv_head in H3. inv H3. congruence. auto.
      * repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
        apply app_inv_head in H3. inv H3. congruence.
      * apply app_inv_head in H3. inv H3. congruence.
      * apply app_inv_head in H3. inv H3. congruence.
    +
      rewrite ! map_app in H3; apply app_inv_head in H3. simpl in H3; inv H3.
      generalize (fun pf perms => source_target_cube TIE pf perms H11 H18 H20 H23). intro STC.
      trim STC. simpl; auto.
      trim STC. congruence.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      destruct_and STC. subst.
      repeat match goal with |- _ /\ _ => split end; auto.
      * constructor.
        repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
        auto.
      * repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
    + rewrite ! map_app in H3; apply app_inv_head in H3. simpl in H3; inv H3.
      generalize (fun pf perms => source_target_cube TIE pf perms H8 H7 H6 H20). intro STC.
      trim STC. simpl; auto.
      trim STC. intros addr1 v1 addr2 v2 A B; inv A; inv B. now intuition.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      destruct_and STC. subst.
      repeat match goal with |- _ /\ _ => split end; auto.
      * constructor.
        repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
        auto.
      * repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
    + rewrite ! map_app in H3; apply app_inv_head in H3. simpl in H3; inv H3.
      generalize (fun pf perms => source_target_cube TIE pf perms H8 H7 H6 H11). intro STC.
      trim STC. simpl; auto.
      trim STC. congruence.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      destruct_and STC. subst.
      repeat match goal with |- _ /\ _ => split end; auto.
      * constructor.
        repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto  using pc_not_zero.
        auto.
      * repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
    + rewrite ! map_app in H3; apply app_inv_head in H3. simpl in H3; inv H3.
      generalize (fun pf perms => source_target_cube TIE pf perms H8 H7 H6 H11). intro STC.
      trim STC. simpl; auto.
      trim STC. congruence.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      trim STC. auto.
      destruct_and STC. subst.
      repeat match goal with |- _ /\ _ => split end; auto.
      * constructor.
        repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
        auto.
      * repeat rewrite set_reg_other by congruence.
        unfold nextinstr; rewrite ! set_reg_same, H12; eauto using pc_not_zero.
  - red. simpl. intros.
    repeat destr_in H1. subst. inv H. inv H0.
    destruct H1.
    edestruct phi0_pc. eauto. split; auto. split; auto.
  - red; simpl; intros. destruct a as (((s1 & s2) & r) & s).
    destruct a' as (((s1' & s2') & r') & s'). destruct r, r'.
    destruct_and H1. subst. simpl in *. subst. split; auto.
    eapply phi0_pc; intuition now eauto.
    split; auto.
    constructor; auto. constructor; auto.
    eapply phi0_pc; intuition now eauto.
    apply equiv_tcache_eq.
  - destruct a, a'; simpl. destruct p, p0.
    inv H. inv H0. destruct r1, r2. destruct_and H1. subst.
    simpl in *.
    destruct_and H4. destruct_and H5. subst.
    Transparent sim_source_target.
    simpl in *. destruct_and H11. destruct_and H12.
    split.
    2: eapply sim_r_inj; eauto.
    eapply phi0_sim_mem; eauto.
    Opaque sim_source_target.
  - red in H5.
    red. intros (b0 & t0 & STEP). simpl in STEP.
    inv H1.
    destruct alpha as (((ss1 & ss2) & r) & s).
    destruct alpha' as (((ss1' & ss2') & r') & s').
    destruct_and H4. subst. simpl in H. destruct H; subst.
    simpl in H0. destruct H0; subst. destruct b, b'.
    eapply step_hole_mi in H2. apply step_hole_mi in H3.
    apply H5. clear H5. inv STEP.
    simpl in STEP_D. apply step_hole_mi in STEP_D.
    edestruct step_impl_target as (sigma1 & ? & t1' & STEP). apply TIP. apply TIW. apply TIE. apply TIS.
    apply H2. apply H3. all: eauto.
    eexists _, _. simpl. eapply step_compose_left. eauto. simpl. eapply step_hole_mi_inv. eauto. eauto.
Qed.

Lemma deterministic_memprog:
  forall init_state init_mem (tc: (input -> mem -> Prop) -> mem_interface (list target_event) mem_input mem_output (mem*tcache)) id,
    deterministic (memprog init_state (tc init_mem) id step_hole).
Proof.
  red. intros init_state0 init_mem0 tc id s t t' s' s'' STEP1 STEP2.
  simpl in *. destruct s, s', s''. apply step_hole_mi in STEP1. apply step_hole_mi in STEP2.
  inv STEP1; inv STEP2; destruct (get_instr_determ GI GI0) as (EQi & EQt & EQm); inv EQi; split; auto; try congruence.
Qed.

Theorem oni_cache
  (tc: _ -> mem_interface (list target_event) _ mem_output _)
  (TIE: target_interface_equiv tc)
  (TIP: target_interface_progress (tc init_mem))
  (TIW: target_interface_wt (tc init_mem))
  (TIS: target_interface_bsim tc)
  :
  forall (symphi: Symmetric phi0),
    ONI
      (fun '(s1, (m1, c1)) '(s2, (m2, c2)) => phi0 (s1, m1) (s2, m2) /\ equiv_scache c1 c2)
      (memprog init_state (source_cache init_mem) D (step_hole)) ->
    ONI
      (fun '(s1, s2, (m,c), s) '(s1', s2', (m', c'), s') => phi0 (s1, m) (s1', m') /\ s2 = s2' /\ c = c' /\ s = s')
      (oni.compose (memprog init_state (tc init_mem) D (step_hole))
         (memprog init_stateA (tc init_memA) A (step_hole))
         sch).
Proof.
  intros.
  eapply proj_oni_pres with
    (equivT := (fun '(s1, s2, r, s) '(s1', s2', r', s') => s1 PC = s1' PC /\ (forall r, s2 r = s2' r) /\ target_mem_equiv r r' /\ s = s'))
    (equivS := equivS (fun r1 r2 => r1 PC = r2 PC) source_mem_equiv)
  . 8: eauto.
  - red. intros (((rsd1, rsa1), (m1, c1)), s1) (((rsd2, rsa2), (m2, c2)), s2). intuition.
  - red. intros (((rsd1, rsa1), (m1, c1)), s1) (((rsd2, rsa2), (m2, c2)), s2). simpl. intuition eauto. symmetry; auto.
  - apply bsim. auto.
  - red. simpl.
    intros (((rsd1,rsa1), (m1,tc1)), st1) (((rsd2,rsa2), (m2,tc2)), st2). simpl.
    intros tau tau' beta beta' EQS STEP1 STEP2. destruct_and EQS; subst.
    inv STEP1; inv STEP2; simpl; congruence.
  - apply attacker_prop_cache. auto. auto.
  - apply compose_deterministic.
    apply deterministic_memprog.
    apply deterministic_memprog.
  - apply l2s; auto.
Qed.

Print Assumptions oni_cache.

