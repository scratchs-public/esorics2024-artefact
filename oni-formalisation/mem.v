From formal Require Import syntax CacheTypes oni tactics Util.
Require Import RelationClasses.

Parameter process_of_tag: tagid -> option process.

Definition process_of_addr (addr: val) : option process :=
  process_of_tag (fst (addr_to_tagset addr)).

Definition check_process_of_addr addr p :=
  if eq_dec (Some p) (process_of_addr addr)
  then Some tt else None.

Lemma check_process_of_addr_impl:
  forall addr p u,
    check_process_of_addr addr p = Some u -> process_of_addr addr = Some p.
Proof.
  unfold check_process_of_addr; intros addr p u CPOA; destr_in CPOA; inv CPOA; auto.
Qed.

Definition mem := val -> val.

Record mem_equiv (M M': mem) :=
  {
    me_same_code:
    forall addr,
      perm addr = rx \/ process_of_addr addr = Some A ->
      M addr = M' addr;
  }.

Lemma mem_equiv_sym: forall s1 s2, mem_equiv s1 s2 -> mem_equiv s2 s1.
Proof.
  intros s1 s2 CE. inv CE; constructor.
  intros addr PERM.
  erewrite me_same_code0; eauto.
Qed.

Instance mem_equiv_eq:
  Equivalence mem_equiv.
Proof.
  constructor.
  - constructor. auto.
  - constructor. simpl. intros. inv H. erewrite me_same_code0; eauto.
  - constructor. intros. inv H; inv H0.
    erewrite me_same_code0, me_same_code1; eauto.
Qed.

Definition do_store (m: mem) (addr: val) (v: val) : mem :=
  fun a => if eq_dec a addr then v else m a.

Lemma mem_equiv_do_store:
  forall m1 m2 addr1 v1 addr2 v2 u1 u2,
    perm addr1 = rw -> perm addr2 = rw ->
    check_process_of_addr addr1 D = Some u1 ->
    check_process_of_addr addr2 D = Some u2 ->
    mem_equiv m1 m2 ->
    mem_equiv (do_store m1 addr1 v1) (do_store m2 addr2 v2).
Proof.
  intros m1 m2 addr1 v1 addr2 v2 u1 u2 P1 P2 POA1 POA2 ME; inv ME; constructor.
  - simpl; intros; eauto.
    specialize (me_same_code0 _ H).
    unfold do_store. destruct H.
    repeat destr; subst; try congruence.
    repeat destr; subst; apply check_process_of_addr_impl in POA1; apply check_process_of_addr_impl in POA2;
      congruence.
Qed.
