From formal Require Import CacheTypes tactics.
Parameter reg: Type.
Parameter aop: Type.
Parameter jop: Type.

Inductive instr :=
  arith  (rd rs1 rs2: reg) (op: aop)
| jump   (rd rs1 rs2: reg) (op: jop)
| load   (rd rs: reg)
| store  (rd rs: reg)
| lock   (rs: reg)
| unlock (rs: reg).


(* Values [val] are stored in registers and memory. *)
Parameter val: Type.
Parameters PC R0: reg.
Axiom pc_not_zero: PC <> R0.

Parameter sem_aop: aop -> val -> val -> val.
Parameter sem_jop: jop -> val -> val -> val -> val.

Definition regset := reg -> val.

Inductive permission := rx | rw.
Definition memperm := val -> permission.

Parameter perm: memperm.
Parameter addr_to_tagset: val -> tagset.
Parameter tagset_to_addr: tagset -> val.

Axiom addr_to_tagset_to_addr:
  forall t s,
    addr_to_tagset (tagset_to_addr (t, s)) = (t,s).

Axiom perm_tagset_to_addr_to_tagset:
  forall addr,
    perm (tagset_to_addr (addr_to_tagset addr)) = perm addr.

Inductive mem_input :=
  mi_load (addr: val)
| mi_store (addr v: val)
| mi_lock (addr: val)
| mi_unlock (addr: val).


Definition addr_of_mi mi :=
  match mi with
    mi_load addr
  | mi_store addr _
  | mi_lock addr
  | mi_unlock addr => addr
  end.

Definition is_load_or_store (mi: mem_input) :=
  match mi with
  | mi_load _ | mi_store _ _ => True
  | _ => False
  end.


Definition same_kind mi1 mi2 :=
  match mi1, mi2 with
  | mi_load _, mi_load _
  | mi_store _ _, mi_store _ _
  | mi_lock _, mi_lock _
  | mi_unlock _, mi_unlock _ => True
  | _, _ => False
  end.

Definition mem_output := option val.

Parameter leak_arith: aop -> val -> option val.
Parameter decode: val -> option instr.
Parameter incr: val -> val.

Parameter reg_eqdec: EqDec reg.
Parameter val_eqdec: EqDec val.
Existing Instance reg_eqdec.
Existing Instance val_eqdec.
