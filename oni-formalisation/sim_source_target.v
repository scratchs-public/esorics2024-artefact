From formal Require Import oni.
Require Import List.
Require Import OrderedType.
Require Import Lia.
Import ListNotations.
Require Import Bool.
Import BoolNotations.
Require Import Permutation.
Set Implicit Arguments.
From formal Require Import Util tactics CacheTypes syntax coresem mem.
From formal Require Import cacheless_intf source_intf target_intf.
Open Scope nat_scope.


Definition simple_load (mt: mem) (rt: tcache) addr : val :=
  let '(t,s) := addr_to_tagset addr in
  let '(u,w) := rt s in
  match line_of_tag (proj1_sig w) t with
    Some l =>
      match proj1_sig w l with
        Some cl => get_word addr (cl_content cl)
      | None => mt addr
      end
  | None => mt addr
  end.

Lemma simple_load_load:
  forall mt rt a mt' rt' t v,
    load_wb mt rt a = (mt', rt', t, v) ->
    v = simple_load mt rt a.
Proof.
  unfold load_wb, simple_load. intros. repeat destr_in H. inv H.
  unfold maybe_writeback_fetch in Heqp.
  repeat destr_in Heqp; inv Heqp.
  - clear Heqs. unfold tcache_line_of_addr in e.
    rewrite read_from_cache_update_usage.
    rewrite read_from_cache_fetch_same.
    destr_in e. destr_in e. rewrite e. rewrite <- get_word_line.
    unfold write_back in Heqp0.
    destr_in Heqp0. 2: inv Heqp0; auto.
    repeat destr_in Heqp0; inv Heqp0; auto.
    unfold address_of_evicted_line in Heqo.
    rewrite Heqt, Heqp in Heqo. destr_in Heqo; inv Heqo.
    rewrite addr_to_tagset_to_addr in Heqt0. inv Heqt0.
    rewrite Heqp in Heqp1. inv Heqp1. rewrite Heqc2 in Heqc1. inv Heqc1.
    assert (t <> cl_tag c1).
    {
      intros ->. eapply line_of_tag_none in e. 2: eauto. congruence.
    }
    unfold set_line.
    apply fold_left_do_store_nin. rewrite in_map_iff.
    intros ((addr & x) & EQ & IN). simpl in *. subst.
    apply in_combine_l in IN.
    apply addresses_of_tagset_in in IN. rewrite addr_to_tagset_to_addr in IN. congruence.
  - unfold tcache_line_of_addr' in Heqs. destr_in Heqs; inv Heqs.
    unfold tcache_line_of_addr in Heqo. repeat destr_in Heqo. rewrite Heqo.
    rewrite read_from_cache_update_usage.
    rewrite read_from_cache_eq. rewrite Heqt, Heqp.
    apply line_of_tag_some in Heqo. destruct Heqo as (? & EQ & TAG). rewrite EQ; auto.
Qed.

Definition sim_mem (ms mt: mem) rt :=
  forall addr,
    process_of_addr addr = Some D ->
    ms addr = simple_load mt rt addr.

Definition sim_r (L: scache) (C: tcache) :=
  forall t s,
    tagset_in_scache (t, s) L = true <-> (tagset_locked (t, s) C /\ process_of_tag t = Some D).

Lemma sim_r_in_impl' L C:
  forall s (SIM: sim_r L C) x
         (IN: TagS.In x (locked_tags C s))
         (DEF: process_of_tag x = Some D),
    TagS.In x (L s).
Proof.
  intros.
  red in SIM.
  unfold tagset_in_scache in SIM. simpl in SIM.
  apply TagS.mem_2. rewrite SIM. split; auto.
  destruct (C s) eqn:?.
  rewrite tag_locked_spec. 2: eauto. auto.
Qed.


Lemma sim_r_in_impl L C:
  forall s (SIM: sim_r L C) x
         (IN: TagS.In x (L s)),
    TagS.In x (locked_tags C s).
Proof.
  intros.
  red in SIM.
  unfold tagset_in_scache in SIM. simpl in SIM.
  apply TagS.mem_1 in IN.
  rewrite SIM in IN.
  destruct (C s) eqn:?.
  rewrite <- tag_locked_spec. 2: eauto. tauto.
Qed.

Lemma sim_r_cardinal:
  forall L C s,
    sim_r L C ->
    TagS.cardinal (L s) <= TagS.cardinal (locked_tags C s).
Proof.
  intros L C s H.
  apply cardinal_le_in_impl.
  apply sim_r_in_impl. auto.
Qed.


Definition sim_source_target (s: mem * scache) (t: mem * tcache) :=
  let '(ms,rs) := s in
  let '(mt,rt) := t in
  sim_mem ms mt rt /\
    sim_r rs rt /\
    coherent_cache mt rt /\
    tcache_locked_dirty rt.


Lemma write_back_bsim:
  forall ms rs mt rt addr l m tr,
    sim_source_target (ms, rs) (mt, rt) ->
    write_back mt rt l (address_of_evicted_line rt l addr) = (m, tr) ->
    sim_source_target (ms, rs) (m, rt).
Proof.
  intros ms rs mt rt addr l m tr (MSIM, (CSIM, (CC, TLD))) WB.
  split; auto.
  unfold write_back in WB.
  repeat destr_in WB; inv WB; auto.
  red. intros.
  rewrite MSIM; eauto.
  unfold simple_load. destr. destr. destr; auto.
  apply line_of_tag_some in Heqo0. destruct Heqo0 as (? & EQ & TAG). rewrite EQ. auto.
  rewrite set_line_get.
  2: apply cl_content_length.
  destr; auto.
  unfold address_of_evicted_line in Heqo. repeat destr_in Heqo; inv Heqo.
  rewrite addr_to_tagset_to_addr in Heqt. inv Heqt.
  rewrite Heqp1 in Heqp; inv Heqp.
  rewrite Heqc3 in Heqc0; inv Heqc0.
  clear Heqs3. rewrite Heqt0 in e.
  rewrite addr_to_tagset_to_addr in e. inv e.
  rewrite Heqp1 in Heqp0; inv Heqp0.
  eapply line_of_tag_none in Heqo0; eauto. congruence.
  split; auto. split; auto.
  red; intros.
  specialize (CC _ _ _ H _ _ H0 _ H1 _ H2 H3).
  unfold write_back in WB.
  repeat destr_in WB; inv WB; auto.
  rewrite get_set_line_other. auto.
  apply cl_content_length.
  rewrite Heqt0, H. intro EQ; inv EQ.
  rewrite Heqp in H0; inv H0.
  unfold address_of_evicted_line in Heqo. repeat destr_in Heqo; inv Heqo.
  rewrite addr_to_tagset_to_addr in Heqt0. inv Heqt0.
  rewrite Heqp0 in Heqp; inv Heqp.
  rewrite Heqc1 in Heqc0; inv Heqc0.
  edestruct line_of_tag_some as (? & EQ & TAG). apply H1. rewrite H2 in EQ; inv EQ.
  destruct (proj2_sig w).
  specialize (wft_nodup _ _ _ _ H2 Heqc1). trim wft_nodup. auto. destruct wft_nodup; subst. congruence.
Qed.

Lemma tcache_update_usage_bsim:
  forall ms rs mt rt v mi,
    sim_source_target (ms, rs) (mt, rt) ->
    sim_source_target (ms, rs) (mt, tcache_update_usage rt v mi).
Proof.
  intros ms rs mt rt v mi (MSIM, (CSIM, (CC, TLD))).
  split; [|split; [|split]]; auto.
  - red; intros; rewrite MSIM; auto.
    unfold simple_load. destr. unfold tcache_update_usage. destr.
    destruct (addr_to_tagset v) eqn:?.
    destruct (rt s1) eqn:?. symmetry. destr. destr_in Heqp1.
    unfold update_tcache in Heqp1. destr_in Heqp1; inv Heqp1.
    + rewrite Heqp in Heqp0. inv Heqp0. destr; auto.
    + rewrite Heqp in H1; inv H1. auto.
  - red; intros. rewrite (CSIM t s).
    unfold tcache_update_usage. destr. destr. destr.
    rewrite tagset_locked_update_spec. destr; auto. 2: tauto.
    unfold tagset_locked. subst. rewrite Heqp. tauto.
  - red; intros.
    unfold tcache_update_usage in H0. destr_in H0. destr_in H0. destr_in H0.
    unfold update_tcache in H0. destr_in H0. inv H0.
    specialize (CC _ _ _ H _ _ Heqp _ H1 _ H2 H3). auto.
    specialize (CC _ _ _ H _ _ H0 _ H1 _ H2 H3). auto.
  - revert TLD. unfold tcache_locked_dirty. intros.
    assert (w = snd (rt s)).
    {
      unfold tcache_update_usage in H0.
      repeat destr_in H0.
      unfold update_tcache in H0; destr_in H0. inv H0. rewrite Heqp; auto. rewrite H0; auto.
    } clear H0. subst.
    destruct (rt s) eqn:?. simpl in *.
    eapply TLD; eauto.
Qed.


Lemma fetch_bsim:
  forall ms rs mt rt,
    sim_source_target (ms, rs) (mt, rt) ->
    forall addr e,
      (
        forall addr0,
          address_of_evicted_line rt (tcache_line_to_evict rt addr) addr = Some addr0 ->
        forall t s,
          addr_to_tagset addr0 = (t, s) ->
          forall w u,
            rt s = (u, w) ->
            forall cl,
              proj1_sig w (tcache_line_to_evict rt addr) = Some cl ->
              get_line mt addr0 = cl_content cl
      ) ->
      sim_source_target (ms, rs) (mt, fetch mt rt addr (tcache_line_to_evict rt addr) e).
Proof.
  intros ms rs mt rt (MSIM, (CSIM, (CC, TLD))) addr e COH.
  split; [|split;[|split]]; auto.
  - red; intros. rewrite MSIM; auto.
    unfold simple_load. destr.
    generalize (fetch_unfold mt rt addr (tcache_line_to_evict rt addr) e). destr. destr. simpl.
    intros (p & EQ). rewrite EQ. clear EQ.
    symmetry. destr. symmetry. destr.
    unfold tcache_line_of_addr in e. rewrite Heqt0, Heqp in e.
    destruct s1, s3, s2; simpl in *.
    unfold update_tcache in Heqp0. destr_in Heqp0.
    2:{
      rewrite Heqp0 in Heqp1. inv Heqp1. auto.
    }
    subst. rewrite Heqp in Heqp1; inv Heqp1. inv Heqp0.
    destr.
    + assert (t <> t0) by congruence.
      destruct (line_of_tag_some _ _ Heqo) as (? & EQ & TAG). subst. rewrite EQ.
      erewrite line_of_tag_update_ways. 3: now eauto. 2: now eauto. simpl.
      destr.
      * destr_in Heqo0. inv Heqo0. congruence. inv Heqo0.
        rewrite Heqo in H2.  destr_in H2; inv H2.
        rewrite update_ways_other. rewrite EQ. auto. auto.
      * repeat destr_in Heqo0; inv Heqo0. inv Heqo.
        unfold address_of_evicted_line in COH.
        rewrite Heqt0, Heqp in COH. simpl in COH.
        rewrite EQ in COH.
        specialize (COH _ eq_refl).
        rewrite addr_to_tagset_to_addr in COH.
        specialize (COH _ _ eq_refl _ _ Heqp). simpl in COH.
        specialize (COH _ EQ). rewrite <- COH.
        rewrite <- Heqt.
        rewrite get_line_tagset_to_addr_to_tagset.
        rewrite get_word_line. auto. congruence.
    + rewrite line_of_tag_update_ways; auto. simpl. destr; auto.
      destr_in Heqo0.
      * subst. inv Heqo0. rewrite update_ways_same. simpl.
        rewrite <- get_line_tagset_to_addr_to_tagset.
        rewrite Heqt0, <- Heqt.
        rewrite get_line_tagset_to_addr_to_tagset. rewrite get_word_line; auto.
      * rewrite Heqo in Heqo0. inv Heqo0.
  - red. intros. red in CSIM. rewrite CSIM.
    rewrite tagset_locked_fetch. tauto.
    unfold line_locked, tcache_line_to_evict.
    destr. destr. destr; auto.
    apply line_to_evict_not_locked in Heqc0. auto.
  - red; intros.
    generalize (fetch_unfold mt rt addr (tcache_line_to_evict rt addr) e). destr. destr. simpl.
    intros (p & EQ). rewrite EQ in H0. clear EQ.
    unfold update_tcache in H0. destr_in H0.
    2: eapply CC; eauto. subst.
    inv H0. simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    destr_in H1. inv H1.
    rewrite update_ways_same in H2. inv H2. simpl in *.
    rewrite <- get_line_tagset_to_addr_to_tagset. rewrite H. rewrite <- Heqt0.
    rewrite get_line_tagset_to_addr_to_tagset. auto.
    repeat destr_in H1; inv H1.
    unfold update_ways in H2. destr_in H2; eauto. inv H2. simpl in *. congruence.
  - generalize (fetch_unfold mt rt addr (tcache_line_to_evict rt addr) e). destr. destr. simpl.
    intros (p & EQ). rewrite EQ. clear EQ.
    unfold tcache_locked_dirty. intros. unfold update_tcache in H0. destr_in H0. 2: eapply TLD; eauto. inv H0.
    simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    destr_in H1. inv H1.
    rewrite update_ways_same in H2. inv H2. simpl in *. congruence.
    repeat destr_in H1; inv H1.
    unfold update_ways in H2. destr_in H2; eauto.
Qed.


Lemma write_back_fetch_maybe_sim:
  forall ms rs mt rt addr mt2 rt2 l0 t,
    sim_source_target (ms, rs) (mt, rt) ->
    maybe_writeback_fetch mt rt addr = (mt2, rt2, l0, t) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 l0 t SIM Heqp.
  unfold maybe_writeback_fetch in Heqp.
  repeat destr_in Heqp; inv Heqp; eauto.
  generalize Heqp0; intro WB.
  eapply write_back_bsim in Heqp0; eauto.
  destruct (addr_to_tagset addr) eqn:ATT.
  apply fetch_bsim. auto. intros. rewrite H in WB.
  unfold write_back in WB.
  rewrite H0, H1 in WB.
  destruct SIM as (MSIM, (CSIM, (CC, TLD))).
  generalize (fun pf => CC _ _ _ H0 _ _ H1 _ pf _ H2).
  rewrite line_of_tag_spec. rewrite H2.
  unfold tcache_line_of_addr in e. clear Heqs.
  unfold tcache_line_to_evict, address_of_evicted_line in *.
  rewrite ATT in *. destr_in e. destr_in H; inv H. rewrite H2 in WB.
  rewrite addr_to_tagset_to_addr in H0. inv H0. rewrite Heqp in H1; inv H1.
  rewrite H2 in Heqc0. inv Heqc0.
  intro A; trim A. eauto.
  destr_in WB; inv WB.
  rewrite get_set_line. auto. reflexivity. apply cl_content_length. apply A. auto. apply proj2_sig.
Qed.

Lemma store_sim_mem:
  forall ms addr v mt rt l t s u w,
    sim_mem ms mt rt ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    sim_mem (do_store ms addr v) mt (write_in_cache rt addr v l).
Proof.
  intros ms addr v mt rt l t s u w SIM ATT RT LOT.
  red; intros.
  unfold do_store. rewrite SIM; auto.
  unfold write_in_cache.
  generalize (update_cache_line_eq rt addr l _ (fun cl => write_in_cache_cl_preserves_tag cl addr v)).
  unfold simple_load. rewrite ATT, RT.
  generalize (line_of_tag_some _ _ LOT). intros (cl & EQ & TAG). setoid_rewrite EQ.
  intros (p & EQ2). rewrite EQ2. clear EQ2.
  destr.
  - subst. rewrite ATT.
    rewrite update_tcache_same. simpl.
    rewrite line_of_tag_update_ways; auto.
    simpl. 2: apply proj2_sig.
    destruct (eq_dec (cl_tag cl) (cl_tag cl)). 2: congruence.
    rewrite update_ways_same. simpl.
    rewrite get_set_word. auto. apply cl_content_length.
  - destr. destr. unfold update_tcache.
    destruct (eq_dec s s0).
    + subst. rewrite RT in Heqp0. inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto.
      simpl. 2: apply proj2_sig.
      destruct (eq_dec (cl_tag cl) t0).
      * subst. rewrite LOT. unfold update_ways.
        destruct (eq_dec l l); try congruence. simpl. rewrite EQ.
        rewrite get_set_word_other; auto.
        intro EQ2; apply n, same_tagset_wordid_eq; auto. congruence.
      * destr. setoid_rewrite Heqo.
        assert (l <> l0). intro; subst.
        apply line_of_tag_some in Heqo. rewrite EQ in Heqo. destruct Heqo. intuition subst. congruence.
        destruct (Fin.eq_dec l l0). congruence.
        rewrite update_ways_other; auto.
        setoid_rewrite Heqo. auto.
    + rewrite Heqp0. auto.
Qed.


Lemma write_in_cache_sim:
  forall ms addr v mt rs rt l t s u w,
    sim_source_target (ms, rs) (mt, rt) ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    sim_source_target (do_store ms addr v, rs) (mt, write_in_cache rt addr v l).
Proof.
  intros ms addr v mt rs rt l t s u w (MSIM, (CSIM, (CC, TLD))) ATT RT LOT. split.
  eapply store_sim_mem; eauto.
  split. red. intros. red in CSIM.
  rewrite CSIM. unfold write_in_cache.
  rewrite tagset_locked_update_cache_line. tauto. reflexivity.
  split.
  red; intros.
  generalize (write_in_cache_unfold rt addr l v ). rewrite ATT, RT.
  edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ.
  intros (p & EQ2); rewrite EQ2 in H0; clear EQ2.
  unfold update_tcache in H0. destr_in H0; inv H0.
  - simpl in *. rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig.
    repeat destr_in H1; inv H1.
    rewrite update_ways_same in H2. inv H2. simpl in *. congruence.
    rewrite update_ways_other in H2; eauto.
  - eauto.
  - eapply write_in_cache_locked_dirty; eauto.
Qed.


Lemma add_tagset_in_scache_lock_line:
  forall addr t s tc l ms rs m,
    addr_to_tagset addr = (t, s) ->
    process_of_tag t = Some D ->
    tcache_line_of_addr tc addr = Some l ->
    sim_source_target (ms, rs) (m, tc) ->
    sim_source_target (ms, add_tagset_in_scache (t, s) rs) (m, lock_line tc addr l).
Proof.
  intros addr t s tc l ms rs m ATT POT TLOA (MSIM, (CSIM, (CC, TLD))).
  split; [|split; [|split]]; auto.
  - red; intros. rewrite MSIM; auto. unfold simple_load. destr. destr.
    unfold lock_line.
    generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ; clear EQ.
    + unfold update_tcache. destruct (eq_dec s s0); subst; auto.
      rewrite Heqp in Heqp0; inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
      destruct (eq_dec t t0).
      * subst.
        rewrite TLOA.
        apply line_of_tag_some in TLOA. setoid_rewrite Heqo in TLOA. destruct TLOA as (? & EQ & TAG); inv EQ.
        destruct (eq_dec (cl_tag x) (cl_tag x)); subst; eauto.
        rewrite update_ways_same. setoid_rewrite Heqo. simpl; auto. congruence.
      * edestruct line_of_tag_some as (? & EQ & TAG). eauto. setoid_rewrite Heqo in EQ. inv EQ.
        destruct (eq_dec (cl_tag x) t0). congruence.
        destruct s2; simpl in *.
        destr; auto.
        assert (l <> l0). intros ->.
        destruct w.
        apply line_of_tag_some in Heqo0. destruct Heqo0 as ( ? & EQ & TAG2). subst.
        clear Heqp. specialize (wft_nodup _ _ _ _ Heqo EQ). congruence. destruct Fin.eq_dec. congruence.
        rewrite update_ways_other; auto.
      * rewrite Heqp. auto.
    + rewrite Heqp; auto.
  - red. intros.
    rewrite add_tagset_in_scache_in.
    rewrite orb_true_iff.
    rewrite (CSIM t0 s0).
    rewrite tagset_locked_lock_line; eauto.
    split.
    intros [(A & B)| C]. auto. unfold beq_dec in C. destr_in C; inv C. clear Heqs1. inv e. split; auto.
    intros ([A|A] & B). auto. inv A.  right. unfold beq_dec. destr; congruence.
  - red; intros. unfold lock_line in H0.
    generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ in H0; clear EQ; eauto.
    unfold update_tcache in H0. destr_in H0; inv H0; eauto.
    simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    repeat destr_in H1; inv H1; eauto.
    rewrite update_ways_same in H2; inv H2. simpl in *. congruence.
    rewrite update_ways_other in H2; auto. eauto.
  - unfold tcache_locked_dirty in TLD |- *. intros.
    unfold lock_line in H0.
    generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag).
    rewrite ATT. destr.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    edestruct line_of_tag_some as ( ? & EQ & TAG). apply TLOA. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2. unfold update_tcache in H0. destr_in H0; eauto.
    inv H0. simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    repeat destr_in H1; inv H1.
    rewrite update_ways_same in H2. inv H2.  simpl. auto.
    rewrite update_ways_other in H2; eauto.
Qed.


Lemma remove_tagset_in_scache_unlock_line:
  forall addr t s tc l ms rs m,
    addr_to_tagset addr = (t, s) ->
    process_of_tag t = Some D ->
    tcache_line_of_addr tc addr = Some l ->
    sim_source_target (ms, rs) (m, tc) ->
    sim_source_target (ms, remove_tagset_in_scache (t, s) rs) (m, unlock_line tc addr l).
Proof.
  intros addr t s tc l ms rs m ATT POT TLOA (MSIM, (CSIM, (CC, TLD))).
  split; [|split; [|split]]; auto.
  - red; intros. rewrite MSIM; auto. unfold simple_load. destr. destr.
    unfold unlock_line.
    generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ; clear EQ.
    + unfold update_tcache. destruct (eq_dec s s0); subst; auto.
      rewrite Heqp in Heqp0; inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
      destruct (eq_dec t t0).
      * subst.
        rewrite TLOA.
        apply line_of_tag_some in TLOA. setoid_rewrite Heqo in TLOA. destruct TLOA as (? & EQ & TAG); inv EQ.
        destruct (eq_dec (cl_tag x) (cl_tag x)); subst; eauto.
        rewrite update_ways_same. setoid_rewrite Heqo. simpl; auto. congruence.
      * edestruct line_of_tag_some as (? & EQ & TAG). eauto. setoid_rewrite Heqo in EQ. inv EQ.
        destruct (eq_dec (cl_tag x) t0). congruence.
        destruct s2; simpl in *.
        destr; auto.
        assert (l <> l0). intros ->.
        destruct w.
        apply line_of_tag_some in Heqo0. destruct Heqo0 as ( ? & EQ & TAG2). subst.
        clear Heqp. specialize (wft_nodup _ _ _ _ Heqo EQ). congruence. destruct Fin.eq_dec. congruence.
        rewrite update_ways_other; auto.
      * rewrite Heqp. auto.
    + rewrite Heqp; auto.
  - red. intros.
    rewrite remove_tagset_spec.
    rewrite (CSIM t0 s0).
    rewrite tagset_locked_unlock_line; eauto. tauto.
  - red; intros. unfold unlock_line in H0.
    generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ in H0; clear EQ; eauto.
    unfold update_tcache in H0. destr_in H0; inv H0; eauto.
    simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    repeat destr_in H1; inv H1; eauto.
    rewrite update_ways_same in H2; inv H2. simpl in *. eapply CC. eauto. eauto. 2: eauto.
    destruct (line_of_tag (proj1_sig s1) (cl_tag c0)) eqn:?.
    apply line_of_tag_some in Heqo0. destruct Heqo0 as (? & EQ & TAG).
    destruct s1. destruct w. simpl in *. generalize (wft_nodup _ _ _ _ EQ Heqo TAG). intuition subst. auto.
    eapply line_of_tag_none in Heqo; eauto. congruence. auto.
    rewrite update_ways_other in H2; eauto.
  - unfold tcache_locked_dirty in TLD |- *. intros.
    unfold unlock_line in H0.
    generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag).
    rewrite ATT. destr.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    edestruct line_of_tag_some as ( ? & EQ & TAG). apply TLOA. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2. unfold update_tcache in H0. destr_in H0; eauto.
    inv H0. simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    repeat destr_in H1; inv H1.
    rewrite update_ways_same in H2. inv H2. simpl in *. congruence. 
    rewrite update_ways_other in H2; eauto.
Qed.


Lemma load_bsim:
  forall ms rs mt rt addr m t tau v,
    sim_source_target (ms, rs) (mt, rt) ->
    load_wb mt rt addr = (m, t, tau, v) ->
    process_of_addr addr = Some D ->
    ms addr = v /\ sim_source_target (ms, rs) (m, t).
Proof.
  unfold load_wb.
  intros ms rs mt rt addr m t tau v SIM LOAD POA.
  repeat destr_in LOAD. inv LOAD.
  generalize (write_back_fetch_maybe_sim _ SIM Heqp). clear SIM; intros SIM.
  rewrite read_from_cache_update_usage.
  split; auto. 2: apply tcache_update_usage_bsim; auto.
  rewrite read_from_cache_eq. destr. destr.
  destruct SIM as (MSIM, ?). rewrite MSIM; auto. unfold simple_load.
  rewrite Heqt, Heqp0.
  apply maybe_writeback_tcache_line_of_addr in Heqp.
  unfold tcache_line_of_addr in Heqp.
  rewrite Heqt, Heqp0 in Heqp. rewrite Heqp.
  apply line_of_tag_some in Heqp. destruct Heqp as (? & EQ & TAG). rewrite EQ. auto.
Qed.

Lemma store_bsim:
  forall ms rs mt rt addr m t tau v,
    sim_source_target (ms, rs) (mt, rt) ->
    store_wb mt rt addr v = (m, t, tau) ->
    process_of_addr addr = Some D ->
    sim_source_target (do_store ms addr v, rs) (m, t).
Proof.
  unfold store_wb.
  intros ms rs mt rt addr m t tau v SIM STORE POA.
  repeat destr_in STORE. inv STORE.
  destruct (addr_to_tagset addr) eqn:ATT.
  destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:TCU.
  eapply write_in_cache_sim. 2-3: eauto.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
  apply maybe_writeback_tcache_line_of_addr in Heqp.
  unfold tcache_line_of_addr in Heqp.
  rewrite ATT in Heqp.
  apply tcache_update_usage_inv in TCU. destruct TCU as (? & TCY).
  rewrite TCY in Heqp. auto.
Qed.

Lemma store_sim_mem_wt:
  forall ms addr v mt rt l t s u w,
    sim_mem ms mt rt ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    sim_mem (do_store ms addr v) (do_store mt addr v) (write_in_cache_wt rt addr v l).
Proof.
  intros ms addr v mt rt l t s u w SIM ATT RT LOT.
  red; intros.
  unfold do_store. rewrite SIM; auto.
  unfold write_in_cache_wt.
  generalize (update_cache_line_eq rt addr l _ (fun cl => write_in_cache_cl_wt_preserves_tag cl addr v)).
  unfold simple_load. rewrite ATT, RT.
  generalize (line_of_tag_some _ _ LOT). intros (cl & EQ & TAG). setoid_rewrite EQ.
  intros (p & EQ2). rewrite EQ2. clear EQ2.
  destr.
  - subst. rewrite ATT.
    rewrite update_tcache_same. simpl.
    rewrite line_of_tag_update_ways; auto.
    simpl. 2: apply proj2_sig.
    destruct (eq_dec (cl_tag cl) (cl_tag cl)). 2: congruence.
    rewrite update_ways_same. simpl.
    rewrite get_set_word. auto. apply cl_content_length.
  - destr. destr. unfold update_tcache.
    destruct (eq_dec s s0).
    + subst. rewrite RT in Heqp0. inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto.
      simpl. 2: apply proj2_sig.
      destruct (eq_dec (cl_tag cl) t0).
      * subst. rewrite LOT. unfold update_ways.
        destruct (eq_dec l l); try congruence. simpl. rewrite EQ.
        rewrite get_set_word_other; auto.
        intro EQ2; apply n, same_tagset_wordid_eq; auto. congruence.
      * destr. setoid_rewrite Heqo.
        assert (l <> l0). intro; subst.
        apply line_of_tag_some in Heqo. rewrite EQ in Heqo. destruct Heqo. intuition subst. congruence.
        destruct (Fin.eq_dec l l0). congruence.
        rewrite update_ways_other; auto.
        setoid_rewrite Heqo. auto.
    + rewrite Heqp0. auto.
Qed.


Lemma store_sim_mem_wt2:
  forall ms addr v mt rt l t s u w,
    sim_mem ms mt rt ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    sim_mem (do_store ms addr v) mt (write_in_cache_wt rt addr v l).
Proof.
  intros ms addr v mt rt l t s u w SIM ATT RT LOT.
  red; intros.
  unfold do_store. rewrite SIM; auto.
  unfold write_in_cache_wt.
  generalize (update_cache_line_eq rt addr l _ (fun cl => write_in_cache_cl_wt_preserves_tag cl addr v)).
  unfold simple_load. rewrite ATT, RT.
  generalize (line_of_tag_some _ _ LOT). intros (cl & EQ & TAG). setoid_rewrite EQ.
  intros (p & EQ2). rewrite EQ2. clear EQ2.
  destr.
  - subst. rewrite ATT.
    rewrite update_tcache_same. simpl.
    rewrite line_of_tag_update_ways; auto.
    simpl. 2: apply proj2_sig.
    destruct (eq_dec (cl_tag cl) (cl_tag cl)). 2: congruence.
    rewrite update_ways_same. simpl.
    rewrite get_set_word. auto. apply cl_content_length.
  - destr. destr. unfold update_tcache.
    destruct (eq_dec s s0).
    + subst. rewrite RT in Heqp0. inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto.
      simpl. 2: apply proj2_sig.
      destruct (eq_dec (cl_tag cl) t0).
      * subst. rewrite LOT. unfold update_ways.
        destruct (eq_dec l l); try congruence. simpl. rewrite EQ.
        rewrite get_set_word_other; auto.
        intro EQ2; apply n, same_tagset_wordid_eq; auto. congruence.
      * destr. setoid_rewrite Heqo.
        assert (l <> l0). intro; subst.
        apply line_of_tag_some in Heqo. rewrite EQ in Heqo. destruct Heqo. intuition subst. congruence.
        destruct (Fin.eq_dec l l0). congruence.
        rewrite update_ways_other; auto.
        setoid_rewrite Heqo. auto.
    + rewrite Heqp0. auto.
Qed.

Lemma nth_error_eq:
  forall {A} (l1 l2: list A),
    (forall n, n < length l1 -> nth_error l1 n = nth_error l2 n) ->
    length l1 = length l2 ->
    l1 = l2.
Proof.
  induction l1; simpl; intros; eauto.
  - destruct l2; auto.
    specialize (H 0); simpl in *; congruence.
  - destruct l2; auto.
    specialize (H 0); simpl in H0. congruence.
    generalize (H 0); simpl. intro B; trim B. lia. inv B. f_equal. apply IHl1.
    intros n.
    intros. specialize (H (S n)); simpl in H. trim H.  simpl in *. lia. auto.
    simpl in *; congruence.
Qed.


Lemma get_word_eq:
  forall (l1 l2: list _),
    (forall n, get_word n l1 = get_word n l2) ->
    l1 = l2.
Proof.
  intros. apply nth_error_eq. intros.
  erewrite ! nth_error_nth'.
Admitted.

Lemma nth_set_nth_gss:
  forall {A:Type} n (l: list A) m v d,
    m < length l ->
    nth n (set_nth l m v) d = if PeanoNat.Nat.eq_dec n m then v else nth n l d.
Proof.
  induction n; simpl; intros; eauto.
  - destr. subst. destruct l; simpl in *. lia. auto.
    destr_in Heqs. congruence. destruct l; simpl in *; auto.
  - destr; eauto.
    repeat destr_in Heqs; inv Heqs.
    destruct l; simpl in *. lia. rewrite IHn. rewrite Heqs0. auto. lia.
    repeat destr_in Heqs; inv Heqs. destruct l; simpl in*; try lia. auto.
    destruct l; simpl in*; try lia. rewrite IHn; auto. destr; auto. inv Heqs0. lia.
Qed.
Lemma nth_error_map : forall {A B: Type}
                             n (l: list A) (f: A -> B),
    nth_error (map f l) n = option_map f (nth_error l n).
Proof.
  unfold option_map.
  intros. destr.
  erewrite map_nth_error. 2: eauto. auto.
  revert l Heqo.
  induction n; simpl; intros; eauto.
  dinv Heqo. simpl. auto.
  dinv Heqo; simpl; auto.
Qed.

Lemma write_in_cache_sim_wt:
  forall ms addr v mt rs rt l t s u w,
    sim_source_target (ms, rs) (mt, rt) ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    sim_source_target (do_store ms addr v, rs) (do_store mt addr v, write_in_cache_wt rt addr v l).
Proof.
  intros ms addr v mt rs rt l t s u w (MSIM, (CSIM, (CC, TLD))) ATT RT LOT. split.
  eapply store_sim_mem_wt; eauto.
  split. red. intros. red in CSIM.
  rewrite CSIM. unfold write_in_cache_wt.
  rewrite tagset_locked_update_cache_line. tauto. reflexivity.
  split.
  red; intros.
  generalize (write_in_cache_wt_unfold rt addr l v ). rewrite ATT, RT.
  edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ.
  intros (p & EQ2); rewrite EQ2 in H0; clear EQ2.
  unfold update_tcache in H0. destr_in H0; inv H0.
  - simpl in *. rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig.
    repeat destr_in H1; inv H1.
    + rewrite update_ways_same in H2. inv H2. simpl in *.
      generalize (CC addr _ _ ATT _ _ RT _ LOT _ EQ H3). 
      intros.
      rewrite <- H0. clear H0.
      apply nth_error_eq. intros.
      unfold get_line, do_store, set_word.
      erewrite (nth_error_nth' (set_nth _ _ _)).
      rewrite nth_set_nth_gss. erewrite nth_error_map.
      destr; subst.
      rewrite H, <- ATT. rewrite address_to_wordid_nth. simpl. destr; auto; congruence.
      erewrite nth_error_nth'. simpl. destr; eauto.
      2: rewrite map_nth; eauto. 2: rewrite ATT, H; eauto. rewrite ATT, <- H.
      rewrite map_nth. rewrite e.
      clear Heqs2.
      assert (nth_error (addresses_of_tagset (addr_to_tagset addr0)) n = Some addr).
      erewrite nth_error_nth'. rewrite e. auto.
      rewrite length_get_line in H0. rewrite length_addresses_of_tagset. auto.
      generalize (address_to_wordid_nth addr). intro H2.
      rewrite ATT, <- H in H2.
      elim n0.
      eapply NoDup_nth_error. apply addresses_of_tagset_nodup.
      rewrite length_get_line in H0. rewrite length_addresses_of_tagset. auto.
      rewrite H1, H2. auto.
      rewrite length_get_line in H0. rewrite length_addresses_of_tagset. auto.
      rewrite length_get_line in H0. rewrite map_length, length_addresses_of_tagset.
      apply address_to_wordid_valid.
      rewrite length_get_line in H0. rewrite set_nth_length, map_length, length_addresses_of_tagset. auto.
      rewrite length_get_line, set_word_preserves_length. auto.
      rewrite length_get_line.  auto.
    + rewrite update_ways_other in H2; eauto.
      rewrite <- CC; eauto.
      unfold get_line. apply map_ext_in.
      intros. apply addresses_of_tagset_in in H0.
      unfold do_store. destr; auto. subst.
      simpl in *. congruence.
  - rewrite <- CC; eauto.
    unfold get_line. apply map_ext_in.
    intros. apply addresses_of_tagset_in in H0.
    unfold do_store. destr; auto. subst.
    simpl in *. congruence.
  - eapply write_in_cache_wt_locked_dirty; eauto.
    Unshelve. eauto.
Qed.

Lemma write_in_cache_sim_wt2:
  forall ms addr v mt rs rt l t s u w,
    sim_source_target (ms, rs) (mt, rt) ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    tcache_is_locked rt addr = true ->
    sim_source_target (do_store ms addr v, rs) (mt, write_in_cache_wt rt addr v l).
Proof.
  intros ms addr v mt rs rt l t s u w (MSIM, (CSIM, (CC, TLD))) ATT RT LOT LOCKED. split.
  eapply store_sim_mem_wt2; eauto.
  split. red. intros. red in CSIM.
  rewrite CSIM. unfold write_in_cache_wt.
  rewrite tagset_locked_update_cache_line. tauto. reflexivity.
  split.
  red; intros.
  generalize (write_in_cache_wt_unfold rt addr l v ). rewrite ATT, RT.
  edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ.
  intros (p & EQ2); rewrite EQ2 in H0; clear EQ2.
  unfold update_tcache in H0. destr_in H0; inv H0.
  - simpl in *. rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig.
    repeat destr_in H1; inv H1.
    rewrite update_ways_same in H2. inv H2. simpl in *. unfold tcache_is_locked in LOCKED.
    rewrite ATT, RT in LOCKED. rewrite LOT, EQ in LOCKED.
    eapply TLD in LOCKED; eauto. congruence.
    rewrite update_ways_other in H2; eauto.
  - eauto.
  - eapply write_in_cache_wt_locked_dirty; eauto.
Qed.

Lemma tcache_line_of_addr_fetch m r addr1 l e addr:
  tcache_line_of_addr (fetch m r addr1 l e) addr =
    let '(t, s) := addr_to_tagset addr in
    let '(t1, s1) := addr_to_tagset addr1 in
    if eq_dec s1 s
    then
      if eq_dec t1 t then Some l
      else
        match tcache_line_of_addr r addr with
        | Some l0 => if eq_dec l l0 then None else Some l0
        | None => None
        end
    else tcache_line_of_addr r addr.
Proof.
  unfold tcache_line_of_addr.
  destr.
  Opaque eq_dec.
  generalize (fetch_unfold m r addr1 l e). destr. destr. simpl.
  intros (p & EQ). rewrite EQ. clear EQ. destr.
  unfold tcache_line_of_addr in e. rewrite Heqt0, Heqp in e.
  unfold update_tcache in Heqp0. destr_in Heqp0.
  - inv Heqp0.
    simpl. rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl. rewrite Heqp. auto.
  - rewrite Heqp0. auto.
Qed.
Transparent eq_dec.

Lemma store_wt_bsim around:
  forall ms rs mt rt addr m t tau v,
    sim_source_target (ms, rs) (mt, rt) ->
    store_wt around mt rt addr v = (m, t, tau) ->
    process_of_addr addr = Some D ->
    sim_source_target (do_store ms addr v, rs) (m, t).
Proof.
  unfold store_wt.
  intros ms rs mt rt addr m t tau v SIM STORE POA.
  do 4 destr_in STORE.
  destr_in Heqp.
  - destr_in Heqp.
    + inv Heqp. inv STORE.
      apply tcache_update_usage_bsim. split.
      red. intros.
      unfold simple_load. destr. destr. unfold do_store.
      destr; subst.
      * unfold tcache_line_of_addr in e. clear Heqs. rewrite Heqt, Heqp in e. rewrite e. auto.
      * simpl in SIM. destruct_and SIM. rewrite SIM0; auto. unfold simple_load. rewrite Heqt, Heqp. auto.
      * simpl in *. intuition.
        unfold coherent_cache.
        intros. rewrite <- H0. 2-6: eauto.
        unfold get_line. apply map_ext_in.
        intros. apply addresses_of_tagset_in in H8.
        unfold do_store. destr; auto. subst.
        clear Heqs; unfold tcache_line_of_addr in e.
        rewrite H8, H2, H4 in e. congruence.
    + destr_in Heqp. inv Heqp. inv STORE.
      destruct (addr_to_tagset addr) eqn:?.
      destruct (tcache_update_usage (fetch m0 rt addr (tcache_line_to_evict rt addr) e) addr (mi_store addr v) s) eqn:?.
      eapply (@write_in_cache_sim_wt ms addr v m0 rs).
      apply tcache_update_usage_bsim.
      eapply write_back_fetch_maybe_sim; eauto.
      unfold maybe_writeback_fetch. rewrite Heqs. rewrite Heqp3. eauto.
      eauto.
      eauto.
      apply tcache_update_usage_inv in Heqp. destruct Heqp as (? & TCY).
      generalize (fetch_unfold m0 rt addr (tcache_line_to_evict rt addr) e). rewrite Heqt. destr. simpl.
      intros (p & EQ). rewrite EQ in TCY. clear EQ.
      rewrite update_tcache_same in TCY. inv TCY. simpl.
      simpl. rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      destr; congruence.
  - inv Heqp. inv STORE. destr.
    destruct (addr_to_tagset addr) eqn:?.
    destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:?.
    eapply (@write_in_cache_sim_wt ms addr v m0 rs).
    apply tcache_update_usage_bsim. auto. eauto. eauto.
    unfold tcache_line_of_addr' in Heqs. destr_in Heqs; inv Heqs. unfold tcache_line_of_addr in Heqo.
    rewrite Heqt in Heqo.
    apply tcache_update_usage_inv in Heqp. destruct Heqp. rewrite H in Heqo. auto.
    destruct (addr_to_tagset addr) eqn:?.
    destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:?.
    eapply write_in_cache_sim_wt2; eauto.
    apply tcache_update_usage_bsim. auto.
    unfold tcache_line_of_addr' in Heqs. destr_in Heqs; inv Heqs. unfold tcache_line_of_addr in Heqo.
    rewrite Heqt in Heqo.
    apply tcache_update_usage_inv in Heqp. destruct Heqp. rewrite H in Heqo. auto.
    unfold tcache_is_locked in Heqb |- *. rewrite Heqt in *. rewrite Heqp.
    apply tcache_update_usage_inv in Heqp. destruct Heqp. rewrite H in Heqb.
    apply negb_false_iff in Heqb. auto.
Qed.

Lemma lock_bsim:
  forall ms rs mt rt addr m t tau ts,
    sim_source_target (ms, rs) (mt, rt) ->
    lock_wb mt rt addr = Some (m, t, tau) ->
    process_of_addr addr = Some D ->
    addr_to_tagset addr = ts ->
    Nat.ltb (TagS.cardinal (rs (snd ts))) (nways - 1) || tagset_in_scache ts rs = true /\
    sim_source_target (ms, add_tagset_in_scache (addr_to_tagset addr) rs) (m, t).
Proof.
  unfold lock_wb.
  intros ms rs mt rt addr m t tau ts SIM LOCK POA ATT.
  repeat destr_in LOCK; inv LOCK.
  split.
  {
    destruct SIM as (MSIM & CSIM & CC & TLD).
    destruct (addr_to_tagset addr) eqn:ATT.
    rewrite orb_true_iff. rewrite (CSIM t s).
    unfold tcache_is_locked in Heqb.
    rewrite ATT in Heqb.
    rewrite orb_true_iff in Heqb. destruct Heqb.
    2:{
      right. split; auto. red. destr.
      repeat destr_in H; inv H. red. exists l, c0. setoid_rewrite Heqc0. split; auto. split; auto.
      apply line_of_tag_some in Heqo. rewrite Heqc0 in Heqo. destruct Heqo as (? & A & EQ); inv A. auto.
      unfold process_of_addr in POA. rewrite ATT in POA; simpl in POA; auto.
    }
    left. apply PeanoNat.Nat.ltb_lt.
    rewrite PeanoNat.Nat.ltb_lt in H. unfold num_lockable_in_set in H. rewrite ATT in H. simpl.
    generalize (sim_r_cardinal s CSIM). lia.
  }

  destruct (addr_to_tagset addr) eqn:ATT.
  apply add_tagset_in_scache_lock_line; auto.
  unfold process_of_addr in POA. rewrite ATT in POA. simpl in POA; auto.
  rewrite tcache_line_of_addr_update_usage. eapply maybe_writeback_tcache_line_of_addr; eauto.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
Qed.

Lemma unlock_bsim:
  forall ms rs mt rt addr m t tau,
    sim_source_target (ms, rs) (mt, rt) ->
    unlock_wb mt rt addr = (m, t, tau) ->
    process_of_addr addr = Some D ->
    sim_source_target (ms, remove_tagset_in_scache (addr_to_tagset addr) rs) (m, t).
Proof.
  unfold unlock_wb.
  intros ms rs mt rt addr m t tau SIM UNLOCK POA.
  repeat destr_in UNLOCK; inv UNLOCK.
  destruct (addr_to_tagset addr) eqn:ATT.
  apply remove_tagset_in_scache_unlock_line; auto.
  unfold process_of_addr in POA. rewrite ATT in POA. simpl in POA; auto.
  rewrite tcache_line_of_addr_update_usage. eapply maybe_writeback_tcache_line_of_addr; eauto.
  apply tcache_update_usage_bsim. eapply write_back_fetch_maybe_sim; eauto.
Qed.


Lemma address_of_evicted_line_unlock_line:
  forall t l addr a l0,
    address_of_evicted_line (unlock_line t addr l) l0 a = address_of_evicted_line t l0 a.
Proof.
  unfold address_of_evicted_line, unlock_line. intros.
  destr. destr.
  generalize (update_cache_line_eq t addr l _ unlock_line_cl_preserves_tag).
  destr. destr. destr.
  - intros (p & EQ); rewrite EQ in Heqp; clear EQ.
    unfold update_tcache in Heqp. destr_in Heqp; inv Heqp.
    + simpl. rewrite Heqp0. destruct (eq_dec l l0).
      subst. rewrite update_ways_same. setoid_rewrite Heqo. simpl. auto.
      rewrite update_ways_other; auto.
    + rewrite H0. auto.
  - intro EQ; rewrite EQ in Heqp; clear EQ. rewrite Heqp. auto.
Qed.


Lemma address_of_evicted_line_tcache_update_usage:
  forall t mi addr a l0,
    address_of_evicted_line (tcache_update_usage t addr mi) l0 a = address_of_evicted_line t l0 a.
Proof.
  unfold address_of_evicted_line, tcache_update_usage. intros.
  destr. destr. destr_in Heqp. destr_in Heqp. destr_in Heqp.
  unfold update_tcache in Heqp. destr_in Heqp; inv Heqp.
  + simpl. rewrite Heqp0. auto.
  + rewrite H0. auto.
Qed.


Lemma write_back_bsim2:
  forall ms rs mt rt addr l m tr oa,
    sim_source_target (ms, rs) (mt, rt) ->
    write_back mt rt l oa = (m, tr) ->
    option_rel (fun v1 v2 => addr_to_tagset v1 = addr_to_tagset v2) oa ((address_of_evicted_line rt l addr)) ->
    sim_source_target (ms, rs) (m, rt).
Proof.
  intros ms rs mt rt addr l m tr oa (MSIM, (CSIM, (CC, TLD))) WB OR.
  split; auto.
  unfold write_back in WB.
  repeat destr_in WB; inv WB; auto.
  - inv OR.
    red. intros.
    rewrite MSIM; eauto.
    unfold simple_load. destr. destr. destr; auto.
    apply line_of_tag_some in Heqo. destruct Heqo as (? & EQ & TAG). rewrite EQ. auto.
    rewrite set_line_get.
    2: apply cl_content_length.
    destr; auto.
    unfold address_of_evicted_line in H0. repeat destr_in H0; inv H0.
    rewrite addr_to_tagset_to_addr in H1. rewrite Heqt in H1. inv H1.
    rewrite Heqp1 in Heqp; inv Heqp.
    rewrite Heqc3 in Heqc0; inv Heqc0.
    clear Heqs3. rewrite Heqt0 in e.
    rewrite Heqt in e. inv e.
    rewrite Heqp1 in Heqp0; inv Heqp0.
    eapply line_of_tag_none in Heqo; eauto. congruence.
  - split; auto. split; auto.
    red; intros.
    specialize (CC _ _ _ H _ _ H0 _ H1 _ H2 H3).
    unfold write_back in WB.
    repeat destr_in WB; inv WB; auto.
    inv OR. rewrite get_set_line_other. auto.
    apply cl_content_length.
    rewrite Heqt0, H. intro EQ; inv EQ.
    rewrite Heqp in H0; inv H0.
    unfold address_of_evicted_line in H5. repeat destr_in H5; inv H5.
    rewrite H6 in Heqt0.
    rewrite addr_to_tagset_to_addr in Heqt0. inv Heqt0.
    rewrite Heqp0 in Heqp; inv Heqp.
    rewrite Heqc1 in Heqc0; inv Heqc0.
    edestruct line_of_tag_some as (? & EQ & TAG). apply H1. rewrite H2 in EQ; inv EQ.
    destruct (proj2_sig w).
    specialize (wft_nodup _ _ _ _ H2 Heqc1). trim wft_nodup. auto. destruct wft_nodup; subst. congruence.
Qed.


Lemma unlock_wt_bsim:
  forall ms rs mt rt addr m t tau,
    sim_source_target (ms, rs) (mt, rt) ->
    unlock_wt mt rt addr = (m, t, tau) ->
    process_of_addr addr = Some D ->
    sim_source_target (ms, remove_tagset_in_scache (addr_to_tagset addr) rs) (m, t).
Proof.
  unfold unlock_wt.
  intros ms rs mt rt addr m t tau SIM UNLOCK POA.
  repeat destr_in UNLOCK; inv UNLOCK.
  eapply write_back_bsim2. 2: eauto.
  destruct (addr_to_tagset addr) eqn:ATT.
  apply remove_tagset_in_scache_unlock_line; auto.
  unfold process_of_addr in POA. rewrite ATT in POA. simpl in POA; auto.
  rewrite tcache_line_of_addr_update_usage. eapply maybe_writeback_tcache_line_of_addr; eauto.
  apply tcache_update_usage_bsim. eapply write_back_fetch_maybe_sim; eauto.
  rewrite address_of_evicted_line_unlock_line.
  rewrite address_of_evicted_line_tcache_update_usage.
  unfold address_of_evicted_line. destr. destr.
  eapply maybe_writeback_tcache_line_of_addr in Heqp; eauto.
  unfold tcache_line_of_addr in Heqp. rewrite Heqt, Heqp0 in Heqp.
  apply line_of_tag_some in Heqp. destruct Heqp as (? & EQ & TAG). rewrite EQ. constructor.
  rewrite addr_to_tagset_to_addr. subst. auto.
Qed.

Lemma write_in_cache_simA:
  forall ms addr v mt rs rt l t s u w,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    sim_source_target (ms, rs) (mt, write_in_cache rt addr v l).
Proof.
  intros ms addr v mt rs rt l t s u w (MSIM, (CSIM, (CC, TLD))) POA ATT RT LOT. split.
  red. intros. rewrite MSIM; auto. unfold simple_load, write_in_cache.
  destr. destr. assert (t0 <> t).
  intro; subst.
  unfold process_of_addr in POA, H. rewrite ATT in POA. rewrite Heqt0 in H. simpl in *. congruence.
  generalize (update_cache_line_eq rt addr l _ (fun cl => write_in_cache_cl_preserves_tag cl addr v)). rewrite ATT, RT.
  edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ. intros (p & EQ2); rewrite EQ2; clear EQ2.
  unfold update_tcache. destruct (eq_dec s s0); subst.
  - simpl. rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
    rewrite RT in Heqp; inv Heqp. destruct (eq_dec (cl_tag x) t0); try congruence.
    destruct s1; simpl in *. destr; auto.
    destruct (Fin.eq_dec l l0). subst. rewrite EQ.
    apply line_of_tag_some in Heqo. rewrite EQ in Heqo. destruct Heqo as ( ? & A & B); inv A; congruence.
    rewrite update_ways_other; auto.
  - rewrite Heqp. auto.
  - split. red. intros. red in CSIM.
    rewrite CSIM. unfold write_in_cache.
    rewrite tagset_locked_update_cache_line. tauto. reflexivity.
    split.
    red; intros.
    generalize (write_in_cache_unfold rt addr l v ). rewrite ATT, RT.
    edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2.
    unfold update_tcache in H0. destr_in H0; inv H0.
    + simpl in *. rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig.
      repeat destr_in H1; inv H1.
      rewrite update_ways_same in H2. inv H2. simpl in *. congruence.
      rewrite update_ways_other in H2; eauto.
    + eauto.
    + eauto using write_in_cache_locked_dirty.
Qed.


Lemma address_of_tagset_nth:
  forall addr0 n v0,
    nth_error (addresses_of_tagset (addr_to_tagset addr0)) n = Some v0 ->
    n = address_to_wordid v0 /\ addr_to_tagset v0 = (addr_to_tagset addr0).
Proof.
  intros.
  generalize (proj1 (NoDup_nth_error _) (addresses_of_tagset_nodup (addr_to_tagset addr0)) n (address_to_wordid v0)).
  rewrite H.
  generalize H; intro H2; apply nth_error_In in H2. apply addresses_of_tagset_in in H2. rewrite <-H2.
  rewrite address_to_wordid_nth. intuition. apply H0; auto.
  eapply nth_error_Some. congruence.
Qed.

Lemma write_in_cache_wt_simA:
  forall ms addr v mt rs rt l t s u w,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    (* tcache_is_locked rt addr = true -> *)
    sim_source_target (ms, rs) (do_store mt addr v, write_in_cache_wt rt addr v l).
Proof.
  intros ms addr v mt rs rt l t s u w (MSIM, (CSIM, (CC, TLD))) POA ATT RT LOT.
  split; [|split;[|split]].
  - red. intros. rewrite MSIM; auto. unfold simple_load, write_in_cache_wt.
    destr. destr. assert (t0 <> t).
    intro; subst.
    unfold process_of_addr in POA, H. rewrite ATT in POA. rewrite Heqt0 in H. simpl in *. congruence.
    generalize (update_cache_line_eq rt addr l _ (fun cl => write_in_cache_cl_wt_preserves_tag cl addr v)). rewrite ATT, RT.
    edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ. intros (p & EQ2); rewrite EQ2; clear EQ2.
    unfold update_tcache. destruct (eq_dec s s0); subst.
    + simpl. rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      rewrite RT in Heqp; inv Heqp. destruct (eq_dec (cl_tag x) t0); try congruence.
      destruct s1; simpl in *. destr; auto.
      destruct (Fin.eq_dec l l0). subst. rewrite EQ.
      apply line_of_tag_some in Heqo. rewrite EQ in Heqo. destruct Heqo as ( ? & A & B); inv A; congruence.
      rewrite update_ways_other; auto.
      apply line_of_tag_some in Heqo. destruct Heqo as (? & EQ2 & TAG). rewrite EQ2. auto.
      unfold do_store; destr; eauto. subst. congruence.
    + rewrite Heqp.
      destr; auto.
      apply line_of_tag_some in Heqo. destruct Heqo as (? & EQ2 & TAG). rewrite EQ2. auto.
      unfold do_store; destr; eauto. subst. congruence.
  - red. intros. red in CSIM.
    rewrite CSIM. unfold write_in_cache_wt.
    rewrite tagset_locked_update_cache_line. tauto. reflexivity.
  - red; intros.
    generalize (write_in_cache_wt_unfold rt addr l v ). rewrite ATT, RT.
    edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2.
    unfold update_tcache in H0.
    destr_in H0; inv H0.
    + simpl in *. rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig.
      repeat destr_in H1; inv H1.
      * rewrite update_ways_same in H2. inv H2. simpl in *.
        apply nth_error_eq. intros.
        unfold get_line, set_word.
        rewrite nth_error_map.
        unfold option_map. destr.
        2:{
          apply nth_error_None in Heqo. rewrite length_addresses_of_tagset in Heqo. rewrite length_get_line in H0. lia.
        }
        erewrite (nth_error_nth' (set_nth _ _ _)).
        rewrite nth_set_nth_gss.
        destr; subst.
        unfold do_store; destr; auto.
        rewrite H, <- ATT in Heqo. rewrite address_to_wordid_nth in Heqo. inv Heqo. congruence.
        rewrite <- CC; eauto.
        transitivity (Some (get_word v0 (get_line mt v0))).
        2:{
          apply address_of_tagset_nth in Heqo. destruct Heqo; subst.
          f_equal. unfold get_word. f_equal.
          unfold get_line. congruence.
        }
        f_equal.
        unfold do_store. rewrite <- get_word_line. destr; auto. subst.
        apply address_of_tagset_nth in Heqo. intuition congruence.
        rewrite cl_content_length; apply address_to_wordid_valid.
        rewrite length_get_line in H0. rewrite set_nth_length, cl_content_length; auto.
        rewrite length_get_line, set_word_preserves_length; auto.
        apply cl_content_length.
      * rewrite update_ways_other in H2; eauto.
        erewrite <- CC; eauto.
        apply nth_error_eq. intros.
        unfold get_line.
        destruct (nth_error (addresses_of_tagset (addr_to_tagset addr0)) n1) eqn:?.
        2:{
          apply nth_error_None in Heqo0. rewrite length_addresses_of_tagset in Heqo0. rewrite length_get_line in H0. lia.
        }
        rewrite ! nth_error_map. rewrite Heqo0. simpl. unfold do_store. destr; auto. subst.
        apply nth_error_In in Heqo0. apply addresses_of_tagset_in in Heqo0. rewrite ATT, H in Heqo0. inv Heqo0.
        erewrite TLD in H3; eauto. congruence.
        rewrite ! length_get_line. auto.
    + erewrite <- CC; eauto.
      apply nth_error_eq. intros.
      unfold get_line.
      destruct (nth_error (addresses_of_tagset (addr_to_tagset addr0)) n0) eqn:?.
      2:{
        apply nth_error_None in Heqo. rewrite length_addresses_of_tagset in Heqo. rewrite length_get_line in H0. lia.
      }
      rewrite ! nth_error_map. rewrite Heqo. simpl. unfold do_store. destr; auto. subst.
      apply nth_error_In in Heqo. apply addresses_of_tagset_in in Heqo. rewrite ATT, H in Heqo. inv Heqo. congruence.
      rewrite ! length_get_line. auto.
  - eauto using write_in_cache_wt_locked_dirty.
Qed.

Lemma write_in_cache_wt_simA_locked:
  forall ms addr v mt rs rt l t s u w,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    addr_to_tagset addr = (t, s) ->
    rt s = (u, w) ->
    line_of_tag (proj1_sig w) t = Some l ->
    tcache_is_locked rt addr = true ->
    sim_source_target (ms, rs) (mt, write_in_cache_wt rt addr v l).
Proof.
  intros ms addr v mt rs rt l t s u w (MSIM, (CSIM, (CC, TLD))) POA ATT RT LOT TIL.
  split; [|split;[|split]].
  - red. intros. rewrite MSIM; auto. unfold simple_load, write_in_cache_wt.
    destr. destr. assert (t0 <> t).
    intro; subst.
    unfold process_of_addr in POA, H. rewrite ATT in POA. rewrite Heqt0 in H. simpl in *. congruence.
    generalize (update_cache_line_eq rt addr l _ (fun cl => write_in_cache_cl_wt_preserves_tag cl addr v)). rewrite ATT, RT.
    edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ. intros (p & EQ2); rewrite EQ2; clear EQ2.
    unfold update_tcache. destruct (eq_dec s s0); subst.
    + simpl. rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      rewrite RT in Heqp; inv Heqp. destruct (eq_dec (cl_tag x) t0); try congruence.
      destruct s1; simpl in *. destr; auto.
      destruct (Fin.eq_dec l l0). subst. rewrite EQ.
      apply line_of_tag_some in Heqo. rewrite EQ in Heqo. destruct Heqo as ( ? & A & B); inv A; congruence.
      rewrite update_ways_other; auto.
    + rewrite Heqp.
      destr; auto.
  - red. intros. red in CSIM.
    rewrite CSIM. unfold write_in_cache_wt.
    rewrite tagset_locked_update_cache_line. tauto. reflexivity.
  - red; intros.
    generalize (write_in_cache_wt_unfold rt addr l v ). rewrite ATT, RT.
    edestruct line_of_tag_some as (? & EQ & TAG). apply LOT. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2.
    unfold update_tcache in H0.
    destr_in H0; inv H0.
    + simpl in *. rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig.
      repeat destr_in H1; inv H1.
      * rewrite update_ways_same in H2. inv H2. simpl in *.
        erewrite TLD in H3; eauto. congruence.
        unfold tcache_is_locked in TIL. rewrite ATT, RT, LOT, EQ in TIL. auto.
      * rewrite update_ways_other in H2; eauto.
    + erewrite <- CC; eauto.
  - eauto using write_in_cache_wt_locked_dirty.
Qed.


Lemma lock_line_simA:
  forall addr t s tc l ms rs m,
    addr_to_tagset addr = (t, s) ->
    process_of_tag t = Some A ->
    tcache_line_of_addr tc addr = Some l ->
    sim_source_target (ms, rs) (m, tc) ->
    sim_source_target (ms, rs) (m, lock_line tc addr l).
Proof.
  intros addr t s tc l ms rs m ATT POT TLOA (MSIM, (CSIM, (CC, TLD))).
  split; [|split;[|split]]; auto.
  - red; intros. rewrite MSIM; auto. unfold simple_load. destr. destr.
    unfold lock_line.
    generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ; clear EQ.
    + unfold update_tcache. destruct (eq_dec s s0); subst; auto.
      rewrite Heqp in Heqp0; inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
      destruct (eq_dec t t0).
      * subst.
        rewrite TLOA.
        apply line_of_tag_some in TLOA. setoid_rewrite Heqo in TLOA. destruct TLOA as (? & EQ & TAG); inv EQ.
        destruct (eq_dec (cl_tag x) (cl_tag x)); subst; eauto.
        rewrite update_ways_same. setoid_rewrite Heqo. simpl; auto. congruence.
      * edestruct line_of_tag_some as (? & EQ & TAG). eauto. setoid_rewrite Heqo in EQ. inv EQ.
        destruct (eq_dec (cl_tag x) t0). congruence.
        destruct s2; simpl in *.
        destr; auto.
        assert (l <> l0). intros ->.
        destruct w.
        apply line_of_tag_some in Heqo0. destruct Heqo0 as ( ? & EQ & TAG2). subst.
        clear Heqp. specialize (wft_nodup _ _ _ _ Heqo EQ). congruence. destruct Fin.eq_dec. congruence.
        rewrite update_ways_other; auto.
      * rewrite Heqp. auto.
    + rewrite Heqp; auto.
  - red. intros.
    rewrite (CSIM t0 s0).
    rewrite tagset_locked_lock_line; eauto.
    split.
    intros (A & B). auto.
    intros ([A|A] & B). auto. inv A.  congruence.
  - red; intros. unfold lock_line in H0.
    generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ in H0; clear EQ; eauto.
    unfold update_tcache in H0. destr_in H0; inv H0; eauto.
    simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    repeat destr_in H1; inv H1; eauto.
    rewrite update_ways_same in H2; inv H2. simpl in *. congruence.
    rewrite update_ways_other in H2; eauto.
  - red; intros. unfold lock_line in H0.
    generalize (update_cache_line_eq tc addr l _ lock_line_cl_preserves_tag).
    rewrite ATT. destr.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    edestruct line_of_tag_some as ( ? & EQ & TAG). apply TLOA. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2. unfold update_tcache in H0. destr_in H0; eauto.
    inv H0. simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    repeat destr_in H1; inv H1.
    rewrite update_ways_same in H2. inv H2.  simpl. auto.
    rewrite update_ways_other in H2; eauto.
Qed.


Lemma unlock_line_simA:
  forall addr t s tc l ms rs m,
    addr_to_tagset addr = (t, s) ->
    process_of_tag t = Some A ->
    tcache_line_of_addr tc addr = Some l ->
    sim_source_target (ms, rs) (m, tc) ->
    sim_source_target (ms, rs) (m, unlock_line tc addr l).
Proof.
  intros addr t s tc l ms rs m ATT POT TLOA (MSIM, (CSIM, (CC, TLD))).
  split; [|split;[|split]]; auto.
  - red; intros. rewrite MSIM; auto. unfold simple_load. destr. destr.
    unfold unlock_line.
    generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ; clear EQ.
    + unfold update_tcache. destruct (eq_dec s s0); subst; auto.
      rewrite Heqp in Heqp0; inv Heqp0. simpl.
      rewrite line_of_tag_update_ways; auto. 2: apply proj2_sig. simpl.
      unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
      destruct (eq_dec t t0).
      * subst.
        rewrite TLOA.
        apply line_of_tag_some in TLOA. setoid_rewrite Heqo in TLOA. destruct TLOA as (? & EQ & TAG); inv EQ.
        destruct (eq_dec (cl_tag x) (cl_tag x)); subst; eauto.
        rewrite update_ways_same. setoid_rewrite Heqo. simpl; auto. congruence.
      * edestruct line_of_tag_some as (? & EQ & TAG). eauto. setoid_rewrite Heqo in EQ. inv EQ.
        destruct (eq_dec (cl_tag x) t0). congruence.
        destruct s2; simpl in *.
        destr; auto.
        assert (l <> l0). intros ->.
        destruct w.
        apply line_of_tag_some in Heqo0. destruct Heqo0 as ( ? & EQ & TAG2). subst.
        clear Heqp. specialize (wft_nodup _ _ _ _ Heqo EQ). congruence. destruct Fin.eq_dec. congruence.
        rewrite update_ways_other; auto.
      * rewrite Heqp. auto.
    + rewrite Heqp; auto.
  - red. intros.
    rewrite (CSIM t0 s0).
    rewrite tagset_locked_unlock_line; eauto. intuition. inv H. congruence.
  - red; intros. unfold unlock_line in H0.
    generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag).
    rewrite ATT. destr.
    destr; [intros (p & EQ)| intros EQ]; rewrite EQ in H0; clear EQ; eauto.
    unfold update_tcache in H0. destr_in H0; inv H0; eauto.
    simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    repeat destr_in H1; inv H1; eauto.
    rewrite update_ways_same in H2; inv H2. simpl in *. eapply CC. eauto. eauto. 2: eauto.
    destruct (line_of_tag (proj1_sig s1) (cl_tag c0)) eqn:?.
    apply line_of_tag_some in Heqo0. destruct Heqo0 as (? & EQ & TAG).
    destruct s1. destruct w. simpl in *. generalize (wft_nodup _ _ _ _ EQ Heqo TAG). intuition subst. auto.
    eapply line_of_tag_none in Heqo; eauto. congruence. auto.
    rewrite update_ways_other in H2; eauto.
  - unfold tcache_locked_dirty in TLD |- *. intros.
    unfold unlock_line in H0.
    generalize (update_cache_line_eq tc addr l _ unlock_line_cl_preserves_tag).
    rewrite ATT. destr.
    unfold tcache_line_of_addr in TLOA. rewrite ATT, Heqp in TLOA.
    edestruct line_of_tag_some as ( ? & EQ & TAG). apply TLOA. setoid_rewrite EQ.
    intros (p & EQ2); rewrite EQ2 in H0; clear EQ2. unfold update_tcache in H0. destr_in H0; eauto.
    inv H0. simpl in *.
    rewrite line_of_tag_update_ways in H1; auto. 2: apply proj2_sig. simpl in H1.
    repeat destr_in H1; inv H1.
    rewrite update_ways_same in H2. inv H2. simpl in *. congruence.
    rewrite update_ways_other in H2; eauto.
Qed.

Lemma opt_bsim_load:
  forall ms rs mt rt addr mt2 rt2 t v,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    load_wb mt rt addr = (mt2, rt2, t, v) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 t v SIM POA LOAD.
  unfold load_wb in LOAD. repeat destr_in LOAD. inv LOAD.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
Qed.

Lemma opt_bsim_store:
  forall ms rs mt rt addr mt2 rt2 t v,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    store_wb mt rt addr v = (mt2, rt2, t) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 t v SIM POA STORE.
  unfold store_wb in STORE. repeat destr_in STORE. inv STORE.
  destruct (addr_to_tagset addr) eqn:ATT.
  destruct (rt s) eqn:RT.
  destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:TCU.
  eapply write_in_cache_simA; eauto.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
  eapply maybe_writeback_tcache_line_of_addr in Heqp; eauto.
  apply tcache_update_usage_inv in TCU. destruct TCU as (? & TCU).
  unfold tcache_line_of_addr in Heqp. rewrite ATT, TCU in Heqp. auto.
Qed.

Lemma opt_bsim_do_store:
  forall ms rs mt rt addr v
         (SIM : sim_source_target (ms, rs) (mt, rt))
         (POA : process_of_addr addr = Some A)
         (NIC: tcache_line_of_addr rt addr = None),
    sim_source_target (ms, rs) (do_store mt addr v, rt).
Proof.
  intros. simpl in SIM. destruct_and SIM. constructor.
  - red; intros. rewrite SIM0; auto.
    unfold simple_load. destr. destr. destr; auto. destr; auto.
    unfold do_store. destr; congruence.
    unfold do_store. destr; congruence.
  - split; auto. split; auto.
    red.
    intros addr0 t s ATT w u EQ l LOT cl EQ2 DIRTY.
    rewrite <- SIM1; eauto.
    apply nth_error_eq.
    intros.
    unfold get_line, do_store.
    rewrite ! nth_error_map.
    unfold option_map. destr; auto.
    destr; subst; auto.
    apply nth_error_In in Heqo.
    apply addresses_of_tagset_in in Heqo.
    unfold tcache_line_of_addr in NIC. rewrite Heqo, ATT, EQ in NIC. congruence.
    rewrite ! length_get_line.  auto.
Qed.

Lemma opt_bsim_store_wt around:
  forall ms rs mt rt addr mt2 rt2 t v,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    store_wt around mt rt addr v = (mt2, rt2, t) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 t v SIM POA STORE.
  unfold store_wt in STORE.
  do 4 destr_in STORE. inv STORE.
  destr_in Heqp. destr_in Heqp. inv Heqp.
  - apply tcache_update_usage_bsim.
    apply opt_bsim_do_store; eauto.
  -
    assert (maybe_writeback_fetch mt rt addr  = (m, t0, (tcache_line_to_evict rt addr),  t) /\ o = Some (tcache_line_to_evict rt addr) /\ b = true).
    {
      unfold maybe_writeback_fetch. rewrite Heqs.
      destr_in Heqp. inv Heqp. split; auto.
    } clear Heqp. destruct_and H; subst.
    eapply write_back_fetch_maybe_sim in SIM. 2: eauto.
    destruct (addr_to_tagset addr) eqn:ATT.
    destruct (t0 s) eqn:RT.
    destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:TCU.
    eapply write_in_cache_wt_simA; eauto.
    apply tcache_update_usage_bsim. auto.
    eapply maybe_writeback_tcache_line_of_addr in H0; eauto.
    apply tcache_update_usage_inv in TCU. destruct TCU as (? & TCU).
    rewrite RT in TCU; inv TCU. unfold tcache_line_of_addr in H0. rewrite ATT, RT in H0. auto.
  - inv Heqp.
    destr.
    destruct (addr_to_tagset addr) eqn:ATT.
    destruct (t0 s) eqn:RT.
    destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:TCU.
    eapply write_in_cache_wt_simA; eauto.
    apply tcache_update_usage_bsim. auto.
    unfold tcache_line_of_addr' in Heqs. destr_in Heqs; inv Heqs.
    unfold tcache_line_of_addr in Heqo. rewrite ATT, RT in Heqo. 
    apply tcache_update_usage_inv in TCU. destruct TCU as (? & TCU). congruence.
    destruct (addr_to_tagset addr) eqn:ATT.
    destruct (t0 s) eqn:RT.
    destruct (tcache_update_usage t0 addr (mi_store addr v) s) eqn:TCU.
    eapply write_in_cache_wt_simA_locked; eauto.
    apply tcache_update_usage_bsim. auto.
    unfold tcache_line_of_addr' in Heqs. destr_in Heqs; inv Heqs.
    unfold tcache_line_of_addr in Heqo. rewrite ATT, RT in Heqo. 
    apply tcache_update_usage_inv in TCU. destruct TCU as (? & TCU). congruence.
    unfold tcache_is_locked in Heqb |- *.
    rewrite ATT, RT in *. rewrite TCU.
    apply tcache_update_usage_inv in TCU. destruct TCU as (? & TCU). rewrite RT in TCU; inv TCU.
    apply negb_false_iff in Heqb. auto.
Qed.

Transparent lock_wb.
Lemma opt_bsim_lock:
  forall ms rs mt rt addr mt2 rt2 t,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    lock_wb mt rt addr = Some (mt2, rt2, t) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 t SIM POA LOCK.
  unfold lock_wb in LOCK. repeat destr_in LOCK; inv LOCK.
  destruct (addr_to_tagset addr) eqn:ATT.
  eapply lock_line_simA; eauto.
  unfold process_of_addr in POA. rewrite ATT in POA. simpl in *; auto.
  rewrite tcache_line_of_addr_update_usage.
  eapply maybe_writeback_tcache_line_of_addr; eauto.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
Qed.

Lemma opt_bsim_unlock:
  forall ms rs mt rt addr mt2 rt2 t,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    unlock_wb mt rt addr = (mt2, rt2, t) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 t SIM POA UNLOCK.
  unfold unlock_wb in UNLOCK. repeat destr_in UNLOCK; inv UNLOCK.
  destruct (addr_to_tagset addr) eqn:ATT.
  eapply unlock_line_simA; eauto.
  unfold process_of_addr in POA. rewrite ATT in POA. simpl in *; auto.
  rewrite tcache_line_of_addr_update_usage. eapply maybe_writeback_tcache_line_of_addr; eauto.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
Qed.

Lemma opt_bsim_unlock_wt:
  forall ms rs mt rt addr mt2 rt2 t,
    sim_source_target (ms, rs) (mt, rt) ->
    process_of_addr addr = Some A ->
    unlock_wt mt rt addr = (mt2, rt2, t) ->
    sim_source_target (ms, rs) (mt2, rt2).
Proof.
  intros ms rs mt rt addr mt2 rt2 t SIM POA UNLOCK.
  unfold unlock_wt in UNLOCK. repeat destr_in UNLOCK; inv UNLOCK.
  destruct (addr_to_tagset addr) eqn:ATT.
  eapply write_back_bsim2. 2: eauto.
  eapply unlock_line_simA; eauto.
  unfold process_of_addr in POA. rewrite ATT in POA. simpl in *; auto.
  rewrite tcache_line_of_addr_update_usage. eapply maybe_writeback_tcache_line_of_addr; eauto.
  apply tcache_update_usage_bsim.
  eapply write_back_fetch_maybe_sim; eauto.
  rewrite address_of_evicted_line_unlock_line.
  rewrite address_of_evicted_line_tcache_update_usage.
  apply maybe_writeback_tcache_line_of_addr in Heqp.
  unfold address_of_evicted_line. rewrite ATT.
  unfold tcache_line_of_addr in Heqp. rewrite ATT in Heqp.
  destr_in Heqp.
  apply line_of_tag_some in Heqp. destruct Heqp as (? & EQ & TAG). rewrite EQ. constructor. rewrite addr_to_tagset_to_addr. congruence.
Qed.

Record target_interface_bsim (mintf: (input -> mem -> Prop) -> mem_interface (list target_event) mem_input mem_output _): Prop :=
  {
    tib_bsim:
    forall im (rs : mem * scache) (rt : mem * tcache),
      sim_source_target rs rt ->
      forall (id : process) (tau : list target_event) (mi : mem_input) (mo : mem_output) (rt' : mem * tcache),
        id = D ->
        mi_interface (mintf im) id rt mi = Some (rt', mo, tau) ->
        exists (rs' : mem * scache) (t' : source_event),
          source_mem_interface id rs mi = Some (rs', mo, t') /\ sim_source_target rs' rt';
    tib_opt_bsim:
    forall im (rs : mem * scache) (rt : mem * tcache),
      sim_source_target rs rt ->
      forall (id : process) (tau : list target_event) (mi : mem_input) (mo : mem_output) (rt' : mem * tcache),
        id = A ->
        mi_interface (mintf im) id rt mi = Some (rt', mo, tau) ->
        sim_source_target rs rt';
    tib_init:
    forall im i m t,
      mi_init (mintf im) i (m, t) ->
        im i m /\ tcache_init t;
  }.

Lemma tib_target_cache: target_interface_bsim target_cache.
Proof.
  constructor.
  - intros im rs rt SIM id0 tau mi mo rt' -> MI.
    Opaque lock_wb.
    destruct rs as (ms,rs). destruct rt as (mt, rt).
    simpl in MI. simpl.
    unfold option_bind in MI; simpl; destruct mi; simpl; unfold option_bind; repeat destr_in MI; inv MI.
    + edestruct load_bsim as (EQ & SIM2); eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
      rewrite EQ. eexists _, _; split; eauto.
    + eexists _, _; split; eauto. eapply store_bsim; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + edestruct lock_bsim as (canlock & SIM2); eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
      rewrite canlock.
      eexists _, _; split; eauto.
    + eexists _, _; split; eauto.
      eapply unlock_bsim; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
  - intros im rs rt SIM id tau mi mo rt' -> RBSTEP.
    simpl in *. subst. destruct rs as (m1, sc), rt as (m2, tc).
    destruct mi; simpl in RBSTEP; unfold option_bind in RBSTEP; repeat destr_in RBSTEP; inv RBSTEP.
    + eapply opt_bsim_load; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + eapply opt_bsim_store; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + eapply opt_bsim_lock; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + eapply opt_bsim_unlock; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
  - intros im i m t H. inv H; auto.
Qed.

Lemma tib_target_cache_wt around: target_interface_bsim (target_cache_wt around).
Proof.
  constructor.
  - intros im rs rt SIM id0 tau mi mo rt' -> MI.
    Opaque lock_wb.
    destruct rs as (ms,rs). destruct rt as (mt, rt).
    simpl in MI. simpl.
    unfold option_bind in MI; simpl; destruct mi; simpl; unfold option_bind; repeat destr_in MI; inv MI.
    + edestruct load_bsim as (EQ & SIM2); eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
      rewrite EQ. eexists _, _; split; eauto.
    + eexists _, _; split; eauto. eapply store_wt_bsim; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + edestruct lock_bsim as (canlock & SIM2); eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
      rewrite canlock.
      eexists _, _; split; eauto.
    + eexists _, _; split; eauto.
      eapply unlock_wt_bsim; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
  - intros im rs rt SIM id tau mi mo rt' -> RBSTEP.
    simpl in *. subst. destruct rs as (m1, sc), rt as (m2, tc).
    destruct mi; simpl in RBSTEP; unfold option_bind in RBSTEP; repeat destr_in RBSTEP; inv RBSTEP.
    + eapply opt_bsim_load; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + eapply opt_bsim_store_wt; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + eapply opt_bsim_lock; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
    + eapply opt_bsim_unlock_wt; eauto.
      unfold check_process_of_addr in Heqo. destr_in Heqo; inv Heqo. auto.
  - intros im i m t H. inv H; auto.
Qed.


Lemma bsim_res init_mem tc (TIB: target_interface_bsim tc):
  bsim_res (source_cache init_mem) (tc init_mem) sim_source_target (fun id => id = D).
Proof.
  split.
  - apply TIB.
  - simpl.
    intros i (mt, rt) MI. apply TIB in MI. destruct MI as (MI & TI).
    exists (mt, fun s => TagS.empty). split; auto. split; auto.
    red. intros. unfold simple_load. red in TI. destr.
    destr. apply TI in Heqp. red in Heqp.
    destr. apply line_of_tag_some in Heqo. rewrite Heqp in Heqo. destruct Heqo as (? & A & B); inv A. auto.
    split.
    red. intros.
    unfold tagset_in_scache. simpl.
    destruct (rt s) eqn:?.
    apply TI in Heqp. red in Heqp.
    unfold tag_locked. setoid_rewrite Heqp.
    vm_compute TagS.mem.
    split. congruence. intros ((? & A) & _). destruct A as (? & A & ?); inv A.
    split.
    red; intros.
    apply TI in H0. red in H0. congruence.
    red. intros. apply TI in H0. red in H0. congruence.
Qed.

Lemma opt_bsim_res_tcache init_mem tc (TIB: target_interface_bsim tc):
  opt_bsim_res (tc init_mem) sim_source_target.
Proof.
  constructor.
  intros. inv TIB. eauto.
Qed.

Lemma get_instr_coherent_cache  im tc (TIB: target_interface_bsim tc):
  forall ss1 m t i1 tr1 m2 t2 sm sc,
    sim_source_target (sm, sc) (m, t) ->
    get_instr (tc im) D (StateC ss1 (m, t)) i1 tr1 ((m2,t2)) ->
    sim_source_target (sm, sc) (m2, t2).
Proof.
  intros. inv H0.
  eapply tib_bsim in H4; eauto.
  destruct H4 as ( ? & ? & MI & SIM). simpl in MI. unfold option_bind in MI.
  destr_in MI; inv MI. destr_in Heqo; inv Heqo. inv H1. auto.
Qed.

Lemma get_instr_sim  init_mem tc (TIB: target_interface_bsim tc):
  forall sm tm rs i ti m' i2 ti2 tm' ,
    sim_source_target sm tm ->
    get_instr (source_cache init_mem) D (StateC rs sm) i ti m' ->
    get_instr (tc init_mem) D (StateC rs tm) i2 ti2 tm' ->
    i = i2 /\ sim_source_target m' tm'.
Proof.
  intros sm tm rs i ti m' i2 ti2 tm' SIM GI1 GI2. inv GI1. inv GI2. destruct sm, tm. simpl in *.
  unfold option_bind in *.
  repeat destr_in H2; inv H2.
  repeat destr_in Heqo; inv Heqo.
  eapply tib_bsim in H4; eauto. 2: instantiate (1:= (_,_)); simpl; eapply SIM.
  destruct H4 as ( ? & ? & MI & SIM2). simpl in MI. unfold option_bind in MI.
  destr_in MI; inv MI. destr_in Heqo; inv Heqo. inv H0.
  intuition congruence.
Qed.

Lemma sim_not_in_cache:
  forall L C t s,
    sim_r L C ->
    process_of_tag t = Some D ->
    tagset_in_scache (t,s) L = false -> TagS.mem t (locked_tags C s) = false.
Proof.
  intros L C t s SIM POT TIS.
  cut (~ TagS.In t (locked_tags C s)).  intro NIN.
  rewrite <- TagS_In_mem in NIN.
  destruct (TagS.mem t (locked_tags C s)); congruence.
  intro IN.
  eapply sim_r_in_impl' in IN; eauto.
  unfold tagset_in_scache in TIS.
  rewrite <- not_true_iff_false in TIS. rewrite TagS_In_mem in TIS.
  simpl in TIS. congruence.
Qed.

Lemma get_instr_sim2  init_mem tc (TIB: target_interface_bsim tc):
  forall sm m sc rt rs i ti2 tm' ,
    sim_source_target (sm, sc) (m, rt) ->
    get_instr (tc init_mem) D (StateC rs (m, rt)) i ti2 tm' ->
    exists ti m',
           get_instr (source_cache init_mem) D (StateC rs (sm, sc)) i ti m' /\
             sim_source_target m' tm'.
Proof.
  intros. inv H0.
  edestruct tib_bsim as (? & ? & MI & SIM); eauto.
  eexists _, _; split. econstructor. eauto. eauto. all: eauto.
Qed.

Lemma attacker_get_instr_sim  init_mem tc (TIB: target_interface_bsim tc):
forall sm tm1 sc tc1
       (SIM : sim_source_target (sm, sc)(tm1, tc1))
       i ti m2 rs1
       (GI : get_instr (tc init_mem) A (StateC rs1 (tm1, tc1)) i ti m2),
  sim_source_target (sm, sc) m2.
Proof.
  intros.
  inv GI.
  eapply tib_opt_bsim; eauto.
Qed.

Lemma get_instr_sim_right  init_mem tc (TIB: target_interface_bsim tc) id:
  forall ss1 m t i1 tr1 m2 t2 sm sc,
    sim_source_target (sm, sc) (m, t) ->
    get_instr (tc init_mem) id (StateC ss1 (m, t)) i1 tr1 ((m2,t2)) ->
    sim_source_target (sm, sc) (m2, t2).
Proof.
  intros.
  destruct id.
  edestruct get_instr_sim2 as (ti & m' & GI & SIM); eauto. inv GI. simpl in H4. unfold option_bind in H4.
  repeat destr_in H4; inv H4. destr_in Heqo; inv Heqo. auto.
  eapply attacker_get_instr_sim; eauto.
Qed.

Lemma attacker_mi_interface_sim   init_mem tc (TIB: target_interface_bsim tc) :
forall sm tm mi tm' mo ti
       (SIM : sim_source_target sm tm)
       (MI : mi_interface (tc init_mem) A tm mi = Some (tm', mo, ti)),
  sim_source_target sm tm'.
Proof.
  intros.
  eapply TIB; eauto.
Qed.

Lemma attacker_preserves_sim   init_mem tc (TIB: target_interface_bsim tc) :
  forall r' r1 r2 rs1 rs2 t,
    sim_source_target r' r1 ->
    core_step (tc init_mem) A (StateC rs1 r1) t (StateC rs2 r2) ->
    sim_source_target r' r2.
Proof.
  intros (sm, sc) (tm1, tc1) (tm2, tc2) rs1 rs2 t SIM STEP.
  inv STEP; eapply attacker_get_instr_sim in GI; eauto using attacker_mi_interface_sim.
Qed.

Lemma slocked_tcache_is_locked:
  forall sc tc, sim_r sc tc ->
                forall a,
                  process_of_addr a = Some D ->
                  slocked sc a = tcache_is_locked tc a.
Proof.
  unfold slocked, tcache_is_locked. intros. destr.
  apply ZMicromega.eq_true_iff_eq.
  rewrite (H t). unfold tagset_locked. destr.
  split.
  intros ((l & cl & EQ & LOCKED & TAG) & POT). rewrite (proj2 (line_of_tag_spec (proj2_sig s0) t l)).
  rewrite EQ. auto. eauto.
  intros A. repeat destr_in A; inv A.
  unfold process_of_addr in H0. rewrite Heqt in H0. simpl in H0. split; auto.
  red.
  apply line_of_tag_some in Heqo.  rewrite Heqc0 in Heqo. destruct Heqo as (? & EQ & TAG); inv EQ. eauto.
Qed.
