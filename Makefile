build-all: binutils	ccomp	simulator compile-benchmark

binutils:
	cd binutils-gdb && \
	mkdir build && \
	cd build && \
	../configure --target=riscv64-unknown-elf --prefix=/opt/riscv_custom --enable-gold=no --enable-gprofng=no --disable-libada --disable-gdb --disable-gprof && \
	make -j8

ccomp:
	cd compcert && \
	eval `opam env --switch=4.13.1 --set-switch` && \
	./configure rv32-scratchs && \
	make -j8

opam-switch:
	(opam switch create 4.13.1 || true) && \
	eval `opam env --switch=4.13.1 --set-switch` && \
	opam install coq=8.13.2 menhir stdint ppx_bitstring bignum


simulator:
	cd leakage-simulator && \
	eval `opam env --switch=4.13.1 --set-switch` && \
	opam install menhir stdint ppx_bitstring bignum && \
	dune build

proof:
	eval `opam env --switch=4.13.1 --set-switch` && \
	make -C oni-formalisation

compile-benchmark:
	make -C benchmark compile-all

tests-fast:
	make -C benchmark tests-fast

tests-complete:
	make -C benchmark tests-complete
	
clean-bench:
	make -C benchmark clean

