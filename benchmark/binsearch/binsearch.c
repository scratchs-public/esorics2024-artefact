
#include "../lib_common.h"
	
parameters param = {LOCKED_BRANCHLESS,1,0,83}; //default values are 1 search in locked-branchless mode with seed = 83


#ifndef ARRAY_LENGTH
	#define ARRAY_LENGTH       	4096 //Amount of elements in random arrays

#endif
#ifndef MAX_SEARCH
	#define MAX_SEARCH 	4096 // Amount of searched elements during test

#endif



// An iterative binary search function.
int binarySearchExposed(char arr[], int l, int r, char x)
{
    while (l <= r) {
        int m = l + (r - l) / 2;
 
        // Check if x is present at mid
        if (arr[m] == x)
            return m;
 
        // If x greater, ignore left half
        if (arr[m] < x)
            l = m + 1;
 
        // If x is smaller, ignore right half
        else
            r = m - 1;
    }
 
    // If we reach here, then element was not present
    return -1;
}

//Version with constant control flow: will keep cutting array in half even if value is found.
//We suppose uniquness of values in arr[]
int binarySearchBranchless(char arr[], int l, int r, char x)
{
    int res = -1;
    int found = 0;
    int higher = 0;
    int m;
    //printf("Start Branchless search of %u.\n", x);
    //int nb_iter =0;
    //while (l <= r) {
	for(int i = (r - l); i>0; i = i/2)
	{
		
//nb_iter++;
        m = l + (r - l) / 2;
        ////pf("  l= %d,  m=%d,  r=%d.\n", l,m,r);
 
        // Check if x is present at mid
        //if (arr[m] == x)
        //    res = m;    //we still have to go to the end of the while, otherwise the attacker would have a little bit of info
        found = (arr[m] == x);
        //res = ((1-found)*res) + (found*m);
		res = CTchoice(found, m, res);


        // If x greater, ignore left half
        //if (arr[m] < x)
        //    l = m + 1;
        higher = (arr[m] < x);

        //l = (higher*(m + 1))+ ((1-higher)*l);
		l = CTchoice(higher, (m + 1), l);
 
        // If x is smaller, ignore right half
        //else
        //    r = m - 1;
        //r = (higher*r)+ ((1-higher)*(m - 1));
		r = CTchoice(higher, r, (m - 1));

    }
    ////pf("Stop search of %u with res %d after nb_iter = %d.\n", x,res,nb_iter);
    return res;
}

//Binsearch impossible in CT classic, the best we can do is linearch search
int linearSearch(char arr[], int l, int r, char x)
{
	//printf("Start linear search of %u.\n", x);
	int res =-1;
    int cond = 0; //used for CT branch
	for(int i = l; i<=r; i++)
	{
        cond = arr[i] == x;
		res = CTchoice(cond,i,res);
		//printf("     arr[%d] = %d, current res = %d.\n", i, arr[i], res);
	}
    return res;
}









int main(int argc,char *argv[])
	{
	set_parameters_from_args(argc, argv, &param);
	display_params(param,argv[0]);
    setRandomSeed(param.seed);

	//generates a sorted array of random values
	char arr[ARRAY_LENGTH];
	int max_distance_between_elements = 8;
	int valid_array = 0;
	int current_value;
	while(valid_array == 0) //will restart array random generation until no overflow
	{
		current_value =0;
		valid_array =1;
		for(int i = 0; (i < param.length && valid_array); i++)
		{
			current_value += (char) randLCGBounded(max_distance_between_elements);

			if(current_value<0)
			{
				valid_array =0;
				printf("\nRandom sorted array generation failed with max distance = %d, will try again with shorter distance.\n ",max_distance_between_elements);
				max_distance_between_elements --;
				//decrease this distance to increase chance of generating a valid array
			} else {
				arr[i] = current_value;
			}
		}
	}
	
	int search_length = param.length; //here, lenght on the experiment is the amount of searched element, not the lenght of the array 
    char search_values[MAX_SEARCH];
    int search_results[MAX_SEARCH];
	int higer_searched_val = 50 + arr[param.length-1]; //Do not search values to high above the max val of the array to avoid to much miss search
    for(int i = 0; i<search_length; i++)
    {
        search_values[i] = randLCGBounded(higer_searched_val);
    }

    
	resume_record(); //start simulation record here

	unsigned lock_length = param.length;
	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		lock_array(1,arr , lock_length);
	}

	signal(3); //mark to check how much time to lock.

	if(param.mode == BRANCHLESS || param.mode == LOCKED_BRANCHLESS)
	{
		for (int i = 0; i<search_length; i++)
		{
			search_results[i] = binarySearchBranchless(arr, 0, param.length - 1, search_values[i]) ;
		}
	
	}
	else if (param.mode == LOCKED || param.mode == EXPOSED)
	{
		for (int i = 0; i<search_length; i++)
		{
			search_results[i] = binarySearchExposed(arr, 0, param.length - 1, search_values[i]) ;
		}
	}
	else if (param.mode == CT_CLASSIC)
	{
		for (int i = 0; i<search_length; i++)
		{
			search_results[i] = linearSearch(arr, 0, param.length - 1, search_values[i]) ;
		}
	}

	signal(4); //mark to check how much time to unlock.

	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		unlock_array(1,arr , lock_length);
	}

  	pause_record(); //stop simulation record here

    //verify results
    int error_count = 0;
    for (int i = 0; i<search_length; i++){
        if(search_results[i]!= -1 && search_values[i] != arr[search_results[i]])
        {
		    printf(" searched %d and found %d\n",search_values[i], search_results[i]);
			printf("  -> False positive\n");
            signal(8);
            error_count++;
        }
		else if( search_results[i]== -1 && param.mode != CT_CLASSIC)
		{
			//we trust linear search to use it for verification
			int verif = linearSearch(arr, 0, param.length - 1, search_values[i]);
			if(verif != -1)
			{
		        printf(" searched %d and found %d\n",search_values[i], search_results[i]);
				printf("   -> False negative\n");
                signal(7);
				error_count++;
			}
		}
    }
    //pf("error_count : %d \n",error_count);
	signal(5);
	printf("Error count :%d \n",error_count);
    return (error_count != 0);
	
	}
