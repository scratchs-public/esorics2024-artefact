
#include "../lib_common.h"
	
parameters param = {LOCKED_BRANCHLESS,1,0,83}; //default values are 1 search in locked-branchless mode with seed = 83


#ifndef ARRAY_LENGTH
	#define ARRAY_LENGTH       	10240 //Amount of elements in random arrays

#endif
#ifndef MAX_SEARCH
	#define MAX_SEARCH 	10240 // Amount of searched elements during test

#endif


//verify that heap is correct
//return = nb error, 0 if correct
int verify_heap(unsigned arr[], int N)
{
    int nb_error =0; 
	//int i = start; //curent node
    int l; // left = 2*i + 1
    int r; // right = 2*i + 2

	for(int i = 0; i<N-1; i++)
	{
		l = 2 * i + 1; // left = 2*i + 1
		r = 2 * i + 2; // right = 2*i + 2
		
		// If left child is larger than root
		if (l < N && arr[l] > arr[i])
		{
			printf("erreur entre index %d (%x) et %d (%x)\n",i,arr[i],l,arr[l]);
			nb_error ++;
		}

		// If right child is larger than root
		if (r < N && arr[r] > arr[i])
		{
			printf("erreur entre index %d (%x) et %d (%x)\n",i,arr[i],r,arr[r]);
			nb_error ++;
		}
	}
	if(nb_error>0){signal(0xe);}

	return nb_error;
}




//from https://www.geeksforgeeks.org/building-heap-from-array/
// C program for building Heap from Array
 
// To heapify a subtree rooted with node i which is
// an index in arr[]. N is size of heap
void swap(unsigned *a, unsigned *b)
{
    int tmp = *a;
      *a = *b;
      *b = tmp;
}

//also return amount of swap done. res = 0 means already a correct heap.
void heapify_rec(unsigned arr[], int N, int i)
{
    int largest = i; // Initialize largest as root
    int l = 2 * i + 1; // left = 2*i + 1
    int r = 2 * i + 2; // right = 2*i + 2
 
    // If left child is larger than root
    if (l < N && arr[l] > arr[largest])
        largest = l;
 
    // If right child is larger than largest so far
    if (r < N && arr[r] > arr[largest])
        largest = r;
 
    // If largest is not root
    if (largest != i) {
        swap(&arr[i], &arr[largest]);
 
        // Recursively heapify the affected sub-tree
        heapify_rec(arr, N, largest);
    }
}
 
// Function to build a Max-Heap from the given array
void buildHeap(unsigned arr[], int N)
{
    // Index of last non-leaf node
    int startIdx = (N / 2) - 1;
 
    // Perform reverse level order traversal
    // from last non-leaf node and heapify
    // each node
    for (int i = startIdx; i >= 0; i--) {
        heapify_rec(arr, N, i);
    }
}
 


// A utility function to print the array
// representation of Heap
void printHeap(unsigned arr[], int N)
{
	int nb_errors = verify_heap(arr,N);
	if(nb_errors ==0){
    	printf("Array representation of Heap is:\n");
	}
	else{
    	printf("Array representation of INCORRECT Heap (%d errors) is:\n",nb_errors);
	}
 
    for (int i = 0; i < N; ++i)
        printf("%x ",arr[i]);
    printf("\n");
}
 




//also return amount of swap done. res = 0 means already a correct heap.
void heapify_exposed(unsigned arr[], int N)
{
	int i = 0; //root node
    int largest = i; // Initialize largest as root
    int l; // left = 2*i + 1
    int r; // right = 2*i + 2
	unsigned char l_cond;//boolean for ct choice
	unsigned char r_cond;//boolean for ct choice


	unsigned i_val;
	unsigned l_val;
	unsigned r_val;
	unsigned largest_val;


	for(int level_floor = 0; level_floor<N; level_floor = level_floor*2 +1)
	{
		l = 2 * i + 1; // left = 2*i + 1 // reg x9
		r = 2 * i + 2; // right = 2*i + 2 // reg x18
		/*
		// If left child is larger than root
		l_cond = (l < N && arr[l] > arr[largest]);
		largest = CTchoice(l_cond,l,largest);

		// If right child is larger than largest so far
		r_cond = (r < N && arr[r] > arr[largest]);
		largest = CTchoice(r_cond,r,largest);
		
        //swap(&arr[i], &arr[largest]); //compiled with branch ?
		tmp =arr[i];
		arr[i] = arr[largest];
		arr[largest] = tmp;*/


		l_cond = (l < N && l_val > i_val);
		//largest = CTchoice(l_cond,l,i);
		largest_val = CTchoice(l_cond,l_val,i_val);

		// If right child is larger than largest so far
		r_cond = (r < N && r_val > largest_val);
		//largest = CTchoice(r_cond,r,largest);
		//largest_val = CTchoice(r_cond,r_val,largest_val);
        //swap(&arr[i], &arr[largest]); would expose value of largest
		l_cond = l_cond-r_cond; //if we swap with right child, then we do not swap with left child

		//may swap node i with left child
		arr[i] = CTchoice(l_cond,l_val,i_val);
		arr[l] = CTchoice(l_cond,i_val,l_val);

		//may swap node i with right child
		arr[i] = CTchoice(r_cond,r_val,i_val);
		arr[r] = CTchoice(r_cond,i_val,r_val);


		if((l_cond+r_cond) == 0)
		{
			return; //can stop heapify here
		}

		//for next level, we must take either l or r for largest
		//(because we keep going on heapify even if the classic version
		// would stop here (in the case where i == largest))
		// keeping i == largest for next level would break evrything
		i = CTchoice(r_cond,r,l);
	}
}



int heap_pop_exposed(unsigned arr[], int N)
{
	int res = arr[0];
	arr[0] = arr[N-1];

	heapify_exposed(arr, N-1); //heapify is vulnerable

	return res;
}


/*
In branchless mode, we must keep heapifying to the last level of the tree
no matter if we could stop earlier
*/
void heapify_branchless(unsigned arr[], int N)
{
	int i = 0; //root node
    int largest = i; // Initialize largest as root
    int l; // left = 2*i + 1
    int r; // right = 2*i + 2
	unsigned char l_cond;//boolean for ct choice
	unsigned char r_cond;//boolean for ct choice


	unsigned i_val;
	unsigned l_val;
	unsigned r_val;
	unsigned largest_val;


	for(int level_floor = 0; level_floor<N; level_floor = level_floor*2 +1)
	{
		l = 2 * i + 1; // left = 2*i + 1 // reg x9
		r = 2 * i + 2; // right = 2*i + 2 // reg x18
		/*
		// If left child is larger than root
		l_cond = (l < N && arr[l] > arr[largest]);
		largest = CTchoice(l_cond,l,largest);

		// If right child is larger than largest so far
		r_cond = (r < N && arr[r] > arr[largest]);
		largest = CTchoice(r_cond,r,largest);
		
        //swap(&arr[i], &arr[largest]); //compiled with branch ?
		tmp =arr[i];
		arr[i] = arr[largest];
		arr[largest] = tmp;*/


		l_cond = (l < N && l_val > i_val);
		//largest = CTchoice(l_cond,l,i);
		largest_val = CTchoice(l_cond,l_val,i_val);

		// If right child is larger than largest so far
		r_cond = (r < N && r_val > largest_val);
		//largest = CTchoice(r_cond,r,largest);
		//largest_val = CTchoice(r_cond,r_val,largest_val);
        //swap(&arr[i], &arr[largest]); would expose value of largest
		l_cond = l_cond-r_cond; //if we swap with right child, then we do not swap with left child

		//may swap node i with left child
		arr[i] = CTchoice(l_cond,l_val,i_val);
		arr[l] = CTchoice(l_cond,i_val,l_val);

		//may swap node i with right child
		arr[i] = CTchoice(r_cond,r_val,i_val);
		arr[r] = CTchoice(r_cond,i_val,r_val);




		//for next level, we must take either l or r for largest
		//(because we keep going on heapify even if the classic version
		// would stop here (in the case where i == largest))
		// keeping i == largest for next level would break evrything
		i = CTchoice(r_cond,r,l);
	}
}



int heap_pop_branchless(unsigned arr[], int N)
{
	int res = arr[0];
	arr[0] = arr[N-1];

	heapify_branchless(arr, N-1);

	return res;
}

//forced to re-heapify on each node
void heapify_constant_time_classic(unsigned arr[], int N)
{
    //int largest; 
	//int i = start; //curent node
    int l; // left = 2*i + 1
    int r; // right = 2*i + 2
	unsigned char l_cond;//boolean for ct choice
	unsigned char r_cond;//boolean for ct choice
	unsigned i_val;
	unsigned l_val;
	unsigned r_val;
	unsigned largest_val;


	for(int i = 0; i<N/2; i++)
	{
		
		l = 2 * i + 1; // left = 2*i + 1
		r = 2 * i + 2; // right = 2*i + 2
		i_val = arr[i];
		l_val = arr[l];
		r_val = arr[r];
		
		// If left child is larger than root
		l_cond = (l < N && l_val > i_val);
		//largest = CTchoice(l_cond,l,i);
		largest_val = CTchoice(l_cond,l_val,i_val);

		// If right child is larger than largest so far
		r_cond = (r < N && r_val > largest_val);
		//largest = CTchoice(r_cond,r,largest);
		//largest_val = CTchoice(r_cond,r_val,largest_val);
        //swap(&arr[i], &arr[largest]); would expose value of largest
		l_cond = l_cond - (l_cond*r_cond); //CTchoice(r_cond,0,l_cond);
		//if we swap with right child, then we do not swap with left child

		//may swap node i with left child
		arr[i] = CTchoice(l_cond,l_val,i_val);
		arr[l] = CTchoice(l_cond,i_val,l_val);

		//may swap node i with right child
		arr[i] = CTchoice(r_cond,r_val,arr[i]);
		arr[r] = CTchoice(r_cond,i_val,r_val);

	}
}


int heap_pop_constant_time_classic(unsigned arr[], int N)
{
	int res = arr[0];
	arr[0] = arr[N-1];

	heapify_constant_time_classic(arr, N-1);

	return res;
}








int main(int argc,char *argv[])
	{
	set_parameters_from_args(argc, argv, &param);
	display_params(param,argv[0]);
    setRandomSeed(param.seed);

	//generates array of random values
	unsigned arr[ARRAY_LENGTH];


	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		resume_record(); //record lock time
		lock_array(4,arr , param.length);
		signal(3); //mark to check how much time to lock.
		pause_record(); 
	}

	//generates array of random values
	for(int i = 0; i < param.length; i++)
	{
		arr[i] = randLCG();
	}
 	printHeap(arr, param.length); //print random array (not a valid heap)

    buildHeap(arr, param.length); //make it into a heap
    printHeap(arr, param.length); //reprint it (now a heap)


	int min_size = 1; //here, lenght on the experiment is the amount of searched element, not the lenght of the array 
	//i.e. we will pop the whole heap elements by elements



	resume_record(); 



	for (int N = param.length; N>min_size; N--)
	{
		if(param.mode == BRANCHLESS || param.mode == LOCKED_BRANCHLESS)
		{
			resume_record(); //record pop time
			heap_pop_branchless(arr, N);
			pause_record(); 
		}
		else if (param.mode == LOCKED || param.mode == EXPOSED)
		{
			resume_record(); //record pop time
			heap_pop_exposed(arr, N);
			pause_record(); 
		}
		else if (param.mode == CT_CLASSIC)
		{
			resume_record(); //record pop time
			heap_pop_constant_time_classic(arr, N);
			pause_record(); 
		}

		if (0 != verify_heap(arr, N-1))
		{
			printf(" \n \n   Error : \n");
			printHeap(arr, N-1);
			return 1;

		}
	}
	
	

	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		resume_record(); //record pop time
		signal(4); //mark to check how much time to unlock.
		unlock_array(4,arr , param.length);
  		pause_record(); 
	}
	//pause_record(); 

    return 0;
}
