
//#include "stdlibcustom.h"
#include RV32_LOCKED_LIB


void __attribute__ ((noinline)) ecall(int code) //prevent inlining to calle save register a7
{
	asm ("add a7,%1,zero" : "=r"(code) : "r"(code)); 
	asm("ecall");
}

#define  EXIT_FAILURE       20
#define  EXIT_MALLOC        21
#define  EXIT_FREE          22
#define  EXIT_FOPEN         23
#define  EXIT_FCLOSE        24
#define  EXIT_FGETS         25
#define  EXIT_READ_UNSIGNED_INPUT   26
//const void * NULL = ((void *)0);

void exit(int status){
	ecall(status);
	ecall(0); //simulator only stops on ecall0 
}


void resume_record(void)
{
    ecall(1); //start record
}
void pause_record(void) 
{
    ecall(2); //stop record
} 

void signal(int s)
{
    ecall(s);
} 


/*
#define printf(...) skip

//#pragma GCC poison fprint
#define fprint(...) skip
//#pragma GCC poison fflush

#define fflush(...) skip

//#pragma GCC poison stdout

#define stdout(...) skip
*/

FILE *fopen(const char *file_name, const char *mode_of_operation){
    exit(EXIT_FOPEN);
    return 0;
}
FILE *fclose(const char *file_name){
    exit(EXIT_FCLOSE);
    return 0;
}

char *fgets (char *str, int n, FILE *stream){
    exit(EXIT_FGETS);
    return 0;
}

int printf (char const * restrict __format, ...){return 0;}
FILE* fprint (int __a, ...) {return 0;}
int fprintf(FILE * restrict __stream, char const * restrict __format, ...){return 0;}
int sprintf(char * restrict __s, char const * restrict __format, ...){return 0;}
int fflush(FILE * __stream) {return 0;}
FILE* stdout = 0;
FILE* stderr = 0;


int skip (int __a, ...){return 0;}





// implementation by https://aticleworld.com/memset-in-c/
void* memset(void *s, int c,size_t len)
{
    unsigned char* p=s;
    while(len--)
    {
        *p++ = (unsigned char)c;
    }
    return s;
}


/*

long read_long_input_offseted(char args[], unsigned offset)
{
    long res =0;
    int i =offset;
    int sign = 1;
    if(args[i] == '-')
    {
        sign= -1;
        i ++;
    }

    while (args[i])
    {
        //printf("i=%d, args[i]=%c\n",i, args[i]);
        res = res*10;
        if(args[i]<'0' || args[i]>'9'){ exit(EXIT_READ_UNSIGNED_INPUT);}
        res += (long unsigned)(args[i]-'0');// ASCII conversion
        i++;
    }
    return res*sign;
}

long read_long_input(char args[]){
    read_long_input_offseted(args, 0);
}
*/

//int fprintf(void *stream s , const char *format f){} 

/*EXIT_READ_UNSIGNED_INPUT
//implementation from https://aticleworld.com/memmove-function-implementation-in-c/
void * memmove(void* dest, const void* src, size_t n)
{
    char *pDest = (char *)dest;
    const char *pSrc =( const char*)src;
    //allocate memory for tmp array
    char *tmp  = (char *)malloc(sizeof(char ) * n);
    if(((void *)0) == tmp) //((void *)0) = NULL
    {
        return ((void *)0);//((void *)0) = NULL
    }
    else
    {
        unsigned int i = 0;
        // copy src to tmp array
        for(i =0; i < n ; ++i)
        {
            *(tmp + i) = *(pSrc + i);
        }
        //copy tmp to dest
        for(i =0 ; i < n ; ++i)
        {
            *(pDest + i) = *(tmp + i);
        }
        free(tmp); //free allocated memory
    }
    return dest;
}*/

// implementation of memcpy by from //https://aticleworld.com/how-to-use-memcpy-and-how-to-write-your-own-memcpy/
void* memcpy(void* dst, const void* src, size_t cnt)
{
    char *pszDest = (char *)dst;
    const char *pszSource =( const char*)src;
    if((pszDest!= ((void *)0) /*NULL*/) && (pszSource!= ((void *)0) /*NULL*/))
    {
        while(cnt) //till cnt
        {
            //Copy byte by byte
            *(pszDest++)= *(pszSource++);
            --cnt;
        }
    }
    return dst;
}





// from https://aticleworld.com/memcmp-in-c/ , Could fail in some cases
//int memcmp(unsigned char* s1, unsigned char* s2, size_t len)
int memcmp(const void* s1, const void* s2, size_t len)
{
    unsigned char *p = (unsigned char *)s1;
    unsigned char *q = (unsigned char *)s2;
    int charCompareStatus = 0;
    //If both pointer pointing same memory block
    if (s1 == s2)
    {
        return charCompareStatus;
    }
    while (len > 0)
    {
        if (*p != *q)
        {  //compare the mismatching character
            charCompareStatus = (*p >*q)?1:-1;
            break;
        }
        len--;
        p++;
        q++;
    }
    return charCompareStatus;
}




int toupper (int letter){
    if( letter >= 'a' && letter <= 'z')
    {
        return letter - ('a' - 'A');
    }
	return letter;
}

/*
typedef struct free_block {
    size_t size;
    struct free_block* next;
} free_block;

static free_block free_block_list_head = { 0, 0 };
static const size_t overhead = sizeof(size_t);
static const size_t align_to = 16;



void* malloc(size_t size) {
    size = (size + sizeof(size_t) + (align_to - 1)) & ~ (align_to - 1);
    free_block* block = free_block_list_head.next;
    free_block** head = &(free_block_list_head.next);
    while (block != 0) {
        if (block->size >= size) {
            *head = block->next;
            return ((char*)block) + sizeof(size_t);
        }
        head = &(block->next);
        block = block->next;
    }

    block = (free_block*)sbrk(size);
    block->size = size;

    return ((char*)block) + sizeof(size_t);
}

void free(void* ptr) {
    free_block* block = (free_block*)(((char*)ptr) - sizeof(size_t));
    block->next = free_block_list_head.next;
    free_block_list_head.next = block;
}*/


//custom malloc and free without sbrk with memory allocated statically

#define pre_allocated_blocks_amount 128
#define block_size 128

static int pre_allocated_blocks[pre_allocated_blocks_amount*block_size] = {0}; //{ [0 ... (pre_allocated_blocks_amount*block_size)-1] = 0};
static unsigned char used_blocks[pre_allocated_blocks_amount]= {0};//{ [0 ... pre_allocated_blocks_amount-1] = false};

//static int assigned_pointer[pre_allocated_blocks_amount]= { [0 ... pre_allocated_blocks_amount-1] = 0};

//map of assigned pointer, used to free blocks
static void* assigned_pointers[pre_allocated_blocks_amount]= {(void *)0};//{ [0 ... pre_allocated_blocks_amount-1] = (void *)0};
static int first_assigned_block_for_pointer[pre_allocated_blocks_amount]= {0}; //{ [0 ... pre_allocated_blocks_amount-1] = 0};
static int last_assigned_block_for_pointer[pre_allocated_blocks_amount]= {0}; //{ [0 ... pre_allocated_blocks_amount-1] = 0};
static int next_index_of_pointer_to_assign = 0;


void* malloc(size_t size) {
    int consecutive_blocks_needed = ((int)size)/ (block_size * sizeof(int));
    if (((int)size) % (block_size * sizeof(int)) != 0)
    {
        consecutive_blocks_needed++; 
    }
    int considered_pointer = 0;
    int consecutive_blocks_found = 0;
    for (int i=0; i<pre_allocated_blocks_amount; i++)
    {
        if(used_blocks[i])
        {
            considered_pointer = i+1;
            consecutive_blocks_found = 0;
        }
        else
        {
            if (consecutive_blocks_found >= consecutive_blocks_needed)
            {

                for(int j = considered_pointer; j<=i; j++)
                {
                    used_blocks[j] = 1;
                }
                assigned_pointers[next_index_of_pointer_to_assign] = &pre_allocated_blocks[considered_pointer*block_size];
                first_assigned_block_for_pointer[next_index_of_pointer_to_assign]= considered_pointer;
                last_assigned_block_for_pointer[next_index_of_pointer_to_assign] = i;
                next_index_of_pointer_to_assign ++;
                return &pre_allocated_blocks[considered_pointer*block_size];
            }
            consecutive_blocks_found++;
        }
    }
    exit(EXIT_MALLOC); //no space left in pre-allocated blocks
    return (void *)0;
}

void free(void* ptr) {
    for(int i = 0; i<pre_allocated_blocks_amount; i++)
    {
        if (assigned_pointers[i] == ptr) //search in the "map" of assigned pointer the corresponding one
        {
            for (
                int next_block_to_free = first_assigned_block_for_pointer[i];
                next_block_to_free <= last_assigned_block_for_pointer[i];
                next_block_to_free ++
            ){
                used_blocks[next_block_to_free]=0;
            }
            //remove entry i of the "map" of assigned pointer
            next_index_of_pointer_to_assign --;
            assigned_pointers[i]= assigned_pointers[next_index_of_pointer_to_assign];
            first_assigned_block_for_pointer[i]= first_assigned_block_for_pointer[next_index_of_pointer_to_assign];
            last_assigned_block_for_pointer[i]= last_assigned_block_for_pointer[next_index_of_pointer_to_assign];

            return; //free done
        }

    }

    exit(EXIT_FREE); //free error
}




//from https://www.geeksforgeeks.org/c-program-to-write-your-own-atoi/
int atoi(char* str)
{
    int res = 0;
    for (int i = 0; str[i] != ' '; ++i)
    {
        res = res * 10 + str[i] - '0';
    }
    return res;
}

long int atol ( const char * str ){
    long int res = 0;
    for (int i = 0; str[i] != ' '; ++i)
    {
        res = res * 10 + str[i] - '0';
    }
    return res;
}


int strcmp(const char s1[], const char s2[] )
{
    int i = 0;  
    while (1)
    {
        if (s1[i] == '\0')
        {
            if (s2[i] == '\0')
            {
            	return 0;
            }
            return (0-(int)s2[i]);
        }
        
        if(s2[i] == '\0')
        {
        	return (int)s1[i];
        }
     
        if (s1[i] != s2[i])
        {
            return (int)(s1[i] - s2[i]);
        }
        
        i++;
    }
}


//portable get_line here : https://solarianprogrammer.com/2019/04/03/c-programming-read-file-lines-fgets-getline-implement-portable-getline/






