#!/bin/bash

SIMU="$1"
PROG="$2"
SIZE="$3"
MODE="$4"
CSVFILE="$5"

for siz in ${SIZE}; do
    echo "Size $siz";
    echo -n "${siz}" >> ./${CSVFILE};
    echo -n " Mode ";
    for mod in $MODE; do
        echo -n " $mod...";
        echo -n " " >> ./$CSVFILE;
        $SIMU $PROG --args -S,123,-M,$mod,-L,$siz --count-only | tail -n 1 | tr -d '\n'>> $CSVFILE ;
    done;
    echo "";
    echo ";" >> ./$CSVFILE;
done
