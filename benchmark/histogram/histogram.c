//Study of histogram algorithm proposed in GhostRider: A Hardware-Software System for Memory Trace Oblivious Computation

#include "../lib_common.h"
	
parameters param = {LOCKED_BRANCHLESS,1,0,83}; //default values are 1 search in locked-branchless mode with seed = 83


//#include <stdio.h>
 
/*void __attribute__ ((noinline)) ecall(int code) //prevent inlining to calle save register a7
{
	asm ("add a7,%1,zero" : "=r"(code) : "r"(code)); 
	asm("ecall");
}*/

//void ecall(int code){
//    printf("Would do ecall %d \n",code);
//}


#define MAX_LENGTH 	100000


void histogramExposed(/*secret*/ int a[], // ERAM
                      /*secret*/ int c[], // ORAM (output)
                                int len) { //len=100000 in GhostRider algo 
    /*public*/ int i;
    /*secret*/ int t, v;
    for(i=0;i<len;i++) // 100000 <= len(c)
        c[i]=0;
    i=0;
    for(i=0;i<len;i++)
    { // 100000 <= len(a)
        v=a[i];
        if(v>0) t=v%1000; //branch expose sign of v
        else t=(0-v)%1000;
        c[t]=c[t]+1; //expose t, thus v
    }
}



void histogramCT(int a[], int c[], int len){
    int i,t,v,cond,temp;
    for(i=0;i<len;i++)
    {
        c[i]=0;
        //ecall(7);
    }
    int thousand = 1000;
    for(i=0;i<len;i++)
    {
        v=a[i];
        temp = CTchoice((v>0),v,(0-v));
        t = temp%thousand;
        //t=CTchoice((v>0),v%thousand,(0-v)%thousand);

        //ecall(16384+i);
        for(int j=0;j<thousand;j++)
        {
        //ecall(9);
            cond = (j==t);
            temp = CTchoice(cond,1,0);
            temp += c[t];
            c[t] = temp;
            //c[t]=CTchoice((j==t),c[t]+1,c[t]);
        }
    }
}


void histogramBranchless(int a[], int c[], int len){
    int i,t,v;
    for(i=0;i<len;i++)
    {
        c[i]=0;
    }
    
    for(i=0;i<len;i++)
    { // 100000 <= len(a)
        v=a[i];
        t=CTchoice((v>0),v%1000,(0-v)%1000);
        c[t]=c[t]+1;
    }

}








int main(int argc, char *argv[])
{


    set_parameters_from_args(argc, argv, &param);
	display_params(param,argv[0]);
    setRandomSeed(param.seed);

    int results[MAX_LENGTH];
    
    int thousand = 1000;

	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		resume_record(); //record lock time
		lock_array(4,results,thousand);
		signal(3); //mark to check how much time to lock.
		pause_record(); 
	}

    int histogram_length = param.length;
	//generates array of random values
	int permutations[MAX_LENGTH];
	for(int i = 0; i < param.length; i++)
	{
		permutations[i] = randLCGBounded(MAX_LENGTH);
	}


    //test begin
	resume_record(); 


    if(param.mode == BRANCHLESS || param.mode == LOCKED_BRANCHLESS)
		{
			histogramBranchless(permutations, results, histogram_length);
		}
		else if (param.mode == LOCKED || param.mode == EXPOSED)
		{
			histogramExposed(permutations, results, histogram_length);
		}
		else if (param.mode == CT_CLASSIC)
		{
			histogramCT(permutations, results, histogram_length);
		}

    if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		signal(4); //mark to check how much time to unlock.
		unlock_array(4,results,thousand);
	}

  	pause_record(); //stop simulation record here
    //test end
    return 0;
}

