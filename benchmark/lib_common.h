#ifndef COMMONLIB
#define COMMONLIB



#ifdef RV32_LOCKED_LIB
	#include RV32_LOCKED_LIB
#else
    #include <stdio.h>  
	#include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

#define lock_array(elem_size,array,array_length) \
{ \
    printf("This would lock an array of size %d with elements of size %d. (currently not locking anything, since the compiler used does not recognize locks, also must be compiled toward RISC-V 32 with lock instruction).\n",array_length,elem_size);\
}

#define unlock_array(elem_size,array,array_length) \
{ \
    printf("This would unlock an array of size %d with elements of size %d. (currently not locking anything, since the compiler used does not recognize locks, also must be compiled toward RISC-V 32 with unlock instruction).\n",array_length,elem_size);\
}

    //record is stopped on phases of the algorithm we don't want to take in account
    //when measuring execution overhead (usually, initialization or verification phases of algorithms)
    void pause_record();  //record only relevant inside the simulator, will do nothing here
    void resume_record(); //record only relevant inside the simulator, will do nothing here
    void signal(int); //signal used in replacement for print, since their is no print in simulation
#endif

typedef unsigned long uint64_t ;
typedef unsigned int uint32_t ;
typedef unsigned short uint16_t ;
#define uint8_t unsigned char

typedef long int64_t ;
typedef int int32_t ;
typedef short int16_t ;
#define int8_t char

typedef enum {CHAR_SECURITY_MODE = 'M', CHAR_LENGTH = 'L', CHAR_SEED = 'S', CHAR_AUX = 'A', CHAR_HELP = 'H',} PARAMETER_CHARS;

typedef enum {LOCKED, LOCKED_BRANCHLESS, CT_CLASSIC, BRANCHLESS, EXPOSED} SECURITY_MODE;
const static struct {
    SECURITY_MODE      val;
    const char *str;
} security_mode_conversion [] = {
    {LOCKED, "lk"},         // lock/unlock instructions will do nothing in the versions not compiled with our custom CompCert
    {LOCKED_BRANCHLESS, "lkbl"},       // Some programs must also be rewrote in branchless to obtain a CTS execution with lock
    {CT_CLASSIC, "ctc"}, // Not all programs have a CT-classic mode
    {BRANCHLESS, "bl"}, // Not all programs have a Branchless mode
    {EXPOSED, "xp"}
};

struct parameters {
    SECURITY_MODE mode; // security used by the program. Not all programs have a CT-classic or Branchless mode
    long length;        // how long the test will be going, on how much data
    long auxiliary_param; //some programs need an additional parameter, e.g. the maximum allowed lock space for hybrid sort.
    long seed;          // random seed to generates test data,
    //changing this seed should change the leakage trace for exposed program but not for CT-secure ones.
};
typedef struct parameters parameters; 



void set_parameters_from_args(int ,char *[], parameters *);
void display_params(parameters,const char []);

long read_long_input(char []);
long read_long_input_offseted(char [], unsigned);



//set a seed for futur random generation
void setRandomSeed(unsigned); 
unsigned randLCG(void); //generates a random value between 0 and 0xffffffff
unsigned randLCGBounded(unsigned limit); //generates a random value between 0 and the bound parameter
unsigned char randByte(void) ; //generates a random byte

#define CTchoice(condition,thenRes,elseRes) ((condition*thenRes)+((1-condition)*elseRes))


#endif