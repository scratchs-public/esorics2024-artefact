/*
Derived from the implementation on https://github.com/0xsirus/RC4/tree/master
	By Sirus Shahini
	~cyn
	sirus.shahini@gmail.com
*/


#include "../lib_common.h"
//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>

parameters param = {LOCKED,2048,0,0x12345678};
//#define KEYLEN  64


unsigned char                               s[256];
unsigned char                               t[256];


void swap(unsigned char *p1, unsigned char *p2){
	unsigned char t = *p1;
	*p1 = *p2;
	*p2 = t;
}

void rc4_init(unsigned char *key,int key_len){
	int	i,j=0;

	//Initial values of both vectors
	for (i=0;i<256;i++){
		s[i] = i;
		t[i] = key[i%key_len];
	}
	//Initial permutation
	for (i=0;i<256;i++){
		j = (j + s[i] + t[i])%256;
		swap(&s[i],&s[j]);
	}
}

void rc4(unsigned char  *key,int key_len,unsigned char *buff,int len){
	int i=0;
	unsigned long t1,t2;
	unsigned char val;
	unsigned char out;
	t1=0;t2=0;
	rc4_init(key,key_len);

	//process one byte at a time
	for (i=0;i<len;i++){
		t1 = (t1 + 1)%256;
		t2 = (t2 + s[t1])%256;
		swap(&s[t1],&s[t2]);
		val = (s[t1] + s[t2])%256;
		out = *buff ^ val;
		*buff=out;
		buff++;
	}
}

#define MAX_DATA_LENGTH 102400

int main(int argc, char *argv[]) {


	set_parameters_from_args(argc, argv, &param);

	if(param.length %16 != 0)//message length must be multiple of 16
	{
		param.length += (16 - (param.length %16));
	}
	display_params(param,argv[0]);

	int message_len = param.length;

    setRandomSeed(param.seed);



	resume_record();
	if(param.mode == LOCKED_BRANCHLESS || param.mode == LOCKED)
	{
		lock_array(1,s,256);
		signal(3); //mark end of locks
	}
	pause_record();


	//unsigned char key[KEYLEN];
	unsigned char message[MAX_DATA_LENGTH];
	unsigned char witness[MAX_DATA_LENGTH];

	int i;


	unsigned char key[11];
	for (i=0;i<10;i++){
		key[i] = randLCGBounded(256); 
	}
	key[10]=0;

	for (i=0;i<message_len;i++){
		message[i] = (unsigned char) randLCGBounded(74)+'0'; 
		witness[i] = message[i];
	}


	printf("> Generated sample key: \033[96m%s\033[0m\n",key);
	printf("> Secret text: \033[1;96m%s\033[0m\n",message);
	printf("> Encryted message: \033[1;94m");


	resume_record();
	rc4(key,10,message,message_len);

	if(param.mode == LOCKED_BRANCHLESS || param.mode == LOCKED)
	{
		signal(4); //mark begin of unlocks
		unlock_array(1,s,256);
	}
	pause_record();

	//print HEX encoding of the encrypted message
	for (i=0;i<message_len;i++){
		printf("%02X", message[i]);
	}
	printf("\n\033[0m> Decryted RC4 data with the same key: ");
	rc4(key,10,message,message_len);
	printf("\033[1;92m%s\033[0m\n",message);

	for(i=0;i<message_len;i++)
	{
		if(witness[i] != message[i])
		{
			printf("Error in RC4");
			signal(0xe);
			return 1;
		}
	}

	return 0;
}

