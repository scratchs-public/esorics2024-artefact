
#include "../lib_common.h"
	
parameters param = {LOCKED_BRANCHLESS,64,0,83}; 


#ifndef MAX_VERTEX
	#define MAX_VERTEX       	64 //max of 64 vertexes, i.e. max of 4096 elem in matrix of edges

#endif
#ifndef MAX_SEARCH
	#define MAX_SEARCH 	4096 // Amount of searched elements during test

#endif

int V = MAX_VERTEX; //default of max vertexes, changed by param

int abs(int input)
{
    if(input <0)
    {
        return (0-input);
    }
    return input;
}



 
// A utility function to find the vertex with minimum
// distance value, from the set of vertices not yet included
// in shortest path tree
#define false 0
#define true 1
#define INT_MAX 0xffffffff
unsigned minDistance_exposed(unsigned out_dist[MAX_VERTEX], unsigned char sptSetBool[MAX_VERTEX])
{
    // Initialize min value
    unsigned min = INT_MAX, min_index;
 
    for (int v = 0; v < V; v++)
        if (sptSetBool[v] == false && out_dist[v] <= min)
            min = out_dist[v], min_index = v;
 
    return min_index;
}
 
// A utility function to print the constructed distance
// array
void printSolution(unsigned out_dist[MAX_VERTEX])
{
    printf("Vertex   Distance from Source\n");
    for (int i = 0; i < V; i++)
        printf("\t%d \t\t\t\t %d\n", i, out_dist[i]);
}
 
 //implementation from https://www.geeksforgeeks.org/c-program-for-dijkstras-shortest-path-algorithm-greedy-algo-7/
// Function that implements Dijkstra's single source
// shortest path algorithm for a graph represented using
// adjacency matrix representation
void dijkstra_exposed(unsigned graph[MAX_VERTEX][MAX_VERTEX],
                        int src,
                        unsigned out_dist[MAX_VERTEX],
                        unsigned char sptSetBool[MAX_VERTEX])
{
    //unsigned out_dist[V]; // The output array.  out_dist[i] will hold the
                 // shortest
    // distance from src to i
 
    //char sptSetBool[V]; // sptSetBool[i] will be true if vertex i is
                    // included in shortest
    // path tree or shortest distance from src to i is
    // finalized
 
    // Initialize all distances as INFINITE and stpSet[] as
    // false
    for (int i = 0; i < V; i++)
        out_dist[i] = INT_MAX, sptSetBool[i] = false;
 
    // Distance of source vertex from itself is always 0
    out_dist[src] = 0;
 
    // Find shortest path for all vertices
    for (int count = 0; count < V - 1; count++) {
        // Pick the minimum distance vertex from the set of
        // vertices not yet processed. u is always equal to
        // src in the first iteration.
        unsigned u = minDistance_exposed(out_dist, sptSetBool);
 
        // Mark the picked vertex as processed
        sptSetBool[u] = true;
 
        // Update out_dist value of the adjacent vertices of the
        // picked vertex.
        for (int v = 0; v < V; v++)
 
            // Update out_dist[v] only if is not in sptSetBool,
            // there is an edge from u to v, and total
            // weight of path from src to  v through u is
            // smaller than current value of out_dist[v]

            if (false==sptSetBool[v] && graph[u][v]
                && out_dist[u] != INT_MAX
                && out_dist[u] + graph[u][v] < out_dist[v])
                out_dist[v] = out_dist[u] + graph[u][v];
    }
 
    // print the constructed distance array
    //printSolution(out_dist, V);
}





unsigned minDistance_branchless(unsigned out_dist[MAX_VERTEX],
                                unsigned char sptSetBool[MAX_VERTEX])
{
    // Initialize min value
    unsigned min = INT_MAX;
	unsigned min_index = INT_MAX;
	unsigned char condition;
 
    for (int v = 0; v < V; v++){
		//condition = (sptSetBool[v] == false && out_dist[v] <= min); //compiled with a branch
        condition = CTchoice(sptSetBool[v],
                            out_dist[v] <= min,
                            1);
		min = CTchoice(condition, out_dist[v], min);
		min_index = CTchoice(condition, v, min_index);
	}
    if(min_index == INT_MAX)
    {
        printf("error: should not have INT MAX here, stop exec.");
        signal(0xe);
        exit(1);
    }
    return min_index;
}

void dijkstra_branchless(unsigned graph[MAX_VERTEX][MAX_VERTEX],
                        int src,
                        unsigned out_dist[MAX_VERTEX],
                        unsigned char sptSetBool[MAX_VERTEX])
{
    //unsigned out_dist[V]; // The output array.  out_dist[i] will hold the
                 // shortest
    // distance from src to i
 
    //char sptSetBool[V]; // sptSetBool[i] will be true if vertex i is
                    // included in shortest
    // path tree or shortest distance from src to i is
    // finalized
 
    // Initialize all distances as INFINITE and stpSet[] as
    // false
    for (int i = 0; i < V; i++)
    {
        out_dist[i] = INT_MAX;
        sptSetBool[i] = false;
    }
        
 
    // Distance of source vertex from itself is always 0
    out_dist[src] = 0;
    unsigned char cond;
    unsigned tmpA;
    unsigned tmpB;
    unsigned tmpC;
    // Find shortest path for all vertices
    for (int count = 0; count < V - 1; count++) {
        // Pick the minimum distance vertex from the set of
        // vertices not yet processed. u is always equal to
        // src in the first iteration.
        
        unsigned u = minDistance_branchless(out_dist, sptSetBool);
 
        //signal(1000+count);

        // Mark the picked vertex as processed
        sptSetBool[u] = true;
 
        // Update out_dist value of the adjacent vertices of the
        // picked vertex.
        for (int v = 0; v < V; v++){
            
            //signal(2000+v);
            // Update out_dist[v] only if is not in sptSetBool,
            // there is an edge from u to v, and total
            // weight of path from src to  v through u is
            // smaller than current value of out_dist[v]
            
            /*
            cond = (graph[u][v]);
            cond = (cond && out_dist[u] != INT_MAX);
            cond = (cond && (out_dist[u] + graph[u][v] < out_dist[v]));
            cond = (cond && false==sptSetBool[v]);
            */
            cond = CTchoice(out_dist[u] != INT_MAX,
                            1 ,
                            graph[u][v]);
            tmpC = out_dist[u] + graph[u][v];
            cond = CTchoice(tmpC < out_dist[v],
                            1,
                            cond);
            
            cond = CTchoice(sptSetBool[v],
                            cond,
                            1);

            tmpA = out_dist[u] + graph[u][v];
            tmpB = out_dist[v];
            
            tmpC = CTchoice(cond, tmpA, tmpB );
            out_dist[v] =tmpC;
            
        }
    }
 
    // print the constructed distance array
    //printSolution(out_dist, V);
}


/*
    CT access val = array[index]

for(int j=0; j<size; j++)
{
    cond = (j == index);
    val = CTchoice(cond, array[j], val);
}


    CT affectation array[index] = val

for(int j=0; j<size; j++)
{
    cond = (j == index);
    array[j] = CTchoice(cond, val, array[j]);
}
*/
//TODO : comme branchless ?
unsigned minDistance_CTC(unsigned out_dist[MAX_VERTEX],
                                unsigned char sptSetBool[MAX_VERTEX])
{
    // Initialize min value
    unsigned min = INT_MAX;
	unsigned min_index = INT_MAX;
	unsigned char condition;
 
    //here v can leak (not secret)
    for (int v = 0; v < V; v++){
        condition = CTchoice(sptSetBool[v],
                            out_dist[v] <= min,
                            1);
		min = CTchoice(condition, out_dist[v], min);
		min_index = CTchoice(condition, v, min_index);
	}
    if(min_index == INT_MAX)
    {
        printf("error: should not have INT MAX here, stop exec.");
        signal(0xe);
        exit(1);
    }
    return min_index;
}



void dijkstra_CTC(unsigned graph[MAX_VERTEX][MAX_VERTEX],
                        int src,
                        unsigned out_dist[MAX_VERTEX],
                        unsigned char sptSetBool[MAX_VERTEX])
{

    // Initialize all distances as INFINITE and stpSet[] as

    for (int i = 0; i < V; i++)
    {
        out_dist[i] = INT_MAX;
        sptSetBool[i] = false;
    }
        
 
    // Distance of source vertex from itself is always 0
    out_dist[src] = 0;
    unsigned char cond;
    unsigned tmpA;
    unsigned tmpB;
    unsigned tmpC;
    unsigned graph_secret_row[MAX_VERTEX];
    // Find shortest path for all vertices
    for (int count = 0; count < V - 1; count++) {
        // Pick the minimum distance vertex from the set of
        // vertices not yet processed. u is always equal to
        // src in the first iteration.
        unsigned u = minDistance_CTC(out_dist, sptSetBool);//u is secret


        //We need several access to graph[u][*] later
        //to protect u, we load the whole u row secretly into a buffer
        //this buffer 'graph_secret_row' can then safely be exposed
        for(int j=0; j<V; j++)
        {
            cond = (j == u);
            for(int k = 0; k<V; k++)
            {
                graph_secret_row[k] = CTchoice(cond, graph[j][k], graph_secret_row[k]);
            }
        }

        // Mark the picked vertex as processed
        //sptSetBool[u] = true; // leak u
        for(int j=0; j<V; j++)
        {
            cond = (j == u);
            sptSetBool[j] = CTchoice(cond, true, sptSetBool[j]);
        }
 
        // Update out_dist value of the adjacent vertices of the
        // picked vertex.
        for (int v = 0; v < V; v++){
            
            //CTchoice(out_dist[u] != INT_MAX,//TODO leaks u
            for(int j=0; j<V; j++)
            {
                cond = (j == u);
                tmpA = CTchoice(cond, out_dist[j], tmpA);
            }
            cond = CTchoice(tmpA != INT_MAX, 
                            1 ,
                            graph_secret_row[v]);

            //tmpC = out_dist[u] + graph_secret_row[v]; //TODO leaks u
            tmpC = tmpA + graph_secret_row[v];

            cond = CTchoice(tmpC < out_dist[v],
                            1,
                            cond);
            
            cond = CTchoice(sptSetBool[v],
                            cond,
                            1);

            //tmpA = out_dist[u] + graph[u][v];
            tmpA = tmpA + graph_secret_row[v];
            tmpB = out_dist[v];
            
            tmpC = CTchoice(cond, tmpA, tmpB );
            out_dist[v] =tmpC;
            
        }
    }
}



int main(int argc,char *argv[])
	{
	set_parameters_from_args(argc, argv, &param);
	display_params(param,argv[0]);
    setRandomSeed(param.seed);

    unsigned results[MAX_VERTEX];
    unsigned char boolbuffer[MAX_VERTEX];
    unsigned graph[MAX_VERTEX][MAX_VERTEX]; /*= { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
                        { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
                        { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
                        { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
                        { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                        { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
                        { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
                        { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
                        { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };*/
    V = param.length;
    int lock_length = param.length;
	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		resume_record(); //record lock time
		lock_array(4,results , lock_length);
		lock_array(1,boolbuffer , lock_length);
        for(int row = 0; row<lock_length; row++)
        {
		    lock_array(4,graph[row] , lock_length);
        }
		signal(3); //mark to check how much time to lock.
		pause_record(); 
	}



                        
	for(int i = 0; i < V; i++)
	{
	    for(int j = 0; j < V; j++)
        {
            graph[i][j] = randLCG();
        }
	}
 




	//for (int N = param.length; N>min_size; N--)
	{
		if(param.mode == BRANCHLESS || param.mode == LOCKED_BRANCHLESS)
		{
			resume_record(); //record dijsktra time

            printf("\n\nbranchless version:\n");
            dijkstra_branchless(graph, 0,results,boolbuffer);
            printSolution(results);

			pause_record(); 
		}
		else if (param.mode == LOCKED || param.mode == EXPOSED)
		{
			resume_record(); //record dijsktra time
			
			printf("exposed control flow version:\n");
            dijkstra_exposed(graph, 0,results,boolbuffer);
            printSolution(results);

			pause_record(); 
		}
		else if (param.mode == CT_CLASSIC)
		{
			resume_record(); //record dijsktra time
			
            dijkstra_CTC(graph, 0,results,boolbuffer);

			pause_record(); 
		}

	}
	

	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		resume_record(); //record pop time
		signal(4); //mark to check how much time to unlock.
		unlock_array(4,results , lock_length);
		unlock_array(1,boolbuffer , lock_length);
        for(int row = 0; row<lock_length; row++)
        {
		    unlock_array(4,graph[row] , lock_length);
        }
  		pause_record(); 
	}

    return 0;
}

/*

//test from https://www.geeksforgeeks.org/c-program-for-dijkstras-shortest-path-algorithm-greedy-algo-7/
unsigned graph[9][9] = { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
                        { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
                        { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
                        { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
                        { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                        { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
                        { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
                        { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
                        { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };
 
    dijkstra(graph, 0);

*/