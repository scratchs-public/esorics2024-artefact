
#include "../lib_common.h"

/*
 * The AES S-box (256-byte table).
 */
extern const unsigned char br_aes_S[];


/*
 * The AES big S-box (256*4-bytes table).
 */
//extern static const uint32_t Ssm0[];

void
br_aes_ct_bitslice_Sbox(uint32_t *);



static inline void
br_enc32be(void *dst, uint32_t x)
{
//#if BR_BE_UNALIGNED
//	((br_union_u32 *)dst)->u = x;
//#else
	unsigned char *buf;

	buf = dst;
	buf[0] = (unsigned char)(x >> 24);
	buf[1] = (unsigned char)(x >> 16);
	buf[2] = (unsigned char)(x >> 8);
	buf[3] = (unsigned char)x;
//#endif
}

static inline uint32_t
br_dec32be(const void *src)
{
//#if BR_BE_UNALIGNED
//	return ((const br_union_u32 *)src)->u;
//#else
	const unsigned char *buf;

	buf = src;
	return ((uint32_t)buf[0] << 24)
		| ((uint32_t)buf[1] << 16)
		| ((uint32_t)buf[2] << 8)
		| (uint32_t)buf[3];
//#endif
}



static inline void
br_enc32le(void *dst, uint32_t x)
{
//#if BR_LE_UNALIGNED
//	((br_union_u32 *)dst)->u = x;
//#else
	unsigned char *buf;

	buf = dst;
	buf[0] = (unsigned char)x;
	buf[1] = (unsigned char)(x >> 8);
	buf[2] = (unsigned char)(x >> 16);
	buf[3] = (unsigned char)(x >> 24);
//#endif
}

static inline uint32_t
br_dec32le(const void *src)
{
//#if BR_LE_UNALIGNED
//	return ((const br_union_u32 *)src)->u;
//#else
	const unsigned char *buf;

	buf = src;
	return (uint32_t)buf[0]
		| ((uint32_t)buf[1] << 8)
		| ((uint32_t)buf[2] << 16)
		| ((uint32_t)buf[3] << 24);
//#endif
}


static inline void br_enc32be(void *dst, uint32_t x);

unsigned
br_aes_keysched(uint32_t *skey, const void *key, int key_len);

void
br_aes_big_cbcenc_run(//const br_aes_big_cbcenc_keys *ctx,
	uint32_t skey[], unsigned num_rounds,
	void *iv, void *data, int len);

void
br_aes_big_encrypt(unsigned num_rounds, const uint32_t *skey, void *data);


void lock_sboxes(int boolean);
void lock_big(int boolean);


/*
static inline void
br_enc32be(void *dst, uint32_t x);

static inline uint32_t
br_dec32be(const void *src);
*/





/*
 * Compute AES encryption on bitsliced data. Since input is stored on
 * eight 32-bit words, two block encryptions are actually performed
 * in parallel.
 */
void br_aes_ct_bitslice_encrypt(unsigned num_rounds,
	const uint32_t *skey, uint32_t *q);


/*
 * Perform bytewise orthogonalization of eight 32-bit words. Bytes
 * of q0..q7 are spread over all words: for a byte x that occurs
 * at rank i in q[j] (byte x uses bits 8*i to 8*i+7 in q[j]), the bit
 * of rank k in x (0 <= k <= 7) goes to q[k] at rank 8*i+j.
 *
 * This operation is an involution.
 */
void br_aes_ct_ortho(uint32_t *q);


/*
 * Expand AES subkeys as produced by br_aes_ct_keysched(), into
 * a larger array suitable for br_aes_ct_bitslice_encrypt() and
 * br_aes_ct_bitslice_decrypt().
 */
void br_aes_ct_skey_expand(uint32_t *skey,
	unsigned num_rounds, const uint32_t *comp_skey);



/** \brief AES block size (16 bytes). */
#define br_aes_ct_BLOCK_SIZE   16

