#include "aes.h"


parameters param = {LOCKED_BRANCHLESS,2048,0,0x12345678};



//const br_aes_big_cbcenc_keys *ctx,
void br_aes_big_cbcenc_run(uint32_t skey[], unsigned num_rounds, void *iv, void *data, int len){
	unsigned char *buf, *ivbuf;
	ivbuf = iv;
	buf = data;
	while (len > 0) {
		int i;

		for (i = 0; i < 16; i ++) {
			buf[i] ^= ivbuf[i];
		}
		br_aes_big_encrypt(/*ctx->*/num_rounds, /*ctx->*/skey, buf);
		memcpy(ivbuf, buf, 16);
		buf += 16;
		len -= 16;
	}
}


//const br_aes_ct_cbcenc_keys *ctx,
void br_aes_ct_cbcenc_run(uint32_t skey[], unsigned num_rounds, 
	void *iv, void *data, size_t len)
{
	unsigned char *buf, *ivbuf;
	uint32_t q[8];
	uint32_t iv0, iv1, iv2, iv3;
	uint32_t sk_exp[120];

	q[1] = 0;
	q[3] = 0;
	q[5] = 0;
	q[7] = 0;
	br_aes_ct_skey_expand(sk_exp, /*ctx->*/num_rounds, /*ctx->*/skey);
	ivbuf = iv;
	iv0 = br_dec32le(ivbuf);
	iv1 = br_dec32le(ivbuf + 4);
	iv2 = br_dec32le(ivbuf + 8);
	iv3 = br_dec32le(ivbuf + 12);
	buf = data;
	while (len > 0) {
		q[0] = iv0 ^ br_dec32le(buf);
		q[2] = iv1 ^ br_dec32le(buf + 4);
		q[4] = iv2 ^ br_dec32le(buf + 8);
		q[6] = iv3 ^ br_dec32le(buf + 12);
		br_aes_ct_ortho(q);
		br_aes_ct_bitslice_encrypt(/*ctx->*/num_rounds, sk_exp, q);
		br_aes_ct_ortho(q);
		iv0 = q[0];
		iv1 = q[2];
		iv2 = q[4];
		iv3 = q[6];
		br_enc32le(buf, iv0);
		br_enc32le(buf + 4, iv1);
		br_enc32le(buf + 8, iv2);
		br_enc32le(buf + 12, iv3);
		buf += 16;
		len -= 16;
	}
	br_enc32le(ivbuf, iv0);
	br_enc32le(ivbuf + 4, iv1);
	br_enc32le(ivbuf + 8, iv2);
	br_enc32le(ivbuf + 12, iv3);
}


#define KEYLEN  32
#define MAX_DATA_LENGTH 10240

int main(int argc, char *argv[]) {


	set_parameters_from_args(argc, argv, &param);

	if(param.length %16 != 0)//message length must be multiple of 16
	{
		param.length += (16 - (param.length %16));
	}
	display_params(param,argv[0]);

    setRandomSeed(param.seed);


	resume_record();
	if(param.mode == LOCKED_BRANCHLESS || param.mode == LOCKED)
	{
		
		lock_sboxes(1);
		lock_big(1);
		signal(3); //mark end of locks
		
	}
	pause_record();

    
    uint32_t skey[14]; //correspond to KEYLEN, see br_aes_keysched
    unsigned char key[KEYLEN];
    for (int i =0; i<KEYLEN;i++){
        key[i]= randByte(); 
    }

    unsigned num_rounds = br_aes_keysched(skey, key, KEYLEN);

    unsigned char iv[MAX_DATA_LENGTH];
    unsigned char data[MAX_DATA_LENGTH];

    int data_length = param.length;; //multiple de 16
    for (int i =0; i<data_length;i++){
        iv[i]= randByte(); 
        data[i]= randByte(); 
    }

	resume_record();

	if(param.mode == CT_CLASSIC )
	{	//bitsliced AES
		br_aes_ct_cbcenc_run(	skey, num_rounds,(void*)iv, (void*)data, data_length);
	}
	else
	{	//SBOX AES (with big sboxes)
		br_aes_big_cbcenc_run(	skey, num_rounds,(void*)iv, (void*)data, data_length);
	}

    

	
	if(param.mode == LOCKED_BRANCHLESS || param.mode == LOCKED)
	{
		signal(4); //mark begin of unlocks
		lock_sboxes(0); //unlock
		lock_big(0);
	}

	pause_record();

    return 0;
}