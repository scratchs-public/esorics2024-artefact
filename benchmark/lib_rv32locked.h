#ifndef STDLIB_CUSTOM_RV32_LOCKED
#define STDLIB_CUSTOM_RV32_LOCKED



#define CACHE_LINE_SIZE 16 //this depends on your cache : how many bytes of data fit in one cache line ?
//if unsure between multiple values, put it to the lower one (un-necessary redundant locks may be done,
// but it will only slow down a little bit the lock phase, not lock more lines in cache)

#define lock_array(elem_size,array,array_length) \
{ \
    for (unsigned i = 0; i< array_length; i+=(CACHE_LINE_SIZE/elem_size))  \
    {   \
        __builtin_lock(&array[i]);   \
    }   \
    __builtin_lock(&array[array_length-1]);\
}


#define unlock_array(elem_size,array,array_length) \
{ \
    for (unsigned i = 0; i< array_length; i+=(CACHE_LINE_SIZE/elem_size))  \
    {   \
        __builtin_unlock(&array[i]);   \
    }   \
    __builtin_unlock(&array[array_length-1]);\
}

//record is stopped on phases of the algorithm we don't want to take in account
//when measuring execution overhead (usually, initialization or verification phases of algorithms)
void resume_record(void);
void pause_record(void);
void signal(int); //Warning : signal 0 kill the prgm, signal 1 resume record and signal 2 pause record
//#include "RV32_LOCKED_LIB.c"

typedef unsigned long size_t;



typedef long long __m128i; //incorrect





#define offsetof(st, m) \
    ((size_t)((char *)&((st *)0)->m - (char *)0))


#define NULL ((void *)0)



int EXIT_FAILURE;
int EXIT_MALLOC;
int EXIT_FREE;
//const void * NULL;


void exit(int);
void __attribute__ ((noinline)) ecall(int);

//#define resume_record_bis ecall(1)

void * memset(void *, int , size_t);
void * memcpy(void*, const void*, size_t);
int toupper (int);
int memcmp(const void* , const void* , size_t);
void* malloc(size_t);
void free(void*);


typedef void FILE ;

FILE *fopen(const char *, const char *);
FILE *fclose(const char *);
FILE * stderr;
FILE * stdout;
int printf (char const * restrict , ...);
FILE * fprint (int , ...);
int fprintf(FILE * restrict , char const * restrict , ...);
int sprintf(char * restrict , char const * restrict , ...);
int fflush(FILE *);

int skip (int a, ...);


int atoi(char* );
long int atol ( const char *  );
char *fgets (char *, int , FILE *);
int strcmp(const char [], const char [] );

long read_long_input(char []);
long read_long_input_offseted(char [], unsigned);


/**
functions removed with

sed -i 's/original/new/g' file.txt


fpr.intf -> fprintf
std.err -> //sr
std.out -> //so
ffl.ush -> //ff
str.len -> 


*/

#endif