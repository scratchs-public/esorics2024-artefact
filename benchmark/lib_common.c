#include "lib_common.h"


#ifndef RV32_LOCKED_LIB

    void resume_record()  //record only relevant inside the simulator, will do nothing here
    {
        printf("If it was in the simulation, record would start here\n");
    }  

    void pause_record() //record only relevant inside the simulator, will do nothing here
    {
        printf("If it was in the simulation, record would pause here\n");
    }

    void signal(int s) //signal used in replacement for print, since their is no print in simulation
    {
        printf("\n[signal %x]\n",s);
    }
#endif

void display_help_param(PARAMETER_CHARS param){
    switch (param)
	{
        case CHAR_SECURITY_MODE:
            printf("  -%c <mode> \tSecurity mode, with <mode> being any of the following :\n", CHAR_SECURITY_MODE);
            printf("     lk\t(Locked. lock/unlock instructions will do nothing if the\n     \t\t program is not compiled with our custom CompCert)\n");
            printf("     lkbl\t(Locked-Branchless. Some programs must also be rewrote in branchless \n     \t\t to obtain a CTS execution with lock)\n");
            printf("     ctc\t(CT-Classic. Not all programs have a CT-classic mode)\n");
            printf("     bl\t(Branchless. Not all programs have a Branchless mode)\n");
            printf("     xp (Exposed)\n\n");
            break;
        case CHAR_LENGTH:
            printf("  -%c <n> \tData length, with <n> an integer expressed in decimal.\n", CHAR_LENGTH);
            printf("          \t(how long the test will be going, how much data,\n          \t the unit depends on the algorithm)\n\n");
            break;
        case CHAR_SEED:
            printf("  -%c <n> \tSeed, with <n> an integer expressed in decimal.\n", CHAR_SEED);
            printf("          \t(random seed to generate test data, changing it should\n          \t change the leakage for exposed mode.)\n\n");
            break;
        case CHAR_AUX:
            printf("  -%c <n> \tAuxiliary param, with <n> an integer expressed in decimal.\n", CHAR_AUX);
            printf("          \t(some programs need an additional parameter, e.g. the\n          \t maximum allowed lock space for hybrid of\n          \t Batcher-odd-even-merge-sort and Merge-sort.)\n\n");
            break;

        case CHAR_HELP:
            printf("  -%c     \tDisplay help (only if printf supported).\n", CHAR_HELP);
            break;
    }
}


void display_params(parameters p, const char algo_name[]){
    printf("%s running, parameters are :\n", algo_name);
    printf("\tSecurity mode (-%c) = %d (%s)\n", CHAR_SECURITY_MODE,p.mode, security_mode_conversion[p.mode].str);
    printf("\tData length (-%c) = %d\n", CHAR_LENGTH,p.length);
    printf("\tAuxiliary param (-%c) = %d\n", CHAR_AUX,p.auxiliary_param);
    printf("\tSeed for random (-%c) = %d\n", CHAR_SEED,p.seed);
}

void display_help(const char *algo_name){
    printf("Input parameters can be:\n\n");
    display_help_param(CHAR_SECURITY_MODE);
    display_help_param(CHAR_LENGTH);
    display_help_param(CHAR_AUX);
    display_help_param(CHAR_SEED);
    display_help_param(CHAR_HELP);

    printf("Example:\n  ");
    printf("  %s -M locked -S 153240 \n\tWill run the algo in its locked mode with test data randomly\n\t generated from seed 153240. length and auxiliary parameters (if any)\n\t will be the defaults defined in %s.\n\n", algo_name,algo_name);

}


long read_long_input_offseted_for_param(char args[], unsigned offset, PARAMETER_CHARS param)
{
    long res =0;
    int i =offset;
    int sign = 1;
    if(args[i] == '-')
    {
        sign= -1;
        i ++;
    }

    while (args[i])
    {
        //printf("i=%d, args[i]=%c\n",i, args[i]);
        res = res*10;
        if(args[i]<'0' || args[i]>'9')
        {
            printf("Input error: function 'read_long_input' can only read digits (or '-' at the beginning of the string), not '%c' at %dth position.\n",args[i],i);
            if(param != CHAR_HELP){
                display_help_param(param);
            }
            exit(0xe00+param);
        }
        res += (long unsigned)(args[i]-'0');// ASCII conversion
        i++;
    }
    return res*sign;
}

long read_long_input_offseted(char args[], unsigned offset)
{
    return read_long_input_offseted_for_param(args, offset, CHAR_HELP);
}

long read_long_input(char args[])
{
    return read_long_input_offseted(args, 0);
}


int start_with_dash_char(char c, char arg[])
{
    return ( arg[0] == '-' && arg[1] && toupper(arg[1]) == toupper(c) );
}




SECURITY_MODE str_to_security_mode (const char *str)
{
    for (int i = 0; i<5; i++)
    {
        if (!strcmp (str, security_mode_conversion[i].str))
        {
            return security_mode_conversion[i].val;  
        }
              
    }
    printf("Invalid -%c input:\n\n",CHAR_SECURITY_MODE);
    display_help_param(CHAR_SECURITY_MODE);
    
    exit (0xe00+CHAR_SECURITY_MODE);
    return CHAR_SECURITY_MODE;
}


/*
SECURITY_MODE get_security_mode(int argc,char *argv[])
{
	for (int i=1;i<argc+1; i++)
    {
		if (start_with_dash_char(CHAR_SECURITY_MODE, argv[i]))
        {
            if(argv[i]){
                return str_to_security_mode (argv[i]);
            }
        }
    }
    return LOCKED; //Locked mode by default
}
*/



void param_empty(char mode_char)
{
    printf("Must give a value for -%c param:\n",mode_char);
    display_help_param(mode_char);
    exit(0xe00+mode_char);
}

//P should already be initialized with the default values corresponding to the calling program
void set_parameters_from_args(int argc,char *argv[], parameters *p)
{
    char* algo_name = argv[0];
    //printf("set args\n");
    for (int i=1;i<(argc); i++)
    {

        //printf("   arg %d\n",i);
        if(argv[i][0] == '-' && argv[i][1])
        {

            //printf("   is -%c\n",argv[i][1]);
            char mode_char = toupper(argv[i][1]);
            switch (mode_char)
			{
            case CHAR_SECURITY_MODE:
                if(argv[i+1])
                {
                    i++;
                    p->mode = str_to_security_mode(argv[i]);
                    //printf("   set to %d\n",p->mode);
                }
                else{ param_empty(mode_char); }
                break;
            case CHAR_LENGTH:
                if(argv[i+1])
                {
                    i++;
                    p->length = read_long_input_offseted_for_param(argv[i],0,mode_char);
                    //printf("   set to %d\n",p->length);
                }
                else{ 
                    param_empty(mode_char); }
                break;
            case CHAR_SEED:
                if(argv[i+1])
                {
                    i++;
                    p->seed = read_long_input_offseted_for_param(argv[i],0,mode_char);
                    //printf("   set to %d\n",p->seed);
                }
                else{ param_empty(mode_char); }
                break;
            case CHAR_AUX:
                if(argv[i+1])
                {
                    i++;
                    p->auxiliary_param = read_long_input_offseted_for_param(argv[i],0,mode_char);
                    //printf("   set to %d\n",p->auxiliary_param);
                }
                else{ param_empty(mode_char); }
                break;
            case CHAR_HELP:
                display_help(algo_name);
                break;
            default:
                display_help(algo_name);
                exit(0xee1);
                break;
            }
        }
        else
        {
            display_help(algo_name);
            exit(0xee2);
        }
    }
}




//LCG pseudo random generator, return an integer between 0 and limit included
unsigned seed = 0x333; // changed by user input

void setRandomSeed(unsigned newseed)
{
    seed = newseed;
} 

unsigned randLCG(void)
{
	seed = ((seed * 69069 ) + 1) % (1<<31);
	return seed;
}

unsigned randLCGBounded(unsigned limit)
{
	return randLCG()%limit;
}

unsigned char randByte(void)
{
	return (unsigned char) (randLCGBounded(0xff));
}