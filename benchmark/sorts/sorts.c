#include "../lib_common.h"
	
#define MAX_SIZE 2048
//maximum size of array to sort and locked buffer

parameters param = {LOCKED_BRANCHLESS,768,256,0x12345678};
//default values are sorted array of 768 int (3 kB) with locked buffer of 250 int (1 kB)
//in locked-branchless mode (Hybrid sort) with seed = 0x12345678 (for random generation of the array)





//#define MIN(a, b) (((a) < (b)) ? (a) : (b))
//#define MAX(a, b) (((a) > (b)) ? (a) : (b))

//cts def of MIN
static inline int CTS_MIN (int32_t a, int32_t b)
{
	int32_t selectA = a<b;
	return (selectA*a)+((1-selectA)*b);
}

//#define INCR(val, limit1, limit2) (((val) == (limit1)) ? (limit2) : (val+1))
//#define DECR(val, limit1, limit2) (((val) == (limit2)) ? (limit1) : (val-1))


	//signal (0xffffffff);
	//signal (a);
	//signal (b);
#define swap_from_ptr(a,b) \
{ \
	int32_t v1 = *a;\
	int32_t v2 = *b;\
	int32_t do_exchange = v1>v2;\
	*a = CTchoice(do_exchange,v2,v1);\
	*b = CTchoice(do_exchange,v1,v2);\
}\

	//*a = do_exchange ? v2 : v1;
	//*b = do_exchange ? v1 : v2;


void printArrayBetween(int32_t a[], int begin, int end)
{
	printf("print array between %d and %d: ",begin,end);
    for (int i = begin; i < end; i++)
	{
		if(i%8==0){printf("\n[%d]:\t ",i);}

		printf("%x ", a[i]);
	}
	printf("\n");
}

void printArray(int32_t a[], int size)
{
	printArrayBetween(a,0,size);
}




int check_correctness (int32_t sorted_array[], unsigned begin, unsigned end)
{
	int32_t tmp_prev = sorted_array[0];
	int32_t tmp_curr = 0;
	//int res = 0;
	//pf("\n                        ");
	for(unsigned i = begin; i<end; i++)
	{
		tmp_curr = sorted_array[i];
		if(tmp_curr<tmp_prev)//not sorted
		{
			printf("\n Error in sort at index %d-%d\n", i-1,i); 
			return -1;
		}
		tmp_prev=tmp_curr;
	}
	//pf("\n");
	return 0; //array is perfectly sorted if res =0
}



void batcher_odd_even_sort(int32_t a[], int l, int r) {
  int n = r-l+1;
  for (int p=1; p<n; p+=p)
  {
    for (int k=p; k>0; k/=2)
    {
      for (int j=k%p; j+k<n; j+=(k+k))
      {
        for (int i=0; i<k; i++)
        {
          if ((l+j+i+k)<=r && (j+i)/(p+p) == (j+i+k)/(p+p))
          {
			//(l+j+i+k)<=r comparison for array of size != 2^P
          	////pf("   Batcher exchange between indexes %d & %d.\n", l+j+i, l+j+i+k);
            //sort_elem(a, l+j+i, l+j+i+k);
			swap_from_ptr(&a[l+j+i], &a[l+j+i+k]);
          }
        }
      }
    }
  }
}






// function to sort the subsection a[i .. j] of the array a[]
void merge_sort_fast_CT(int i, int j, int32_t a[], int32_t locked_aux[])
{
    if (j <= i) {
        return;     // the subsection is empty or a single element
    }
    int mid = (i + j) / 2;


    // left sub-array is a[i .. mid]
    // right sub-array is a[mid + 1 .. j]
    
    merge_sort_fast_CT(i, mid, a, locked_aux);     // sort the left sub-array recursively
    merge_sort_fast_CT(mid + 1, j, a, locked_aux);     // sort the right sub-array recursively

	//both pointers will be used as secret access index for locked_aux[]
    int pointer_left = 0;       // pointer_left points to the beginning of the left sub-array
    int pointer_right = mid + 1 - i;        // pointer_right points to the beginning of the right sub-array

    int k;      // k is the loop counter, this is not a secret value (use as access index for a[])

	//We copy the elements on the locked buffer, from which we will be able to recover them
	//from any index without leaking informations

	for (k = i; k <= j; k++) {      // copy the elements from a[] to locked_aux[] 
        locked_aux[k-i] = a[k];
    }

	int end_left = mid-i;
	int end_right = j-i;
	uint8_t no_stop_left = 1;
	//uint8_t stop_left = 0;

	uint8_t stop_right = 0;

	int32_t tmp_left;// = locked_aux[pointer_left];
	int32_t tmp_right;// = locked_aux[pointer_right];
	//uint8_t tmp;
	int32_t take_left_pointer;


	for (k = i; k <= j; k++) {      // merge back from locked_aux[] toward a[]

		//signal(0xaf);
		tmp_left = locked_aux[CTS_MIN (pointer_left, end_left)];	//secret access
		tmp_right = locked_aux[CTS_MIN (pointer_right, end_right)];	//secret access
		//signal(0xfa);

		take_left_pointer = (
			(no_stop_left) //never take value from left if left pointer reach its end
			&
			(
				(stop_right) //always take value from left if right pointer reach its end
				|
				(tmp_left < tmp_right)
			)
		);

		

		a[k] = (take_left_pointer * tmp_left) + ((1-take_left_pointer) * tmp_right);

		pointer_left += take_left_pointer;
		pointer_right += (1-take_left_pointer);
		no_stop_left = pointer_left<=end_left;
		stop_right = pointer_right>end_right;
		//stop_left = pointer_left>end_left;

    }

}





void sub_arrays_merge_and_dispatch(int32_t array[],int32_t locked_aux[], /*int locked_aux_size,*/
					int first_half_pointer,int second_half_pointer,int half_size)
{
	int k;
	int dispatch_pointer =first_half_pointer;
	//signal(1001);
	for (k = 0; k < half_size; k++) {      
		//pf("copy arr[%d] in buf[%d]\n", k+dispatch_pointer, k);
        locked_aux[k] = array[k+dispatch_pointer];
    }
	dispatch_pointer=second_half_pointer-half_size;
	//signal(1002);
	for (k = half_size; k < 2*half_size; k++) {      
		//pf("copy arr[%d] in buf[%d]\n", k+dispatch_pointer, k);
        locked_aux[k] = array[k+dispatch_pointer];
		
    }
	//signal(1003);
	int pointer_left =0;
	int pointer_right = half_size;
	int end_left = half_size-1;
	int end_right = (2*half_size)-1;
	int32_t no_stop_left = 1;
	int32_t stop_right = 0;

	int32_t tmp_left;// = locked_aux[pointer_left];
	int32_t tmp_right;// = locked_aux[pointer_right];
	//int32_t tmp;
	uint8_t take_left_pointer;
	//signal(1004);
	dispatch_pointer =first_half_pointer;
	for (k = 0; k < half_size*2; k++) {      // merge back from locked_aux[] toward a[]
		//signal(1005);
		if (k==half_size)//first sub array have been filled, will now dispatch in second sub array 
		{
			dispatch_pointer=second_half_pointer-half_size;
		}
		//signal(1006);
		tmp_left = CTS_MIN (pointer_left, end_left);
		//signal(10000+tmp_left);
		tmp_left = locked_aux[tmp_left];	//secret access


		tmp_right = CTS_MIN (pointer_right, end_right);	
		//signal(10000+tmp_right);
		tmp_right = locked_aux[tmp_right];	//secret access
		
		//signal(1007);
		take_left_pointer = (
			(no_stop_left) //never take value from left if left pointer reach its end
			&
			(
				(stop_right) //always take value from left if right pointer reach its end
				|
				(tmp_left < tmp_right)
			)
		);
		//signal(1008);
		//pf("take_left_pointer=%d, tmp left=%d, tmp right=%d\n",take_left_pointer,tmp_left,tmp_right);
		array[k+dispatch_pointer] = (take_left_pointer * tmp_left) + ((1-take_left_pointer) * tmp_right);
		//signal(1009);

		pointer_left += take_left_pointer;
		pointer_right += (1-take_left_pointer);
		//signal(1010);
		no_stop_left = pointer_left<=end_left;
		stop_right = pointer_right>end_right;
		//signal(1011);

	}
}


// function to sort the subsection a[i .. j] of the array a[]
void merge_batcher_hybrid(int begin, int end, int32_t array[], int32_t locked_aux[], int locked_aux_size) {
	unsigned array_size = end-begin+1;
	int i;

	//pa(array, array_size);
	//int sub_array_end;
	for(i=begin; i<=end; i+=(locked_aux_size/2))
	{
		//pf("\n \n    i=%d\n \n", i);
		//sub arrays small enought to be sorted with classic merge sort
		// within the locked auxiliary buffer (allowing constant time sort)
		//pf("\n merge sort between %d and %d \n",i,i+(locked_aux_size/2)-1);
		//sub_array_end = MIN(i+(locked_aux_size/2)-1,end);
		merge_sort_fast_CT(i,i+(locked_aux_size/2)-1, array, locked_aux);
		//signal(0xb000);
	}

	//signal(7777);

	//printf(" ===============\n  etat intermediare : \n");
	//printArray(array,end+1);
	//printf("  fin intermediare \n ===============\n");
 
  int stride = locked_aux_size/2; //size of sub_arrays to merge/dispatch with batcher network
  int nb_sub_arrays = array_size/stride;//TODO:  adjust this to works even on array_size != 2^n
  //warning : this would not work if we had sub_arrays of different sizes
  //this would need a little bit more polishing to works with any number and size of sub_arrays.

  //int nb_batcher_swap = 0;
  for (int p=1; p<nb_sub_arrays; p+=p)
  {
    for (int k=p; k>0; k/=2)
    {
      for (int j=k%p; j+k<nb_sub_arrays; j+=(k+k))
      {
        //for (int i=0; i<k; i++)
        for (int i=0; i<k; i+=1)
        {
          if ((j+i)/(p+p) == (j+i+k)/(p+p) && (stride*(begin+j+i+k))<end)
          {
          	////pf("   Batcher exchange between indexes %d & %d.\n", begin+j+i, begin+j+i+k);
            //sort_elem(a, begin+j+i, begin+j+i+k);
			//swap_from_ptr(&a[begin+j+i], &a[begin+j+i+k]);
			//pf("\n=======\n Batcher exchange between indexes %d & %d with stride of %d.\n", stride*(begin+j+i), stride*(begin+j+i+k), stride);
            sub_arrays_merge_and_dispatch(array, locked_aux, /*locked_aux_size,*/
							 stride*(begin+j+i), stride*(begin+j+i+k), stride);
			//pa(array, array_size);
			//nb_batcher_swap ++;
          }
        }
      }
    }
  }
  ////pf("\nTotal nb swap = %d\n", nb_batcher_swap);
}



void merge_djb_hybrid(int begin, int end, int32_t array[], int32_t locked_aux[], int locked_aux_size) {
	unsigned array_size = end-begin+1;
	//pa(array, array_size);
	//int sub_array_end;
	for(int i=begin; i<=end; i+=(locked_aux_size/2))
	{
		//pf("\n \n    i=%d\n \n", i);
		//sub arrays small enought to be sorted with classic merge sort
		// within the locked auxiliary buffer (allowing constant time sort)
		//pf("\n merge sort between %d and %d \n",i,i+(locked_aux_size/2)-1);
		//sub_array_end = MIN(i+(locked_aux_size/2)-1,end);
		merge_sort_fast_CT(i,i+(locked_aux_size/2)-1, array, locked_aux);
		//signal(0xb000);
	}

	//signal(7777);

	//printf(" ===============\n  etat intermediare : \n");
	//printArray(array,end+1);
	//printf("  fin intermediare \n ===============\n");
 
  int stride = locked_aux_size/2; //size of sub_arrays to merge/dispatch with batcher network
  int nb_sub_arrays = array_size/stride;//TODO:  adjust this to works even on array_size != 2^n

	//DJB network, but with sub_arrays_merge_and_dispatch instead of minmax on array
  int n = nb_sub_arrays;
  int top,p,q,r,i;

  if (n < 2) return;
  top = 1;
  while (top < n - top) top += top;

  for (p = top;p > 0;p >>= 1) {
    for (i = 0;i < n - p;++i)
      if (!(i & p))
	  	sub_arrays_merge_and_dispatch(array, locked_aux, /*locked_aux_size,*/
							 stride*(i), stride*(i+p), stride);
        //int32_MINMAX(x[i],x[i+p]);
    i = 0;
    for (q = top;q > p;q >>= 1) {
      for (;i < n - q;++i) {
        if (!(i & p)) {
          //int32_t a = x[i + p];
          for (r = q;r > p;r >>= 1)
		  	sub_arrays_merge_and_dispatch(array, locked_aux, /*locked_aux_size,*/
							 stride*(i+p), stride*(i+r), stride);
            //int32_MINMAX(a,x[i+r]);
	  //x[i + p] = a;
		}
      }
    }
  }
}





// function to sort the subsection a[i .. j] of the array a[]
void merge_sort_fast(int i, int j, int32_t a[], int32_t aux[]) {
    if (j <= i) {
        return;     // the subsection is empty or a single element
    }
    int mid = (i + j) / 2;

    // left sub-array is a[i .. mid]
    // right sub-array is a[mid + 1 .. j]
    
    merge_sort_fast(i, mid, a, aux);     // sort the left sub-array recursively
    merge_sort_fast(mid + 1, j, a, aux);     // sort the right sub-array recursively

    int pointer_left = i;       // pointer_left points to the beginning of the left sub-array
    int pointer_right = mid + 1;        // pointer_right points to the beginning of the right sub-array
    int k;      // k is the loop counter

    // we loop from i to j to fill each element of the final merged array
    for (k = i; k <= j; k++) {
        if (pointer_left == mid + 1) {      // left pointer has reached the limit
            aux[k] = a[pointer_right];
            pointer_right++;
        } else if (pointer_right == j + 1) {        // right pointer has reached the limit
            aux[k] = a[pointer_left];
            pointer_left++;
        } else if (a[pointer_left] < a[pointer_right]) {        // pointer left points to smaller element
            aux[k] = a[pointer_left];
            pointer_left++;
        } else {        // pointer right points to smaller element
            aux[k] = a[pointer_right];
            pointer_right++;
        }
    }

    for (k = i; k <= j; k++) {      // copy the elements from aux[] to a[]
        a[k] = aux[k];
    }
}






#define int32_MINMAX(a,b) \
do { \
  int32_t ab = b ^ a; \
  int32_t c = b - a; \
  c ^= ab & (c ^ b); \
  c >>= 31; \
  c &= ab; \
  a ^= c; \
  b ^= c; \
} while(0)

void djb_sort(int32_t *x,long long n)
{
  long long top,p,q,r,i;

  if (n < 2) return;
  top = 1;
  while (top < n - top) top += top;

  for (p = top;p > 0;p >>= 1) {
    for (i = 0;i < n - p;++i)
      if (!(i & p))
        int32_MINMAX(x[i],x[i+p]);
    i = 0;
    for (q = top;q > p;q >>= 1) {
      for (;i < n - q;++i) {
        if (!(i & p)) {
          int32_t a = x[i + p];
          for (r = q;r > p;r >>= 1)
            int32_MINMAX(a,x[i+r]);
	  x[i + p] = a;
	}
      }
    }
  }
}






#define POST_QUANTIC_RANDOM_ARRAY_SIZE 768
//some post quantic secu algo need to perform secret sort of arrays of 768 32bits elements


int main(int argc, char *argv[]) {


	int32_t array_to_sort[MAX_SIZE];
	int32_t auxiliary_buffer[MAX_SIZE];

	set_parameters_from_args(argc, argv, &param);
    
	display_params(param,argv[0]);

	unsigned range_to_sort = param.length;
    setRandomSeed(param.seed);
	//fill the array with pseudo random values (depends on seed)
	for(int i=0; i<range_to_sort; i++)
	{
		array_to_sort[i] = (int32_t)randLCG();
	}
	printArray(array_to_sort,range_to_sort);


  	//start simulation record here
	resume_record();

	if(param.mode == EXPOSED) // classic merge sort
	{
		printf("classic merge_sort (fast)\n");
		merge_sort_fast(0, range_to_sort - 1, array_to_sort,auxiliary_buffer);
	}
	else if(param.mode == LOCKED_BRANCHLESS || param.mode == LOCKED) // hybrid of merge and batcher sort
	{
		
		unsigned lock_length = param.auxiliary_param;
		unsigned val_size = 4; //int 32 bits on 4 bytes
		lock_array(val_size,auxiliary_buffer, lock_length);

		signal(3); //mark to check how much time to lock.

		
		if(param.mode == LOCKED_BRANCHLESS)
		{
			printf("hybrid sort\n");

			//merge_batcher_hybrid(0, range_to_sort-1, array_to_sort,	auxiliary_buffer, lock_length);
			//hybrid DJB better than hybrid Batcher (coherent with DJB better than Batcher)
			merge_djb_hybrid(0, range_to_sort-1, array_to_sort,	auxiliary_buffer, lock_length);
		}
		else if(param.mode == LOCKED && lock_length>=range_to_sort)
		{
			printf("Merge sort CT in locked buffer\n");
			merge_sort_fast_CT(0, range_to_sort - 1, array_to_sort, auxiliary_buffer);
		}
		else
		{
			printf("Tried to do merge sort CT in locked buffer, but locked buffer has a size of %d and array to sort has a size of %d.",lock_length,range_to_sort);
			signal(0xeeff);
		}

		signal(4); //mark to check how much time to unlock.
		unlock_array(val_size,auxiliary_buffer, lock_length);
	}
	else if(param.mode == BRANCHLESS) // Batcher odd even sort
	{
		printf("batcher_odd_even_sort\n");
		batcher_odd_even_sort(array_to_sort, 0, range_to_sort - 1);
	}
	else if(param.mode == CT_CLASSIC) //DJB portable sort
	{
		printf("DJB portable sort (this version is less efficient than the true DJB sort, which was optimized for Intel256.\n");
		djb_sort(array_to_sort, (long long) range_to_sort);
	}


		// AVX2 DJB sort is optimized for Intel assembly with 256 bits registers
		//We will not be able to reach this level of optimization on RISCV with 32 bits registers
	//else if(sort_algo_used == 5)// DJB sort AVX2
	//{
	//	return -1; // DJB sort AVX2 not ported to RISCV32 yet (was optimized for Intel 256 bits registers)
	//}


  	//stop simulation record here
	pause_record();

	printArray(array_to_sort,range_to_sort); 

	int32_t result = 0;
	result = check_correctness(array_to_sort, 0,range_to_sort);
	if(result > 0)
	{
		signal(0xeeee);
		signal(result);
		return 1;
	}
	else
	{
		signal(0xaaaa);
		return 0;
	}  
}


