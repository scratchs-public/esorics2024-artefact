//Study of permutation algorithm, executing a[b[i]] = i for all i

#include "../lib_common.h"
	
parameters param = {LOCKED_BRANCHLESS,1,0,83}; //default values are 1 search in locked-branchless mode with seed = 83

#define MAX_LENGTH 	100000

void permut(unsigned a[], unsigned b[], int size)
{
    for(int i=0; i<size; i++)
    {
        a[b[i]] = i;
    }
}

void permut_CTC(unsigned a[], unsigned b[], int size)
{
    char cond =0;
    int waited_index;
    for(int i=0; i<size; i++)
    {
        waited_index = b[i];
        for(int j=0; j<size; j++)
        {
            cond = (j == waited_index);
            a[j] = CTchoice(cond, i, a[j]);
        }
    }
}


/*int abs(int input)
{
    if(input <0)
    {
        return (0-input);
    }
    return input;
}

//LCG pseudo random generator, return an integer between 0 and limit included
int seed = 83; //83 = ASCII of 'S', changed by user input. global variable
int randLCG(unsigned limit)
{
	seed = ((seed * 69069 ) + 1) % (1<<31);
	return abs(seed)%limit;
}*/




int main(int argc, char *argv[])
{


    set_parameters_from_args(argc, argv, &param);
    
	display_params(param,argv[0]);

    //seed = param.seed;
    setRandomSeed(param.seed);

    unsigned a[MAX_LENGTH];
    unsigned b[MAX_LENGTH];
    const int array_elem_byte_size = 4;

	if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		resume_record(); //record lock time
		lock_array(array_elem_byte_size,a,param.length);
		signal(3); //mark to check how much time to lock.
		pause_record(); 
	}

	for(int i = 0; i < param.length; i++)
	{
		a[i] = randLCGBounded(param.length);
		b[i] = randLCGBounded(param.length);
	}


    //test begin
	resume_record(); 

    if(param.mode == CT_CLASSIC)
	{
		permut_CTC(a,b,param.length);
	}
    else{
        permut(a,b,param.length);
    }


    if(param.mode == LOCKED || param.mode == LOCKED_BRANCHLESS)
	{
		signal(4); //mark to check how much time to unlock.
		unlock_array(array_elem_byte_size,a,param.length);
	}

  	pause_record(); //stop simulation record here
    //test end
    return 0;
}
