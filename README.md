# ESORICS2024-artefact

First, install the modified version of binutils with support for lock instructions

```bash
$ make binutils
```

Then, prepare an OPAM switch with OCaml 4.13.1 and install `coq` 8.13.2, `menhir`, `stdint`, `bignum` and `ppx_bitstring`.

```bash
$ make opam-switch
```

Compile our modified version of Compcert within that switch by simply typing:

```bash
$ make ccomp
```

Build the simulator:

```bash
$ make simulator
```

Check the proof of ONI preservation:
```bash
$ make proof
```

You can compile the benchmarks by:
```bash
$ make compile-benchmark
```

And run them with

```bash
$ make tests-complete
```

However, this might take a lot of time. Faster tests can be run with:

```bash
$ make tests-fast
```

All results are put in .csv files in the directories of the corresponding algorithms.
The labels correspond to the execution modes :

- xp: Exposed mode, classic version of the algorithm with no protections against timing attack. The version against which we compare the other versions to get the overhead
- lk: Locked mode, this version is like the exposed one, but with locks on the array that we want to secretly access. We refer to it as the memory Memory Oblivious (MO(L)) version in the paper.
- bl: Branchless mode, this version is like the exposed one, but with a control flow independent of the secrets. Some algorithms are already Branchless in their exposed version (crypto algorithms and permut).
- lkbl: Locked Branchless mode. This mode applies locks on the branchless version. If the current algorithm is already branchless in its exposed version, lk and lkbl modes are identical. We refer to it as the memory Constant Time Secure with Locks (CTS(L)) version in the paper.
- ctc: Constant Time Classic mode. This mode is constant time without the use of locks. We refer to it as the memory Constant Time Secure (CTS) version in the paper.


The numbers displayed in the generated .csv files are the number of hardware clock cycles that would be required on real hardware during the recorded part of the simulation. E.g. for AES, we record only the time to lock the s-boxes, cipher the plaintext, and then unlock the s-boxes, This time does not account for data generation or any other initialization at the beginning or verification computations at the end.


We provide the .elf files already compiled in RV_lock for all benchmarks. If you want to recompile them yourself with the previously installed compcert and binutils, you can clear all the .elf files with

```bash
$ make clean-bench
```
Warning: this will also clean the .csv files in the benchmark directory.



You can also simulate the algorithms independently of each other, for example, if you want to test that AES will have identical leakage with different inputs in CTS(L) mode  but not in exposed mode :

```bash
$ cd ./benchmark/aes/
make test_leak TEST_MODE=lk
make test_leak TEST_MODE=xp
```
(the test with xp mode will generate an error, as the diff of its trace is not empty. The leakage traces and their differences are generated in the directory ./benchmark/aes/leaks/)