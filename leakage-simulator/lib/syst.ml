type t =
{
  configuration : Conf.t;
  os: Os.t; (*process management (scheduling, available addresses, save and restore on context switch)*)
  pc : Int32.t;   (*Program Counter*)
  ct : bool;  (*Constant Time state (False = regular execution, True = Constant Time secure execution)*)
  registers : Regp.r_list;
  l1i : Cach.t;
  l1d : Cach.t;
  ram : Memo.t;
  record_execution: bool;
}

let to_string (sys : t) : string =
  Printf.sprintf "PC = %ld (%lx); CT = %b\n Registers: %s\n"
    sys.pc sys.pc
    sys.ct (Regp.list_to_string sys.registers)

let to_string_with_memory (sys : t) (l_bound : Int64.t) (h_bound : Int64.t) : string =
  (*let l_bound, h_bound = 135290,135360 in*)
  
  try(
    (to_string sys) ^
    "\n RAM between " ^ Int64.to_string l_bound ^ " and "^  Int64.to_string h_bound ^" : \n" ^ (Memo.to_string_as_ram sys.ram l_bound h_bound (*range 0 8 8*)) ^ "\n Cache L1-d " ^ Cach.to_string sys.l1d ^ "\n===================================\n" (*display_first_n_memory_bytes*)
    (*
    "\nFirst "^  Int64.to_string range ^" bytes of RAM: " ^ (Memo.to_string_as_ram sys.ram 0 range (*range 0 8 8*)) ^ "\n Cache L1-d " ^ Cach.to_string sys.l1d ^ "\n===================================\n" (*display_first_n_memory_bytes*)
    *)
  ) with e -> ("Error in syst.to_string_with_memory : " ^ Printexc.to_string e ^ "\n" ^ Printexc.get_backtrace () )


let build (configuration : Conf.t) (record_from_start : bool) (mpu : bool): t = {
  configuration = (configuration); 
  os = Os.build (Sche.new_round_robin(30)) mpu; (*TODO configuration du round robin et des processus a ajouter*)
  pc = 0l;   (*Program Counter*)
  ct = false;  (*Constant Time state (False = regular execution, True = Constant Time secure execution)*)
  registers = Array.init 32 (fun _ ->0l) ; (*x0 = 0, other registers not initialized yet*)
  l1i = (Cach.build  4 1);  (*direct mapped cache with 4 lines*)
  l1d = (Cach.build  128 4);  
  (*l1d = (Cach.build  1 4);  fully associative cache with 4 lines *)
  (*l2 = (build_new_cache "L1d" 0 0);   (*No L2 cache for now*) *)
  ram = Memo.empty (); (*encode input program at the beginning of the ram*)
  record_execution = record_from_start;
}


  
let add_process (system : t) (encoded_prgm : Prgm.t) : t =
      (* TODO : align_addresses_on_cache only used to check that LOCK/UNLOCK/LINK/ROCS are used on the right addresses
        if (system.configuration.align_addresses_on_cache)
        then (system.configuration.words_per_cache_line) (*alignment done on cache line size (counter in number of words)*)
        else (1) (*If there is no alignement, it is equivalent to do alignement on 1-word size cache line*)
      *)
  if Conf.show_debug
  then (
    Memo.display encoded_prgm.initial_memory;
    Printf.printf "encoded_prgm.memory_begin = 0x%Lx\n" encoded_prgm.memory_allocation.lower_bound
  );
  {
    system with
    os = Os.add_process system.os encoded_prgm;
    ram = Memo.write_from_memory system.ram encoded_prgm.initial_memory;
  }

(*
let add_process_from_memory (system : t) (mem_begin : int) (mem_end : int) (init_memory : Memo.t) (entry : int) (final_return_address : int) (encoded_prgm : Prgm.t) : t =
  let updated_OS =
    Os.add_process
      system.os
      encoded_prgm
      (* TODO : align_addresses_on_cache only used to check that LOCK/UNLOCK/LINK/ROCS are used on the right addresses
        if (system.configuration.align_addresses_on_cache)
        then (system.configuration.words_per_cache_line) (*alignment done on cache line size (counter in number of words)*)
        else (1) (*If there is no alignement, it is equivalent to do alignement on 1-word size cache line*)
      *)
  in
  let memory_from_new_process =
    (Memo.of_int_list encoded_prgm.initial_memory encoded_prgm.memory_begin Binm.Unsigned)
  in
  
  let _debug =
    if (Conf.show_debug)
    then(
      let _ = Memo.display memory_from_new_process in
      print_endline ("encoded_prgm.memory_begin =" ^  Int64.to_string encoded_prgm.memory_begin)
    )
    else ()
  in
  {system with
      os = updated_OS;
      ram =
      (Memo.write_from_memory
        (system.ram)
        (memory_from_new_process)
      );
  }
*)




let context_switch (system:t) (new_pid : int) : t =
  let interrupted_process = Proc.save (system.pc) (system.ct) (system.registers) in
  let updated_OS = Os.switch (system.os) (new_pid) (interrupted_process) in 
  let new_pc, new_ct, new_registers =
    match (Os.restore_process updated_OS new_pid)
    with restored_process -> (restored_process.pc), (restored_process.ct), (restored_process.registers)
  in
  {system with
    os = updated_OS;
    pc = new_pc;
    ct = new_ct;
    registers = new_registers;
  }
  
let update_schedule (system:t) : t =
  match Os.scheduler_want_context_switch system.os with
  | Some(pid) -> (context_switch (system) (pid))
  | None -> {system with os = (Os.advance_scheduling (system.os))}




(*Inst.process_instruction delegate handling of Ecall here*)
let handle_Ecall (system : t) : ((Event.t * t) option) =
  let ecall_number = Regp.read system.registers (Regp.build 17) in
  Printf.printf "Ecall (a7=0x%lx)\n" ecall_number;
  let system = { system with pc = Int32.add system.pc 4l } in
  let system =
    match ecall_number with
    | 0l -> (*ECALL 0 stop execution*)
      (*Printf.printf "REGS: %s\n" (Regp.list_to_string system.registers);*)
      None
    | 1l -> (*ECALL 1 start simulation record*)
      Some {system with record_execution = true; }

    | 2l -> (*ECALL 2 stop simulation record*)
      Some {system with record_execution = false; }

    | _ -> (*Other ecalls have no effect, can be used as a mark in simulation*)
      Some system

  in
  match system with
  | None -> None
  | Some sys -> Some(Event.ecall_leak ecall_number, sys)



  (*
  For later upgrades : if we want to properly handle Ecall, we must look at values in x10-x17 (a0-a10)
  which will determines what kind of Ecall with what arguments it is.
  Ecall codes are specific to OS, not ISA.

  The return value must be None if the execution of this process is definitly hatled (exit() sys call)

  or Some(leaks : Event.T * state : Syst.t), with state the new system state in which the process will be resumed
  (PC updated, result of sys call in a0 register), and leaks being what observable events and attacker
  could see (did the system access memory during the interruption ? is the interruption time dependant
  of the system state when the process was paused, e.g. cache content that must be saved or restored)
  *)


  
(* RELATIONS ENTRE ETATS *)

(*
type source = Source of t * Proc.t (*on doit savoir pour quel processus est ce programe*)
type cible = Cible of t

(*test a ≈ alpha , *)
let source_contextualisable_en_cible (a : source) (alpha : cible) : bool = raise(Failure ("TODO tester que alpha pourrait etre un etat cible possible de l'etat source a "))
(*
memes registres (avec pc),
memes ram
*)

(* test a1 ≡s a2 *)
let equivalence_etats_sources (a1 : source) (a2 : source) : bool = raise(Failure ("TODO tester equiv entre deux etats sources"))

(* test alpha1 ≡c alpha2  *)
let equivalence_etats_cibles (alpha1 : cible) (alpha2 : cible) : bool = raise(Failure ("TODO tester equiv entre deux etats cible"))

*)