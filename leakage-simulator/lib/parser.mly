%{
    open Inst
    
%}

%token EOF EOL
%token<int> REG INT
%token RPAREN LPAREN
%token MV ADD SUB SLT OR XOR AND NOT IMM LUI BEQ BNE BLT BGE JAL JALR LW LH LB SW SH SB LOCK ROCS LINK UNLOCK ECALL SCTM END
%token ADDI SUBI SRAI SLLI SRLI
%token MUL MULH MULHU MULHSU DIV DIVU REM REMU
(* rewrite modes : write back on unlock, write through, careless *)
%token WBOU WT CL
%start main
%type <Inst.t list> main

%%

  main:
    | lines EOF { $1 }
    ;

      lines:
        | line EOL lines { $1 :: $3 }
        | line { [$1] }
        | { [] }
    ;

      line:
        op3 reg reg reg { $1 $2 $3 $4 }
        | op3i reg reg int32 { $1 $2 $3 $4 }
        | op2 reg reg { $1 $2 $3 }
        (*| UNLOCK int32 LPAREN reg RPAREN { UnlockCache( $4, $2)  }*)
        | ROCS reg { RocsCache $2 }
        | op0 { $1 }
        | IMM reg int32 { Imm ($2, $3) }
        | LUI reg int32 { Lui ($2, $3) }
        | branch reg reg int32 { $1 ($2, $3, $4) }
        | JAL reg int32 { Jal ($2, $3) }
        | JALR reg reg int32 { Jalr ( $2, $3, $4) }
        | UNLOCK offset_and_address { UnlockCache( fst $2, snd $2)  } (*unlock 4(x2) => Unlock(x2,4) *)
        | LOCK mode offset_and_address { LockCache($2,fst $3, snd $3)} (*lock cl 4(x2) => Lock(Careless,x2,4) *)
        | LW reg offset_and_address { Load(Memo.Word, Binm.Signed, $2,fst $3, snd $3)}  (* ld x1 x2 => ld x1 0(x2) => Load(x1,x2,0)*)
        | LH reg offset_and_address { Load(Memo.Halfword, Binm.Signed, $2,fst $3, snd $3)} 
        | LB reg offset_and_address { Load(Memo.Byte, Binm.Signed, $2,fst $3, snd $3)} 
        | SW reg offset_and_address  { Store(Memo.Word, $2,fst $3, snd $3)}
        | SH reg offset_and_address  { Store(Memo.Halfword, $2,fst $3, snd $3)}
        | SB reg offset_and_address  { Store(Memo.Byte, $2,fst $3, snd $3)}
    ;

      offset_and_address : 
      | reg {$1,0l}
      | LPAREN reg RPAREN {$2,0l}
      | int32 LPAREN reg RPAREN {$3, $1}

      reg:
        | REG { Regp.build $1 }
    ;

      int32:
        | INT { Int32.of_int $1}
;


      mode:
        | WBOU { Mode.Write_Back_On_Unlock }
        | WT { Mode.Write_Through}
        | CL { Mode.Careless}

      branch:
        | BEQ { fun (x,y,z) -> Beq (x,y,z) }
        | BNE { fun (x,y,z) -> Bne (x,y,z) }
        | BLT { fun (x,y,z) -> Blt (x,y,z) }
        | BGE { fun (x,y,z) -> Bge (x,y,z) }
    ;

      op3:
        | ADD { fun a b c -> Add (a,b,c) }
        | SUB { fun a b c -> Sub (a,b,c) }
        | MUL { fun a b c -> Mul (a,b,c) }
        | DIV { fun a b c -> Div (a,b,c) }
        | REM { fun a b c -> Rem (a,b,c) }
        | MULH { fun a b c -> Mulh (a,b,c) }
        | MULHU { fun a b c -> Mulhu (a,b,c) }
        | MULHSU { fun a b c -> Mulhsu (a,b,c) }
        | DIVU { fun a b c -> Divu (a,b,c) }
        | REMU { fun a b c -> Remu (a,b,c) }
        | SLT { fun a b c -> Slt (a,b,c) }
        | OR { fun a b c -> Or (a,b,c) }
        | XOR { fun a b c -> Xor (a,b,c) }
        | AND { fun a b c -> Inst.And (a,b,c) }
    ;

      op3i:
        | ADDI { fun a b c -> Addi (a,b,c) }
        | SUBI { fun a b c -> Subi (a,b,c) }
        | SRAI { fun a b c -> Srai (a,b,c) }
        | SLLI { fun a b c -> Sli (a,b,c) }
        | SRLI { fun a b c -> Srli (a,b,c) }

      op2:
        | MV { fun a b -> Mv (a,b)}
        | NOT { fun a b -> Not (a,b)}
        | SCTM { fun a b -> Inst.SCTM (a, b)}
        | LINK { fun a b -> Inst.LinkCache (a, b)}
    ;

      op0:
        | END { End }
        | ECALL { Ecall }
    ;
      
