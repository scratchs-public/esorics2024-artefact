(* Only preemptive scheduling for now, semi-cooperative might be added later (usage of Yield instruction and possibility of Interruptions)*)
type t =
  | RoundRobin of int * int * int (*(remaining_steps,max_count, next_pid)  give max_count steps to each process*)
  | Manual of ((int*int) list) (*2-uplets of (remaining_steps * next_process) taht do context switch toward next_process when remaining_steps = 0*)


let to_string (scheduler : t) : string =
  match scheduler with
  | RoundRobin(_, max_count, _) -> "Round Robin with " ^ string_of_int max_count ^ "steps for each process"
  | Manual(_) -> "Manual scheduling"



let new_round_robin (max_count : int) = RoundRobin(0,max_count,0)

let new_manual_scheduling (schedule : ((int*int) list)) = Manual(schedule)


let advance_scheduling (scheduler : t) : t =
  match scheduler with
  | RoundRobin(remaining_steps,max_count, pid) -> RoundRobin((remaining_steps-1),max_count, pid)
  | Manual([]) -> Manual([]) (*Keep going forever once end of manual scheduling reached*)
  | Manual ((remaining_steps,next_pid) :: tail) -> Manual (((remaining_steps-1),next_pid) :: tail)


let switch (scheduler : t) (next_pid : int) : t =
  match scheduler with
  | RoundRobin(_remaining_steps,max_count, _old_pid) -> RoundRobin(max_count,max_count, next_pid)
  | Manual([]) -> raise (Failure ("manual scheduler unable to switch : no more scheduling planned"))
  | Manual (_head :: tail) -> Manual (tail)




let scheduler_want_context_switch (scheduler : t) : (int option) =
  match scheduler with
  | RoundRobin(remaining_steps,_max_count,next_pid) ->
    (
      if (remaining_steps = 0)
      then (Some(next_pid))
      else (None)
    )
  | Manual ([]) -> None (*Never switch once end of manual schedule reached*)
  | Manual ((remaining_steps,next_pid) :: _tail) ->
    (
      if (remaining_steps = 0)
      then (Some(next_pid))
      else (None)
    )



