
type t =
{
  current_pid : int;
  processes : (Proc.t list);   (*List of processes states, indexed by pid. First element is for process with pid 0*)
  allocated_memory : ((Memo.allocation) list); (*allocated_memory (memory_begin * memory_end), indexed by pid. *)
  scheduler : Sche.t;  (*determines how many instruction there is between each context switch*)
  memory_protection: bool; (*determines if memory access outside of memory space will kill the process or be ignored (with a warning message)*)
}


let build (s : Sche.t) (mpu : bool): (t) =
  {
    current_pid = -1;
    processes = [];
    allocated_memory = [];
    scheduler = s;
    memory_protection = mpu;
  }

let rec to_string_aux (counter : int) (proc : Proc.t list) (alloc : Memo.allocation list) : string =
  match proc, alloc with
  | [],[] ->""
  | head_proc :: tail_proc, head_alloc :: tail_alloc ->
    (
      "   - Process " ^ string_of_int counter ^ "\n" ^
      (Proc.to_string head_proc) ^ 
      "with memory allocation [" ^ Int64.to_string head_alloc.lower_bound  ^ "-" ^ Int64.to_string head_alloc.upper_bound  ^ "]\n"  ^
      (to_string_aux (counter+1) (tail_proc) (tail_alloc))
    )
  | _::_,[] -> "Error in OS: too many saved process state and not enough memory alloc in OS"
  | [],_::_ -> "Error in OS: too many memory alloc and not enough saved process state in OS"


let to_string (os : t) : string =
  "# OS display. Current PID = " ^string_of_int os.current_pid ^ "\n# List of processes:\n" ^
  (to_string_aux (0) (os.processes) (os.allocated_memory)) ^ "\n# Scheduler:\n" ^
  (Sche.to_string os.scheduler)

let check_memory_allocation (allocated_memory : Memo.allocation list)
    (new_process_begin : Int64.t) (new_process_end : Int64.t) : unit =
  let open Memo in
  match List.find_opt (fun alloc ->
      Int64.max new_process_begin alloc.lower_bound > Int64.min new_process_end alloc.upper_bound
    ) allocated_memory with
  | None -> ()
  | Some alloc ->
    failwith (
      Printf.sprintf
        "Memory allocation [0x%Lx; 0x%Lx] collides with memory allocation [%s]"
        new_process_begin new_process_end
        (Memo.allocation_to_string alloc)
    )

(*Return (updated os, starting address of process assembly code) *)
let add_process (os : t) (new_prog : Prgm.t) (*alignment_stride : int*) : (t) =
  if new_prog.memory_allocation.lower_bound > new_prog.memory_allocation.upper_bound
  then failwith (
      Printf.sprintf "Bad memory allocation : begin (0x%Lx) cannot be after end (0x%Lx)."
        new_prog.memory_allocation.lower_bound
        new_prog.memory_allocation.upper_bound);
  check_memory_allocation os.allocated_memory
    new_prog.memory_allocation.lower_bound
    new_prog.memory_allocation.upper_bound;
  let new_process_state =
    Proc.save
      (new_prog.initial_pc)
      (false) (*CT register always on false at init*)
      (Regp.build_new_register_list new_prog) (*registers à empty at init, except x1, x2 and x3 which contains return address, stack pointer and global pointer*)

  in
  (
    {os with
      processes = List.append (os.processes) (new_process_state :: []);
      allocated_memory = List.append (os.allocated_memory) ((new_prog.memory_allocation):: []);
    }
  )




(*Return None if no context switch is required, Some(pid) if a CS is required toward process pid*)
let scheduler_want_context_switch (os : t) : (int option) =
  Sche.scheduler_want_context_switch (os.scheduler)


let restore_process (os : t) (pid : int) : Proc.t =
  let _debug =
    if (Conf.show_debug)
    then(print_endline ("\n     RESTORE PROCESS " ^ string_of_int pid))
    else ()
  in
  if((List.length os.processes) > (pid) && (pid)>(-1))
  then (List.nth (os.processes) (pid))
  else raise(Failure ("Cannot restore process " ^ string_of_int pid ^ " when there is only " ^ string_of_int (List.length os.processes) ^ " processes known by the OS."))


let rec store_process_state (processes : (Proc.t list)) (interrupted_process : Proc.t) (count : int) (pid : int) : (Proc.t list) =
  match processes with 
  | [] -> raise(Failure("Process with pID "^ string_of_int pid ^" not found during Context Switch."))
  | head :: tail -> 
    (
      if (count = 0)
      then (interrupted_process :: tail)
      else (
        head ::
        (store_process_state 
          (tail)
          (interrupted_process)
          (count-1)
          (pid)
        )
      ) 
    )


let advance_scheduling (os : t) : t =
  {os with scheduler = Sche.advance_scheduling os.scheduler}

let switch (os : t) (new_pid : int) (interrupted_process : Proc.t) : t =
  let new_processes_list =
    if (os.current_pid = -1)
    then (os.processes) (*don't save the -1 process : only use for init of system*)
    else (store_process_state (os.processes) (interrupted_process) (os.current_pid) (os.current_pid))
  in
  if Conf.show_debug
  then(
    Printf.printf "\n\n###Context Switch from process %d\n" os.current_pid;
    Printf.printf "   %s\n" (Proc.to_string interrupted_process);
    Printf.printf "   to process %d\n" new_pid;
    Printf.printf "   %s\n" (Proc.to_string (List.nth new_processes_list new_pid))
  );
  let next_pid = (new_pid+1) mod (List.length new_processes_list) in (*Next Pid is current +1, starting back at 0 when last process reached. Manual scheduling can ignore this order*)
  let updated_os =
    {os with
      current_pid = new_pid;
      processes = new_processes_list;
      scheduler = Sche.switch (os.scheduler) (next_pid);
    }
  in
  let _debug =
    if (Conf.show_debug)
    then(
      print_endline (
        "updated OS: "^ to_string updated_os
      )
    )
    else ()
  in
  updated_os

(*

*)

let address_allowed (_os : t) (_address : Int64.t) (_size : Memo.size) (_error_message : string): (unit) = ()
(*error with some programs : start main with
  lui x31 FFFCE (*set a negative value*)
	addi	x31, x31, ...
	add	x2, x2, x31
with put stack pointer in 0xFFFFCE*** address, why ? 
resolve this before puting memory check
   *)
let address_allowed_bug (os : t) (address : Int64.t) (size : Memo.size) (error_message : string): (unit) = 
  
  let mem_alloc = (List.nth (os.allocated_memory) (os.current_pid)) in
  
  let out_of_memory_bound_access (_a : unit) : unit =
    Printf.printf "\nWARNING!!  ";
      Printf.printf "Address 0x%Lx with access size %s is out of memory allocation:\n%s\n%s.\n"
        address
        (Memo.size_to_string size)
        (Memo.allocation_to_string mem_alloc)
        error_message;
    Printf.printf "You should start the simulation again with more allocated space for the stacks of processes using the option --stack-size\n";
    match os.memory_protection with
    | true -> raise(Failure ("Memory protection killed the process (option '--mpu n' to disable memory protection)\n"))
    | false -> Printf.printf "Memory protection is disable in this simulation (option '--mpu y' to enable memory protection), the process will keep going even if it did a memory access outside of its memory space.\n"
  in


  

  if (address <> Memo.print_address) (*All processes are allowed to access print_address*)
  then (
    if   (*check that memory access is between allowed memory bound*)
      address >= mem_alloc.lower_bound &&
      Int64.add address (Int64.of_int (Memo.nb_bytes size)) < mem_alloc.upper_bound
    then
    (
      if  (*check for stack overflow*)
        not ((address > (Int64.add mem_alloc.stack_fence Memo.stack_fence_range)) ||
        ((Int64.add address (Int64.of_int (Memo.nb_bytes size))) < mem_alloc.stack_fence))
      then
        (
        Printf.printf "==================Stack overflow detected";
        out_of_memory_bound_access()
        )
      else(
        (*valid memory access, keep going*)
      )

        
    )
    else
    (
      Printf.printf "=====================Forbidden memory access detected";
      out_of_memory_bound_access()
    )
  )
