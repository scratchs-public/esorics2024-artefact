{
open Parser

let regname_to_int r =
  Scanf.sscanf r "x%d" (fun x -> x)
}

let digit = ['0'-'9']
let letter = ['a'-'z' 'A'-'Z']
let id = letter (digit|letter|'_')*


let reg = "x" digit*
let number = "-"? digit+

rule token = parse
  | [' ' '\t' ','] { token lexbuf }
  | '\n' { Lexing.new_line lexbuf; EOL }
  | reg as r { REG (regname_to_int r) }
  | number as n { INT (int_of_string n) }
  | ";" { comment lexbuf }
  | "(" { LPAREN }
  | ")" { RPAREN }
  | "mv" { MV }
  | "add" { ADD }
  | "sub" { SUB }
  | "mul" { MUL }
  | "mulh" { MULH }
  | "mulhu" { MULHU }
  | "mulhsu" { MULHSU }
  | "div" { DIV }
  | "divu" { DIVU }
  | "rem" { REM }
  | "remu" { REMU }
  | "slt"  { SLT }
  | "or"  { OR }
  | "or"  { XOR }
  | "and" { AND }
  | "not" { NOT }
  | "addi" { ADDI }
  | "subi" { SUBI }
  | "srai" { SRAI }
  | "slli" { SLLI }
  | "srli" { SRLI }
  | "imm" { IMM }
  | "lui" { LUI }
  | "beq" { BEQ }
  | "bne" { BNE }
  | "blt" { BLT }
  | "bge" { BGE }
  | "jal" { JAL }
  | "jalr" { JALR }
  | "ld" { LW } (*for backward compatibility, Ld = Load Word*)
  | "st" { SW } (*for backward compatibility, St = Store Word*)
  | "lw" { LW }
  | "sw" { SW }
  | "lh" { LH }
  | "sh" { SH }
  | "lb" { LB }
  | "sb" { SB }
  | "lock" { LOCK }
  | "rocs" { ROCS }
  | "link" { LINK }
  | "unlock" { UNLOCK }
  | "ecall" { ECALL }
  | "end" { END }
  | "sctm" { SCTM }
  | "wbou" { WBOU }
  | "wt" { WT }
  | "cl" { CL }
  | eof { EOF }
  | _ as x { let open Lexing in
             failwith (Printf.sprintf "unexpected char '%c' at line %d col %d\n" x
                         (lexbuf.lex_curr_p.pos_lnum)
                         (lexbuf.lex_curr_p.pos_cnum - lexbuf.lex_curr_p.pos_bol))}

and comment = parse
  | ['\n' '\r'] { Lexing.new_line lexbuf; EOL }
  | eof { EOF }
  | _ { comment lexbuf }




(*

      | Paddiw (rd, rs, imm) ->
         fprintf oc "	addi%t	%a, %a, %a\n" w ireg rd ireg0 rs coqint imm
      | Psltiw (rd, rs, imm) ->
         fprintf oc "	slti	%a, %a, %a\n" ireg rd ireg0 rs coqint imm
      | Psltiuw (rd, rs, imm) ->
         fprintf oc "	sltiu	%a, %a, %a\n" ireg rd ireg0 rs coqint imm
      | Pandiw (rd, rs, imm) ->
         fprintf oc "	andi	%a, %a, %a\n" ireg rd ireg0 rs coqint imm
      | Poriw (rd, rs, imm) ->
         fprintf oc "	ori	%a, %a, %a\n" ireg rd ireg0 rs coqint imm
      | Pxoriw (rd, rs, imm) ->
         fprintf oc "	xori	%a, %a, %a\n" ireg rd ireg0 rs coqint imm
      | Pslliw (rd, rs, imm) ->
         fprintf oc "	slli%t	%a, %a, %a\n" w ireg rd ireg0 rs coqint imm
      | Psrliw (rd, rs, imm) ->
         fprintf oc "	srli%t	%a, %a, %a\n" w ireg rd ireg0 rs coqint imm
      | Psraiw (rd, rs, imm) ->
         fprintf oc "	srai%t	%a, %a, %a\n" w ireg rd ireg0 rs coqint imm
      | Pluiw (rd, imm) ->
         fprintf oc "	lui	%a, %a\n"     ireg rd coqint imm

      (* 32-bit integer register-register instructions *)
      | Paddw(rd, rs1, rs2) ->
         fprintf oc "	add%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Psubw(rd, rs1, rs2) ->
         fprintf oc "	sub%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2

      | Pmulw(rd, rs1, rs2) ->
         fprintf oc "	mul%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Pmulhw(rd, rs1, rs2) ->  assert (not Archi.ptr64);
         fprintf oc "	mulh	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2
      | Pmulhuw(rd, rs1, rs2) ->  assert (not Archi.ptr64);
         fprintf oc "	mulhu	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2

      | Pdivw(rd, rs1, rs2) ->
         fprintf oc "	div%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Pdivuw(rd, rs1, rs2) ->
         fprintf oc "	divu%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Premw(rd, rs1, rs2) ->
         fprintf oc "	rem%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Premuw(rd, rs1, rs2) ->
         fprintf oc "	remu%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2

      | Psltw(rd, rs1, rs2) ->
         fprintf oc "	slt	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2
      | Psltuw(rd, rs1, rs2) ->
         fprintf oc "	sltu	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2

      | Pandw(rd, rs1, rs2) ->
         fprintf oc "	and	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2
      | Porw(rd, rs1, rs2) ->
         fprintf oc "	or	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2
      | Pxorw(rd, rs1, rs2) ->
         fprintf oc "	xor	%a, %a, %a\n" ireg rd ireg0 rs1 ireg0 rs2
      | Psllw(rd, rs1, rs2) ->
         fprintf oc "	sll%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Psrlw(rd, rs1, rs2) ->
         fprintf oc "	srl%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2
      | Psraw(rd, rs1, rs2) ->
         fprintf oc "	sra%t	%a, %a, %a\n" w ireg rd ireg0 rs1 ireg0 rs2

  
      (* Unconditional jumps.  Links are always to X1/RA. *)
      | Pj_l(l) ->
         fprintf oc "	j	%a\n" print_label l
      | Pj_s(s, sg) ->
         fprintf oc "	jump	%a, x31\n" symbol s
      | Pj_r(r, sg) ->
         fprintf oc "	jr	%a\n" ireg r
      | Pjal_s(s, sg) ->
         fprintf oc "	call	%a\n" symbol s
      | Pjal_r(r, sg) ->
         fprintf oc "	jalr	%a\n" ireg r

      (* Conditional branches, 32-bit comparisons *)
      | Pbeqw(rs1, rs2, l) ->
         fprintf oc "	beq	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbnew(rs1, rs2, l) ->
         fprintf oc "	bne	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbltw(rs1, rs2, l) ->
         fprintf oc "	blt	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbltuw(rs1, rs2, l) ->
         fprintf oc "	bltu	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbgew(rs1, rs2, l) ->
         fprintf oc "	bge	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbgeuw(rs1, rs2, l) ->
         fprintf oc "	bgeu	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l

      (* Conditional branches, 64-bit comparisons *)
      | Pbeql(rs1, rs2, l) -> assert Archi.ptr64;
         fprintf oc "	beq	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbnel(rs1, rs2, l) -> assert Archi.ptr64;
         fprintf oc "	bne	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbltl(rs1, rs2, l) -> assert Archi.ptr64;
         fprintf oc "	blt	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbltul(rs1, rs2, l) -> assert Archi.ptr64;
         fprintf oc "	bltu	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbgel(rs1, rs2, l) -> assert Archi.ptr64;
         fprintf oc "	bge	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l
      | Pbgeul(rs1, rs2, l) -> assert Archi.ptr64;
         fprintf oc "	bgeu	%a, %a, %a\n" ireg0 rs1 ireg0 rs2 print_label l

      (* Loads and stores *)
      | Plb(rd, ra, ofs) ->
         fprintf oc "	lb	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Plbu(rd, ra, ofs) ->
         fprintf oc "	lbu	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Plh(rd, ra, ofs) ->
         fprintf oc "	lh	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Plhu(rd, ra, ofs) ->
         fprintf oc "	lhu	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Plw(rd, ra, ofs) | Plw_a(rd, ra, ofs) ->
         fprintf oc "	lw	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Pld(rd, ra, ofs) | Pld_a(rd, ra, ofs) -> assert Archi.ptr64;
         fprintf oc "	ld	%a, %a(%a)\n" ireg rd offset ofs ireg ra

      | Psb(rd, ra, ofs) ->
         fprintf oc "	sb	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Psh(rd, ra, ofs) ->
         fprintf oc "	sh	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Psw(rd, ra, ofs) | Psw_a(rd, ra, ofs) ->
         fprintf oc "	sw	%a, %a(%a)\n" ireg rd offset ofs ireg ra
      | Psd(rd, ra, ofs) | Psd_a(rd, ra, ofs) -> assert Archi.ptr64;
         fprintf oc "	sd	%a, %a(%a)\n" ireg rd offset ofs ireg ra


      (* Synchronization *)
      | Pfence ->
         fprintf oc "	fence\n"

      (* floating point register move.
         fmv.d preserves single-precision register contents, and hence
         is applicable to both single- and double-precision moves.
       *)
      | Pfmv (fd,fs) ->
         fprintf oc "	fmv.d	%a, %a\n"     freg fd freg fs
      | Pfmvxs (rd,fs) ->
         fprintf oc "	fmv.x.s	%a, %a\n"     ireg rd freg fs
      | Pfmvsx (fd,rs) ->
         fprintf oc "	fmv.s.x	%a, %a\n"     freg fd ireg rs
      | Pfmvxd (rd,fs) ->
         fprintf oc "	fmv.x.d	%a, %a\n"     ireg rd freg fs
      | Pfmvdx (fd,rs) ->
         fprintf oc "	fmv.d.x	%a, %a\n"     freg fd ireg rs

      (* 32-bit (single-precision) floating point *)
      | Pfls (fd, ra, ofs) ->
         fprintf oc "	flw	%a, %a(%a)\n" freg fd offset ofs ireg ra
      | Pfss (fs, ra, ofs) ->
         fprintf oc "	fsw	%a, %a(%a)\n" freg fs offset ofs ireg ra

      | Pfnegs (fd, fs) ->
         fprintf oc "	fneg.s	%a, %a\n"     freg fd freg fs
      | Pfabss (fd, fs) ->
         fprintf oc "	fabs.s	%a, %a\n"     freg fd freg fs

      | Pfadds (fd, fs1, fs2) ->
         fprintf oc "	fadd.s	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfsubs (fd, fs1, fs2) ->
         fprintf oc "	fsub.s	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfmuls (fd, fs1, fs2) ->
         fprintf oc "	fmul.s	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfdivs (fd, fs1, fs2) ->
         fprintf oc "	fdiv.s	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfmins (fd, fs1, fs2) ->
         fprintf oc "	fmin.s	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfmaxs (fd, fs1, fs2) ->
         fprintf oc "	fmax.s	%a, %a, %a\n" freg fd freg fs1 freg fs2

      | Pfeqs (rd, fs1, fs2) ->
         fprintf oc "	feq.s   %a, %a, %a\n" ireg rd freg fs1 freg fs2
      | Pflts (rd, fs1, fs2) ->
         fprintf oc "	flt.s   %a, %a, %a\n" ireg rd freg fs1 freg fs2
      | Pfles (rd, fs1, fs2) ->
         fprintf oc "	fle.s   %a, %a, %a\n" ireg rd freg fs1 freg fs2

      | Pfsqrts (fd, fs) ->
         fprintf oc "	fsqrt.s %a, %a\n"     freg fd freg fs

      | Pfmadds (fd, fs1, fs2, fs3) ->
         fprintf oc "	fmadd.s	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3
      | Pfmsubs (fd, fs1, fs2, fs3) ->
         fprintf oc "	fmsub.s	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3
      | Pfnmadds (fd, fs1, fs2, fs3) ->
         fprintf oc "	fnmadd.s	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3
      | Pfnmsubs (fd, fs1, fs2, fs3) ->
         fprintf oc "	fnmsub.s	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3

      | Pfcvtws (rd, fs) ->
         fprintf oc "	fcvt.w.s	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtwus (rd, fs) ->
         fprintf oc "	fcvt.wu.s	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtsw (fd, rs) ->
         fprintf oc "	fcvt.s.w	%a, %a\n" freg fd ireg0 rs
      | Pfcvtswu (fd, rs) ->
         fprintf oc "	fcvt.s.wu	%a, %a\n" freg fd ireg0 rs

      | Pfcvtls (rd, fs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.l.s	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtlus (rd, fs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.lu.s	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtsl (fd, rs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.s.l	%a, %a\n" freg fd ireg0 rs
      | Pfcvtslu (fd, rs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.s.lu	%a, %a\n" freg fd ireg0 rs

      (* 64-bit (double-precision) floating point *)
      | Pfld (fd, ra, ofs) | Pfld_a (fd, ra, ofs) ->
         fprintf oc "	fld	%a, %a(%a)\n" freg fd offset ofs ireg ra
      | Pfsd (fs, ra, ofs) | Pfsd_a (fs, ra, ofs) ->
         fprintf oc "	fsd	%a, %a(%a)\n" freg fs offset ofs ireg ra

      | Pfnegd (fd, fs) ->
         fprintf oc "	fneg.d	%a, %a\n"     freg fd freg fs
      | Pfabsd (fd, fs) ->
         fprintf oc "	fabs.d	%a, %a\n"     freg fd freg fs

      | Pfaddd (fd, fs1, fs2) ->
         fprintf oc "	fadd.d	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfsubd (fd, fs1, fs2) ->
         fprintf oc "	fsub.d	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfmuld (fd, fs1, fs2) ->
         fprintf oc "	fmul.d	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfdivd (fd, fs1, fs2) ->
         fprintf oc "	fdiv.d	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfmind (fd, fs1, fs2) ->
         fprintf oc "	fmin.d	%a, %a, %a\n" freg fd freg fs1 freg fs2
      | Pfmaxd (fd, fs1, fs2) ->
         fprintf oc "	fmax.d	%a, %a, %a\n" freg fd freg fs1 freg fs2

      | Pfeqd (rd, fs1, fs2) ->
         fprintf oc "	feq.d	%a, %a, %a\n" ireg rd freg fs1 freg fs2
      | Pfltd (rd, fs1, fs2) ->
         fprintf oc "	flt.d	%a, %a, %a\n" ireg rd freg fs1 freg fs2
      | Pfled (rd, fs1, fs2) ->
         fprintf oc "	fle.d	%a, %a, %a\n" ireg rd freg fs1 freg fs2

      | Pfsqrtd (fd, fs) ->
         fprintf oc "	fsqrt.d	%a, %a\n" freg fd freg fs

      | Pfmaddd (fd, fs1, fs2, fs3) ->
         fprintf oc "	fmadd.d	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3
      | Pfmsubd (fd, fs1, fs2, fs3) ->
         fprintf oc "	fmsub.d	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3
      | Pfnmaddd (fd, fs1, fs2, fs3) ->
         fprintf oc "	fnmadd.d	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3
      | Pfnmsubd (fd, fs1, fs2, fs3) ->
         fprintf oc "	fnmsub.d	%a, %a, %a, %a\n" freg fd freg fs1 freg fs2 freg fs3

      | Pfcvtwd (rd, fs) ->
         fprintf oc "	fcvt.w.d	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtwud (rd, fs) ->
         fprintf oc "	fcvt.wu.d	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtdw (fd, rs) ->
         fprintf oc "	fcvt.d.w	%a, %a\n" freg fd ireg0 rs
      | Pfcvtdwu (fd, rs) ->
         fprintf oc "	fcvt.d.wu	%a, %a\n" freg fd ireg0 rs

      | Pfcvtld (rd, fs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.l.d	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtlud (rd, fs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.lu.d	%a, %a, rtz\n" ireg rd freg fs
      | Pfcvtdl (fd, rs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.d.l	%a, %a\n" freg fd ireg0 rs
      | Pfcvtdlu (fd, rs) -> assert Archi.ptr64;
         fprintf oc "	fcvt.d.lu	%a, %a\n" freg fd ireg0 rs

      | Pfcvtds (fd, fs) ->
         fprintf oc "	fcvt.d.s	%a, %a\n" freg fd freg fs
      | Pfcvtsd (fd, fs) ->
         fprintf oc "	fcvt.s.d	%a, %a\n" freg fd freg fs

      (* Pseudo-instructions expanded in Asmexpand *)
      | Pallocframe(sz, ofs) ->
         assert false
      | Pfreeframe(sz, ofs) ->
         assert false
      | Pseqw _ | Psnew _ | Pseql _ | Psnel _ | Pcvtl2w _ | Pcvtw2l _ ->
         assert false

      (* Pseudo-instructions that remain *)
      | Plabel lbl ->
         fprintf oc "%a:\n" print_label lbl
      | Ploadsymbol(rd, id, ofs) ->
         loadsymbol oc rd id ofs
      | Ploadsymbol_high(rd, id, ofs) ->
         fprintf oc "	lui	%a, %%hi(%a)\n" ireg rd symbol_offset (id, ofs)
      | Ploadli(rd, n) ->
         let d = camlint64_of_coqint n in
         let lbl = label_literal64 d in
         fprintf oc "	ld	%a, %a %s %Lx\n" ireg rd label lbl comment d
      | Ploadfi(rd, f) ->
         let d   = camlint64_of_coqint(Floats.Float.to_bits f) in
         let lbl = label_literal64 d in
         fprintf oc "	fld	%a, %a, x31 %s %.18g\n"
                    freg rd label lbl comment (camlfloat_of_coqfloat f)
      | Ploadsi(rd, f) ->
         let s   = camlint_of_coqint(Floats.Float32.to_bits f) in
         let lbl = label_literal32 s in
         fprintf oc "	flw	%a, %a, x31 %s %.18g\n"
                    freg rd label lbl comment (camlfloat_of_coqfloat32 f)
      | Pbtbl(r, tbl) ->
         let lbl = new_label() in
         fprintf oc "%s jumptable [ " comment;
         List.iter (fun l -> fprintf oc "%a " print_label l) tbl;
         fprintf oc "]\n";
         fprintf oc "	sll	x5, %a, 2\n" ireg r;
         fprintf oc "	la	x31, %a\n" label lbl;
         fprintf oc "	add	x5, x31, x5\n";
         fprintf oc "	lw	x5, 0(x5)\n";
         fprintf oc "	add	x5, x31, x5\n";
         fprintf oc "	jr	x5\n";
         jumptables := (lbl, tbl) :: !jumptables;
         fprintf oc "%s end pseudoinstr btbl\n" comment
      | Pnop ->
        fprintf oc "	nop\n"
      | Plock -> fprintf oc "	lock\n"
      | Punlock -> fprintf oc "	unlock\n"
      | Pbuiltin(ef, args, res) ->
         begin match ef with
           | EF_annot(kind,txt, targs) ->
             begin match (P.to_int kind) with
               | 1 -> let annot = annot_text preg_annot "x2" (camlstring_of_coqstring txt) args  in
                 fprintf oc "%s annotation: %S\n" comment annot
               | 2 -> let lbl = new_label () in
                 fprintf oc "%a:\n" label lbl;
                 add_ais_annot lbl preg_annot "x2" (camlstring_of_coqstring txt) args
               | _ -> assert false
             end
          | EF_debug(kind, txt, targs) ->
              print_debug_info comment print_file_line preg_annot "sp" oc
                               (P.to_int kind) (extern_atom txt) args
          | EF_inline_asm(txt, sg, clob) ->
              fprintf oc "%s begin inline assembly\n\t" comment;
              print_inline_asm preg_asm oc (camlstring_of_coqstring txt) sg args res;
              fprintf oc "%s end inline assembly\n" comment
          | _ ->
              assert false
         end
*)