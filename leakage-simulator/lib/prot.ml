type t = 
  | Unprotected
  | Lock of Mode.rewrite
  | Rocs of bool
  | Linked of bool * int (**Linked set id*)
  | Bad_Protection
(**memory protection on a cache line*)

let is_evictable (protection : t) : bool =
  match protection with
  | Unprotected -> true
  | Lock(_) -> false
  | Rocs(_) -> false
  | Linked(_,_) -> true
  | Bad_Protection -> false (**Let bad protec in cache until the end of exec for debug*)

let write_mode_to_string (write_mode : bool) : string =
  (if(write_mode) then ("W") else ("R"))

let int_write_mode_to_string (write_mode : int) : string =
  (if(write_mode=0)
  then ("W")
  else (
    if(write_mode=1)
    then("R")
    else("-") (*Careless write mod, vulnerable*)
    )
  )

let to_string (protection : t) : string =
  match protection with
  | Unprotected -> "   ----   "
  | Lock(write_mode) -> "  Lock-" ^ (Mode.rewrite_to_char write_mode) ^ "  "  (*If we add PID in lock protection :^(Disp.string_of_int_with_padding pid 3) ^*)
  | Rocs(write_mode) -> "  Rocs-" ^ (write_mode_to_string write_mode) ^ "  "
  | Linked(write_mode,value) -> "Link-" ^ (write_mode_to_string write_mode) ^ ":" ^(Disp.string_of_int_with_padding value 3)
  | Bad_Protection -> "Bad_Protec"
