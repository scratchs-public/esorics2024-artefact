(**Display methods*)

let myopen fname = 
  if fname = "-" then
    (stdout, fun () -> ())
  else
    let oc = open_out fname in
    oc, fun () -> close_out oc

let print_out (fname : string) (s : string) : unit=
    let oc, close =
      if fname = "-" then
        (stdout, fun () -> ())
      else
        let oc = open_out fname in
        oc, fun () -> close_out oc
    in
    Printf.fprintf oc "%s\n" s;
    close ()


let print_out_option (out : string option) (s : string) : unit  =
  match out with
  | None -> ()
  | Some fname -> print_out fname s


let print_buffer_out  (fname : string) (b : Buffer.t) : unit=
  let oc, close =
    if fname = "-" then
      (stdout, fun () -> ())
    else
      let oc = open_out fname in
      oc, fun () -> close_out oc
  in
  Buffer.output_buffer oc b;
  close ()


let rec padding (value : Int64.t) (space :int) : string =
  if (space<2) (*If only one space left, no more padding to do (One space required to display the minimal number)*)
  then ("")
  else if (value < 0L) (*Negitive signe need one more space*)
  then (padding (Int64.mul (-1L) value) (space-1))
  else if (value > 9L) (*One digit*)
  then (padding (Int64.div value 10L) (space-1))
  else(" " ^ padding 0L (space-1)) (*Final padding part*)

let string_of_int_with_padding (value :int) (space :int) : string = 
  ((padding (Int64.of_int value) space) ^ (string_of_int value))

  

let string_of_int64_with_padding (value :Int64.t) (space :int) : string = 
  ((padding value space) ^ (Int64.to_string value))
  

let get_printable_string_from_int32 (value : Int32.t) : string =
  
  if (value>= 32l && value<127l)
  then(Char.escaped (Char.chr (Int32.to_int value)))
  else (Int32.to_string value)
  (*Convert in ASCII when possible*)

let int32_as_hex (value :Int32.t ) : string =
  let valint = (Int64.to_int (Binm.int32_as_positive_int64 value)) in
  (Printf.sprintf "0x%x" valint)


let export_text (filename : string) (content : string) : unit =
  (*let file = "example.txt"
  let message = "Hello2!"*)
  
    (* Write content to file *)
    let oc = open_out filename in
    (* create or truncate file, return channel *)
    Printf.fprintf oc "%s\n" content;
    (* write something *)
    close_out oc;
  
    (* flush and close the channel *)
  
    (* Read file and display the first line *)
    let ic = open_in filename in
    try
      let line = input_line ic in
      (* read line, discard \n *)
      print_endline line;
      (* write the result to stdout *)
      flush stdout;
      (* write on the underlying device now *)
      close_in ic
      (* close the input channel *)
    with e ->
      (* some unexpected exception occurs *)
      close_in_noerr ic;
      (* emergency closing *)
      raise e
  (* exit with error: files are closed but channels are not flushed *)
  
  (* normal exit: all channels are flushed and closed *)
