
type inst3 =
  | Sl (*shift left*)
  | Sra (*shift right arithmetic*)
  | Srl (*shift right logical*)
  | Add
  | Sub
  | Mul
  | Mulh
  | Mulhsu
  | Mulhu | Div | Divu | Rem | Remu | Slt | Sltu
  | Or | And | Xor

type inst2i =
  | Sli
  | Srai
  | Srli
  | Addi | Subi | Slti | Sltiu
  | Ori | Andi | Xori

type cmp = Beq | Bne | Blt | Bltu | Bge | Bgeu

type t =
| Mv of Regp.t * Regp.t (*Move a b => a <- b *)
| Imm of Regp.t * Int32.t (* Imm a b = a <- b where b is an immediate*)
| Lui of Regp.t * Int32.t (*Load_Upper_Immediate*)
| Auipc of Regp.t * Int32.t  (*Add_Upper_Immediate_To_PC*)
| Inst3 of inst3 * Regp.t * Regp.t * Regp.t
| Inst2i of inst2i * Regp.t * Regp.t * Int32.t
(*Compare, Counters ??*)
| Not of Regp.t * Regp.t (*Not a b => a = lnot b *)
(*
BEQZ	Branch If Equal Zero	BEQ rs, x0, offset	
BNEZ	Branch If Not Equal Zero	BNE rs, x0, offset	
BLEZ	Branch If Less Than or Equal Zero	BGE x0, rs, offset	
BGEZ	Branch If Greater Than or Equal Zero	BGE x0, rs, offset	
BLTZ	Branch If Less Than Zero	BLT rs, x0, offset	
BGTZ	Branch If Greater Than Zero	BLT x0, rs, offset
BGT	Branch If Greater Than	BLT rt, rs, offset	
BLE	Branch If Less Than or Equal	BGE rt, rs, offset	
BGTU	Branch If Greater Than, Unsigned	BLTU rt, rs, offset	
BLEU	Branch If Greater Than or Equal, Unsigned	BGEU rt, rs, offset	
*)
| Cmp of cmp * Regp.t * Regp.t * Int32.t (*Beq a b Imm => if (a=b) then (PC = PC + Imm) else  (PC = PC+4)*)
| Jal of Regp.t * Int32.t (*Jal a Imm => (a = PC+4); (PC = PC + Imm)*)
| Jalr of Regp.t * Regp.t * Int32.t (*Jalr a b Imm => (a = PC+4); (PC = b + Imm)*)
| Load of Memo.size * Binm.param_signe * Regp.t * Regp.t * Int32.t  (* Load (bystesize) a b c = load value at address b with offset c into register a, sign extend when loading size smaller than a word*)
| Store of Memo.size * Regp.t * Regp.t * Int32.t  (* Store (bytesize) a b c = store value of register a at address b with offset c*)
| LockCache of Mode.rewrite * Regp.t * Int32.t  (* LockCache rewrite_mode address  = try to load value at address into cache under LOCK protection with specific rewrite mode *)
| RocsCache of Regp.t   (* RocsCache rewrite_mode a  = try to load value at address a into cache under ROCS protection  *)
| LinkCache of Regp.t * Regp.t  (* LinkCache rewrite_mode a  = try to load value at address a into cache under LINK(c) protection *)
| UnlockCache of Regp.t * Int32.t  (* UnlockCache a b  = free the cache cell that was protected for the value at address b , erase it from cache at the same time. a=1 if success, a=0 if failed *)
| Ecall
| SCTM of Regp.t * Regp.t (*SCTM a b => (a=CT);(CT=b). Switch Constant Time Mode : Only instruction able to change or read CT (determines if in Constant Time mode, only accepts 0 or 1) *)
| End
| Bad_Instruction of string (*Can contains error message*)
(*label, branch conditionnel, requisition (begin-end) *)

let inst3_to_string (i3: inst3) =
  match i3 with
  | Sl -> "Shift Left"
  | Sra -> "Shift right arithmetic"
  | Srl -> "Shift right logical"
  | Add -> "Add"
  | Sub -> "Sub"
  | Mul -> "Mul"
  | Mulh -> "Mul high signedend"
  | Mulhsu -> "Mul high"
  | Mulhu -> "Mul high unsigned"
  | Div -> "Div"
  | Divu -> "Div unsigned"
  | Rem -> "Rem"
  | Remu -> "Rem unsigned"
  | Slt -> "Slt"
  | Sltu -> "Slt unsigned"
  | Or -> "Or"
  | And -> "And"
  | Xor -> "Xor"


let inst3_to_infix_string (i3: inst3) =
  match i3 with
  | Sl -> "<<"
  | Sra -> ">>a"
  | Srl -> ">>l"
  | Add -> "+"
  | Sub -> "-"
  | Mul -> "*"
  | Mulh -> "*h"
  | Mulhsu -> "*hsu"
  | Mulhu -> "*hu"
  | Div -> "/"
  | Divu -> "/u"
  | Rem -> "%"
  | Remu -> "%u"
  | Slt -> "<"
  | Sltu -> "<u"
  | Or -> "|"
  | And -> "&"
  | Xor -> "^"

let inst2i_to_string (i2: inst2i) =
  match i2 with
  | Sli -> "Shift left immediate"
  | Srai -> "Shift right arithmetic immediate"
  | Srli -> "Shift right logical immediate"
  | Addi -> "Add immediate"
  | Subi -> "Sub immediate"
  | Slti -> "Slt immediate"
  | Sltiu -> "Slt immediate unsigned"
  | Ori -> "Or immediate"
  | Andi -> "And immediate"
  | Xori -> "Xor immediate"
let inst2i_to_infix_string (i2: inst2i) =
  match i2 with
  | Sli -> "<<"
  | Srai -> ">>a"
  | Srli -> ">>l"
  | Addi -> "+"
  | Subi -> "-"
  | Slti -> "<"
  | Sltiu -> "<u"
  | Ori -> "|"
  | Andi -> "&"
  | Xori -> "^"

let cmp_to_string = function
    Beq -> "Beq"
  | Bne -> "Bne"
  | Blt -> "Blt"
  | Bltu -> "Bltu"
  | Bge -> "Bge"
  | Bgeu -> "Bgeu"


let cmp_to_infix_string = function
    Beq -> "=="
  | Bne -> "!="
  | Blt -> "<"
  | Bltu -> "<u"
  | Bge -> ">="
  | Bgeu -> ">=u"

let to_string (instruction : t) : string =
  match instruction with
  | Imm (res, immediate) -> "Imm: " ^ Regp.to_string_reduced res^ " <- " ^ Int32.to_string immediate
  | Lui (res, immediate) -> "Lui: " ^ Regp.to_string_reduced res^ " <- " ^ Int32.to_string immediate 
  | Auipc (res, immediate) -> "Auipc: " ^ Regp.to_string_reduced res^ " <- " ^ Int32.to_string immediate 
  | Mv (res, op) -> "Move: " ^ Regp.to_string_reduced res^ " <- " ^ Regp.to_string_reduced op
  | Inst3 (i3,res, op1, op2) ->
    Printf.sprintf "%s: %s <- %s %s %s"
      (inst3_to_string i3)
      (Regp.to_string_reduced res)
      (Regp.to_string_reduced op1) (inst3_to_infix_string i3) (Regp.to_string_reduced op2)
  | Inst2i (i2,res, op1, immediate) ->
    Printf.sprintf "%s: %s <- %s %s %ld"
      (inst2i_to_string i2)
      (Regp.to_string_reduced res) (Regp.to_string_reduced op1) (inst2i_to_infix_string i2) immediate
  | Not (res, op) -> "Not: " ^ Regp.to_string_reduced res^ " <- ~ " ^ Regp.to_string_reduced op 
  | Cmp (cmp, op1, op2, immediate) ->
    Printf.sprintf "%s: if ( %s %s %s ) then (PC+=%ld) else (PC++)"
      (cmp_to_string cmp)
      (Regp.to_string_reduced op1)
      (cmp_to_infix_string cmp) (Regp.to_string_reduced op2) immediate
  | Jal (res, immediate) -> "Jal: " ^ Regp.to_string_reduced res ^ " <- PC; PC+=" ^ Int32.to_string immediate ^  " \n"
  | Jalr  (res, address, immediate) -> "Jalr: " ^ Regp.to_string_reduced res ^ " <- PC; PC = " ^ Regp.to_string_reduced address ^ " + " ^ Int32.to_string immediate 
  | Load (size, signed, res, op, offset) -> "Load " ^ (Memo.size_to_string size) ^ (if(signed = Binm.Unsigned) then (" Unsigned") else ("")) ^ " : " ^ Regp.to_string_reduced res^ " <- "^ Int32.to_string offset ^"[" ^ Regp.to_string_reduced op ^ "]"
  | Store (size, new_value, target_address, offset) -> "Store " ^ (Memo.size_to_string size) ^ " : " ^ Int32.to_string offset ^"[" ^ Regp.to_string_reduced target_address^ "] <- " ^ Regp.to_string_reduced new_value 
  | LockCache (mode, op, offset) -> "LOCK "^ Int32.to_string offset ^"[" ^ Regp.to_string_reduced op ^ "] with " ^ Mode.rewrite_to_string mode 
  | RocsCache (op1) -> "ROCS [" ^ Regp.to_string_reduced op1 ^"]\n"
  | LinkCache (op1, op2) -> "LINK [" ^ Regp.to_string_reduced op1 ^ "] in set " ^ Regp.to_string_reduced op2 
  | UnlockCache (op, offset) -> "UNLOCK "^ Int32.to_string offset ^"[" ^ Regp.to_string_reduced op ^ "]"
  | Ecall -> "Ecall."
  | SCTM (res, op) -> "SCTM: " ^ Regp.to_string_reduced res^ " <- CT; CT <- " ^ Regp.to_string_reduced op 
  | End -> "End."
  | Bad_Instruction(message) -> "Bad instruction [" ^ message ^ "]"




let to_class (instruction : t) : Instclass.t =
  match instruction with
  | Mv (_, _)
  | Imm (_,_)
  | Lui (_,_)
  | Auipc (_,_)
  | Not (_,_) -> ALU_only

  | Inst3 (Mul,_,_,_) -> Multiplication_low
  | Inst3 ((Mulh|Mulhu|Mulhsu),_,_,_) -> Multiplication_high
  | Inst3 ((Div|Divu|Rem|Remu), _,_,_) -> Division
  | Inst3 _ | Inst2i _ -> ALU_only

  | Cmp (_, _,_,_)
  | Jal (_,_)
  | Jalr  (_,_,_) -> Jump

  | Load (_, _, _,_,_) -> Load
  | Store (_, _, _, _) -> Store
  | LockCache (_,_,_) -> Lock
  | RocsCache (_) -> Rocs
  | LinkCache (_, _) -> Link
  | UnlockCache (_, _) -> Unlock
  | Ecall -> Ecall
  | End -> Ecall
  | Bad_Instruction(_) -> Ecall
  | SCTM (_, _) -> SCTM

let error_of_bad_instruction (instr : t) : (string option) =
  match instr with
  | Bad_Instruction (message) -> Some (message)
  | _ -> None


let is_updating_memory (instruction : t) : bool =
  match instruction with
  | Ecall -> true
  | Load (_,_,_,_,_) -> true
  | Store (_,_,_,_) -> true
  | LockCache (_,_,_) -> true
  | RocsCache (_) -> true
  | LinkCache (_,_) -> true
  | UnlockCache (_,_) -> true
  | _ -> false



let get_address_as_int64 (base : Int32.t) (offset : Int32.t) : Int64.t =
    Int64.add (Binm.int32_as_positive_int64 base) (Int64.of_int32 offset)
      

let get_accessed_address (instruction : t) (registers : Regp.r_list) : Int64.t option =
  let address_and_offset =
    match instruction with
    | Ecall -> Some(Regp.build 2, 0l) (*Display memory around stack pointer (x2) at the end of execution*)
    | Load (_,_,_,address_reg, offset) -> Some(address_reg, offset)
    | Store (_,_,address_reg, offset) -> Some(address_reg, offset)
    | LockCache (_,address_reg, offset) -> Some(address_reg, offset)
    | RocsCache (_) -> raise(Failure("TODO RocsCache in updating_memory_on_address"))
    | LinkCache (_,_) -> raise(Failure("TODO LinkCache in updating_memory_on_address"))
    | UnlockCache (address_reg, offset) -> Some(address_reg, offset)
    | _ -> None
  in
  match address_and_offset with 
  | None -> None
  | Some (address_reg, offset) -> Some(get_address_as_int64 (Regp.read registers address_reg) offset)

  
  

(*#### INSTRUCTION PROCESSING*)  

type reg_or_imm = 
  | Register of Regp.t
  | Immediate of Int32.t

let read_reg_or_imm (operand : reg_or_imm) (register_list : Regp.r_list) : Int32.t =
  match operand with
  | Register(r) -> (Regp.read register_list r)
  | Immediate(i) -> i
    (*
      match signed with
      | Binm.Unsigned -> Binm.encode_signed_int i Regp.nb_bits_in_reg
      | Binm.Signed -> i
    *)
  
let process_move (destination_register : Regp.t) (op : reg_or_imm) (register_list : Regp.r_list) : (Event.t *(Regp.r_list)) =
  let value = read_reg_or_imm op register_list in
  Event.alu_leak,
  Regp.write (register_list) (destination_register) (value)
  (*
     The instruction contains a signed number which must be converted when put in register
     add_processe.g. Imm (regP 1,-1) will put value 0b11111111 in the register 1, Imm (regP 1,-2) will put value 0b11111110 in the register 1, etc.*)


(*Used for LUI and AUIPC instructions*)
let upper_immediate (input : Int32.t) : Int32.t =
  Int32.shift_left input 12

let process_load_upper_immediate (destination_register : Regp.t) (op : Int32.t) (register_list : Regp.r_list) : (Event.t *( Regp.r_list)) =
  (*let value = op * (Binm.power 2 (2*Binm.byte_size)) in*)
  Event.alu_leak,
  Regp.write (register_list) (destination_register) (upper_immediate op)
  (*The instruction contains a signed number which must be converted when put in register
  e.g. Imm (regP 1,-1) will put value 0b11111111 in the register 1, Imm (regP 1,-2) will put value 0b11111110 in the register 1, etc.*)

let do_alu_operation (operation : (Int32.t -> Int32.t -> Int32.t)) (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t *(Regp.r_list)) =
  let (x,y) =
  (   (*operands are signed for all arithmetic operations*)
    (Regp.read register_list op1),
    read_reg_or_imm op2 register_list
  )
  in
  
  let _debug =
    if (Conf.show_debug)
    then(
      print_endline (
        "process op "^ Int32.to_string x ^ " # "^ Int32.to_string y ^ " = "^ Int32.to_string (operation (x) (y)) ^ ".\n"
      )
    )
    else ()
  in
  Event.alu_leak,
  (Regp.write
    (register_list)
    (destination_register) (*store result in destination registes*)
    (operation (x) (y)) (*apply the arithmetic operation*)
  )

  

let do_bitwise_operation (operation : (bool -> bool -> bool)) (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t *(Regp.r_list)) =
  let (x,y) =
  (   (*operands are signed for all arithmetic operations*)
    (Regp.read register_list op1),
    read_reg_or_imm op2 register_list
  )
  in
  let res = (Binm.bitwise_operation (operation) x y) in (*apply the bitwise operation*)
  let _debug =
    if (Conf.show_debug)
    then(
      print_endline (
        "process bitwise op "^ Int32.to_string x ^ " # "^ Int32.to_string y ^ " = "^ Int32.to_string res ^ ".\n"
      )
    )
    else ()
  in
  Event.alu_leak,
  (Regp.write
    (register_list)
    (destination_register) (*store result in destination registes*)
    res
  )



(*
let do_M_extension_arithmetic_operation (operation : (Int32.t -> Int32.t -> Int32.t))
                                        (destination_register : Regp.t) (_res_signed : Binm.param_signe)
                                        (op1 : Regp.t) (_op1_signed : Binm.param_signe)
                                        (op2 : Regp.t) (_op2_signed : Binm.param_signe)
                                        (register_list : Regp.r_list) : (Regp.r_list) =
  let (x,y) =
  (
    (Regp.read register_list op1),
    (Regp.read register_list op2)
  )
  in
  let _debug =
    if (Conf.show_debug)
    then(
      print_endline (
        "process M-extension op "^ Int32.to_string x ^ " # "^ Int32.to_string y ^ " = "^ Int32.to_string (operation (x) (y)) ^ ".\n"
      )
    )
    else ()
  in
  let _ = raise(Failure("do not support unsigned mult and div")) in
  (Regp.write
    (register_list)
    (destination_register) (*store result in destination registes*)
    (operation (x) (y)) (*apply the arithmetic operation*)
    (*Binm.Unsigned*)
  )
*)

let process_shift_left (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t *(Regp.r_list)) =
  (do_alu_operation (fun x y -> Int32.shift_left x (Int32.to_int y)) (destination_register) (op1) (op2) (register_list))


let process_shift_right_arithmetic (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t *(Regp.r_list)) =
  (do_alu_operation (fun x y -> Int32.shift_right x (Int32.to_int y)) (destination_register) (op1) (op2) (register_list))


let process_shift_right_logical (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (fun x y -> Int32.shift_right_logical x (Int32.to_int y)) (destination_register) (op1) (op2) (register_list))
  
            
let process_addition (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (Int32.add) (destination_register) (op1) (op2) (register_list))


let process_subtraction (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (Int32.sub) (destination_register) (op1) (op2) (register_list))

(*
let process_multiplication (part : Binm.half) (destination_register : Regp.t)
                            (op1 : Regp.t) (op1_signed : Binm.param_signe)
                            (op2 : Regp.t) (op2_signed : Binm.param_signe)
                            (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  let _res_is_signed = match op1_signed, op2_signed with
    | Binm.Unsigned, Binm.Unsigned -> Binm.Unsigned
    | _, _ -> Binm.Signed 
  in
  Event.arithmetic_leak, (*multplication is constant time on the studied hardware*)
  (do_M_extension_arithmetic_operation (Binm.mult part) (destination_register) Binm.Unsigned (op1) op1_signed (op2) op2_signed (register_list))
  *)

let process_multiplication_low (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  Event.add_hw_event (Event.build Evsw.Bullet Multiplication_low) (Evhw.Artihmetic_op), (*multiplication do not only use ALU, can takes more cycles than ALU only operation*)
  snd  (do_alu_operation (Int32.mul) (destination_register) (op1) (op2) (register_list))





let process_multiplication_high (destination_register : Regp.t) (signed1 : Binm.param_signe) (op1 : Regp.t) (signed2 : Binm.param_signe) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  let to_int64_sign (signed : Binm.param_signe) (little : Int32.t)  : Int64.t =
    match signed with
    | Binm.Signed -> Int64.of_int32 little
    | Binm.Unsigned -> raise(Failure("Unsigned Mulh not done yet"))
  in

  let mulhigh (a : Int32.t) (b : Int32.t) : Int32.t =
    let res = Int64.mul (to_int64_sign signed1 a) (to_int64_sign signed2 b) in
    Int64.to_int32 (Int64.shift_right res 32) (*keep the high bits of the result*)
  in

  Event.add_hw_event (Event.build Evsw.Bullet Multiplication_low) (Evhw.Artihmetic_op), (*multiplication do not only use ALU, can takes more cycles than ALU only operation*)
  snd  (do_alu_operation (mulhigh) (destination_register) (op1) (op2) (register_list))

  




let division_leak (value : reg_or_imm) (*signed : Binm.param_signe*) (register_list : Regp.r_list) (ct_mode : bool) : Event.t =
  if(ct_mode)
  then ( Event.add_hw_event (Event.build Evsw.Bullet Division) (Evhw.Artihmetic_op) )
  else
  (
    let leaking_log = (Binm.log (2) (Int32.to_int (read_reg_or_imm value register_list)) (*read_reg_or_imm operand register_list*)) in
    Event.add_hw_event (Event.build(Evsw.Div(leaking_log)) Division) (Evhw.Div(leaking_log))
  )

let process_division (signed : Binm.param_signe) (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) (ct_mode : bool) : (Event.t * (Regp.r_list)) =
  let operator =
    match signed with
    | Binm.Signed -> Int32.div
    | Binm.Unsigned -> Int32.unsigned_div
  in

  division_leak op2 register_list ct_mode, (*division can produce more leakage than classic arith op dependinc of ct mode*)
  snd (do_alu_operation (operator) (destination_register) (op1) (op2) (register_list))

    (*do_M_extension_arithmetic_operation ( Int32.div ) (destination_register) signed (op1) signed (op2) signed (register_list)*)

let process_remainder (signed : Binm.param_signe) (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) (ct_mode : bool) : (Event.t * (Regp.r_list)) =
  let operator =
    match signed with
    | Binm.Signed -> Int32.rem
    | Binm.Unsigned -> Int32.unsigned_rem
  in
  
  division_leak op2 register_list ct_mode,
  snd (do_alu_operation (operator) (destination_register) (op1) (op2) (register_list))

    (*do_M_extension_arithmetic_operation ( Int32.rem ) (destination_register) signed (op1) signed (op2) signed (register_list)*)


let process_lower_than (signed : Binm.param_signe) (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  let comparator =
    match signed with
    | Binm.Signed -> Int32.compare
    | Binm.Unsigned -> Int32.unsigned_compare
  in
  do_alu_operation (fun x y -> if ((comparator x y)<0) then 1l else 0l) (destination_register) (op1) (op2) (register_list)


let process_bitwise_and (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (Int32.logand) (destination_register) (op1) (op2) (register_list))


let process_bitwise_or (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (Int32.logor) (destination_register) (op1) (op2) (register_list))

  
let process_bitwise_xor (destination_register : Regp.t) (op1 : Regp.t) (op2 : reg_or_imm) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (Int32.logxor) (destination_register) (op1) (op2) (register_list))


let process_bitwise_not (destination_register : Regp.t) (op : Regp.t) (register_list : Regp.r_list) : (Event.t * (Regp.r_list)) =
  (do_alu_operation (fun (x) (_y) -> (Int32.lognot x)) (destination_register) (op) (Register op) (register_list)) (*not takes one argument, second argument useless*)

let process_inst3 i3 destination_register op1 op2 register_list ct_mode =
match i3 with
| Sl     -> process_shift_left destination_register op1 (Register op2) register_list
| Srl    -> process_shift_right_logical destination_register op1 (Register op2) register_list
| Sra    -> process_shift_right_arithmetic destination_register op1 (Register op2) register_list
| Add    -> process_addition destination_register op1 (Register op2) register_list
| Sub    -> process_subtraction destination_register op1 (Register op2) register_list
| Mul    -> process_multiplication_low destination_register op1 (Register op2) register_list
| Mulh   -> process_multiplication_high destination_register Binm.Signed   op1   Binm.Signed   (Register op2) register_list
| Mulhsu -> process_multiplication_high destination_register Binm.Signed   op1   Binm.Unsigned (Register op2) register_list
| Mulhu  -> process_multiplication_high destination_register Binm.Unsigned op1   Binm.Unsigned (Register op2) register_list
| Div    -> process_division Binm.Signed destination_register op1 (Register op2) register_list (ct_mode)
| Divu   -> process_division Binm.Unsigned destination_register op1 (Register op2) register_list (ct_mode)
| Rem    -> process_remainder Binm.Signed destination_register op1 (Register op2) register_list (ct_mode)
| Remu   -> process_remainder Binm.Unsigned destination_register op1 (Register op2) register_list (ct_mode)
| Slt    -> process_lower_than Binm.Signed destination_register op1 (Register op2) register_list

| Sltu   -> process_lower_than Binm.Unsigned destination_register op1 (Register op2) register_list
| And    -> process_bitwise_and destination_register op1 (Register op2) register_list
| Or     -> process_bitwise_or destination_register op1 (Register op2) register_list
| Xor    -> process_bitwise_xor destination_register op1 (Register op2) register_list

let process_inst2i i2 destination_register op1 op2 register_list =
  match i2 with
| Sli    -> process_shift_left destination_register op1 (Immediate op2) register_list
| Srli   -> process_shift_right_logical destination_register op1 (Immediate op2) register_list
| Srai   -> process_shift_right_arithmetic destination_register op1 (Immediate op2) register_list
| Addi   -> process_addition destination_register op1 (Immediate op2) register_list
| Subi   -> process_subtraction destination_register op1 (Immediate op2) register_list
| Slti   -> process_lower_than Binm.Signed destination_register op1 (Immediate op2) register_list
| Sltiu  -> process_lower_than Binm.Unsigned destination_register op1 (Immediate op2) register_list
| Andi   -> process_bitwise_and destination_register op1 (Immediate op2) register_list
| Ori    -> process_bitwise_or destination_register op1 (Immediate op2) register_list
| Xori   -> process_bitwise_xor destination_register op1 (Immediate op2) register_list


let process_instruction_register_only (instruction : t) (register_list : Regp.r_list) (ct_mode : bool) : (Event.t * (Regp.r_list)) =
  match instruction with
  | Inst3 (i3, destination_register, op1, op2)    -> process_inst3 i3 destination_register op1 op2 register_list ct_mode
  | Inst2i (i2, destination_register, op1, op2)    -> process_inst2i i2 destination_register op1 op2 register_list

  | Not (destination_register, op)         -> process_bitwise_not destination_register op register_list
  | Mv (destination_register, op)          -> process_move destination_register (Register op) register_list
  | Imm (destination_register, immediate)  -> process_move destination_register (Immediate immediate) register_list
  | Lui (destination_register, immediate)  -> process_load_upper_immediate destination_register (immediate) register_list
  | Load ( _size , _signed ,_destination_register, _address, _offset) -> failwith "Cannot Process Load" (*load doit etre traite a part*)
  | LockCache (_mode, _address, _offset) -> failwith "Cannot Process LOCK" (*ProtectCache doit etre traite a part*)
  | RocsCache (_address) -> failwith "Cannot Process ROCS" (*ProtectCache doit etre traite a part*)
  | LinkCache (_address, _set) -> failwith "Cannot Process LINK" (*ProtectCache doit etre traite a part*)
  | UnlockCache (_address, _offset) -> failwith "Cannot Process UnlockCache" (*UnlockCache doit etre traite a part*)
  | Store  (_size, _new_value, _target_address, _offset) -> failwith "Cannot Process Store" (*store doit etre traite a part*)
  | End -> failwith "Cannot Process End" (*End doit etre traite par run*)
  | Bad_Instruction(message) -> failwith ("Bad Instruction [" ^message ^"]")
  | _ -> failwith "Insruction not supported yet"



let pc_progress = Int32.of_int (Memo.nb_bytes Memo.Word)


let process_swap_ct_mode (destination_register : Regp.t) (op1_register : Regp.t) (sys : Syst.t ) : (Event.t * Syst.t) =
  let new_CT = Regp.read sys.registers (op1_register) in
  Event.sctm_leak new_CT,
  {sys with
   pc = (Int32.add sys.pc pc_progress);
   ct = (if new_CT=0l then false else if new_CT = 1l then true else sys.ct);
   registers = Regp.write (sys.registers) (destination_register) (if sys.ct then 1l else 0l)
  }

let compare (operation : (Int32.t->Int32.t->bool)) (op1_register : Regp.t) (op2_register : Regp.t) (register_list : Regp.r_list): bool =
  (operation)
    (Regp.read register_list op1_register)
    (Regp.read register_list op2_register)
  

let jump_relative (sys : Syst.t) (pc_offset : Int32.t) (choice : Evhw.branch_choice) : (Event.t * Syst.t) =
  let new_pc = Int32.add sys.pc pc_offset in 
  (
    Event.add_hw_event (Event.build(Evsw.Jump(Int32.to_int new_pc)) Jump) (Evhw.Jump(Int32.to_int new_pc, choice)) 
  ),
  {sys with
    pc = new_pc; (*Addresses always unsigned*)
    l1i = sys.l1i (*TODO update cache d'instruction apres jump*)
  }

let process_branch  (sys : Syst.t) (comparator : (Int32.t->Int32.t->bool)) (op1_register : Regp.t) (op2_register : Regp.t) (pc_offset : Int32.t) : (Event.t * Syst.t) =
  let ofs,branch_choice = if compare comparator op1_register op2_register sys.registers
    then pc_offset, Evhw.Taken else pc_progress, Evhw.Not_taken in
  jump_relative sys ofs branch_choice

let process_jal (sys : Syst.t) (save_PC_reg : Regp.t) (pc_offset : Int32.t) : (Event.t * Syst.t) =
  (jump_relative 
    ({sys with registers =
      (Regp.write
        (sys.registers)
        (save_PC_reg)
        (Int32.add sys.pc pc_progress)
      );
    })
    (pc_offset)
    Evhw.Taken (*No condition on jump, always take the branch*)
  )


let process_auipc (sys : Syst.t) (save_PC_reg : Regp.t) (pc_offset : Int32.t) : (Event.t * Syst.t) =
  Event.add_hw_event (Event.build Evsw.Bullet Instclass.ALU_only) (Evhw.Artihmetic_op),
  {sys with
    pc = Int32.add sys.pc pc_progress;
    registers =
      (Regp.write
        (sys.registers)
        (save_PC_reg)
        (Int32.add sys.pc (upper_immediate pc_offset))
      );
  }


let process_jalr (sys : Syst.t) (save_PC_reg : Regp.t) (new_PC_reg : Regp.t) (pc_offset : Int32.t) : (Event.t * Syst.t) =
  let new_pc = (Int32.add (Regp.read sys.registers new_PC_reg) pc_offset) in
  Event.add_hw_event (Event.build(Evsw.Jump(Int32.to_int new_pc)) Jump) (Evhw.Jump(Int32.to_int new_pc, Evhw.Taken)),  (*No condition on jump, always take the branch*)
  {sys with
    pc = new_pc; (*Addresses always unsigned*)
    registers =
      (Regp.write
        (sys.registers)
        (save_PC_reg)
        (Int32.add sys.pc pc_progress)
      );
    l1i = sys.l1i (*TODO update cache d'instruction apres jump*)
  }


let process_cache_miss (sys : Syst.t) (address : Int64.t) (size : Memo.size) (ev : Event.t) (write_value : Int32.t option) : (Event.t * Syst.t) =
  let (_tag,set,_word_id) = Cach.split_address_into_tag_set_wordid address (sys.l1d) in
  let updated_ram =
    match write_value with
    | None -> sys.ram (*No update to do in RAM if no value to write*)
    | Some(value) ->  (Memo.write sys.ram address value size) (*Store instruction*)
  in
  match sys.configuration.write_miss_policy with
  | Write_around -> ev, {sys with ram = updated_ram}
  | Write_allocation -> 
      let fetched_line = (Memo.fetch_data_line_from_RAM sys.ram address) in
      match Cach.choose_way_to_evict (sys.configuration) (sys.l1d) (address) (false) with
      | _new_conf, None ->
        failwith("CACHE EVICTION FAIL, no evictable way found in set "^ string_of_int set ^" \n" ^
        ( Cach.string_of_cache_set (Cach.cache_set_of_address sys.l1d address) (set) ) ^ "\n;")
        (*(Event.add_hw_event ev Evhw.No_evictable_way),
        {sys with configuration = new_conf; ram = updated_ram}*) (*No evictable way in this cache set => no update to do*)

      | new_conf, Some(way_id, is_dirty) ->
        let cache_with_fetched_line = Cach.replace_cache_line (sys.l1d) (address) (way_id) (Prot.Unprotected) (fetched_line) in
        let updated_cache = 
          match write_value with
          | None -> cache_with_fetched_line (*no value to write in the cache line*)
          | Some(value) ->  Cach.update_on_store_miss (cache_with_fetched_line) (address) (value) (sys.configuration.write_policy) (way_id) (size) (*Store instruction*)
        in
        match sys.configuration.write_policy with
        | Write_through -> ev, {sys with
                                  configuration = new_conf; (*updates pseudo random eviction seed (required for deterministic evictions)*)
                                  l1d = updated_cache;
                                  ram = updated_ram}
        | Write_back -> (*The write_value is not put in RAM*)
          let wroteback_ram =
            if(is_dirty) (*dirty_bit=true of evicted line means its content must be wrote back in ram before being erased*)
            then (Cach.write_back_to_ram (sys.ram) (sys.l1d) (address) (way_id))
            else (sys.ram)
          in
          let new_ev = 
            if(is_dirty)  (*add Write_back event in hardware events*)
            then (Event.add_hw_event ev Evhw.Write_back)
            else (ev)
          in
          new_ev, {sys with
                    configuration = new_conf;
                    l1d = updated_cache;
                    ram = wroteback_ram}




(*Write back policy -> Write in cache when possible and set dirty bit to True*)
let process_store (sys : Syst.t) (new_value_reg : Regp.t) (target_address_reg : Regp.t) (offset : Int32.t)  (size : Memo.size) : (Event.t * Syst.t) =
  let address = get_address_as_int64 (Regp.read (sys.registers) (target_address_reg)) offset in 
  let (tag,set,_word_id) = Cach.split_address_into_tag_set_wordid address (sys.l1d) in

  

  Os.address_allowed sys.os address size (" caused by a Store instruction at address "^ Int64.to_string address ^" (From reg "^Regp.to_string_full (target_address_reg) (sys.registers) ^") during process " ^ string_of_int sys.os.current_pid);
  let value = Regp.read_lower_bytes sys.registers new_value_reg size in

  let print =
    if(address = Memo.print_address)
    then (
      let converted_to_ascii = Disp.get_printable_string_from_int32 value in
      let _display = print_endline ("Print " ^ converted_to_ascii) in
      Some(converted_to_ascii)
    )
    else (None)
  in
  let res_ev, res_sys =
    match Cach.get_line_with_tag (sys.l1d) (address) with
    | None -> (*Cache MISS*)
      let miss_event = 
        Event.set_print
          (Event.add_hw_event
            (Event.build(Evsw.Mem_access(set, tag)) Store)
            (Evhw.Cache_miss)
          )
          print
      in 
      process_cache_miss ({sys with pc = (Int32.add sys.pc  pc_progress)}) (address) (size) (miss_event) (Some value)
      

    |  Some (way,cacheLine) -> (*Cache HIT*)
      let hit_event =
        Event.set_print 
          (Event.add_hw_event 
          (match (Line.get_protection cacheLine) with
            | Lock(_) -> Event.build(Evsw.Protected_mem_access(set, tag)) Store
            | Unprotected -> Event.build(Evsw.Mem_access(set, tag)) Store
            | unsupported_protection -> raise(Failure("Protection " ^ Prot.to_string unsupported_protection ^ " not supported yet for load."))
          )
          (Evhw.Cache_hit))
        print
      in
      let wb_sys = {sys with
          pc = (Int32.add sys.pc pc_progress);
          l1d =Cach.update_on_store (sys.l1d) (address) (value) (sys.configuration.write_policy) (cacheLine) (way) (size)
        } 
      in
      match sys.configuration.write_policy with
      | Write_back -> hit_event,wb_sys
      | Write_through -> hit_event,{wb_sys with ram = (Memo.write sys.ram address value size)} 
    in
    res_ev, res_sys




let process_load (sys : Syst.t) (destination_reg : Regp.t) (address_reg : Regp.t)  (offset : Int32.t) (size : Memo.size) (sign_extend : Binm.param_signe): (Event.t * Syst.t) =
  let address = get_address_as_int64 (Regp.read (sys.registers) (address_reg)) offset in
  let (tag,set,_word_id) = Cach.split_address_into_tag_set_wordid address (sys.l1d) in

  Os.address_allowed sys.os address size
    (Printf.sprintf "Load on illegal address 0x%Lx during process %d, pc=0x%lx" address sys.os.current_pid sys.pc);

  let _set_id, result, updatedSys, final_leak =
    (match Cach.get_line_with_tag (sys.l1d) (address) with
      | Some (way,cacheLine) -> (*cache hit*)
          let store_leak =
            match (Line.get_protection cacheLine) with (*facto avec code de process_store*)
            | Lock(_) -> Event.build(Evsw.Protected_mem_access(set, tag)) Load
            | Unprotected -> Event.build(Evsw.Mem_access(set, tag)) Load
            | unsupported_protection -> raise(Failure("Protection " ^ Prot.to_string unsupported_protection ^ " not supported yet for load."))
          in
          (*let _tempo_print = print_endline ("\n\n=============\nCache hit on line : " ^ Line.to_string cacheLine ^
            "\n  at word id " ^ string_of_int (Binm.map_address_to_word_id address) ^
            " which give " ^ string_of_int (Line.read cacheLine address size Binm.Unsigned) ^
            "\n COrresponding value in ram :" ^ string_of_int (Memo.read sys.ram address size Binm.Unsigned))
          in*)
          let updated_sys = match Cach.update_cache_usage (sys.l1d) (address) (way) with
            | None -> sys
            | Some(updated_cache) -> {sys with l1d = updated_cache}
          in
          set,
          (Line.read cacheLine address size sign_extend),
          updated_sys, (*Update usage of accessed cache line*)
          (Event.add_hw_event (store_leak) (Evhw.Cache_hit))
      | None -> (*cache miss*)
        let fetched_line = (Memo.fetch_data_line_from_RAM sys.ram address) in
        let updated_sys, leak_after_write_back = 
          match Cach.choose_way_to_evict (sys.configuration) (sys.l1d) (address) (false) with
          |new_conf, None ->
            {sys with configuration = new_conf}, (*No evictable way in this cache set => no update to do*)
            (Event.add_hw_event
              (Event.build(Evsw.Mem_access(set, tag)) Load)
              Evhw.No_evictable_way
            )
          |new_conf, Some(way_id, is_dirty) ->
            let updated_cache = Cach.replace_cache_line (sys.l1d) (address) (way_id) (Prot.Unprotected) (fetched_line) in
            if(is_dirty) (*dirty_bit=true of evicted line means its content must be wrote back in ram before being erased*)
            then
            (
              {sys with
                configuration = new_conf;
                ram = Cach.write_back_to_ram (sys.ram) (sys.l1d) (address) (way_id);
                l1d = updated_cache;
              },
              (Event.add_hw_event
                (Event.build(Evsw.Mem_access(set, tag)) Load)
                Evhw.Write_back
              )
            )
            else
            (
              {sys with
                configuration = new_conf;
                l1d = updated_cache;
              },
              (Event.build(Evsw.Mem_access(set, tag)) Load)
            )
        in
        set,
        (Memo.read fetched_line address (*Binm.map_address_to_word_id address*)(sign_extend) (size) ),
        updated_sys,
        (Event.add_hw_event
          leak_after_write_back
          Evhw.Cache_miss
        )
        
    )
  in (
    final_leak,
    {updatedSys with 
      pc = (Int32.add sys.pc pc_progress);
      registers = (Regp.write (sys.registers) (destination_reg) (result))
    }
  )


let process_lock (sys : Syst.t) (mode : Mode.rewrite) (address_reg : Regp.t) (offset : Int32.t) : (Event.t * Syst.t) =
  let address = get_address_as_int64 (Regp.read (sys.registers) (address_reg)) offset in
  let (tag,set,_word_id) = Cach.split_address_into_tag_set_wordid address (sys.l1d) in
  let _test_memory_permission =
    (Os.address_allowed (sys.os) (address) (Memo.Word) ("Lock on illegal address " ^ Int64.to_string address ^ " during process " ^ string_of_int sys.os.current_pid))
  in
  let _test_address_alignement =
    (Conf.valid_address_alignement (sys.configuration) (address))
  in
  (*check if line to protect already in cache, otherwise, evict way_to_evict and load line to protect in place, can request write back of evicted line*)
  let _set_id, hw_leak, way_id, updated_cache, need_write_back, new_conf  =
    match Cach.get_line_with_tag (sys.l1d) (address) with
    | Some (way_id,_line) -> set, Evhw.Cache_hit, way_id, sys.l1d, false(*the address to lock is already in cache*), (sys.configuration)(*no evict: no conf change*)
    | None -> (*the address to lock is not already in cache*)  
        (
          (*search what way could be evicted if line not in cache. Will throw exception if not enough unlocked lines in set*)
          let new_conf_after_evict, way_to_evict, way_to_evict_is_dirty = 
            (match Cach.choose_way_to_evict (sys.configuration) (sys.l1d) (address) (true) with 
            | _new_conf, None -> raise(Failure("Cannot lock in cache, all cache sets must always have at least one unlocked way")) (*No lockable way in this cache set*)
            | conf_after_evict, Some(way_id, is_dirty) -> conf_after_evict, way_id, is_dirty
            )
          in
          let fetched_line = Memo.fetch_data_line_from_RAM (sys.ram) (address) in
          let updated_cache = Cach.replace_cache_line (sys.l1d) (address) (way_to_evict) (Prot.Unprotected) (fetched_line) in
          set, Evhw.Cache_miss, way_to_evict, updated_cache, way_to_evict_is_dirty, new_conf_after_evict
        )
  in
  let locked_updated_cache =
    match (Cach.protect (updated_cache) (address) (way_id) (Prot.Lock(mode))) with
    (protected_cache, _unprotected_line_if_write_back_needed_before_protection) -> protected_cache
  in
  let event_without_write_back = (Event.add_hw_event (Event.build (Evsw.Lock(set,tag)) Lock) (hw_leak)) in 
  let event, updated_ram =
    if (need_write_back)
    then(
      (Event.add_hw_event (event_without_write_back) (Evhw.Write_back)),
      (Cach.write_back_to_ram (sys.ram) (sys.l1d) (address) (way_id))
    )
    else (event_without_write_back, sys.ram)
  in
  event,
  {sys with
    pc = (Int32.add sys.pc pc_progress);
    configuration = new_conf; (*Configuration contains the random seed that decide which way to evict for pseudo random eviction, must be updated after evictions*)
    ram = updated_ram;
    l1d = locked_updated_cache;
  }



let process_unlock (sys : Syst.t) (address_reg : Regp.t) (offset : Int32.t) : (Event.t * Syst.t) =
  let address = get_address_as_int64 (Regp.read (sys.registers) (address_reg)) offset in
  let _test_memory_permission =
    (Os.address_allowed (sys.os) (address) (Memo.Word) ("Unlock on illegal address " ^ Int64.to_string address ^ " during process " ^ string_of_int sys.os.current_pid))
  in
  let _test_address_alignement =
    (Conf.valid_address_alignement (sys.configuration) (address))
  in
  let result, leak, updated_cache, write_back_way = Cach.unlock_cache_line (sys.l1d) (address) in
  let _check_success =
    if (result = 0)
    then ()
    else (failwith("UnLock FAIL [" ^ (  Regp.to_string_full address_reg sys.registers ) ^ "];"))
  in
  let ram_after_write_back =
    match write_back_way with
    | None -> sys.ram
    | Some (way_id) -> Cach.write_back_to_ram (sys.ram) (sys.l1d) (address) (way_id);
  in
  leak,
  {sys with
    pc = (Int32.add sys.pc pc_progress);
    (*registers = (Regp.write (sys.registers) (output_reg) (output));*)
    l1d = updated_cache;
    ram = ram_after_write_back;
  }

let cmp_sem cmp x y =
  match cmp with
    Beq -> Int32.equal x y
  | Bne -> not (Int32.equal x y)
  | Blt -> x < y
  | Bge -> x >= y
  | Bltu -> Int32.unsigned_compare x y < 0
  | Bgeu -> Int32.unsigned_compare x y >= 0

let process_instruction (instruction: t) (sys : Syst.t) : ((Event.t * Syst.t) option) =
  let res : ((Event.t * Syst.t) option) =
    match instruction with
    | End -> let () =
               if(Conf.show_warning)
               then(print_endline "Using custom 'End' instruction to halt the simulation is deprecated, use Ecall instead")
               else()
      in None
    | Ecall -> Syst.handle_Ecall sys
    | Store (size, new_value, target_address, offset) -> Some(process_store (sys) (new_value) (target_address) (offset) (size))
    | Load (size, signed, destination_register, address_register, offset) -> Some(process_load (sys) (destination_register) (address_register) (offset) (size) (signed))
    | LockCache (mode, address_register, offset) ->  Some(process_lock (sys) (mode) (address_register) (offset))
    | RocsCache (_address) -> failwith "TODO Process ROCS"
    | LinkCache (_address, _set) -> failwith "TODO Process LINK"
    | UnlockCache (address_register, offset) -> Some(process_unlock (sys) (address_register) (offset))
    | Cmp (cmp, op1, op2, pc_offset) -> Some (process_branch sys (cmp_sem cmp) op1 op2 pc_offset)
    | Auipc (save_PC_reg, pc_offset)-> Some(process_auipc (sys) (save_PC_reg) (pc_offset))
    | Jal (save_PC_reg, pc_offset) -> Some(process_jal (sys) (save_PC_reg) (pc_offset))
    | Jalr (save_PC_reg, new_PC_reg, pc_offset) -> Some(process_jalr (sys) (save_PC_reg) (new_PC_reg) (pc_offset))
    | SCTM (previous_CT,new_CT) -> Some(process_swap_ct_mode previous_CT new_CT sys)
    | instr ->
      (match process_instruction_register_only (instr) (sys.registers) (sys.ct) with
         (trace, new_register_list) -> Some (trace,
                                             {sys with
                                              pc = (Int32.add sys.pc pc_progress);
                                              registers = new_register_list
                                             }
                                            )
      )
  in
  match res with (*check that the events are consistent*)
  | None -> None
  | Some (ev, new_sys) ->
    if(Event.is_consistent ev)
    then (res)
    else (
      print_endline(
        "\n Inconsistent events "^ Event.to_string ev ^
        "\n when executing instruction " ^ to_string instruction ^
        "\n while cache set state is " ^ Cach.to_string new_sys.l1d ^
        "\n Simulation stop because of event inconsistency.\n"
      );
      None)

