(*Branch choice is only used to determines hom many cycles is taken for the branch,
  but is not part of the actual instclass representation of branch instruction*)
  type branch_choice =
  | Taken (*Jump are considerd equivalent to a branch if TRUE, i.e. branch always taken*)
  | Not_taken (*for branch where condition are false*)

type t = 
  | Artihmetic_op
  | Div of int (*most significant bit of the diviser*)
  | Jump of int * branch_choice (*destination of jump. Will change in future implementation containing safe jump*)
  | Cache_miss
  | Cache_hit 
  | Write_back 
  | No_evictable_way
  | Context_Switch (*??*)
  | Swap_CT_mode of Int32.t (*new value of ct mode*)
  | Ecall of Int32.t

  
let to_string (elem : t) = 
  (
    match elem with
    | Artihmetic_op -> "Artihmetic_op"
    | Div(log) -> "Div(2^" ^ string_of_int log ^ ")"
    | Jump(target,Taken) -> "Jump_op(branch taken toward " ^ string_of_int target ^ ")"
    | Jump(target,Not_taken) -> "Jump_op(branch not taken, stay at " ^ string_of_int target ^ ")"
    | Cache_hit -> "Cache_hit"
    | Cache_miss -> "Cache_miss"
    | Write_back -> "Write_back"
    | No_evictable_way -> "No_evictable_way"
    | Context_Switch -> "Context_Switch"
    | Swap_CT_mode(target) -> (Printf.sprintf "Swap CT mode to %ld" target)
    | Ecall n -> Printf.sprintf "Ecall %ld" n
  )


let list_to_string (ev_list : t list) : string =
  (List.map to_string ev_list) |>
  (String.concat ",") |>
  (Printf.sprintf "{%s}")
