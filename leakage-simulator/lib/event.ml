
type t =
  {
    hardware_events : Evhw.t list; (*possibilit of multiple HW for one instruction.*)
    software_event : Evsw.t;
    instruction_class : Instclass.t;
    print : string option;
  }

let contains_hw_event (event :t) (searched_event : Evhw.t) : int =
  List.filter ((=) searched_event) event.hardware_events |> List.length

let to_string_hw (ev : t) : string =
  (Evhw.list_to_string ev.hardware_events) ^", "^
  (Instclass.to_string ev.instruction_class)


let to_string_sw (ev : t) : string =
  (Evsw.to_string ev.software_event)  ^", "^
  (Instclass.to_string ev.instruction_class)


let to_string (ev : t) : string =
  "HW events: " ^ (Evhw.list_to_string ev.hardware_events) ^ "\n"^
  "SW event: " ^ (Evsw.to_string ev.software_event) ^ "\n" ^
  Instclass.to_string ev.instruction_class ^ "\n" ^
  (match ev.print with
    | None -> ""
    | Some(text) -> "Print: " ^ text
  )


let build (sw_ev : Evsw.t) (inst_class : Instclass.t) : t =
  {
    hardware_events = [];
    software_event = sw_ev;
    instruction_class = inst_class;
    print = None
  }

(*Multiple leaks can be added on the same step*)
let add_hw_event (event : t) (hw_ev : Evhw.t): t =
  {event with
    hardware_events = List.append (event.hardware_events) (hw_ev :: []);
  }


let set_print (event : t) (text : string option): t =
  {event with
    print = text;
  }

let is_consistent (event : t) : bool =
  let is_cache_miss : bool = 0> (contains_hw_event event Evhw.Cache_miss) in
  match event.software_event with
  | Evsw.Protected_mem_access _ -> if is_cache_miss then false else true (*cache miss impossible for protected memory access*)
  | _ -> true

let alu_leak = {
  hardware_events = [Evhw.Artihmetic_op];
  software_event = Evsw.Bullet;
  instruction_class = Instclass.ALU_only;
  print = None
}

let ecall_leak num = {
  hardware_events = [Evhw.Ecall num];
  software_event = Evsw.Ecall num;
  instruction_class = Instclass.Ecall;
  print = None
}

let sctm_leak new_CT = {
  hardware_events = [Evhw.Swap_CT_mode new_CT];
  software_event = Evsw.Swap_CT_mode new_CT;
  instruction_class = SCTM;
  print = None
}


let cycle_count (ev : t) : int =
  let res:int =
  match ev.instruction_class with
  | ALU_only -> 1
  | Multiplication_low -> 1
  | Multiplication_high -> 5
  | Ecall -> 1
  | SCTM -> 1
  | Jump -> 1
  | Branch  ->
    (match ev.hardware_events with
    | Jump(_dest, Evhw.Taken)::[] -> 1 
    | Jump(_dest, Evhw.Not_taken)::[] -> 3
    | _ -> raise (Failure("Error while counting cycle. Incoherency between instruction class Division and "^ Evsw.to_string ev.software_event))
    )
  | Load
  | Store
  | Lock
  | Rocs
  | Link
  | Unlock -> 
    (let is_cache_miss : bool = 0> (contains_hw_event ev Evhw.Cache_miss) in
      if is_cache_miss then 36 else 4
    )
  | Division ->
    (match ev.hardware_events with
    | Div(leaking_log)::[] -> 3+ 32-leaking_log (* Dvisision prend 3-35 cycles : 3 + (32-bit_poid_fort) *) 
    | _ -> raise (Failure("Error while counting cycle. Incoherency between instruction class and "^ Evsw.to_string ev.software_event))
    )
  in
  res
