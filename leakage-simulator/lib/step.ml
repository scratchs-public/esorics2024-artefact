(*Trace lists are reversed : most recently produced trace will be at the head of the list*)
type t =
  {
    system_state : Syst.t;  (*State BEFORE the step*)
    instruction : Inst.t;  
    events : Event.t ;
  }

(**
  type Event.t = (SW_Event  * HW_Event list)

  HW_Event =
  | Div(log_diviseur : int)
  | Artihmetic_op
  | Jump_op
  | Cache_miss
  | Cache_hit 
  | Write_back 
  | ... autres choses qui prennent du temps 

  SW_Event = 
  | Bullet
  | Div(log_diviseur : int)
  | Mem_access(cache_set : int)
  | Lock (cache_set)
  | Free (cache_set)
  | ... autres protections sur cache

*)


let build (state : Syst.t) (new_instr : Inst.t) (leak : Event.t) : t =
  {
    system_state = state;  
    instruction = new_instr;
    events = leak;
  }

(*Multiple leaks can be added on the same step*)
let add_hw_leakage (step : t) (new_hw_leak : Evhw.t): t =
  {step with
    events = Event.add_hw_event (step.events) (new_hw_leak);
  }

(**
let rec to_string_inst_and_leak (trace : t) : string =
  match trace with 
  | [] -> "End of trace."
  | head :: tail -> head ^ "\n" ^ to_string_inst_and_leak tail 
*)

let fprint_inst_and_leak out (step : t) : unit =
  Printf.fprintf out 
    "\n==========\n";
  Printf.fprintf out "Instruction: %s\n" (Inst.to_string step.instruction);
  Printf.fprintf out "==========\n";
  Printf.fprintf out "Events: %s\n" (Event.to_string step.events)



let display_ram_around_address out (ram : Memo.t) (possible_address : Int64.t option) : unit =
  match possible_address with
  | None -> ()
  | Some(address) ->
    Printf.fprintf out "\n RAM around address %Lx:\n" address;
    Printf.fprintf out "%s"
      (Memo.to_string_as_ram ram (Int64.sub (Binm.align_address_on_byte address) 64L) (Int64.add (Binm.align_address_on_byte address) 64L))



(**First element of return tuple is the string output,
  second element says if the displayed instruction could have updated memory (caches or RAM -> Load,Store,CS,Protection)
  If display_state is on true, or if current instruction is updating memory : all the system state will be displayed
  otherwise : only the registers will be displayed
    *)
let fprint_adaptative out (step : t) (previously_accessed_memory : Int64.t option) : Int64.t option =
  let currently_accessed_memory =  Inst.get_accessed_address step.instruction step.system_state.registers in
  (
    Printf.fprintf out "%s" (Syst.to_string step.system_state);
    display_ram_around_address out step.system_state.ram previously_accessed_memory;
    display_ram_around_address out step.system_state.ram currently_accessed_memory;
    (match previously_accessed_memory, currently_accessed_memory with
      | None, None -> () (*No memory access in previous or current step, cache display not required*)
      | _, _ -> (Printf.fprintf out "\n Cache L1-d %s\n" (Cach.to_string step.system_state.l1d);
                 Printf.fprintf out "===================================\n")
    );
    fprint_inst_and_leak out step
  );
  currently_accessed_memory


let fprint_with_memory out (step : t) (l_bound : Int64.t) (h_bound : Int64.t) : unit =
  Printf.fprintf out "%s" (Syst.to_string_with_memory step.system_state l_bound h_bound );
  fprint_inst_and_leak out step

let fprint_without_memory out (step : t) : unit =
  Printf.fprintf out "%s" (Syst.to_string step.system_state);
  fprint_inst_and_leak out step

let get_print (step : t) : string option = step.events.print


let cache_history_to_csv_string (step : t) (step_number : int) (nb_processes : int) : string =
  let current_pid = step.system_state.os.current_pid in
  
  let (pos, set_id) = match step.events.software_event with
    | Evsw.Mem_access (cache_set, _tag) ->
      if ((Event.contains_hw_event step.events Evhw.Cache_hit) > 0)
      then ((0, cache_set))
      else
      (
        if ((Event.contains_hw_event step.events Evhw.Cache_miss) > 0)
        then ((0, cache_set))
        else (raise(Failure("Memory access of SW trace is neither a cache miss nor a cache hit in HW trace at step " ^
            string_of_int step_number ^ " on instruction " ^ Inst.to_string step.instruction)))
      )
    | Evsw.Protected_mem_access (_cache_set, _tag) -> (1, 16)
    | Evsw.Lock (cache_set, _tag) | Evsw.Unlock (cache_set, _tag) -> (2, cache_set)
    | _ -> (-5*nb_processes ,-1) (*nothing to print*)
  in

  let line_as_list =
    List.init
      (nb_processes*3)
      (fun i -> if(i=((3*current_pid)+pos)) then (", "^(string_of_int set_id)) else (",   "))
  in

  List.fold_left
    (^)
    ("\n" ^ (string_of_int step_number))
    line_as_list


