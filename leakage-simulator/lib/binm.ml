

type param_signe =
  | Unsigned
  | Signed


let rec power_int64 (base: Int64.t) (exponent : int) :  Int64.t =
  if(exponent=0)
  then( Int64.one)
  else( Int64.mul base (power_int64 base (exponent-1)))

let power (base:int) (exponent : int) : int =
  if(exponent > (Sys.int_size-3))
  then (raise (Failure ("Exponent "^ string_of_int exponent ^" is too big compare to Sys.int_size " ^ string_of_int Sys.int_size ^ ".")))
  else (
    let res = power_int64 (Int64.of_int base) exponent in
    if((res < 0L)  && (base > 0))
    then (raise (Failure ("Overflow detected in power, res : "^ Int64.to_string res ^" incorrect for " ^ string_of_int base ^ "^" ^ string_of_int exponent ^ ".")))
    else (Int64.to_int res)
  )

(*open Stdint
   Uint32.t*)
let power2 (exponent : int) : Int32.t =
  if(exponent > (30))
  then (raise (Failure (" 2 exponent "^ string_of_int exponent ^" is too big for int32.")))
  else (Int64.to_int32 (power_int64 2L exponent))

let rec log (base:int) (input:int) : int =
  if(input < base)
  then(0)
  else(1 +(log base (input/base)))

let rec log2 (value : Int64.t) : int =
  if(value < 2L)
  then(0)
  else(1 +(log2 (Int64.div value 2L)))
  

(*Maximum positive number on an int32*)
let max_positive_int32 : Int64.t = Int64.sub (power_int64 2L 31) 1L;;
let two_complement_int32 : Int64.t = power_int64 2L 32;;

let byte_size = 8;;
let nb_bytes_in_a_word = 4;;
let word_size = nb_bytes_in_a_word*byte_size;;
let nb_words_per_cache_line = 4;;
let nb_bits_encoding_word_position = (log 2 nb_bytes_in_a_word) + (log 2 nb_words_per_cache_line);;

let instruction_end_code = power 2 (byte_size-1);; (*Instruction remplie de 1; utiliser (power 2 (word_size))-1 pour faire une ligne remplie de 1*)


let int32_as_positive_int64 (value : Int32.t) : Int64.t = 
  if(value>=0l)
  then (Int64.of_int32 value)
  else (Int64.add (Int64.of_int32 value) two_complement_int32)

let positive_int64_as_int32 (value : Int64.t) : Int32.t = 
  let _is_positive = 
    if(value<0L)
    then (raise(Failure("positive_int64_as_int32 must take a positive Int64.t, not " ^ Int64.to_string value)))
    else()
  in
  let res =
    if(value > max_positive_int32)
    then (Int64.to_int32 (Int64.sub value two_complement_int32))
    else (Int64.to_int32 value)
  in
  let _is_correct =
    if(value = (int32_as_positive_int64 res) && res>=0l)
    then ()
    else(raise(Failure("Conversion positive_int64_as_int32 failed on " ^ Int64.to_string value ^
        ": returned " ^  Int32.to_string res ^" which is converted back as "^  Int64.to_string (int32_as_positive_int64 res))))
  in
  res




type binary =
  | One of binary
  | Zero of binary
  | BinEnd
(* One (One (Zero (BinEnd))) = 110 = 6 en base decimale  *)


(*cut_right on 0 will return an empty bin (BinEnd), splitleft on (get_length_of_bin bin)+1 will return the full binary*)
let rec cut_right (bin: binary) (split_index : int) : binary = 
  if
    (split_index > 0)
  then
    (
      match bin with
      | BinEnd -> BinEnd
      | One(tail) -> One(cut_right tail (split_index-1))
      | Zero(tail) -> Zero(cut_right tail (split_index-1))
    )
  else
    (BinEnd)


(*cut_right on 0 will return the full binary, splitleft on (get_length_of_bin bin)+1 will return an empty bin (BinEnd)*)
let rec cut_left (bin: binary) (split_index : int) : binary = 
  if
    (split_index > 0)
  then
    (
      match bin with
      | BinEnd -> BinEnd
      | One(tail) -> cut_left tail (split_index-1)
      | Zero(tail) -> cut_left tail (split_index-1)
    )
  else
    (bin)



let rec string_of_bin (bin : binary) =
  match bin with
  | BinEnd -> ""
  | One(tail) -> "1"^string_of_bin tail
  | Zero(tail) -> "0"^string_of_bin tail

(* unused methods, commented out because of [unused-var-strict]

let rec padding_zero (number_of_zero : int) : binary =
  if
    (number_of_zero > 0)
  then
    (Zero(padding_zero (number_of_zero-1)))
  else
    (BinEnd)

let rec concatenate_bin (bin1: binary) (bin2: binary) : binary = 
  match bin1 with
  | BinEnd -> bin2
  | One(tail) -> One(concatenate_bin tail bin2)
  | Zero(tail) -> Zero(concatenate_bin tail bin2)

End of unused methods*)

let rec get_length_of_bin (bin: binary) : int =
  match bin with
  | BinEnd -> 0
  | One(tail) -> 1 + get_length_of_bin tail
  | Zero(tail) -> 1 + get_length_of_bin tail




let rec replace_least_significant_bits_with_0_aux (bin : binary) (countdown : int) : binary =
  if
    (countdown = 0)
  then
    (match bin with 
    | BinEnd -> BinEnd
    | One(tail) -> Zero(replace_least_significant_bits_with_0_aux tail 0) 
    | Zero(tail) -> Zero(replace_least_significant_bits_with_0_aux tail 0)
    )
  else
    (match bin with 
    | BinEnd -> BinEnd
    | One(tail) -> One(replace_least_significant_bits_with_0_aux tail (countdown-1))
    | Zero(tail) -> Zero(replace_least_significant_bits_with_0_aux tail (countdown -1))
    )

let replace_least_significant_bits_with_0 (bin : binary) (nb_bits_to_replace : int) : binary =
  replace_least_significant_bits_with_0_aux bin ((get_length_of_bin bin) - nb_bits_to_replace)



let rec int_of_bin_aux (bin : binary) (higher_significant_bits : Int64.t) : Int64.t =
  match bin with
  | BinEnd -> higher_significant_bits
  | Zero(tail) -> (int_of_bin_aux tail (Int64.mul 2L higher_significant_bits))
  | One(tail) -> (int_of_bin_aux tail (Int64.add 1L (Int64.mul 2L higher_significant_bits)))

let int64_of_bin (bin : binary) : Int64.t =
  int_of_bin_aux bin 0L

(**Appeler bin_of_int64  avec un size negatif aura pour effet d utomatiquement ajuster size au decimal converti; TODO : en faire un param optionnel ?(size: int)=-1*)
let rec bin_of_int64 (decimal : Int64.t) (size: int) : binary =
  if
    (size < 0)
  then
    (bin_of_int64 decimal (1 + (log2 decimal)))
  else
  (
    if
      (size = 0)
    then 
      (BinEnd)
    else
    (
      match (Int64.div decimal (power_int64 2L (size-1))) with
      | 0L -> Zero(bin_of_int64 (Int64.rem decimal (power_int64 2L (size -1))) (size-1))
      | 1L -> One(bin_of_int64 (Int64.rem decimal (power_int64 2L (size -1))) (size-1))
      | _ -> raise (Failure ("bin_of_int64 failed, the number "^ Int64.to_string decimal ^" is too big to be converted in a binary of " ^ string_of_int size ^ " bits."))
    )
  )

(*
let test_int_of_bin_of_int (test_value : Int64.t) : Int64.t =
  int_of_bin (bin_of_int64 (test_value) (-1))


let encode_4_numbers (a : int) (b : int) (c : int) (d : int) : int =
  if
    (a<0 || b<0 || c<0 || d<0 || a>(power 2 byte_size)-1 || b>(power 2 byte_size)-1 || c>(power 2 byte_size)-1 || d>(power 2 byte_size)-1)
  then
    raise (Failure ("encode_4_numbers must take number within bit limit "^ string_of_int byte_size ^
    ",\n at least one of the following arguments is too big :\na="^string_of_int a ^";\nb="^string_of_int b ^";\nc="^string_of_int c ^";\nd="^string_of_int d ^"." ))
  else
    (a * (power 2 (3*byte_size)) + b * (power 2 (2*byte_size)) + c * (power 2 (byte_size)) + d)



let encode_signed_int (signed:int) (bit_limit : int) : int =
  if
    ((signed > (power 2 (bit_limit-1) )-1) || ((0-signed) > (power 2 (bit_limit-1))))
  then
    raise (Failure ("The signed number "^ string_of_int signed ^ " is too big to be converted in unsigned of max size " ^ string_of_int bit_limit))
  else
  (
    if
      (signed < 0)
    then
      (signed + (power 2 (bit_limit)))
    else
      (signed)
  )
*)
(*
let bin_display_of_int (i : int) (size : int) (isSigned: param_signe): string =
  if
    (isSigned = Signed) 
  then
    (string_of_bin (bin_of_int64 (encode_signed_int i size) size))
  else
    (string_of_bin (bin_of_int64 i size))
*)
let rec decode_larger_int (bit_limit:int) (code : Int64.t) (position : int) (isSigned : param_signe) : Int64.t =
  if
    (isSigned = Signed)
  then
  (
    match (Int64.rem code (power_int64 2L ((position+1)*bit_limit-1))), (Int64.div code (power_int64 2L ((position+1)*bit_limit-1))) with 
    | remainder, signe ->
      (
        if
          (signe = 0L || signe = 1L) (*signe = 0 => positive, signe = 1 => negative*)
        then 
          (
            Int64.sub
              (decode_larger_int
                bit_limit
                remainder
                position
                Unsigned
              )
              (Int64.mul signe (power_int64 2L (bit_limit-1))) (*if positive, this line equals 0 = ignored, if negative, substract 2^(bit_limit-1)*)
          )
        else  (*if number oversized for the bitlimit*)
          raise (Failure ("The signed binary "^ Int64.to_string code ^ " at position "^string_of_int position^" is biger than expected by the max size " ^ string_of_int bit_limit))
      )
  )
  else
  (
    (Int64.div code (power_int64 2L ((position)*bit_limit)))
  )

(*

let decode_int_from_code (code : int) (position : int) (position_max : int) (isSigned : param_signe) : int =
    if
      (position > position_max)
    then
      (raise (Failure("Error in decode_byte : position ("^string_of_int position^") bigger than position_max("^string_of_int position_max^").")))
    else
      (decode_larger_int byte_size (code mod (power 2 (byte_size* (position+1)))) position isSigned);;


let decode_signed_int (unsigned : int) (bit_limit : int) : int =
  decode_larger_int (bit_limit) (unsigned) (0) (Signed)
  
*)


let compute_lower_address_limit (address : Int64.t) : Int64.t =
  int64_of_bin
  (replace_least_significant_bits_with_0
    (
      bin_of_int64
      address
      (word_size)
    )
    (nb_bits_encoding_word_position)
  )

let map_address_to_cache_set_id (address : Int64.t) (cache_nbSets : int) : int =
  let address_with_word_position_removed = Int64.shift_right_logical address nb_bits_encoding_word_position in
  Int64.to_int (Int64.rem address_with_word_position_removed (Int64.of_int cache_nbSets))
  
(*the tag of a cache line is the address without the (n+m) least significant bits, n being the number of bits encoding the cache set id, and m the number of bits encoding the word id
which means the tag is made from the (get_length_of_bin address)-(n+m) most significant bits*)
let map_address_to_cache_tag (address : Int64.t) (cache_nbSets : int): int =
    Int64.to_int (Int64.shift_right_logical address (nb_bits_encoding_word_position + (log 2 cache_nbSets)))


(*determine which word must be selected in a cache line*)
let map_address_to_word_id (address : Int64.t) : int =
  Int64.to_int (Int64.rem address (power_int64 2L nb_bits_encoding_word_position))

let align_address_on_byte (address : Int64.t) : Int64.t =
  Int64.sub address (Int64.rem address 8L)


type half =
  | Low 
  | High

let half_to_string (part : half) : string = 
  match part with
  | Low -> "Low"
  | High -> "High"

let mult (part : half) (a : Int32.t) (b : Int32.t) : Int32.t =
  let res : Bigint.t = 

    let tmp_res : Bigint.t =  (Bigint.( * )) (Bigint.of_int32 a)  (Bigint.of_int32 b) in 

    if ((a<0l) <> (b<0l)) (*if the result is negative*)
    then ((Bigint.( + )) tmp_res (Bigint.( ** ) (Bigint.of_int 2) (Bigint.of_int 64)))
    else tmp_res
  in
  let splitted_res = match part with
  | High -> Bigint.to_int32 (Bigint.( / ) res (Bigint.( ** )  (Bigint.of_int 2) (Bigint.of_int 32)))
  | Low -> Bigint.to_int32 (Bigint.( % ) res  (Bigint.( ** )  (Bigint.of_int 2) (Bigint.of_int 32)))
    (*handmade modulo
    let diviser  = Bigint.( / ) res (Bigint.( ** )  (Bigint.of_int 2) 32) in

    let _print = print_endline ("  diviser is " ^ Bigint.to_string_hum diviser ) in
    let substraction = (Bigint.( * ) diviser (Bigint.( ** )  (Bigint.of_int 2) 32)) in
    let _print = print_endline ("  substraction is " ^ Bigint.to_string_hum substraction ) in
    Bigint.to_int_exn (Bigint.( - ) res substraction)
    *)
  in
  match splitted_res with
  | Some(final_value) -> final_value 
  | None -> raise(Failure("Overflow on multiplication " ^ half_to_string part ^ " of " ^ Int32.to_string a ^ " * "  ^ Int32.to_string b))




let uint32_to_bitstring (value : Int32.t) : Bitstring.bitstring =
  (*let val64 = Uint32.to_int64 value in *)
  let%bitstring bits = {| value : 32|} in
  (*let _tempo_print_hex = Bitstring.hexdump_bitstring stdout bits in*)
  bits




let bitwise_operation (operator : (bool->bool->bool)) (a : Int32.t) (b : Int32.t) : Int32.t =

  let rec apply_op (operator : (bool->bool->bool)) (bitsA : Bitstring.bitstring) (bitsB : Bitstring.bitstring) (remaining_bits : int) : Int32.t =
    if(remaining_bits>0)
    then(
      let (bA : bool), (tailA : Bitstring.bitstring) = match%bitstring bitsA with
        | {| head : 1 ; tail : (remaining_bits-1) : bitstring |} -> head,tail
        | {|_|} -> raise(Failure("Error on value "^ Int32.to_string a ^"  during bitwise operation"))
      in
      let (bB : bool), (tailB : Bitstring.bitstring) = match%bitstring bitsB with
        | {| head : 1 ; tail : (remaining_bits-1) : bitstring |} -> head,tail
        | {|_|} -> raise(Failure("Error on value "^ Int32.to_string b ^"  during bitwise operation"))
      in
      let current_bit_res = if (operator bA bB) then (Int32.one) else (Int32.zero) in 
      Int32.add
        (Int32.mul current_bit_res (Int32.of_int (power 2 (remaining_bits-1))))
        (apply_op operator tailA tailB (remaining_bits-1))
    )
    else (Int32.zero)
  in
  let res = apply_op operator (uint32_to_bitstring a) (uint32_to_bitstring b) 32 in
  let _debug =
    if (true || Conf.show_debug)
    then(
      print_endline (
        "bitwise_operation "^ Int32.to_string a ^ " # "^ Int32.to_string  b ^ " = "^ Int32.to_string res ^ ".\n"
      )
    )
    else ()
  in
  res


(*Encodage temporaire pour instruction avec gros offset (load et store). A refaire plus tard avec vrai encodage RISCV?
let sA,sB,sC,sD = 8,5,5,14
let encode_inst_reg_reg_offset_temporaire (a : int) (b : int) (c : int) (signed_d : int) : int =
  let d = encode_signed_int signed_d sD in
  if
    (a<0 || b<0 || c<0 || d<0 || a>(power 2 sA)-1 || b>(power 2 sB)-1 || c>(power 2 sC)-1 || d>(power 2 sD)-1)
  then
    raise (Failure ("encode_inst_reg_reg_offset_temporaire must take number within bit limit "^
      string_of_int sA ^", "^string_of_int sB ^", "^string_of_int sC ^", "^string_of_int sD ^
      ".\n At least one of the following arguments is too big :\na="^
      string_of_int a ^";\nb="^string_of_int b ^";\nc="^string_of_int c ^";\nd="^string_of_int signed_d ^"." ))
  else
    (a * (power 2 (4*byte_size - sA)) + b * (power 2 (4*byte_size - (sA+sB))) + c * (power 2 (4*byte_size - (sA+sB+sC))) + d)


let encode_inst_reg_imm_temporaire (a : int) (b : int) (signed_imm : int) : int =
  let imm = encode_signed_int signed_imm (sC+sD) in
  if
    (a<0 || b<0 || imm<0 || a>(power 2 sA)-1 || b>(power 2 sB)-1 || imm>(power 2 (sC+sD))-1)
  then
    raise (Failure ("encode_inst_reg_imm_temporaire must take number within bit limit "^
      string_of_int sA ^", "^string_of_int sB ^", "^string_of_int (sC+sD) ^
      ".\n At least one of the following arguments is too big :\na="^
      string_of_int a ^";\nb="^string_of_int b ^";\nimm="^string_of_int signed_imm ^ "." ))
  else
    (a * (power 2 (4*byte_size - sA)) + b * (power 2 (4*byte_size - (sA+sB))) + imm)
    

let decode_reg_reg_offset_temporaire (encoded : int) : (int * int * int) =
  let d = encoded mod (power 2 sD) in
  let c = (encoded-d) mod (power 2 (sD+sC)) in
  let b = (encoded-c) mod (power 2 (sD+sC+sB)) in
  (b/(power 2 (sD+sC))),(c/(power 2 sD)),(decode_signed_int d sD)


let decode_reg_imm_temporaire (encoded : int) : (int * int) =
let imm = (encoded) mod (power 2 (sD+sC)) in
let b = (encoded-imm) mod (power 2 (sD+sC+sB)) in
(b/(power 2 (sD+sC))),(decode_signed_int imm (sC+sD))
*)