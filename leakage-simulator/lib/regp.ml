type t = R of int

let max_register = 32

let memory_size_of_register = Memo.Word
let nb_bits_in_reg = ((Memo.nb_bytes memory_size_of_register) * Binm.byte_size) (*32 bits*)

(*
open Stdint
type reg_list = Uint32.t list
*)

type r_list = Int32.t array


let build (index : int) : t = R(index)


let to_index (register : t) : int =
  match register with
  | R index -> index

(*Read only the lower bytes of the register, corresponding to the required size*)
let read_lower_bytes (register_list: r_list) (register : t) (*isSigned : Binm.param_signe*) (size : Memo.size): Int32.t =
  match register with R index ->
    if(index < 0 || index >= max_register)
    then failwith(Printf.sprintf "Register %d does not exist" index);
    if index = 0 then 0l (*x0 always contains 0*)
    else
      match Array.get register_list index with
      | exception Not_found -> failwith(Printf.sprintf "Register %d is not initialized" index)
      | value ->
        let lower_bytes = Int32.logand value (Memo.mask_of size) in
        if Conf.show_debug
        then Printf.printf "Read lower bytes of %ld as %ld.\n" value lower_bytes;
        lower_bytes


let read (register_list: r_list) (register : t) : Int32.t =
  let res  = read_lower_bytes (register_list) (register) (*Binm.Unsigned*) (memory_size_of_register) in
  if Conf.show_debug then Printf.printf "Read reg x%d=%ld\n" (to_index register) res;
  res


let to_string_reduced (register : t) =
  match register with
  | R index -> Printf.sprintf "x%d" index

let write  (register_list: r_list) (register : t) (new_value : Int32.t) : r_list =
  (match register with
  | R index ->
    if index = 0 then () (*writing any value in x0 has no effect, x0 always contains 0*)
    else Array.set register_list index new_value);
  register_list


let display_int_as_register  (register_index : int) (value : Int32.t) : string =
  Printf.sprintf "x%d=% 4ld(0x%08lx)" register_index value value

let index_to_string (register_index : int) (register_list : r_list) : string  =
  display_int_as_register (register_index) (Array.get register_list register_index)

let to_string_full (register : t) (register_list : r_list) =
  match register with
  | R index -> index_to_string index register_list

let list_to_string (register_list : r_list) =
  Array.to_seqi register_list
  |> Seq.fold_left (fun acc (index, value) ->
      acc ^ (if index mod 4 = 0 then "\n" else "\t") ^ display_int_as_register index value
    ) ""

let build_new_register_list (new_prog : Prgm.t) : r_list =
  let regs = Array.init max_register (fun _ -> Int32.zero) in
  let regs = write regs (R 1) new_prog.initial_return_address in
  let regs = write regs (R 2) new_prog.initial_stack_pointer in
  let regs = write regs (R 3) new_prog.initial_global_pointer in
  let regs = write regs (R 10) new_prog.initial_argc in
  let regs = write regs (R 11) new_prog.initial_argv_pointer in
  let () = if Conf.show_debug then print_endline ("\n Initial registers state : "^list_to_string regs) else () in
  regs
(*x0 = 0, x1 = ra, x2 = sp, x3 = gp*)
(*a0=x10 -> contains argc, a1=x11 contains argv[] *)
    