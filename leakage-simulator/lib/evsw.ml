type t = 
  | Bullet
  | Div of int (*most significant bit of the diviser*)
  | Jump of int (*destination of jump. Will change in future implementation containing safe jump*)
  | Mem_access of int * int (*cache_set index, tag*)
  | Protected_mem_access of int * int (*cache_set index, tag*)
  | Lock of int * int (*cache_set index, tag*)
  | Rocs of int * int (*cache_set index, tag*)
  | Link of int * int * string (*cache_set index, linked group id*)
  | Mem_access_on_linked of string (*linked group id*)
  | Unlock of int * int (*cache_set index, tag*)
  | Swap_CT_mode of Int32.t (*new value of ct mode*)
  | Error of string
  | Ecall of Int32.t
  (*| ...*)



let to_string (ev : t) : string =
  let display = 
    match ev with
    | Bullet -> ("∙")
    | Div(log) -> Printf.sprintf "Div(2^%d)" log
    | Jump(target) -> Printf.sprintf "Jump_op(%d)"  target
    | Mem_access(targetSet, targetTag) ->
      Printf.sprintf "Mem_access(Set:%d,%d)" targetSet targetTag
    | Protected_mem_access(_targetSet, _targetTag) -> ("Protected_mem_access")
    | Lock(targetSet, targetTag) ->
      Printf.sprintf "Lock(Set:%d,%d)" targetSet targetTag
    | Rocs(targetSet, targetTag) ->
      Printf.sprintf "Rocs(Set:%d,%d)" targetSet targetTag
    | Link(targetSet, targetTag, group_id) ->
      Printf.sprintf "Link(Set:%d, Tag: %d -> %s)" targetSet targetTag group_id
    | Mem_access_on_linked(group_id) ->
      Printf.sprintf "Mem_access_on_linked_group(%s)" group_id
    | Unlock(targetSet, targetTag) ->
      Printf.sprintf "Unlock(Set:%d, Tag:%d)" targetSet targetTag
    | Swap_CT_mode(target) -> (Printf.sprintf "Swap CT mode to %ld"  target)
    | Ecall n -> Printf.sprintf "Ecall %ld" n
    | Error(msg) -> ("Error raised :" ^ msg )
  in "[" ^ display ^ "]"
