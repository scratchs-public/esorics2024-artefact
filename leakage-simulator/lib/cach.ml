type cache_set = 
{
  lines : Line.t list;
  eviction_order : int list;(*List of ways index, head is the next way to evict, locked ways are removed fro this list, unlocked ways are put on the head of the list*)
}



let string_of_cache_set (set : cache_set) (index_set : int) : string =
  ((List.mapi
    (fun index_way line ->
      "|" ^(Disp.string_of_int_with_padding index_set 3)^
      "|"^ (Disp.string_of_int_with_padding index_way 3)^
      "|"^ (Line.to_string line) ^
      "|" 
    )
    (set.lines)
  ) |> (String.concat "\n")) ^ "LRU order: ["^
  ((List.map (string_of_int) (set.eviction_order)) |> (String.concat ", ")) ^ "]\n"



let empty_cache_set (nb_ways : int) : cache_set =
  {
    lines = List.init (nb_ways) (fun _ -> Line.empty ());
    eviction_order = List.init (nb_ways) (fun x -> x);
  }

let remove_from_eviction_order (order : int list) (way_id : int) : (int list) =
  if not (List.mem way_id order)
  then raise (Failure("Error in remove_from_eviction_order : element " ^
                string_of_int way_id ^ " not present in eviction_order"))
  else List.filter (fun x -> x <> way_id) order
  (* match order with *)
  (* | [] -> raise (Failure("Error in remove_from_eviction_order : element " ^ *)
  (*         string_of_int way_id ^ " not present in eviction_order")) *)
  (* | head :: tail -> *)
  (*   if (head = way_id) *)
  (*   then (tail) *)
  (*   else (head :: (remove_from_eviction_order (tail) (way_id))) *)

(*updated way id is put at the end of evection order : last to evict*)
let update_eviction_order (order : int list) (used_way : int) : int list option =
  try
    let new_order = List.append (remove_from_eviction_order (order) (used_way)) (used_way::[]) in
    let _debug =
      if (Conf.show_debug)
      then print_endline(
        "previous order : "^
        ((List.map (string_of_int) (order)) |> (String.concat ",")) ^
        "new order : "^
        ((List.map (string_of_int) (new_order)) |> (String.concat ",")) 
      )
      else ()
    in
    Some(new_order)
  with _ -> None (*If the used_way is not in eviction order, it means it was locked, no update to do on the order*)

type t =
{
  (*nb_sets : int;*)
  nb_ways : int;
  sets : cache_set list
}


let nb_sets (cache : t) : int = List.length cache.sets

let to_string (cache : t) : string =
  "contains " ^string_of_int (nb_sets cache)^ " sets of " ^string_of_int cache.nb_ways^ " ways.\n" ^
  "|SET|WAY|    TAG    |DIRTY|PROTECTION| " ^
  (List.fold_left
    ( ^)
    ("")
    (List.init (Binm.nb_words_per_cache_line) (fun x->("   WORD "^string_of_int x^"  |")))
  ) ^ "\n" ^
  (*string_of_cache_sets content 0*)
  ((List.mapi
    (fun index_set set -> (string_of_cache_set (set) (index_set)))
    (cache.sets)
  ) |> (String.concat ""))

let split_address_into_tag_set_wordid (address : Int64.t) (cache : t) : (int * int * int) =
    let tag = Binm.map_address_to_cache_tag (address) (nb_sets cache) in 
    let set = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in 
    let word_id = Binm.map_address_to_word_id (address) in
  tag, set, word_id


let rec replace_in_list (old_list : 'a list) (index : int) (new_elem : 'a) : 'a list =
  match old_list with
  | [] -> raise (Failure ("Error in Cach.replace_in_list : List too short"))
  | head :: tail ->
    if (index = 0)
    then (new_elem :: tail)
    else (head :: replace_in_list (tail) (index-1) (new_elem)) 

(*return updated cache is update of usage was possible, None otherwise (protected line)*)
let update_cache_usage (cache : t) (address : Int64.t) (way_id : int) : t option =
  let index_of_set_to_update  = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in
  let old_cache_set = List.nth (cache.sets) (index_of_set_to_update) in
  match update_eviction_order (old_cache_set.eviction_order) (way_id) with
  | None -> None (*This line is protected (out of eviction order), no update to do on cache*)
  | Some (new_order) ->
    let new_cache_set = {old_cache_set with eviction_order = new_order} in
    Some {cache with sets =
      replace_in_list (cache.sets) (index_of_set_to_update) (new_cache_set)
    }


let index_of_way_with_tag_in_cache_set (tag : int) (set : cache_set) : (int option) =
  let rec find (tag : int) (lines : Line.t list) (index : int) =
    match lines with
    | [] -> None
    | head :: tail ->
      if (Line.match_tag (tag) (head))
      then (Some index)
      else (find (tag) (tail) (index+1))
  in
  find (tag) (set.lines) (0)


let build (n_sets : int) (n_ways : int) : (t) =
  {
    (*nb_sets = n_sets;*)
    nb_ways = n_ways;
    sets = List.init (n_sets) (fun _ -> empty_cache_set (n_ways))
  }


let rec seperate_list_around_element (list : ('a list)) (elem_position : int) (previous_elems_accumulator : ('a list)) : (('a list) * 'a * ('a list)) =
  match list with
  | [] -> raise (Failure ("Element not found in seperate_list_around_element."))
  | head :: tail ->
    if(elem_position=0)
    then (List.rev previous_elems_accumulator, head, tail)
    else (seperate_list_around_element (tail) (elem_position-1) (head :: previous_elems_accumulator))


(*return None if the tag is not in this list of lines*)
let rec seperate_line_list_around_tag (list : (Line.t list)) (tag : int) (previous_elems_accumulator : (Line.t list)) : ((Line.t list) * Line.t * (Line.t list)) option =
  match list with
  | [] -> None
  | head :: tail ->
    if(Line.match_tag tag head)
    then Some(List.rev previous_elems_accumulator, head, tail)
    else (seperate_line_list_around_tag (tail) (tag) (head :: previous_elems_accumulator))

  (*  
let lock_cache_line (initial_cache : t) (address : int) : (int * Event.t * t * (int option)) (*output, leak, updated_cache, dirty*) =
  match cache with Cache(nb_sets,nb_way, content) ->*)
    

let unlock_cache_line (initial_cache : t) (address : Int64.t) : (int * Event.t * t * (int option)) (*output, leak, updated_cache, dirty*) =
  (*match cache with Cache(nb_sets,nb_way, content) ->*)
    let tag, set_id,_wordid = (split_address_into_tag_set_wordid (address) (initial_cache)) in 
    let predecessing_sets, selected_set, successing_sets = seperate_list_around_element (initial_cache.sets) (set_id) ([]) in
    let searched_tag = Binm.map_address_to_cache_tag (address) (nb_sets initial_cache) in
    let way_split = seperate_line_list_around_tag (selected_set.lines) (searched_tag) ([]) in
    let output = 0 in (*Unlock can only return 0 for now*)
    let initial_leak =
      (Event.add_hw_event
        (Event.build (Evsw.Unlock(set_id,tag)) Instclass.Unlock) (*software event : unlock on this cache set*)
        Evhw.Cache_hit (*hardware event : cache hit while searching for line to unlock*)
      )
    in
    match way_split with
    | None -> output, initial_leak, initial_cache, None
    (*
      Now we allow Unlock on lines that are not lock. Do nothing
      raise (Failure("Unlock did not find locked line in cache (Future works: allow unlock on Link lines that are not always in cache)."))
    *)
    
    | Some(predecessing_ways, selected_way, successing_ways) -> 
        (*"instruction Unlock line on address " ^ string_of_int address ^ " Succeed" in*)
      let updated_cache_content =
        (List.append 
          (predecessing_sets)
          (
            {
              lines =
                (List.append 
                  (predecessing_ways)
                  (
                    (Line.empty ())
                    ::
                    successing_ways)
                );
              (*Whatever the eviction policy is, unlocked ways are put back in eviction order, next to evict.
              Note that eviction order is ignored in pseudo_Random eviction policy, as it simulates LRU*)
              eviction_order = 
                (List.length predecessing_ways) :: selected_set.eviction_order
                (*length of predecessing_ways list is equals to the way id of the selected way*)
            }
            ::
            (successing_sets))
        )
      in
      let dirty_way, leak_after_write_back = 
        (*
        if(Line.is_dirty selected_way) 
        then (Some (List.length predecessing_ways)) (*Way ID of the dirty way is equals to the number of element in predecessing_ways list*)
        else (None) (*Dirty bit = false, no write back to perform*) 
          *)
        let write_back =
          match (Line.get_protection selected_way) with
          | Unprotected -> raise (Failure("Error : Cache Unlock on unprotected line"))
          | Lock(Write_Back_On_Unlock) -> true
          | Lock(Write_Through) -> false
          | Lock(Careless) -> (Line.is_dirty selected_way)
          | Rocs(_write_mode) -> raise (Failure("Error : ROCS unlock not supported yet"))
          | Linked(_write_mode,_value) -> raise (Failure("Error : LINK unlock not supported yet"))
          | Bad_Protection -> raise (Failure("Error : Cache Unlock on line with Bad_Protection"))
        in
        if (write_back)
        then (Some (List.length predecessing_ways), (Event.add_hw_event initial_leak Evhw.Write_back)) (*Way ID of the dirty way is equals to the number of element in predecessing_ways list*)
        else (None, initial_leak) (*no write back to perform*) 
      in
      output,
      leak_after_write_back,
      {initial_cache with sets = updated_cache_content},
      dirty_way



let cache_set_of_address (cache : t) (address : Int64.t) : (cache_set) =
  let index_of_set_to_find = (Binm.map_address_to_cache_set_id (address) (nb_sets cache)) in
  if (List.length cache.sets) > (index_of_set_to_find)
  then (List.nth (cache.sets) (index_of_set_to_find))
  else (raise (Failure ("Error : bad index_of_set_to_find :" ^ string_of_int index_of_set_to_find ^ " with " ^ string_of_int (List.length cache.sets) ^ " sets.")))
  
  



let rec get_line_matching_address_aux (set : Line.t list) (tag : int) (way_counter : int) : ((int * Line.t) option) =
  match set with
  | [] -> None (*No line of cache math the address in this cache*)
  | head :: tail ->
    match Line.match_tag tag head with
    | true -> Some(way_counter,head) (*this line (and only this one in this cache) match the address *)
    | false -> get_line_matching_address_aux tail tag (way_counter+1)


(*If a line of the catch match the address, it is returned as set_id,Some(way_id, cacheLine). otherwise: set_id,None*)
let get_line_with_tag (cache : t) (address : Int64.t) :  ((int * Line.t) option) =
  let set_id = (Binm.map_address_to_cache_set_id (address) (nb_sets cache)) in
  
  (get_line_matching_address_aux
    ((List.nth (*There is only one set corresponding to this address*)
      cache.sets (*List of sets (cache_line list) of nb_sets elements*)
      set_id (*Get the set id corresponding to the address*)
    ).lines)
    (Binm.map_address_to_cache_tag (address) (nb_sets cache)) (*Get the tag corresponding to the address*)
    0
  )


let get_line (cache : t) (address : Int64.t) (way_id : int) : Line.t = 
  let index_of_set  = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in
  List.nth
    ((List.nth 
      (cache.sets)
      (index_of_set)
    ).lines)
  way_id
  


let evict_and_replace_line (cache : t) (set_id : int) (way_id : int) (new_line : Line.t) : t =
  if(set_id >= (nb_sets cache))
  then (raise(Failure("Error in evict_and_replace_line : set_id " ^ string_of_int set_id ^ " larger than nb_sets "^ string_of_int (nb_sets cache))))
  else if(way_id >= cache.nb_ways)
  then (raise(Failure("Error in evict_and_replace_line : way_id " ^ string_of_int way_id ^ " larger than nb_ways "^ string_of_int (nb_sets cache))))
  else(
    let old_cache_set = List.nth (cache.sets) (set_id) in
    let new_order = match update_eviction_order (old_cache_set.eviction_order) (way_id) with
      | None -> raise(Failure("Cannot evict protected line (out of eviction order)"))
      | Some (order) -> order
    in
    let new_cache_set = {
      lines = replace_in_list (old_cache_set.lines) (way_id) (new_line);
      eviction_order = new_order;
    } in
    {cache with sets =
      replace_in_list (cache.sets) (set_id) (new_cache_set)
    }
  )


let rec find_evictable_ways_index (cacheSet : (Line.t list)) (counter : int): ((int * bool) list) = (*way_id and dirty_bit*)
  match cacheSet with
  | [] -> []
  | head :: tail ->
    (
      if (Line.is_evictable head )
      then ((counter, Line.is_dirty head) :: find_evictable_ways_index tail (counter+1) )
      else (find_evictable_ways_index tail (counter+1) )
    )


let choose_way_to_evict_pseudo_random (random_generator_state : Random.State.t) (cache : t) (address : Int64.t) (keep_one_free_way : bool) : ((Random.State.t)*(int*bool) option) =
  let set_to_update  = cache_set_of_address cache address in
  let evictable_ways = (find_evictable_ways_index (set_to_update.lines) (0)) in
  if((evictable_ways = []) || (keep_one_free_way && (List.length evictable_ways < 2)))
  then (random_generator_state,None) (*The cache cannot be updated if no ways are evictable for this set*)
  else
    (
      let new_conf, random_int = (Conf.generate_random (random_generator_state) (List.length (evictable_ways))) in
      let index_and_dirty_bit_of_way_to_evict = (*Random eviction (among possible choices)*)
        (List.nth
          (evictable_ways)
          (random_int)
        )
      in
      new_conf, Some(index_and_dirty_bit_of_way_to_evict) (*integer corresponding to the way to evict*)
    )


let choose_way_to_evict_LRU (cache : t) (address : Int64.t) (implementation : Conf.eviction_policy_implementation) (keep_one_free_way : bool) : ((int*bool) option) =
  let _check_if_supported_implementation =
    match implementation with
    | Careless ->  raise(Failure("Careless LRU  eviction choice not implemented")) (*LRU or LFU tag in cache update has no consideration for protection, and is not cleared by FREE*)
    | No_Usage_Impact_On_Protected_Lines -> () (*(NUIOPL) LRU or LFU tag in cache are never updated on protected lines (set to a constant)*)
    | Ignore_Usage_Of_Protected_Lines_And_Clean_Upon_Unlock_Or_Evict ->  raise(Failure("Ignore protected LRU  eviction choice not implemented"))(*(IUOPLACUUOE) LRU or LFU tag in cache update has no consideration for protection, FREE reset this tag, and eviction search never look at protected lines*)
    | Consider_Protected_Lines_And_Clean_Upon_Unlock_Or_Evict ->  raise(Failure("Weak Ignore protected LRU  eviction choice not implemented"))(*(CPLACUUOE) LRU or LFU tag in cache update has no consideration for protection, FREE reset this tag, but eviction search can loose time by looking tag of protected line*)
  in
  let index_of_set_to_evict  = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in
  let set_to_evict = List.nth cache.sets index_of_set_to_evict in
  let way_to_evict = 
    match set_to_evict.eviction_order with
    | head :: _next :: _tail -> Some head
    | [] -> None
    | head :: [] -> if (keep_one_free_way) then (None) else (Some head) 
  in
  match way_to_evict with
  | None -> None
  | Some (way) -> Some(way, (Line.is_dirty (List.nth set_to_evict.lines way)))


let choose_way_to_evict (configuration : Conf.t) (cache : t) (address : Int64.t) (keep_one_free_way : bool) : ((Conf.t)*((int*bool) option)) =
  match configuration.eviction_policy with
  | Pseudo_random_determinist(random_generator_state) ->
    let new_random_generator_state, way_to_evict =
      (choose_way_to_evict_pseudo_random
        (random_generator_state)
        (cache)
        (address)
        (keep_one_free_way)
      )
    in
    {configuration with eviction_policy = Pseudo_random_determinist(new_random_generator_state)},
    way_to_evict
  | LRU(implementation) -> configuration, choose_way_to_evict_LRU (cache) (address) (implementation) (keep_one_free_way)
  | LFU(_implementation) -> configuration, raise(Failure("LFU eviction policy not implemented"))
    
let write_back_to_ram (ram : Memo.t) (cache : t) (address : Int64.t) (selected_way : int)  : Memo.t =
  let way_to_write_back  = (List.nth (cache_set_of_address cache address).lines selected_way) in
  match (Line.get_tag way_to_write_back) with
   | None -> raise(Failure("Tried to write back an invalid line")) (*Invalid line, no write back*)
   | Some(_tag) -> Memo.write_from_memory ram (Line.get_memory way_to_write_back)


let replace_cache_line (cache : t) (address : Int64.t)  (selected_way : int) (protection : Prot.t) (new_line_content : Memo.t): t =
  let index_of_set_to_update  = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in
  let aligned_address = Int64.sub address (Int64.rem address (Int64.of_int Line.nb_bytes_per_cache_line)) in
  let new_cache_line =
    (Line.build 
      (Binm.map_address_to_cache_tag (address) (nb_sets cache))
      (protection)
      (new_line_content)
      (aligned_address)
    )
  in
  (evict_and_replace_line (cache) (index_of_set_to_update) (selected_way) (new_cache_line))  

  let update_on_store (cache : t) (address : Int64.t) (stored_data : Int32.t) (write_policy : Conf.write_policy_t) (previous_cache_line : Line.t) (_way_of_updated_cache_line : int) (size : Memo.size) : (t) =
    let new_cache_line = Line.update_on_store (previous_cache_line) (address) (stored_data) (write_policy) (size) in
  let index_of_set_to_update  = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in
  let tag =
    match (Line.get_tag new_cache_line) with 
    | None -> raise (Failure("Got an invalid line to store while updating cache : "^ Line.to_string new_cache_line))
    | Some(tag) -> tag
  in
  let old_cache_set = List.nth (cache.sets) (index_of_set_to_update) in
  let way_id = 
    match index_of_way_with_tag_in_cache_set (tag) (old_cache_set) with
    | None -> raise(Failure("Way to update on load with line " ^ Line.to_string new_cache_line ^ " was not found."))
    | Some(index) -> index
  in
  let new_order = match update_eviction_order (old_cache_set.eviction_order) (way_id) with
    | None -> old_cache_set.eviction_order (*This line is protected (out of eviction order), no update to do on order*)
    | Some (order) -> order
  in
  let new_cache_set = {
    lines = replace_in_list (old_cache_set.lines) (way_id) (new_cache_line);
    eviction_order = new_order;
  } in
  {cache with sets =
    replace_in_list (cache.sets) (index_of_set_to_update) (new_cache_set)
  }


let update_on_store_miss (cache : t) (address : Int64.t) (stored_data : Int32.t) (write_policy : Conf.write_policy_t) (way_of_updated_cache_line : int) (size : Memo.size) : (t) =
  let previous_cache_line = get_line (cache) (address) (way_of_updated_cache_line) in
  update_on_store (cache) (address) (stored_data) (write_policy) (previous_cache_line) (way_of_updated_cache_line) (size)

(*No cache L1-i implemented yet*)
let update_cache_on_context_switch (_cache : t) : t =
  raise(Failure("update_cache_on_context_switch not implemented.\n"))



let protect (cache : t) (address : Int64.t) (way_id : int) (protection : Prot.t) : t * ((Line.t * int) option) =
  try
    let index_of_set_to_protect = Binm.map_address_to_cache_set_id (address) (nb_sets cache) in
    let old_cache_set = List.nth (cache.sets) (index_of_set_to_protect) in
    let old_line = (List.nth (old_cache_set.lines) (way_id)) in
    let protected_line =
      (Line.set_protection
        (old_line)
        (protection)
      ) in
    let new_cache_set = {
      lines = replace_in_list (old_cache_set.lines) (way_id) (protected_line);
      eviction_order = (remove_from_eviction_order (old_cache_set.eviction_order) (way_id));
    } in
    {cache with sets =
      replace_in_list (cache.sets) (index_of_set_to_protect) (new_cache_set)
    },
    (if (Line.is_dirty old_line) then Some(old_line, way_id) else None)

  with _ -> cache, None (*If the used_way is not in eviction order, it means it was locked, no update to do on the order*)

