

type t =
  | ALU_only
  | Multiplication_low (*could be joined with ALU since it also takes 1 cycle*)
  | Multiplication_high
  | Division
  | Jump
  | Branch
  | Load
  | Store
  | Lock
  | Rocs
  | Link
  | Unlock
  | Ecall
  | SCTM

let to_string (inst_class : t) : string =
  "Instruction class " ^
  (match inst_class with
  | ALU_only -> "ALU only"
  | Multiplication_low -> "Multiplication, keep low bytes"
  | Multiplication_high -> "Multiplication, keep high bytes"
  | Division -> "Division"
  | Jump -> "Jump"
  | Branch -> "Branch"
  | Load -> "Load"
  | Store -> "Store"
  | Lock -> "Lock"
  | Rocs -> "Rocs"
  | Link -> "Link"
  | Unlock -> "Unlock"
  | Ecall -> "Ecall"
  | SCTM -> "SCTM"
  )
