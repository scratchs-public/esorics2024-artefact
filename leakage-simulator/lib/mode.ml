type rewrite =
| Write_Back_On_Unlock
| Write_Through
| Careless

let rewrite_to_string (mode : rewrite) : string =
  match mode with 
  | Write_Back_On_Unlock -> "Write_Back_On_Unlock mode"
  | Write_Through -> "Write_Through mode"
  | Careless -> "Careless rewrite mode"

let rewrite_to_char (mode : rewrite) : string =
match mode with 
| Write_Back_On_Unlock -> "W" (*For read AND WRITE arrays*)
| Write_Through -> "R" (*For READ ONLY arrays*)
| Careless -> "C"


let rewrite_to_int (mode : rewrite) : int =
  match mode with 
  | Write_Back_On_Unlock -> 0
  | Write_Through -> 1
  | Careless -> 2


let int_to_rewrite (mode : int) : rewrite =
  match mode with 
  | 0 -> Write_Back_On_Unlock
  | 1 -> Write_Through
  | 2 -> Careless
  | _ -> failwith("Integer "^ string_of_int mode ^" correspond to no rewrite mode.")
  
  