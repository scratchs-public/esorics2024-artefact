(*configuration state*) 

let show_debug : bool = false
let show_mem_init : bool = false
let show_warning : bool = true
(*let check_correctness : bool = true  Not implemented!*) (*Correctness check after instructions. Slow down simulation but can help debug*)
let default_max_stack_size : int = 1024 (*arbitrary amount of allocated size for the stack of each program*)

(*How to threat the cache-line usage information*)
type eviction_policy_implementation =
  | Careless (*LRU or LFU tag in cache update has no consideration for protection, and is not cleared by FREE*)
  | No_Usage_Impact_On_Protected_Lines (*(NUIOPL) LRU or LFU tag in cache are never updated on protected lines (set to a constant)*)
  | Ignore_Usage_Of_Protected_Lines_And_Clean_Upon_Unlock_Or_Evict (*(IUOPLACUUOE) LRU or LFU tag in cache update has no consideration for protection, FREE reset this tag, and eviction search never look at protected lines*)
  | Consider_Protected_Lines_And_Clean_Upon_Unlock_Or_Evict (*(CPLACUUOE) LRU or LFU tag in cache update has no consideration for protection, FREE reset this tag, but eviction search can loose time by looking tag of protected line*)


type eviction_policy_t =
  | Pseudo_random_determinist of Random.State.t (*state the random generator, making the eviction determist (two simulation with the same seed and starting from the same system will have the same evolution)*)
  | LRU of eviction_policy_implementation
  | LFU of eviction_policy_implementation

  (*
The functions from module Random.
State manipulate the current state of the random generator explicitly.
This allows using one or several deterministic PRNGs, even in a multi-threaded program, without interference from other parts of the program.

module State: sig .. end

val get_state : unit -> State.t
Return the current state of the generator used by the basic functions.

val set_state : State.t -> unit
Set the state of the generator used by the basic functions.
*)


(*When a cache line is updated by a store operation ...*)
type write_policy_t =
  | Write_back (*... put Dirty bit of the cache line to True. Write-back in RAM upon eviction of dirty cache-lines.*)
  | Write_through (*... update both in cache and in RAM at the same moment *)

(*When a cache miss occurs on a Store operation...*)
type write_miss_policy_t = 
  | Write_around (*... write directly in RAM without altering cache*)
  | Write_allocation (*... fetch line from RAM into cache, then write (only in cache with dirty bit=true in write_back mode, both in cache and RAM in write_thourgh mode)*)

type t =
{
  words_per_cache_line : int;
  align_addresses_on_cache : bool;
  eviction_policy : eviction_policy_t;
  write_policy : write_policy_t;
  write_miss_policy : write_miss_policy_t;
}

let build (wpcl : int) (align : bool) (ep : eviction_policy_t) (wp : write_policy_t) (wmp : write_miss_policy_t) : t =
  {
    words_per_cache_line = wpcl;
    align_addresses_on_cache = align;
    eviction_policy = ep;
    write_policy = wp;
    write_miss_policy = wmp;
  }

let classic : t = 
  (*let () = (Random.self_init ()) in*)
  {
    words_per_cache_line = 4;
    align_addresses_on_cache = true;
    eviction_policy = LRU(No_Usage_Impact_On_Protected_Lines);(* Pseudo_random_determinist(Random.get_state ());*)
    write_policy = Write_back;
    write_miss_policy = Write_allocation;
  }

let valid_address_alignement (configuration : t) (address : Int64.t) : (unit) =
    if  ((configuration.align_addresses_on_cache = false) || (Int64.rem address (Int64.of_int configuration.words_per_cache_line)) =0L)
    then ()
    else (print_endline ("Warning : Unaligned address " ^ Int64.to_string address))
      (*raise (Failure ("Unaligned address " ^ string_of_int address))*)

let generate_random (previous_state : Random.State.t) (bound : int) : (Random.State.t * int) =
  let () = (Random.set_state previous_state) in 
  let res = Random.int bound in
  let new_state = Random.get_state () in
  new_state, res
