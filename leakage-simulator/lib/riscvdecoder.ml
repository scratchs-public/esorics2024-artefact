


type inst_slice =  {
  funct7_slice : Bitstring.bitstring;
  rs2_slice : Bitstring.bitstring;
  rs1_slice : Bitstring.bitstring;
  funct3_slice : Bitstring.bitstring;
  rd_slice : Bitstring.bitstring;
  opcode : Bitstring.bitstring;
}(*Most instructions use the same 'slices' of bits, some slices will have to be merged for some instruction formats
  as exemple, I format instruction will have last_slice and rs2_slice concat in one immediate, I format inst do not have a rs2 field
*)

let rec string_of_bitstring (bits : Bitstring.bitstring) : string =
  match%bitstring bits with
  | {| head : 1 ; tail : -1 : bitstring |} -> (if head then "1" else "0") ^ string_of_bitstring tail
  | {|_|} -> ""

let string_of_slices (slices : inst_slice) : string =
  "Instructions slices:\n" ^
  "  funct7_slice:\t" ^ (string_of_bitstring slices.funct7_slice ) ^"\n"^
  "  rs2_slice:\t" ^ (string_of_bitstring slices.rs2_slice )^"\n"^
  "  rs1_slice:\t" ^ (string_of_bitstring slices.rs1_slice )^"\n"^
  "  funct3_slice:\t" ^ (string_of_bitstring slices.funct3_slice )^"\n"^
  "  rd_slice:\t" ^ (string_of_bitstring slices.rd_slice)^"\n"^
  "  opcode:\t" ^ (string_of_bitstring slices.opcode )^"\n"


let construct_bitstring_of_lenght (length : int) (value : int) : Bitstring.bitstring =
  
  let val64 = Int64.of_int value in 
  (*let _tempo_print = print_endline("\nint:" ^ string_of_int value ^ ", int64:" ^ Int64.to_string val64) in*)
  let%bitstring bits = {|
    val64 : length
  |} in (*let _tempo_print_hex = Bitstring.hexdump_bitstring stdout bits in*)
  bits



let construct_bitstring_of_int32 (value : Int32.t) : Bitstring.bitstring =
  let%bitstring bits = {|
  value : 32
  |} in (*let _tempo_print_hex = Bitstring.hexdump_bitstring stdout bits in*)
  bits


(*
let disp_reverse_endianess (bits : Bitstring.bitstring) : unit =
  match%bitstring bits with
  {| 
    b3 : 8 : unsigned; 
    b2 : 8 : unsigned; 
    b1 : 8 : unsigned;
    b0 : 8 : unsigned
  |} -> 
    let _disp1 = print_endline ("\nbefore reverse :" ^ (Printf.sprintf "%x %x %x %x" b3 b2 b1 b0)) in
    let _disp2 = print_endline ("\n after reverse :" ^ (Printf.sprintf "%x %x %x %x" b0 b1 b2 b3)) in
    ()*)


let rec split_bitstring (bits : Bitstring.bitstring) (split_size :Memo.size) : Bitstring.bitstring list =
  let nb_bits_per_slice = 8 * Memo.nb_bytes split_size in
  if (nb_bits_per_slice = (Bitstring.bitstring_length bits))
  then (bits :: [])
  else (
    if (nb_bits_per_slice > (Bitstring.bitstring_length bits))
    then (raise(Failure("split_bitstring works with bitstring of lenght being a multiple of 8 * nb bytes of " ^ Memo.size_to_string split_size)))
    else (
      match%bitstring bits with {| word_head : nb_bits_per_slice : bitstring; words_tail : -1 : bitstring |}
        ->   word_head :: (split_bitstring words_tail split_size)
    )
  )

let rec reverse_endianess (bits : Bitstring.bitstring) : Bitstring.bitstring =
  if (8 = (Bitstring.bitstring_length bits))
  then (bits)
  else (
    if (8 > (Bitstring.bitstring_length bits))
    then (raise(Failure("Reverse endianess works with bitstring of lenght being a multiple of 8 (split in bytes).")))
    else (
      match%bitstring bits with {| byte_head : 8 : bitstring; bytes_tail : -1 : bitstring |}
        ->  Bitstring.concat((reverse_endianess bytes_tail) :: byte_head :: [])
    )
  )(*TODO : redo with Bitstring.concat (List.rev (split_bitstring_in_bytes))*)

let inst_slices_of_bitstring (bits : Bitstring.bitstring) : inst_slice   = 
  (*let little_endian_bits = reverse_endianess bits in*)
  match%bitstring (bits) with
  | {|funct7_slice    : 7 : bitstring;
      rs2_slice     : 5 : bitstring;
      rs1_slice     : 5 : bitstring;
      funct3_slice  : 3 : bitstring;
      rd_slice      : 5 : bitstring;
      opcode        : 7 : bitstring
    |} -> let slices = {
      funct7_slice;
      rs2_slice;
      rs1_slice;
      funct3_slice;
      rd_slice;
      opcode;
    } in
    (*let _tempo_print = print_endline (string_of_slices slices) in*)
    slices
  | {| _ |} -> raise(Failure("Cannot slice the bitstring " ^ Bitstring.string_of_bitstring bits))




let reg_of_bitstring (bits : Bitstring.bitstring) : Regp.t =
  match%bitstring bits with
  | {| regIndex : 5  : unsigned|} -> Regp.build regIndex
  | {| _ |}  -> raise(Failure("Cannot make register form the bitstring " ^ Bitstring.string_of_bitstring bits))


  
let int32_of_bitstring (signed : Binm.param_signe) (expected_size : int) (bits : Bitstring.bitstring) (error_message : string) : Int32.t =
  match%bitstring bits with
  | {| immediate64 : expected_size :  unsigned |} -> 
    begin
      match immediate64>=0L, signed with
      | false, _ -> raise(Failure ("immediate64 " ^ Int64.to_string immediate64 ^ " should be unsigned (read from " ^ string_of_bitstring bits ^ ")\n "^error_message))
      | _, Binm.Unsigned -> Int64.to_int32 immediate64
      | _, Binm.Signed -> 
        match%bitstring bits with {| is_negative : 1; _ |} ->
          if (is_negative)
          then(Int64.to_int32 (Int64.sub immediate64 (Int64.shift_left 1L expected_size)))
          else(Int64.to_int32 immediate64)

    end
    (*let res = Int64.to_int immediate in 
    (*let _ = print_endline (
      "unsigned res = "^ string_of_int res
    ) in*)
    Binm.decode_larger_int (expected_size) (res) (0) (signed) (*convert unsigned to signed integer (two's complement)*)*)
  | {| _ |} -> raise(Failure(error_message))




(*funct7 is either 0000000 (true) or 0100000 (false), other values are error*)
let bool_of_funct7 (bits : Bitstring.bitstring) : bool =
  match%bitstring bits with 
  | {| 0b0000000 : 7 : unsigned |} -> true
  | {| 0b0100000 : 7 : unsigned |} -> false
  | {| _ |} -> raise(Failure("Unrecognized funct7 bits"))

let int_of_funct3 (bits : Bitstring.bitstring) : int =
  match%bitstring bits with 
  | {| funct3 : 3 : unsigned |} -> funct3
  | {| _ |} -> raise(Failure("Unrecognized funct3 bits"))






let inst_reg_reg_reg_of_builder_and_inst_slices (builder : Regp.t -> Regp.t -> Regp.t -> Inst.t) (slices : inst_slice) : Inst.t =
  let reg_operand1 = reg_of_bitstring slices.rs1_slice in
  let reg_operand2 = reg_of_bitstring slices.rs2_slice in
  let reg_resul = reg_of_bitstring slices.rd_slice in (*result not returned*)
  builder reg_resul reg_operand1 reg_operand2

  

(*Arithmetic*)
let inst_R_of_inst_slices (slices : inst_slice) : Inst.t =
  let builder = fun x y z ->
  ( match (int_of_funct3 slices.funct3_slice), (bool_of_funct7 slices.funct7_slice) with 
    | 0b000, true 
      -> Inst.Inst3(Add,x, y, z)

    | 0b000, false
      -> Inst.Inst3(Sub,x, y, z)

    | 0b001, true 
    -> Inst.Inst3(Sl,x, y, z)

    | 0b010, true 
    -> Inst.Inst3(Slt,x, y, z)

    | 0b011, true 
    -> Inst.Inst3(Sltu,x, y, z)

    | 0b100, true 
      -> Inst.Inst3(Xor,x, y, z)

    | 0b101, true 
      -> Inst.Inst3(Srl,x, y, z)

    | 0b101, false
      -> Inst.Inst3(Sra,x, y, z)

    | 0b110, true 
      -> Inst.Inst3(Or,x, y, z)

    | 0b111, true 
      -> Inst.Inst3(And,x, y, z)

    | _, _  -> raise(Failure("Unrecognized R format instruction"))
  ) in 
inst_reg_reg_reg_of_builder_and_inst_slices builder slices


(*Multiplication and Divisions*)
let inst_M_of_inst_slices (slices : inst_slice) : Inst.t =
  let builder = fun x y z ->
    (
      match%bitstring slices.funct3_slice with 
      | {| 0b000 : 3 : unsigned |} -> Inst.Inst3(Mul,x, y, z)
      | {| 0b001 : 3 : unsigned |} -> Inst.Inst3(Mulh,x, y, z)
      | {| 0b010 : 3 : unsigned |} -> Inst.Inst3(Mulhsu,x, y, z)
      | {| 0b011 : 3 : unsigned |} -> Inst.Inst3(Mulhu,x, y, z)
      | {| 0b100 : 3 : unsigned |} -> Inst.Inst3(Div,x, y, z)
      | {| 0b101 : 3 : unsigned |} -> Inst.Inst3(Divu,x, y, z)
      | {| 0b110 : 3 : unsigned |} -> Inst.Inst3(Rem,x, y, z)
      | {| 0b111 : 3 : unsigned |} -> Inst.Inst3(Remu,x, y, z)
      | {| bad_funct3 : 3 : int |} ->
        raise(Failure("Unrecognized funct3 " ^ string_of_int bad_funct3 ^ " for M format intruction"))
      
      | {| _ |} -> raise(Failure("funct3 must have a length of 3 bits"))
    ) in
    inst_reg_reg_reg_of_builder_and_inst_slices builder slices
  



let inst_I_format_of_inst_slices (builder : Regp.t -> Regp.t -> Int32.t -> Inst.t) (slices : inst_slice) : Inst.t =
  let imm_bits = Bitstring.concat(slices.funct7_slice :: slices.rs2_slice :: []) in
  let immediate = int32_of_bitstring Binm.Signed 12 imm_bits "Immediate of I format instruction must be on 12 bits" in
  let reg_operand = reg_of_bitstring slices.rs1_slice in
  let reg_resul = reg_of_bitstring slices.rd_slice in
  builder reg_resul reg_operand immediate


(*Integer Register-Immediate Instructions, instructions LOAD, JALR are I format inst, but not handled in this method*)
let inst_I_immediate_of_inst_slices (slices : inst_slice) : Inst.t =
  let builder = fun x y z ->
  ( 
    match%bitstring slices.funct3_slice with 
    | {| 0b000 : 3 : unsigned |} -> Inst.Inst2i(Addi,x,y,z) (*Inst.Inst2i(Addi,reg_resul, reg_operand, immediate)*)
    | {| 0b010 : 3 : unsigned |} -> Inst.Inst2i(Slti,x,y,z)
    | {| 0b011 : 3 : unsigned |} -> Inst.Inst2i(Sltiu,x,y,z)
    | {| 0b100 : 3 : unsigned |} -> Inst.Inst2i(Xori,x,y,z)
    | {| 0b110 : 3 : unsigned |} -> Inst.Inst2i(Ori,x,y,z)
    | {| 0b111 : 3 : unsigned |} -> Inst.Inst2i(Andi,x,y,z)
    | {| is_shift_right : 1 : int ; 0b01 : 2 : int |} (*some kind of shift immediate instruction*)
      ->(
        let shift_amount = int32_of_bitstring Binm.Unsigned 5 (slices.rs2_slice) "Immediate of Shift instruction must be on 5 bits" in
        if (is_shift_right)
        then
        (
          if (bool_of_funct7 slices.funct7_slice)
          then (Inst.Inst2i(Srli,x, y, shift_amount))
          else (Inst.Inst2i(Srai,x, y, shift_amount))
        )
        else
        (
          if (bool_of_funct7 slices.funct7_slice)
          then(
            (*let _ =
              print_endline ("decode Shift Left Immediate " ^ Regp.to_string_reduced x  ^
              " <- " ^  Regp.to_string_reduced y ^ " << " ^ string_of_int shift_amount )
            in*)
            Inst.Inst2i(Sli,x, y, shift_amount)
            )
          else ( raise(Failure("Unrecognized upper bits 0100000 for shift left immediate instruction")))
        )
      )
    | {| bad_funct3 : 3 : int |} ->
      raise(Failure("Unrecognized funct3 " ^ string_of_int bad_funct3 ^ " for I format intruction"))
    
    | {| _ |} -> raise(Failure("funct3 must have a length of 3 bits"))
  ) in
  inst_I_format_of_inst_slices builder slices


(*All Load instructions (I format)*)
let inst_I_load_of_inst_slices (slices : inst_slice) : Inst.t =
  let builder = fun x y z ->
  (
    match%bitstring slices.funct3_slice with 
    | {| 0b000 : 3 : unsigned |} -> Inst.Load(Memo.Byte, Binm.Signed, x, y, z)
    | {| 0b001 : 3 : unsigned |} -> Inst.Load(Memo.Halfword, Binm.Signed, x, y, z)
    | {| 0b010 : 3 : unsigned |} -> Inst.Load(Memo.Word, Binm.Signed, x, y, z)
    | {| 0b100 : 3 : unsigned |} -> Inst.Load(Memo.Byte, Binm.Unsigned, x, y, z)
    | {| 0b101 : 3 : unsigned |} -> Inst.Load(Memo.Halfword, Binm.Unsigned, x, y, z)
    | {| bad_funct3 : 3 : int |} ->
      raise(Failure("Unrecognized funct3 " ^ string_of_int bad_funct3 ^ " for I format intruction"))
    
    | {| _ |} -> raise(Failure("funct3 must have a length of 3 bits"))
  ) in
  inst_I_format_of_inst_slices builder slices

(*All custom protection instructions for memory (lock, unlock, lockWriteThrough ?, Link?, Rocs ?) (I format)*)
let inst_I_custom_protection_of_inst_slices (slices : inst_slice) : Inst.t =
  let builder = fun _x y z ->
  (
    match%bitstring slices.funct3_slice with 
    | {| 0b000 : 3 : unsigned |} -> Inst.LockCache(Mode.Write_Back_On_Unlock, y, z) (*For now, only one lock mode supported*)
    | {| 0b001 : 3 : unsigned |} -> Inst.UnlockCache(y, z) 
    | {| bad_funct3 : 3 : int |} -> raise(Failure("Unrecognized funct3 " ^ string_of_int bad_funct3 ^ " for I format custom intruction"))
    | {| _ |} -> raise(Failure("funct3 must have a length of 3 bits"))
  ) in
  inst_I_format_of_inst_slices builder slices


let inst_JALR_of_inst_slices (slices : inst_slice) : Inst.t =
  match%bitstring slices.funct3_slice with 
  | {| 0b000 : 3 : unsigned |} -> inst_I_format_of_inst_slices (fun x y z -> Inst.Jalr(x, y, z)) slices
  | {| _ |} -> raise(Failure("Invalid funct3 for JALR"))
  

  

(* , contains STORE*)
let inst_S_of_inst_slices (slices : inst_slice) : Inst.t =
  let imm_bits = Bitstring.concat(slices.funct7_slice :: slices.rd_slice :: []) in
  let immediate = int32_of_bitstring Binm.Signed  12 imm_bits "Immediate of S format instruction must be on 12 bits" in
  let reg_dest = reg_of_bitstring slices.rs1_slice in
  let reg_value = reg_of_bitstring slices.rs2_slice in
      match%bitstring slices.funct3_slice with 
      | {| 0b000 : 3 : unsigned |} -> Inst.Store(Memo.Byte, reg_value, reg_dest, immediate)
      | {| 0b001 : 3 : unsigned |} -> Inst.Store(Memo.Halfword, reg_value, reg_dest, immediate)
      | {| 0b010 : 3 : unsigned |} -> Inst.Store(Memo.Word, reg_value, reg_dest, immediate)
      | {| bad_funct3 : 3 : int |} ->
        raise(Failure("Unrecognized funct3 " ^ string_of_int bad_funct3 ^ " for S format intruction"))
      | {| _ |} -> raise(Failure("funct3 must have a length of 3 bits"))


let inst_JAL_of_inst_slices (slices : inst_slice) : Inst.t =
  let reg_rd = reg_of_bitstring slices.rd_slice in
  
  let imm_19_12 = Bitstring.concat(slices.rs1_slice :: slices.funct3_slice :: []) in
  let imm_20, imm_10_1, imm_11 =
    match%bitstring Bitstring.concat(slices.funct7_slice :: slices.rs2_slice :: [])  with
     {|
        imm_20 : 1  :  bitstring; 
        imm_10_1 : 10 :  bitstring;
        imm_11 : 1  :  bitstring
      |} -> imm_20,imm_10_1, imm_11 
  in

  let immediate = 
    int32_of_bitstring
    Binm.Signed 
    21
    (Bitstring.concat
      (
        imm_20 ::
        imm_19_12 ::
        imm_11 ::
        imm_10_1 ::
        (construct_bitstring_of_lenght 1 0) ::
      [] )
    )
    "Immediate of JAL instruction must be on 21 bits (20 encoded bits + imm[0] = 0)"
  in
      Inst.Jal (reg_rd, immediate)

(*Ecall & CSRR*)
let inst_control_of_inst_slices (slices : inst_slice) : Inst.t =
  match%bitstring slices.funct3_slice with 
  | {| 0b000 : 3 : unsigned |} -> Inst.Ecall
  | {| 0b001 : 3 : unsigned |} -> Inst.Bad_Instruction "CSRRW"
  | {| 0b010 : 3 : unsigned |} -> Inst.Bad_Instruction "CSRRS"
  | {| 0b011 : 3 : unsigned |} -> Inst.Bad_Instruction "CSRRC"
  | {| 0b101 : 3 : unsigned |} -> Inst.Bad_Instruction "CSRRWI"
  | {| 0b110 : 3 : unsigned |} -> Inst.Bad_Instruction "CSRRSI"
  | {| 0b111 : 3 : unsigned |} -> Inst.Bad_Instruction "CSRRCI"
  | {| bad_funct3 : 3 : unsigned |} ->
    raise(Failure("Unrecognized funct3 " ^ string_of_int bad_funct3 ^ " for Zicsr intruction"))
  | {| _ |} -> raise(Failure("funct3 must have a length of 3 bits"))


(*Branch*)
let inst_B_of_inst_slices (slices : inst_slice) : Inst.t =
  let imm_12,imm_10_5  = match%bitstring slices.funct7_slice  with
    | {| imm_12 : 1  :  bitstring;  imm_10_5 : 6 :  bitstring|}
    -> imm_12, imm_10_5
  in
  let imm_4_1, imm_11 = match%bitstring slices.rd_slice  with
      {| imm_4_1 : 4  :  bitstring;  imm_11 : 1 :  bitstring|}
    ->imm_4_1, imm_11
  in
  let immediate = 
    int32_of_bitstring Binm.Signed 
      13
      (Bitstring.concat (imm_12 :: imm_11 :: imm_10_5 :: imm_4_1 :: (construct_bitstring_of_lenght 1 0) :: []))
      "Error on immediate for B format instruction"
  in (*B format immediate*)

  let reg1 = reg_of_bitstring slices.rs1_slice in
  let reg2 = reg_of_bitstring slices.rs2_slice in

  match%bitstring slices.funct3_slice with 
  | {| 0b000 : 3 : unsigned |} -> Inst.Cmp(Beq,reg1,reg2, immediate)
  | {| 0b001 : 3 : unsigned |} -> Inst.Cmp(Bne,reg1,reg2, immediate)
  | {| 0b100 : 3 : unsigned |} -> Inst.Cmp(Blt,reg1,reg2, immediate)
  | {| 0b101 : 3 : unsigned |} -> Inst.Cmp(Bge,reg1,reg2, immediate)
  | {| 0b110 : 3 : unsigned |} -> Inst.Cmp(Bltu,reg1,reg2, immediate)
  | {| 0b111 : 3 : unsigned |} -> Inst.Cmp(Bgeu,reg1,reg2, immediate)
  | {| _ |} -> raise(Failure("Invalid funct3 for B format instruction"))

let concat_imm_of_U_format_slices (slices : inst_slice) : Int32.t =
  let f7 =
    match%bitstring slices.funct7_slice with
    | {| funct7 : 7  : unsigned|}->  funct7
    | {| _ |} -> raise(Failure("Invalid funct7 slice when reading U format immediate"))
  in let r2 =
    match%bitstring  slices.rs2_slice with 
    | {| rs2 : 5  : unsigned|}->  rs2
    | {| _ |} -> raise(Failure("Invalid rs2 slice when reading U format immediate"))
  in let r1 =
    match%bitstring  slices.rs1_slice  with 
    | {| rs1 : 5  : unsigned|}->  rs1
    | {| _ |} -> raise(Failure("Invalid rs1 slice when reading U format immediate"))
  in 
  let f3 = match%bitstring  slices.funct3_slice with 
    | {| funct3 : 3  : unsigned|}->  funct3
    | {| _ |} -> raise(Failure("Invalid funct3 slice when reading U format immediate"))
  in
  Int32.of_int (f3 + (r1 * Binm.power 2 3) + (r2 * Binm.power 2 8) + (f7 * Binm.power 2 13) )
  (*let _ =
    match%bitstring slices.funct7_slice with {| funct7 : 7  : unsigned|}-> 
      print_endline ("\nf7:" ^ (string_of_int funct7) )
    | {| _ |} ->print_endline "\nplof1!\n"
  in  let _ =
    match%bitstring  slices.rs2_slice with {| rs2 : 5 : unsigned|} -> 
      print_endline (
        "\nrs2:" ^(string_of_int rs2) )
    | {| _ |} ->print_endline "\nplof2!\n"
  in let _ =
    match%bitstring  slices.rs1_slice with {| rs1 : 5 : unsigned|} -> 
      print_endline (
        "\nrs1:" ^(string_of_int rs1) )
    | {| _ |} ->print_endline "\nplof2!\n"
  in  let _ =
    match%bitstring  slices.funct3_slice with {| funct3 : 3 : unsigned|} -> 
      print_endline (
        "\nf3:" ^ (string_of_int funct3)^".\n")
    | {| _ |} ->print_endline "\nplof!\n"
  in
  let bitstring_concat =
    Bitstring.concat (slices.funct7_slice :: slices.rs2_slice :: slices.rs1_slice :: slices.funct3_slice :: [] )
  in
  match%bitstring bitstring_concat with
  | {| immediate : 16  : unsigned|} -> let res = immediate in let _ = print_endline ("\n U_format imm: " ^string_of_int res) in res
  | {| _ |} -> raise(Failure ("Cannot read immediate of an U format instruction"))
  *)
  
(*
let opcode_R_half = Bitstring.make_bitstring 3 '\x03' (*   \b011      R format is 011*011  *)
let opcode_I_format = Bitstring.make_bitstring 7 '\x13' (*'\b0010011' pour non load, et 0000011 pour load*)
let opcode_S_format = Bitstring.make_bitstring 7 '\x23' (*'\b0100011'*)
let opcode_B_format = Bitstring.make_bitstring 7 '\x63' (*'\b1100011'  *)
let opcode_JAL = Bitstring.make_bitstring 7 '\x6F'  (*b1101111*)
let opcode_LUI = Bitstring.make_bitstring 7  '\x37' (*'\b0110111'*)
let opcode_AUIPC = Bitstring.make_bitstring 7 '\x17' (*'\b0010111' *)
*)


let inst_of_inst_slices (slices : inst_slice) : Inst.t =
  match%bitstring slices.opcode with
  | {| 0b011 : 3 : int; _joker : 1; 0b011 : 3  : int|} -> ( (*joker bit can be 0 or 1*)
    match%bitstring slices.funct7_slice with
    | {| 0b0000000 : 7  : unsigned|} -> inst_R_of_inst_slices slices
    | {| 0b0100000 : 7  : unsigned|} -> inst_R_of_inst_slices slices
    | {| 0b0000001 : 7  : unsigned|} -> inst_M_of_inst_slices slices
    | {| bad_funct7 : 7 : unsigned |} -> raise(Failure("Unrecognized funct7 0x " ^ (Printf.sprintf "%x" bad_funct7)))
  )
  | {| 0b0010011 : 7  : unsigned|} -> inst_I_immediate_of_inst_slices slices
  | {| 0b0000011 : 7  : unsigned|} -> inst_I_load_of_inst_slices slices
  | {| 0b0001011 : 7  : unsigned|} -> inst_I_custom_protection_of_inst_slices slices
  | {| 0b0100011 : 7 : unsigned |} -> inst_S_of_inst_slices slices
  | {| 0b1100011 : 7 : unsigned |} -> inst_B_of_inst_slices slices
  | {| 0b1110011 : 7 : unsigned |} -> inst_control_of_inst_slices slices (*Ecall & CSRR*)


(*reste : J (JAL), U (LUI et AUIPC),*)
  | {| 0b1101111 : 7 : unsigned |} -> inst_JAL_of_inst_slices slices
  | {| 0b1100111 : 7 : unsigned |} -> inst_JALR_of_inst_slices slices
  | {| 0b0110111 : 7 : unsigned |} -> Inst.Lui ((reg_of_bitstring slices.rd_slice), (concat_imm_of_U_format_slices slices))
  | {| 0b0010111 : 7 : unsigned |} -> Inst.Auipc ((reg_of_bitstring slices.rd_slice), (concat_imm_of_U_format_slices slices))
  | {| bad_opcode : 7 : unsigned |} -> raise(Failure("Unrecognized opcode 0x " ^ (Printf.sprintf "%x" bad_opcode)))
  | {| _ |} -> raise(Failure("Opcode must have a lenhgt of 7 bits"))

let inst_of_bitstring (bits : Bitstring.bitstring) : Inst.t =
  (*let _ = Bitstring.hexdump_bitstring stdout bits in*)
  let inst =
    try(inst_of_inst_slices (inst_slices_of_bitstring ((*reverse_endianess*) bits)))
    with error -> Inst.Bad_Instruction(Printexc.to_string error)
  in
  (*let _tempo_print = print_endline (Inst.to_string inst) in*)
  inst

let const_bitstring (value : int) : Bitstring.bitstring =
  construct_bitstring_of_lenght 32 value

let inst_of_int_depracated (encoded_inst : int) : Inst.t =
  let inst  =
    inst_of_bitstring (const_bitstring encoded_inst)
  in
  inst
  
let inst_of_int32 (encoded_inst : Int32.t) : Inst.t =
  inst_of_bitstring (construct_bitstring_of_int32 encoded_inst)

    

(*I format, addi x2, x2, 1 :  00F10113*)
let instruction_test_2 = Bitstring.concat (
  (Bitstring.make_bitstring 8 '\xFF') ::
  (Bitstring.make_bitstring 8 '\xF1') ::
  (Bitstring.make_bitstring 8 '\x01') ::
  (Bitstring.make_bitstring 8 '\x13') ::
  [] )

(*I format, addi x2, x2, -16*)
let instruction_test_1 = Bitstring.concat (
  (Bitstring.make_bitstring 8 '\xFF') ::
  (Bitstring.make_bitstring 8 '\x01') ::
  (Bitstring.make_bitstring 8 '\x01') ::
  (Bitstring.make_bitstring 8 '\x13') ::
  [] )

(**
let inst_addi_test = inst_of_bitstring instruction_test_1
*)
let test () : unit =
  let _ = print_endline "\n test riscV decoder desactive\n" (**
    "\n #Test de decodage avec bitstring : " ^ Inst.to_string inst_addi_test
  *) in
  ()

let rec inst_list_of_bitstring (bits : Bitstring.bitstring) : Inst.t list =
  if((Bitstring.bitstring_length bits) < 32)
  then (Inst.End :: []) (*Put halt instruction at the end, will be targeted by x1 register, i.e. return pointer*)
  else(
    let bits_of_next_inst = Bitstring.takebits 32 bits in
    let remainging_bits = Bitstring.dropbits 32 bits in
    let decoded_inst = inst_of_bitstring bits_of_next_inst in
    (*let _ = print_endline (Inst.to_string decoded_inst) in*)
    decoded_inst :: inst_list_of_bitstring remainging_bits
  )

(*
let rec convert_bytes_to_executable_memory_aux (memory : Memo.t) (bits : Bitstring.bitstring) (address : int) : Memo.t * int =
  if((Bitstring.bitstring_length bits) < 32)
  then(
    Memo.write
      memory 
      address
      (Inst.to_int Inst.End)
      Memo.Word
      (Binm.Unsigned)
  ), address (*Put halt instruction at the end, will be targeted by x1 register, i.e. return pointer*)
  else(
    let bits_of_current_word = Bitstring.takebits 32 bits in
    let remainging_bits = Bitstring.dropbits 32 bits in
    let updated_memory, final_return_address =
      (convert_bytes_to_executable_memory_aux 
        memory
        remainging_bits
        (address + Memo.nb_bytes Memo.Word)
      )
    in
    let loaded_word = try  (
      (*
        If the coming byte correspond to an instruction, it must be re-encoded
        to by compatible with the simulator
      *)
      let instruction =  (inst_of_bitstring bits_of_current_word) in
      let _debug =
        if (Conf.show_debug)
        then(print_endline ("\nLoaded instruction " ^ Inst.to_string instruction ^
        " goes at address " ^ string_of_int address))
        else ()
      in
      try(
      (Inst.to_int instruction)
      ) with e2 -> raise(Failure ("Instruction was decoded from elf as " ^ Inst.to_string instruction ^
        "\n but could not be re-encoded because \n:"^ (Printexc.to_string e2)))
    ) with e -> (*cannot be read as an instruction regular data.*)

        let value = (Int32.to_int
          (match%bitstring bits_of_current_word with {| value : 32 : unsigned |} -> value)
        ) in
        let val_as_bitstring  = (
          (match%bitstring bits_of_current_word with {| value : 32 : bitstring |} -> reverse_endianess value)
        ) in
        let _debug =
          if (Conf.show_debug)
          then(let _ = print_endline ("\nNot loadable as an instruction because \n:"^ (Printexc.to_string e) ^
          " value ="^ string_of_int value ^
          " will stay as raw data at address " ^ string_of_int address ^
          "\nexpressed as hexdump : ") in
          Bitstring.hexdump_bitstring stdout val_as_bitstring)
          else ()
        in
        value
      in 
      (Memo.write
        updated_memory 
        address
        loaded_word
        Memo.Word
        (Binm.Unsigned)
      ), final_return_address
  )
  
let convert_bytes_to_executable_memory (memory : Memo.t) (input : bytes) (address : int) : Memo.t * int =
  convert_bytes_to_executable_memory_aux
    memory
    (Bitstring.bitstring_of_string (Bytes.to_string input))
    address
*)

let rec display_instructions_of_bytes_aux (bits : Bitstring.bitstring) (address : int) : unit =
  if((Bitstring.bitstring_length bits) < 32)
  then(
    print_endline ("\n fin du segment d'instructions")
  )
  else(
    let bits_of_current_word = reverse_endianess (Bitstring.takebits 32 bits) in
    let remainging_bits = Bitstring.dropbits 32 bits in

    let address_and_value_to_string = 
      "\t" ^ (Printf.sprintf "%x" address) ^ ": " ^
        (Printf.sprintf "%x" (Int32.to_int(int32_of_bitstring (Binm.Unsigned) (32) (bits_of_current_word) ("bad bitstring")))) ^
        "\t\t"
    in

    let read_as = try  (
      (*
        If the coming byte correspond to an instruction, it must be re-encoded
        to by compatible with the simulator
      *)
      let instruction =  (inst_of_bitstring bits_of_current_word) in
      let _debug =
        if (Conf.show_debug)
        then(print_endline ("\nLoaded instruction " ^ Inst.to_string instruction ^
        " goes at address " ^ string_of_int address))
        else ()
      in
      Inst.to_string instruction
    
    ) with e -> "Unreadable as instruction" ^ (Printexc.to_string e)
      in 
    let _ = print_endline (address_and_value_to_string ^ read_as) in
    (display_instructions_of_bytes_aux 
      remainging_bits
      (address + Memo.nb_bytes Memo.Word)
    )
  )

let display_instructions_of_bytes (input : bytes) (address : int) : unit =
  display_instructions_of_bytes_aux
    (Bitstring.bitstring_of_string (Bytes.to_string input))
    address


let inst_list_of_bytes (input : bytes) (bytes_to_drop : int) (): Inst.t list =
  let full_bitstring =
    (Bitstring.dropbits (8 * bytes_to_drop) (Bitstring.bitstring_of_string (Bytes.to_string input)))
  in
  inst_list_of_bitstring full_bitstring
  
(*return memory where instruction are re-encoded to correspond to the simulator encoding, and the address of the added End instruction*)

let rec read_multiple_inst_in_bitstring (bits : Bitstring.bitstring) : string =
  if((Bitstring.bitstring_length bits) < 32)
  then ("\n There is not enought bits remaining for an instruction.")
  else(
    let bits_of_next_inst = Bitstring.takebits 32 bits in
    let remainging_bits= Bitstring.dropbits 32 bits in
    let decoded_inst  =
      try (Inst.to_string (inst_of_bitstring bits_of_next_inst))
      with e -> "Error when decoding inst :"^ Printexc.to_string e    (*"\n##\n##\n##\n##\n##\n## ERROR " ^ Printexc.to_string e)*) 
    in
    if(decoded_inst = "Error when decoding inst :Failure(\"Unrecognized opcode 0\")")
    then(read_multiple_inst_in_bitstring remainging_bits)
    else (
      "\n Instruction display  :" ^ decoded_inst ^
      read_multiple_inst_in_bitstring remainging_bits
    )
  )



let read_bytes_as_bitstring (input : bytes) (bytes_to_drop : int): string =
  try (
    let full_bitstring = (Bitstring.dropbits (8 * bytes_to_drop) (Bitstring.bitstring_of_string (Bytes.to_string input))) in
    (*let bitstrings_for_one_inst = Bitstring.takebits 32 full_bitstring in
    (Inst.to_string (inst_of_bitstring instruction_test_1)) ^*)
    (*let _ = print_endline ("\nHexdump de read_bytes_as_bitstring: \n") in
    let _ = Bitstring.hexdump_bitstring stdout full_bitstring in*)
    "\n ## TODO read_bytes_as_bitstring, \n full bitstring from input bytes : " ^
    (Bitstring.string_of_bitstring full_bitstring) ^
    "\nread_multiple_inst_in_bitstring : " ^ read_multiple_inst_in_bitstring full_bitstring
  ) with _e -> ""



let convert_bytes_to_memory (input : bytes) (begin_address : Int64.t) : Memo.t =
  let rec add_bitstring_to_memory (memory : Memo.t) (bits : Bitstring.bitstring list) (address : Int64.t) : Memo.t =
    match bits with 
    | [] -> memory
    | head_byte :: tail_bytes ->
      Memo.write
        (add_bitstring_to_memory memory tail_bytes (Int64.add address 1L))
        (address)
        (match%bitstring head_byte with {| byte : 8 : unsigned |} -> 
          (*let _ = print_endline (
            "\n byte : " ^ string_of_int byte ^ "\t as bits : " ^ (Printf.sprintf "%x" byte) ^ "\n Hexdump :")
          in
          let _ = Bitstring.hexdump_bitstring stdout head_byte in*)
          Int32.of_int byte
        )
        (Memo.Byte)
        (*Binm.Unsigned*)
  in
  let unified_bitstring = (Bitstring.bitstring_of_string (Bytes.to_string input)) in 
  (*let _ = print_endline ("\nHexdump de convert_bytes_to_memory: \n") in
  let _ = Bitstring.hexdump_bitstring stdout unified_bitstring in*)
  let splitted_bitstring = split_bitstring unified_bitstring Memo.Byte in
  add_bitstring_to_memory (Memo.empty ()) splitted_bitstring begin_address 
  
