(*used to specify which memory addresses are allowed for a process*)
type allocation =
{
  lower_bound : Int64.t;   
  upper_bound : Int64.t;
  stack_fence : Int64.t; (*used to detect stack overflow*)
}
let stack_fence_range : Int64.t = 8L (*range of forbidden addresses starting at stack_fence*)

let allocation_to_string (alloc : allocation) : string =
  Printf.sprintf
    "  Allowed addresses: 0x%Lx - 0x%Lx"
    alloc.lower_bound
    alloc.upper_bound
    (* alloc.stack_fence *)
    (* (Int64.add alloc.stack_fence stack_fence_range) *)

(*print_address is accessible to everyone.
  Store on print_address result in a display of the stored value (visible in the trace)
  All processes are allowed to access print_address*)
let print_address : Int64.t = 0x10000000L (*=268435456, avec Lui 65536*)


type byte = B of Int32.t
(*let empty () : byte = B(0)*)

(*do two's-complement if signed and raise exception in case of overflow*)
let write_byte (value : Int32.t)  : byte =
  (
    if
      (value < (Binm.power2 Binm.byte_size) && (value > -1l)) (*check for overflow*)
    then
      B(value)
    else
      raise(Failure("The value " ^ Int32.to_string value ^ "is overflowing, cannot be stored as a byte.\n")) 
  )

let read_byte (byte : byte) : Int32.t =
  match byte with B(value) -> value

let equals_bytes (byte1 : byte) (byte2 : byte) : bool =
  match byte1, byte2 with
  B(val1), B(val2) -> (val1=val2)


let byte_to_string (b : byte) : string =
  match b with B(value) -> (*Disp.string_of_int_with_padding value 11*)
    (
      if(value< 0x10l)
      then("0")
      else("")
    )^ (Printf.sprintf "%x" (Int32.to_int value)) ^""


type size =
  | Byte
  | Halfword
  | Word

let size_to_string (s : size) =
  match s with
  | Byte -> "Byte"
  | Halfword -> "Halfword"
  | Word -> "Word"


let nb_bytes (s : size) : int =
  match s with
  | Byte -> 1
  | Halfword -> 2
  | Word -> 4

let mask_of (s : size) : Int32.t =
  match s with
  | Byte ->     0x000000ffl
  | Halfword -> 0x0000ffffl
  | Word ->     0xffffffffl


let rec unsigned_int_of_bytes (byts : byte list): Int32.t =
  match byts with
  | [] -> 0l
  | B(head_value) :: tail ->
    let current_val =(
      Int32.mul
        (Binm.power2 ((List.length tail) * Binm.byte_size))
        (head_value)
    ) in
    Int32.add current_val (unsigned_int_of_bytes tail)


let sign_extend (unsigned : Int32.t) (sign : Binm.param_signe) (size : size) : Int32.t =
  match sign, size with
  | Binm.Unsigned, _ -> unsigned (*no sign extend to do for unsigned*)
  | _ , Word -> unsigned (*no sign extend to do for word*)
  | _,_ -> 
    let sign_bit_mask = (Int32.shift_left 1l ((Binm.byte_size * nb_bytes size)-1)) in
    if ((Int32.logand unsigned sign_bit_mask) = sign_bit_mask)
    then (Int32.logor unsigned (Int32.logxor (mask_of Word) (mask_of size))) (*is negative number, need sign extend on 32 bits*)
    else (unsigned) (*is positive number, no sign extend requuired*)

let split_int32_as_bytes (value : Int32.t) (size : size) : (byte list) =
  let rec split_in_list (valu : Int64.t) (expo : int) : (byte list) =
    if(expo = 0)
    then ( [])
    else (
      let pow = (Binm.power_int64 2L ((expo-1) * Binm.byte_size)) in
      match (Int64.div valu pow), (Int64.rem valu pow) with upper_bits,remainder
        -> B(Int64.to_int32 upper_bits) :: split_in_list remainder (expo-1)
    )
  in
  let positive_int64 = Binm.int32_as_positive_int64 value in
  split_in_list positive_int64 (nb_bytes size)


module IntMap = Map.Make(Int64)

let elem_to_string (key : Int64.t) (value : byte) : string =
  "<" ^ Int64.to_string key ^ "> :" ^ byte_to_string value ^"; "



type t = byte IntMap.t
(*keys are addresses (integer), values are bytes*)


let empty () : t = IntMap.empty

let equals (mem1 : t) (mem2 : t) : bool =
  IntMap.equal equals_bytes mem1 mem2

let to_string (map : t) : string =
  IntMap.fold (fun k v prev -> (prev ^ elem_to_string k v)) (map) ("")
  (*fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b*)

let get_lower_bound (memo : t) : Int64.t =
  match IntMap.min_binding_opt memo with
  | Some(lower_address, _val) -> lower_address
  | None -> raise(Failure("Cannot find lower bound of an empty memory"))
  
let get_upper_bound (memo : t) : Int64.t =
  match IntMap.max_binding_opt memo with
  | Some(higher_address, _val) -> higher_address
  | None -> raise(Failure("Cannot find upper bound of an empty memory"))


let rec to_string_as_ram (ram : t) (begin_address : Int64.t) (end_address :  Int64.t) : string =
  if (begin_address >= end_address)
  then ("")
  else(
    (
      if ((Int64.rem begin_address 32L) = 0L)
      then (
        "\n [" ^ Disp.string_of_int64_with_padding begin_address 3 ^ "~"
        ^ Disp.string_of_int64_with_padding (Int64.add begin_address 31L) 3 ^"]"
        )
      else ("")
    ) ^ (
      if (( Int64.rem begin_address (Int64.of_int (nb_bytes Word))) = 0L)
      then ("   ")
      else (" ")
    ) ^ (
      match IntMap.find_opt begin_address ram with
        | None -> "00"
        | Some(found_byte) -> byte_to_string found_byte 
    ) ^ to_string_as_ram ram (Int64.add begin_address 1L) end_address
  )


let to_string_as_ram_only_filled_parts (ram : t) : string =
  to_string_as_ram ram (get_lower_bound ram) (Int64.add 1L (get_upper_bound ram))



let display (map : t) : unit =
  (IntMap.iter (fun k v -> print_string (elem_to_string k v) (*"<"^ string_of_int k ^ ">:"^v^"\n"*)) map)

(*On suppose la memoire infinie, si on veut ecrire a la case n + m alors que la liste est de taille n,
on rajoute m+1 cases memoire vides a la fin puis on ecrit dans la derniere 
let rec write_in_fresh_memory (address : int) (value : int) (_isSigned : Binm.param_signe) : (t) =
  if (address = 0)
  then ((write_byte value) :: [])
  else B(0) :: write_in_fresh_memory (address-1) (value) (_isSigned)
*)


(*
let rec write_in_fresh_memory_from_memory (begin_address : int) (copied_memory : t) : (t) =
  if (begin_address = 0)
  then (copied_memory)
  else (B(0)) :: write_in_fresh_memory_from_memory (begin_address-1) (copied_memory)
*)

let write_from_memory (receiving_memory: t) (*begin_address : int*) (copied_memory : t) : (t) =
  let merging_fun (_key : Int64.t) (b1: byte option)  (b2 : byte option) : byte option =
    match b1,b2 with
    | None, None                -> None
    | _, Some(new_byte)         -> Some(new_byte) 
    | Some(previous_byte), None -> Some(previous_byte)
  in
  IntMap.merge (merging_fun) (receiving_memory) (copied_memory)


let rec write_from_byte_list (receiving_memory: t) (address : Int64.t) (writen_bytes : byte list) : (t) =
  match  writen_bytes with 
  | [] -> receiving_memory (*nothing more to copy*)
  | current_byte :: next_bytes ->
    IntMap.add
      address
      current_byte
      (write_from_byte_list
        receiving_memory
        (Int64.add address 1L)
        next_bytes
      )


let write (memory: t) (address : Int64.t) (new_value : Int32.t) (size : size) : (t) =
  let value_as_byte_list = List.rev (split_int32_as_bytes new_value size) in (*Reverse list because little endian memory*)
  let _check_input_size = 
    if( List.length value_as_byte_list > nb_bytes size)
    then (raise(Failure("The byte list is too big ("^ string_of_int (List.length value_as_byte_list) ^") compare to its expected size :" ^ size_to_string size ^ ".\n")) )
    else ()
  in 
  write_from_byte_list memory address value_as_byte_list


let write_string (memory: t) (address : Int64.t) (input : string) : (t) =
  let string_as_chars : char list = 
    List.init
      (String.length input)
      (fun i -> String.get input i)
  in
  let string_as_bytes : byte list = 
    List.map
      (fun letter -> write_byte (Int32.of_int (Char.code letter)))
      string_as_chars
  in
  write_from_byte_list memory address string_as_bytes



let rec set_input_args_aux (ram : t) (args : string list)  (pointer_address : Int64.t): t =
  match args with 
  | [] -> ram
  | head_arg :: tail_args -> (
      let updated_ram = set_input_args_aux ram tail_args (Int64.add pointer_address (Int64.of_int (nb_bytes Word))) in
      let arg_address =  Int64.add 8L (Binm.align_address_on_byte (get_upper_bound updated_ram)) in
      let updated_ram_with_pointer : t = write updated_ram pointer_address (Binm.positive_int64_as_int32 arg_address) Word in
      (write_string updated_ram_with_pointer arg_address head_arg)
    )

let set_input_args (ram : t) (args : string list) : t * Int64.t =
  let first_free_address = Int64.add 8L  (Binm.align_address_on_byte (get_upper_bound ram)) in
  let ram_with_space_allocated_for_args_addressing : t =
    write ram (Int64.add first_free_address (Int64.of_int ((nb_bytes Word)*List.length args))) 1l Word (*increase the upper_bound of used ram to book space of argv pointers *)
  in
  (set_input_args_aux ram_with_space_allocated_for_args_addressing args first_free_address), first_free_address

    
let rec get_bytes (memory: t) (address : Int64.t) (amount : int): (byte list) =
  if (amount =0)
  then ([])
  else (
    (
      match IntMap.find_opt address memory with
      | None -> B(0l) (*No bindings means empty byte = 0*)
      | Some(found_byte) -> found_byte 
    )
    :: get_bytes memory (Int64.add address 1L) (amount-1)
  )

let read (memory: t) (address : Int64.t) (extend : Binm.param_signe) (size : size) : Int32.t =
  let unsigned_tmp_res =
    unsigned_int_of_bytes
      (List.rev (get_bytes memory address (nb_bytes size))) (*Reverse list because little endian memory*)
  in
  let res = sign_extend unsigned_tmp_res extend size in
  let _debug =
    if (Conf.show_debug)
    then(print_endline ("read " ^ Int32.to_string res ^" at address"  ^ Int64.to_string address))
    else ()
  in
  res
    

let to_string_as_line (memory : t) (begin_address : Int64.t) (number_of_bytes : int): string =
  let bytes_to_display =
    get_bytes
      memory
      begin_address
      number_of_bytes
  in
  List.fold_left
    (fun strA strB -> strA ^ " " ^ strB)
    ""
    (List.map
      byte_to_string
      bytes_to_display
    )


(*
let rec empty_line_of_bytes (line : t) : (t) =
  match line with
  | [] -> []
  | _previous_word :: tail -> B(0) :: empty_line_of_bytes tail
*)




let of_int_list (input : Int32.t list) (starting_address : Int64.t): (t) =
  (*List.fold_left
    (fun memory new_val -> write memory (starting_address+offset?) new_val Word isSigned)
    (empty ())
    (input)
  fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a*)
  let rec write_int_list_in_mem (mem : t) (inp : Int32.t list) (address : Int64.t) : t =
    match inp with
    | [] -> mem
    | head :: tail ->
      let _debug =
        if (Conf.show_debug)
        then(print_endline ("\naffichage de Memo.of_int_list " ^ Int32.to_string head))
        else ()
      in
      write
        (write_int_list_in_mem mem tail (Int64.add address (Int64.of_int (nb_bytes Word))))
        (address)
        head
        Word
  in
  write_int_list_in_mem (empty ()) input starting_address

(*return the line of words from RAM that contains the targeted address*)
let fetch_data_line_from_RAM (ram : t) (address : Int64.t) : (t) =
  (*match (Binm.compute_lower_address_limit address) with (*get the lower address limit, which would be the tag of the corresponding cache line*)
  | lower_address_limit -> List.flatten(
      fetch_data_line_from_RAM_aux memory_space lower_address_limit (lower_address_limit + Binm.nb_words_per_cache_line)
    )
  *)
  let lower_bound = Binm.compute_lower_address_limit address in
  let upper_bound = (Int64.add lower_bound  (Int64.of_int (Binm.nb_words_per_cache_line * (nb_bytes Word)))) in
  (*IntMap.filter (fun key _val -> (key>= lower_bound && key < upper_bound)) memory_space
     (*this don't put 0 in empty bytes of the cache line*)
  *)
  let rec get_cache_line_from_ram (mem: t) (address : Int64.t) (end_address : Int64.t) : t =
    if (address<end_address)
    then( IntMap.add
      address
      (match IntMap.find_opt address ram with
        | None -> B(0l) (*No bindings means empty byte = 0*)
        | Some(found_byte) -> found_byte
      ) 
      (get_cache_line_from_ram
        mem
        (Int64.add address 1L)
        end_address
      )
    )
    else (mem)
  in let res = get_cache_line_from_ram (empty ()) lower_bound upper_bound in
  let _debug =
    if (Conf.show_debug)
    then(print_endline ("Fetched line :\n" ^ to_string res))
    else ()
  in
  res



let rec to_string_as_list_aux (ram : t) (begin_address : Int64.t) (end_address : Int64.t) : string =
  if (begin_address >= end_address)
  then ("\n")
  else(
    "\n  " ^ (Int64.to_string begin_address) ^ " = "
    ^ (Int32.to_string (read ram begin_address Binm.Unsigned Byte))
    ^ to_string_as_list_aux ram (Int64.add begin_address 1L) end_address
  )


let to_string_as_list (ram : t) : string =
to_string_as_list_aux ram (get_lower_bound ram) (Int64.add 1L (get_upper_bound ram))


let test_equal_ram_except_one_value (ram1 : t) (ram2 : t) (address : Int64.t) (new_value : Int32.t) (size : size) : unit =
  let ram1_updated = write (ram1) (address) (new_value) (size) in
  if (IntMap.equal (fun x y -> x = y) ram1_updated ram2)
  then () (*both ram are equals, except for the new written value*)
  else (
    let () =  print_endline ("Error while checking equality of two RAMs (execpt for one update at address <" ^ Int64.to_string address ^ ">\n")  in
    let differences_list = (
      IntMap.fold
        (fun k v accu -> if(Some v = IntMap.find_opt k ram2) then accu else (k::accu))
        ram1_updated
        [] 
    ) in

    let opt_byte_to_string b = match b with
      | None -> "  None  "
      | Some value -> byte_to_string value
    in
    let () = List.iter
      (fun key -> print_endline ("<" ^ Int64.to_string key ^ "> :" ^ opt_byte_to_string (IntMap.find_opt key ram1_updated) ^" =/= "^ opt_byte_to_string (IntMap.find_opt key ram2) ^"\n"))
      differences_list
    in

    failwith("Error while updating RAM")
  )


(*check_correctness not implemented yet
let test_equal_ram_except_one_value (ram1 : t) (ram2 : t) (address : Int64.t) (new_value : Int32.t) (size : size) : unit =
  let ram1_updated = write (ram1) (address) (new_value) (size) in
  if (IntMap.equal (fun x y -> x = y) ram1_updated ram2)
  then () (*both ram are equals, except for the new written value*)
  else (
    let () =  print_endline ("Error while checking equality of two RAMs (execpt for one update at address <" ^ Int64.to_string address ^ ">\n")  in
    let differences_list = (
      IntMap.fold
        (fun k v accu -> if(Some v = IntMap.find_opt k ram2) then accu else (k::accu))
        ram1_updated
        [] 
    ) in

    let opt_byte_to_string b = match b with
      | None -> "  None  "
      | Some value -> byte_to_string value
    in
    let () = List.iter
      (fun key -> print_endline ("<" ^ Int64.to_string key ^ "> :" ^ opt_byte_to_string (IntMap.find_opt key ram1_updated) ^" =/= "^ opt_byte_to_string (IntMap.find_opt key ram2) ^"\n"))
      differences_list
    in

    failwith("Error while updating RAM")
  )
    *)
