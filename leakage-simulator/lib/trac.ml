 
(*Trace lists are reversed : most recently produced trace will be at the head of the list*)
type t = Step.t list

let empty () : t = []

let add (trace : t) (step : Step.t) : t =
  (step :: trace)

let rec count_hw_event (trace :t) (event : Evhw.t) : int = 
  match trace with 
  | [] -> 0
  | step :: trace_tail -> (Event.contains_hw_event step.events event) + count_hw_event trace_tail event


let get_all_prints (trace : t) (condensed : bool): string =
  (List.fold_left
    (^)
    ("")
    (List.mapi
      (fun index step ->
        match Step.get_print step with
        | None -> ""
        | Some(text) -> (if(condensed)then("")else("\nStep " ^ (string_of_int index) ^ ": ")) ^ text
      )
      trace
    )
  )

let rec display_executed_instructions (trace : t) : string =
  match trace with 
  | [] -> ""
  | step :: tail -> (Inst.to_string step.instruction) ^"\n" ^ (display_executed_instructions tail)

let summary (trace : t) : string =
  string_of_int (List.length trace) ^ " steps with a total of " ^
  string_of_int (count_hw_event trace Evhw.Cache_miss ) ^ " cache miss.\n" ^
  "Print :" ^ get_all_prints trace true ^ get_all_prints trace false


let rec fprint_aux out (trace : t) (count : int) (display_address: Int64.t option): unit =
  match trace with 
  | [] -> Printf.fprintf out "\nEnd of trace."
  | head :: tail ->
    Printf.fprintf out "\n############################\n# Step %d\n" count;
    let memory_updated = Step.fprint_adaptative out head display_address in
    Printf.fprintf out "\n";
    fprint_aux out tail (count+1) (memory_updated)

let fprint out (trace : t) : unit =
  let l_bound, h_bound = 70000L, 74000L in (*display final state of ram between these bounds*)
  (*"executed instructions :\n" ^ (display_executed_instructions trace) ^*)
  let trace_rev = (List.rev trace) in
  Printf.fprintf out "%s" (summary trace_rev) ;
  fprint_aux out trace_rev 1 None; (*Start do display end of list first (older steps first)*)
  Printf.fprintf out "\n\nFinal state of RAM between %Lx and %Lx\n" l_bound h_bound;
  Printf.fprintf out "%s" (Memo.to_string_as_ram ((List.hd trace).system_state.ram) l_bound h_bound)


let fprint_leak  out (trace : t) : unit =
  let fprint_leak_aux out (count : int) (step : Step.t) : unit =
    Printf.fprintf out "\n############################\n# Step %d\n" (count+1);
    Step.fprint_inst_and_leak out step;
    Printf.fprintf out "\n"
  in
  Printf.fprintf out "Trace with instructions and leaks (SW and HW):\n";
  List.iteri (fprint_leak_aux out) (List.rev trace);
  Printf.fprintf out "\nEnd of trace."


(*let to_buffer (trace :t) 

  let leak_to_buffer (trace : t) : Buffer.t =
  *)


let cache_history_to_csv_string  (trace : t) : string =

  let nb_processes =
    match trace with
    | [] -> print_endline("Warning : no cache history to display because the trace is empty."); 0
    | first_step :: _tail -> (* we suppose that all processes are already loaded before first step.*)
       List.length first_step.system_state.os.processes
  in
  let first_line =
    List.fold_left
    ( ^)
    ("step")
    (List.init
      (nb_processes)
      (fun pid -> (Printf.sprintf ", p%i_misses, p%i_hits, p%i_protected, p%i_lock " pid pid pid pid))
    )
  in
  (List.fold_left
    ( ^)
    (first_line)
    (List.mapi
      (fun count step -> Step.cache_history_to_csv_string step count nb_processes)
      (List.rev trace)
    )
  )




(*
 will generates files like prefix_process_1_hits.csv, prefix_process_2_misses.csv, etc.    


let rec print_out_cache_history_aux (trace : t) (step_by_step_history : 'a list) : 'a list = 
  match trace with
  | [] -> step_by_step_history
  | step :: tail -> 
    let updated_history = Step.cache_history_of_step step step_by_step_history in
    print_out_cache_history_aux tail updated_history

let print_out_cache_history (trace : t) (output_file_prefix : string) : unit =
  match trace with
  | [] -> print_endline("Warning : no cache history to export in files prefixed by " ^ output_file_prefix ^ " because the trace is empty.")
  | first_step :: tail -> (* we suppose that all processes are already loaded before first step.*)
    let nb_processes = List.length first_step.system_state.os.processes in
    let empty_step_by_step_history =
      List.init
        (nb_processes)
        (fun _ -> {misses = []; hits = []; locks = []; protected = []}) in (*3 lists per processes : cache misses, cache hits, access on locked address*)

    let complete_step_by_step_history = Trac.fill_step_by_step_cache_history trace empty_step_by_step_history in



  raise(Failure("TODO export cache history of trace ["^ (summary trace) ^ "] in files prefixed by " ^ output_file_prefix))
*)



let last_to_string (_trace :t) : string =
  raise(Failure("TODO last elements of Trace to string"))
