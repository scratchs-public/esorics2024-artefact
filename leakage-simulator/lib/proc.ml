(*process state*) 

(*
New − The process is being created.
Running − In this state the instructions are being executed.
Waiting − The process is in waiting state until an event occurs like I/O operation completion or receiving a signal.
Ready − The process is waiting to be assigned to a processor.
Terminated − the process has finished execution.
*)

type state =
  | New (*Not used*)
  | Running
  | Waiting (*Not used yet, would be useful if a more complex OS was added that was able to pause and resume processes*)
  | Ready
  | Terminated

type t =
{
  pc : Int32.t;   (*Program Counter*)
  ct : bool;  (*Constant Time state (False = regular execution, True = Constant Time secure execution)*)
  registers : Regp.r_list;
  activity_state : state;
}

let save (new_pc : Int32.t) (new_ct : bool) (new_registers : Regp.r_list) : t =
{
  pc = new_pc;
  ct = new_ct;
  registers = Array.copy new_registers;
  activity_state = Ready;
}

let to_string (proc : t) : string =
  "PC =" ^ Int32.to_string proc.pc ^ "; CT =" ^ string_of_bool proc.ct ^ "\nRegisters : " ^ (Regp.list_to_string proc.registers) ^ "\n"
