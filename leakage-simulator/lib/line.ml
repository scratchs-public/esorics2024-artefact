type t =
{
  tag : int option;
  dirty : bool;     (*Ignored in write-through configuration*)
  protection : Prot.t;
  content : Memo.t; (*nb max of word per memory word is fixed by (Binm.nb_words_per_cache_line)=(power 2 Binm.nb_bits_encoding_word_position)*)
  first_address : Int64.t;
}
(**There is no validity bit or type bit in this representation
  Invalid lines are represented by a line with None tag in this model
  We only handle split caches (L1i and L1d) in this model for now, Type bit will be added if we want to add unified caches    
*)


let nb_bytes_per_cache_line = (Binm.nb_words_per_cache_line * (Memo.nb_bytes Word))

let empty () : t =
  {
    tag = None;
    dirty = false;
    protection = Prot.Unprotected;
    content = Memo.empty (); 
    first_address = -1L; (*unecessary in real system but makes it easier to handle in simulation, especially for display functions*)
  }


let build (tag : int) (protection : Prot.t) (line_content : Memo.t) (first_address : Int64.t) : t =
  {
    tag = Some(tag);
    dirty = false;
    protection = protection;
    content = line_content; 
    first_address = first_address;
  }
    
let match_tag (tagA : int) (line : t) : bool =
  match line.tag with
  | None -> false
  | Some(tagB) ->
    if (tagA != tagB)
    then (false)
    else (true)
    

let is_evictable (line : t) : bool =
  Prot.is_evictable line.protection



let is_dirty (line : t) : bool =
  line.dirty
     

let set_protection (line : t) (new_protection : Prot.t): (t) =
  {line with
    protection = new_protection;
    dirty = match new_protection with 
      | Prot.Lock(_mode) -> true (*We set locked lines as dirty, even without write back policy. (Write in RAM could leak address, wait until after Unlock to write back)*)
      | _p -> raise(Failure("set_protection for " ^ Prot.to_string new_protection ^" not implemented.\n Should it change dirtiness of the line ?")) 
  }

let get_protection (line : t) : (Prot.t) = line.protection

let get_tag (line : t) : (int option) = line.tag

let get_memory (line : t) : (Memo.t) = line.content

    
let to_string (line : t) (*nb_sets : int) (set_id : int*): string =
    (*let first_address = 
      (line.tag * nb_bytes_per_cache_line * nb_sets)
      + (set_id * (nb_sets)) 
    in*)
    (match line.tag with
      | None -> "    None   "(*Word.to_string(Word.empty ())*)
      | Some(tag_value) -> Disp.string_of_int_with_padding tag_value 11
    ) ^ "|" ^
    (match line.dirty with
      | true -> " Yes " 
      | false -> "  No " 
    ) ^ "|" ^
    (Prot.to_string line.protection) ^ "|" ^
    (Memo.to_string_as_line line.content (line.first_address) ((Binm.nb_words_per_cache_line * (Memo.nb_bytes Word))))



let read (line : t) (address : Int64.t) (size : Memo.size) (isSigned : Binm.param_signe)  : Int32.t = 
  if (
    address < line.first_address
    ||
    address > (Int64.add (Int64.of_int (Binm.nb_words_per_cache_line * (Memo.nb_bytes Word))) line.first_address)
  )
  then (
    raise(Failure ("address " ^ Int64.to_string address ^ " is invalid for read on cacheline starting at address " ^
    Int64.to_string line.first_address^ "\n which contains: " ^ to_string line ))
  )
  else(
    let value = Memo.read (line.content) ((*Binm.map_address_to_word_id*) address) (isSigned) (size) in
    (*let _tempo_print = print_endline
    ("got value "^ Int32.to_string value^" at address " ^ Int64.to_string address ^ " at read on cacheline starting at address " ^
    Int64.to_string line.first_address^ "\n which contains: " ^ to_string line )
    in*)
    value
  )

  let update_on_store (line:t) (address : Int64.t) (new_value : Int32.t) (write_policy : Conf.write_policy_t) (size : Memo.size) : t =
    (*match line with CacheLine(tag,_dirty,protection,content) ->
      CacheLine(tag, true, protection, (Memo.write (content) (word_id) (new_value) (Binm.Unsigned)))*)
    match write_policy with
    | Write_through -> {line with content = (Memo.write (line.content) (address) (new_value) (size))}
    | Write_back -> {line with
        dirty = true;
        content = (Memo.write (line.content) (address) (new_value) (size));
      }