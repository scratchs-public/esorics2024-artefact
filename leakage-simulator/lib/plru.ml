(*Pseudo LRU with a binary tree*)
(*NOT USED FOR NOW*)
type direction = 
| Up
| Down

let direction_to_bool (d : direction) : bool =
  match d with
  | Up -> true
  | Down -> false


type node =
| Tree of t
| Leaf of int (*index of a way in the cache set*)
and t = {
  next_to_evict : direction;
  up : node;
  down : node;
  locked : bool;
}


let rec update_tree_for_access_on_line (n : node) (line_index : int) : node option =
  match n with 
  | Leaf (index) -> if (index = line_index) then (Some(n)) else (None)
  | Tree (tree) ->
    if (tree.locked = false)
    then (None) (*if this node is locked, no update to do here*)
    else
    (
      match update_tree_for_access_on_line (tree.up) (line_index) with
      | Some(updated_up_node) -> Some(Tree
          ({tree with 
            next_to_evict = Down;
            up = updated_up_node;
          })  (*line_index found, tree returned with up_branch updated*)
        )
      | None ->
        (match update_tree_for_access_on_line (tree.down) (line_index) with
          | Some(updated_down_node) -> Some(Tree
              ({tree with 
                next_to_evict = Up;
                down = updated_down_node;
              })  (*line_index found, tree returned with down_branch updated*)
            )
          | None -> None (*line_index was not found in both branches*)
        )
    )










(*
let designate_next_evicted (lines : list t) : int =
  match lines with
  |
*)