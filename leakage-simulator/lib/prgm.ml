type t =
{
  memory_allocation : Memo.allocation; (*allocated RAM for this process*)
  initial_memory : Memo.t;
  initial_pc : Int32.t;   (*Program Counter of the first instruction, = entry point of the program*)
  initial_return_address : Int32.t; (*Initial and final return address, should go to an End instruction to halt the program*)
  initial_stack_pointer : Int32.t; (*initial value of x2 (designated stack pointer register in RISC-V standards), usually = memory_end*)
  initial_global_pointer : Int32.t; (*initial value of x3*)
  initial_argc : Int32.t ; (*initial values of x10 (correspond to a0, function arguments)*)
  initial_argv_pointer : Int32.t ; (*initial values of x11 (correspond to a1, function arguments)*)
}


let build (mem_begin : Int64.t) (mem_end : Int64.t)  (initial_data : Int32.t list) (initial_inst : Int32.t list) : t =
  let encoded_memory = (Memo.of_int_list (initial_data @ initial_inst) mem_begin) in 
  let final_instruction_address = Memo.get_upper_bound encoded_memory in
  let _check_initial_memory_fit_in_allocated_memory =
    if (mem_end < final_instruction_address)
    then (raise(Failure("Initial memory of process does not fit in allocated memory [" ^Int64.to_string mem_begin  ^ "-" ^Int64.to_string mem_end  ^ "]")))
    else ()
  in
  {
    memory_allocation = {
      Memo.lower_bound = mem_begin;
      Memo.upper_bound = mem_end;
      Memo.stack_fence = Int64.add mem_end 1L; (*No stack fence defined for .x prgm for now*)
    };
    initial_memory = encoded_memory;
    initial_pc = Binm.positive_int64_as_int32 (Int64.add mem_begin (Int64.of_int ((List.length initial_data) * Memo.nb_bytes Memo.Word)));
    initial_return_address = Binm.positive_int64_as_int32 final_instruction_address;
    initial_stack_pointer = Binm.positive_int64_as_int32 mem_end;
    initial_global_pointer = 0l; (*Simulator does not initialized the global pointer itself*)
    initial_argc = 0l;
    initial_argv_pointer = 0l;
  }
