open Leakage_simulator_lib
open Elf

let () = Printexc.record_backtrace true (*display stack trace on exception*)



let count_steps: int ref = ref 0
let count_miss: int ref = ref 0
let count_hits: int ref = ref 0
let count_cycles: int ref = ref 0

let count_only = ref false


(*TODO : deplacer une partie de run dans des methodes de syst.ml*)
let rec run (sys : Syst.t) (trace : Trac.t) (step_number : int) : Trac.t =
  let sys = Syst.update_schedule sys in
  Os.address_allowed sys.os (Binm.int32_as_positive_int64 sys.pc) Memo.Word
    (Printf.sprintf "PC on illegal address for process %d" sys.os.current_pid);
  let decoded_instruction =
    Riscvdecoder.inst_of_int32
      (Memo.read sys.ram (Binm.int32_as_positive_int64 sys.pc) Binm.Unsigned Memo.Word) in
  if Conf.show_debug then print_endline (Inst.to_string decoded_instruction);
  let step_builder = Step.build sys decoded_instruction in
  match Inst.error_of_bad_instruction decoded_instruction with
    | Some(error_message) ->
      Trac.add trace (step_builder (Event.build(Evsw.Error(error_message)) Instclass.Ecall))
    | None ->
      (*try*) (*try catch with cause stack overflow ? -> Slow a lot execution + do stack overflow for big exec*)
      match Inst.process_instruction decoded_instruction sys with
        | None ->
          (*Currenlty we stop the simulation as soon as one process reach its end instruction. Must be upgraded to allwo multiple processes simulation :
             only remove a processes from running list when end instruction reached, stop simulation when all processes are stopped*)
          Printf.printf "Process stopped with return value %s\n" (Regp.index_to_string 10 sys.registers);
          (Trac.add trace
            (step_builder
              (Event.build(Evsw.Error("TODO : Process termination handling : this should only stop this process with state 'Terminated', not stop the whole simulation. Should be corrected for multiple processes simulations")) Instclass.Ecall)
            )
          )
        | Some (step_leak, sys') ->
          let trace =
            if(sys.record_execution)
            then(
              count_steps := !count_steps +1;
              count_miss := !count_miss + (Event.contains_hw_event step_leak Evhw.Cache_miss);
              count_hits := !count_hits + (Event.contains_hw_event step_leak Evhw.Cache_hit);
              count_cycles := !count_cycles + (Event.cycle_count step_leak);
              if(!count_only) then trace
              else Trac.add trace (step_builder step_leak)
            )
            else (trace (*dont record anything*))
          in
          run (*[@tailcall] Todo*) sys' trace (step_number + 1)
      (*with run_error -> (*catch error during process instruction, stop execution*)
          let msg = Printexc.to_string run_error
        and stack = Printexc.get_backtrace () in
      Printf.eprintf "there was an error: %s%s\n" msg stack;
      (Trac.add trace
        (step_builder
          (Event.build(Evsw.Error("TODO : Process termination handling")) Instclass.Ecall)
        )
      )*)



(* open Lexing *)
(* open Printf *)

(* let print_position outx lexbuf = *)
(*   let pos = lexbuf.lex_curr_p in *)
(*   fprintf outx "%s:%d:%d" pos.pos_fname *)
(*     pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1) *)

(* let parse_lexbuf lexbuf = *)
(*   try Parser.main Lexer.token lexbuf with *)
(*   | Parser.Error -> *)
(*     fprintf stderr "%a: syntax error\n" print_position lexbuf; *)
(*     exit (-1) *)
(* let parse filename () = *)
(*   let inx = open_in filename in *)
(*   let lexbuf = Lexing.from_channel inx in *)
(*   lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename }; *)
(*   let x = parse_lexbuf lexbuf in *)
(*   In_channel.close inx; x *)


let in_file : string option ref = ref None
let in_config : string option ref = ref None
let out_leak : string option ref = ref None
let out_trace : string option ref = ref None
let out_cache_history : string option ref = ref None
let show_pgm: bool ref = ref false
let record_from_start: bool ref = ref false
let out : string ref = ref "none"
(*let memstart = ref 0
let memend = ref 1024 *)
let stack_size = ref (Conf.default_max_stack_size)
let mpu : bool ref = ref true
let init_data = ref []
let init_args = ref []
let speclist =
  [
    ("-i", Arg.String (fun s -> in_file := Some s), "Input program.");
    (* ("-iconf", Arg.String (fun s -> in_file := Some s), "Input configuration file (overrides default configuration). Not supported yet."); *)
    ("--show", Arg.Set show_pgm, "Show input program.");
    ("--record-from-start", Arg.Set record_from_start, "Start record from start of execution of the prgm. (otherwise, will only record after reaching specific ecall)");
    ("--count-only", Arg.Set count_only, "Count amount of instructions executed, cache hit and cache miss instead of recording a trace (useful for big simulations)");
    ("--leak", Arg.String (fun s -> out_leak := Some s), "Where to output leak.");
    ("--trace", Arg.String (fun s -> out_trace := Some s), "Where to output trace.");
    ("--cache-history", Arg.String (fun s -> out_cache_history := Some s), "Prefix name of cache history files (exported as a .CSV).");
    ("--out", Arg.Symbol (["none"; "trace"; "leak"], (fun s -> out := s)), "What to output on stdout. Options: _none_, trace, leak.");
    (*("--mem-start", Arg.Set_int memstart, "Memory start (default 0)");
    ("--mem-end", Arg.Set_int memend, "Memory end (default 1024)");*)
    ("--stack-size", Arg.Set_int stack_size, ("Space allocated for stack of processes (default is " ^ (string_of_int Conf.default_max_stack_size) ^ ")"));
    ("--mpu", Arg.Symbol (["y"; "n"], (fun s -> mpu:= (s="y"))), "Memory protection unit, if disable, memory access out of allocated space will not kill the process, only raise a message");
    ("--init-data", Arg.String (fun s ->
         init_data := String.split_on_char ',' s |> List.map int_of_string
       ), "Initial data (comma separated integers)");
    ("--args", Arg.String (fun s ->
    init_args := String.split_on_char ',' s
      ), "Input for the simulated C prgm (comma separated string instead of space separated string)")
  ]


let maybe_show_pgm b pgm =
  if b then
    List.iter (fun i -> Printf.printf "%s" (Inst.to_string i)) pgm

let print_out out s =
  match out with
  | None -> ()
  | Some fname ->
    let oc, close =
      if fname = "-" then
        (stdout, fun () -> ())
      else
        let oc = open_out fname in
        oc, fun () -> close_out oc
    in
    Printf.fprintf oc "%s\n" s;
    close ()

let filter_segments (e_segments) (keep_executables : bool) =
  e_segments |>
  List.filter (fun seg ->
    (seg.p_type = PT_LOAD &&
    (keep_executables == (List.mem Elf.PF_X seg.p_flags))
    )
  ) |>  List.map (fun seg -> (seg.p_vaddr, Bytes.of_string seg.p_data))


let get_loadable_segments elf =
  begin match Elf.parse elf with
    | None -> None
    | Some { e_entry; e_phoff; e_segments; e_sections; _ } ->
      let _debug =
        if (Conf.show_debug)
        then(print_endline ("Entry point of ELF is " ^ Int64.to_string e_entry ^ " and e_phoff = " ^ Int64.to_string e_phoff))
        else ()
      in
      let to_load_as_data = filter_segments e_segments false in
      let to_load_as_instruction = filter_segments e_segments true in
      let _symbol_table_list = (*not used if global pointer (x3) is not initilized by the simulator*)
        e_sections |>
        List.filter (fun sec ->(sec.sh_type = SHT_SYMTAB))  |>
        List.map (fun sec -> (sec.sh_addr, Bytes.of_string sec.sh_data))
      in
      Some (e_entry, e_phoff, 0, to_load_as_data, to_load_as_instruction)
  end




let disp_bytes (b : bytes) (first_instr_byte_address : int) (_s) : string =
  Riscvdecoder.read_bytes_as_bitstring b (first_instr_byte_address)



let disp_instructions_from_segment segments : unit =
  match segments with
  | [] -> raise(Failure ("No executable segments to load in the elf"))
  | (seg_int64, seg_bytes) :: [] ->
    if (Conf.show_debug)
    then Printf.printf "\n********\nseg_int64 of executable segment : %Lx\n" seg_int64;
    Riscvdecoder.display_instructions_of_bytes seg_bytes (Int64.to_int seg_int64)
  | _ -> raise(Failure("bug in disp_instructions_from_segment"))



let rec fill_memory_with_seg (memory : Memo.t) segments : Memo.t =
  match segments with
  | [] -> memory
  | (seg_int64, seg_bytes) :: tail ->
    let filled_memo = Memo.write_from_memory
      (fill_memory_with_seg memory tail)
      (Riscvdecoder.convert_bytes_to_memory seg_bytes (seg_int64))
    in
    if (Conf.show_mem_init || Conf.show_debug)
    then print_endline ("########\nLoaded data segment\n"^ Memo.to_string_as_ram_only_filled_parts filled_memo  );
    filled_memo



let rec get_memory_bound (segments) (op : int -> int -> int) : int =
  match segments with
  | [] -> raise(Failure("Cannot find memory bound if there is no input segment."))
  | (seg_int64, _seg_bytes) :: [] -> Int64.to_int seg_int64
  | (seg_int64, _seg_bytes) :: tail -> op (Int64.to_int seg_int64) (get_memory_bound tail (op))

let get_lower_memory_bound (segments) : int =
  get_memory_bound segments Stdlib.min

let get_higher_memory_bound (segments) : int =
  get_memory_bound segments Stdlib.max

let rec disp_seg (entry : int) (segments : (int64 * bytes) list) : string =
  match segments with
  | [] -> "\nEnd of segments."
  | (seg_int64, seg_bytes) :: tail ->
      let seg_byte_size = Bytes.length seg_bytes in
      "\nSegment ["^Int64.to_string seg_int64^"] with size " ^ string_of_int (seg_byte_size) ^ " = " ^
      (disp_bytes seg_bytes (entry- (Int64.to_int seg_int64)) seg_byte_size) ^
      Bytes.to_string seg_bytes ^
      (disp_seg entry tail)

let disp_seg_of_elf_file (seg_entry)  (data_seg_to_load) (instr_seg_to_load) : string =
  "Segments entry : (" ^ Int64.to_string seg_entry ^ ")\n" ^
  "\nSeg to load as data : \n" ^ 
  disp_seg (Int64.to_int seg_entry) data_seg_to_load ^ 
  "\nSeg to load as instructions : \n" ^ 
  disp_seg (Int64.to_int seg_entry) instr_seg_to_load 
        


let make_system_from_elf_file
    (seg_entry : Int64.t) (_global_pointer : int)
    (data_seg_to_load : 'a list)
    (instr_seg_to_load : 'a list) : Syst.t =
  let mem0 = (fill_memory_with_seg (Memo.empty ()) data_seg_to_load) in
  let mem1 = (fill_memory_with_seg mem0 instr_seg_to_load) in
  let input_args = ("First_arg_should_be_prgm_name"::!init_args) in
  let mem, a1_value = Memo.set_input_args mem1 (input_args) in
  let mem_begin = Memo.get_lower_bound mem in
  let initialized_mem_end = Int64.add 8L  (Binm.align_address_on_byte (Memo.get_upper_bound mem)) in
  let mem_end = Int64.add (Int64.of_int !stack_size)  initialized_mem_end in

  let open Prgm in
  let p =
    {
      memory_allocation = {
        Memo.lower_bound = mem_begin;
        Memo.upper_bound = mem_end;
        Memo.stack_fence = Int64.add initialized_mem_end 64L;
      };
      initial_memory = mem;
      initial_pc = Binm.positive_int64_as_int32 seg_entry ;
      initial_return_address = 0l;(*seg_entry + (4 * (-1 + List.length encoded_prgm));*)
      initial_stack_pointer = Binm.positive_int64_as_int32 mem_end;
      initial_global_pointer = 0l;
      initial_argc = Int32.of_int (List.length input_args);
      initial_argv_pointer = Binm.positive_int64_as_int32 a1_value;
      (*a0 contains argc (i.e. the number of arguments), a1-a7 contain the argv[] addresses ()*)
    } in

  let system_memoryless = Syst.add_process (Syst.build Conf.classic !record_from_start !mpu) p in
  {system_memoryless with
   ram = Memo.write_from_memory system_memoryless.ram mem;
  }


let _ =
  Arg.parse speclist (fun s -> in_file := Some s) "Usage";
  let system : Syst.t =
    match !in_file with
    | None -> begin
        Printf.printf "No input file.\n";
        Arg.usage speclist "Usage";
        exit 1
      end
    | Some fname ->
        match get_loadable_segments fname with
        | Some(seg_entry, _p_offset, global_pointer, data_seg_to_load, instr_seg_to_load) ->
          make_system_from_elf_file (seg_entry) global_pointer (data_seg_to_load) (instr_seg_to_load)
        | None -> failwith (Printf.sprintf "No segments loaded from %s, exiting.\n" fname)
  in
  if Conf.show_debug then Printf.printf "\n\n================\nExecution start here \n";
  let trace = run (*[@tailcall] TODO*) system (Trac.empty ()) 1 in
  let args_display = "input arguments: " ^ (String.concat " " !init_args) in
  (match (!out_leak) with
    | None -> ()
    | Some(output_file_leak) ->
      let out, close = Disp.myopen output_file_leak in
      Trac.fprint_leak out trace;
      close ()
  );
  (match (!out_cache_history) with
    | None -> ()
    | Some(output_file_cache) -> Disp.print_out output_file_cache (Trac.cache_history_to_csv_string trace)
  );
  (match (!out_trace) with
    | None -> () (*don't spend time to generates trace if not requested*)
    | Some(output_file_trace) ->
      let out, close = Disp.myopen output_file_trace in
      Printf.fprintf out "%s\n" args_display;
      Trac.fprint out trace;
      close ()
  );
  if(!count_only)
  then(
    print_endline ("\nCount steps : " ^ string_of_int !count_steps);
    print_endline ("\nCount misses : " ^ string_of_int !count_miss);
    print_endline ("\nCount hits : " ^ string_of_int !count_hits);
    print_endline ("\nCount cycles of recorded instructions: \n");
    print_endline (string_of_int !count_cycles)
  )
  else()

